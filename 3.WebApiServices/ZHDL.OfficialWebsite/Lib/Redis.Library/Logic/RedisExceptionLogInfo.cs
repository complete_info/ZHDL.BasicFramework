﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: Redis.Library
*
* 功 能： 异常日志实体类型
*
* 类 名： ExceptionLogInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/10 9:52:33 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Redis.Library
{
    /// <summary>
    /// 异常日志实体类型
    /// </summary>
    public class RedisExceptionLogInfo
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 异常类型 100：警告  101：严重警告 102：错误 103：致命异常信息
        /// </summary>
        public int exception_type
        {
            get; set;
        }

        /// <summary>
        /// 系统类型 100:运维系统  200:桌面应用系统  300:Web客户端
        /// </summary>
        public int system_type
        {
            get; set;
        }

        /// <summary>
        /// 异常标题
        /// </summary>
        public string exception_title
        {
            get; set;
        }

        /// <summary>
        /// 操作明细
        /// </summary>
        public string detail
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 状态 100-未解决，101-已解决
        /// </summary>
        public int status
        {
            get; set;
        }
    }
}
