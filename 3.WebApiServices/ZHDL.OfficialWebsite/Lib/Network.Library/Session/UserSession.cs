﻿using Microsoft.AspNetCore.Http;
using System;

namespace Network.Library
{
    /// <summary>
    /// 用户session操作
    /// </summary>
    public static  class UserSession
    {
        /// <summary>
        /// HttpContext 对象
        /// </summary>
        public static HttpContext HttpHelper => HttpContextHelper.HttpContext;

        /// <summary>
        /// 获取当前后台登录者信息
        /// </summary>
        /// <returns>处理结果</returns>
        public static T GetLoginUser<T>() where T : class, new()
        {
            #region 获取当前登录者信息
            var result = default(T);
            try
            {
                result = HttpHelper.Session.GetObjectFromJson<T>(typeof(T).Name);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            #endregion

            return result;
        }

        /// <summary>
        /// 增加session
        /// </summary>
        /// <param name="clientInfo"></param>
        public static void AddLoginUser<T>(T clientInfo)
        {
            HttpHelper.Session.SetObjectAsJson(typeof(T).Name, clientInfo);
        }

        /// <summary>
        /// 删除session
        /// </summary>
        public static void RemoveLoginUser<T>()
        {
            HttpHelper.Session.Remove(typeof(T).Name);
        }
    }
}
