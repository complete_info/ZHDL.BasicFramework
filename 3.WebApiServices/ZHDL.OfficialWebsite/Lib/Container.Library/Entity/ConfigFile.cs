﻿namespace Container.Library
{
    /// <summary>
    /// 配置文件信息
    /// </summary>
    public class ConfigFile
    {
        /// <summary>
        /// 文件的路径
        /// </summary>
        public string FilePath { get; set; }
    }
}
