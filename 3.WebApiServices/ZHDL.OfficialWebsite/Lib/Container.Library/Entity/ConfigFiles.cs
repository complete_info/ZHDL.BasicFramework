﻿using System.Collections.Generic;

namespace Container.Library
{
    /// <summary>
    /// 配置文件组
    /// </summary>
    public class ConfigFiles
    {
        /// <summary>
        /// 配置文件集合
        /// </summary>
        public List<ConfigFile> InstanceList { get; set; }
    }
}
