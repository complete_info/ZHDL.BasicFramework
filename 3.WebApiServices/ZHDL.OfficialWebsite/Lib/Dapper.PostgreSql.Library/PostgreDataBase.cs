﻿using Dapper.Library;
using Dapper.Library.DataAccessSql;
using Npgsql;
using System;
using System.Data;

namespace Dapper.PostgreSql.Library
{
    public static class PostgreDataBase
    {
        #region 链接
        /// <summary>
        /// 获取连接
        /// </summary>
        /// <returns>打开的链接，如果失败返回的是null</returns>
        public static NpgsqlConnection GetConnection(DatabaseName database)
        {
            //链接字符串，从工具中读取配置
            string connectionString = string.Empty;
            NpgsqlConnection connection = null;
            try
            {
                DatabaseInstance instance = DatabaseManager.GetDatabase(database.ToString());
                connectionString = bool.Parse(instance.IsEncryption) ? AESEncryptHelper.Decrypt(instance.ConnectionString) : instance.ConnectionString;

                if (string.IsNullOrEmpty(connectionString)) return null;

                connection = new NpgsqlConnection(connectionString);
                connection.Open();
                return connection;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 关闭数据库连接
        /// </summary>
        /// <param name="connection">数据库连接</param>
        private static void CloseConnection(NpgsqlConnection connection)
        {
            try
            {
                if (connection != null && connection.State == ConnectionState.Open)
                {
                    connection.Close();
                    connection.Dispose();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 查询

        /// <summary>
        /// 非事物查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <returns></returns>
        public static IQuerySet<T> QuerySet<T>(this NpgsqlConnection sqlConnection)
        {
            try
            {
                return new QuerySet<T>(sqlConnection, new PostgreSqlProvider(), null);
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
        }

        /// <summary>
        /// 事物查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public static IQuerySet<T> QuerySet<T>(this NpgsqlConnection sqlConnection, IDbTransaction dbTransaction = null)
        {
            try
            {
                return new QuerySet<T>(sqlConnection, new PostgreSqlProvider(), dbTransaction);
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
        }

        /// <summary>
        /// 非事物查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <returns></returns>
        public static QuerySql<T> QuerySql<T>(this NpgsqlConnection sqlConnection)
        {
            try
            {
                return new QuerySql<T>(sqlConnection, new PostgreSqlProvider());
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
        }

        /// <summary>
        /// 事物查询
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public static QuerySql<T> QuerySql<T>(this NpgsqlConnection sqlConnection, IDbTransaction dbTransaction)
        {
            try
            {
                return new QuerySql<T>(sqlConnection, new PostgreSqlProvider(), dbTransaction);
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
        }

        #endregion

        #region 执行

        /// <summary>
        /// 非事物执行，用来解决表达式树不能使用默认参数
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <returns></returns>
        public static ICommandSet<T> CommandSet<T>(this NpgsqlConnection sqlConnection)
		{
            try
            {
                return new CommandSet<T>(sqlConnection, new PostgreSqlProvider(), null);
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
           
		}

        /// <summary>
        /// 事物执行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
		public static ICommandSet<T> CommandSet<T>(this NpgsqlConnection sqlConnection, IDbTransaction dbTransaction = null)
		{
            try
            {
                return new CommandSet<T>(sqlConnection, new PostgreSqlProvider(), dbTransaction);
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
        }

        /// <summary>
        /// 非事物执行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <returns></returns>
        public static CommandSql<T> CommandSql<T>(this NpgsqlConnection sqlConnection)
        {
            try
            {
                return new CommandSql<T>(sqlConnection, new PostgreSqlProvider());
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
        }

        /// <summary>
        /// 事物执行
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="sqlConnection"></param>
        /// <param name="dbTransaction"></param>
        /// <returns></returns>
        public static CommandSql<T> CommandSql<T>(this NpgsqlConnection sqlConnection, IDbTransaction dbTransaction)
        {
            try
            {
                return new CommandSql<T>(sqlConnection, new PostgreSqlProvider(), dbTransaction);
            }
            catch (Exception ex)
            {
                CloseConnection(sqlConnection);
                throw ex;
            }
        }
        #endregion

        /// <summary>
        /// 事物
        /// </summary>
        /// <param name="sqlConnection"></param>
        /// <param name="action"></param>
        /// <param name="exAction"></param>
        public static void Transaction(this NpgsqlConnection sqlConnection, Action<TransContext> action,
            Action<System.Exception> exAction = null)
        {
            if (sqlConnection.State == ConnectionState.Closed)
                sqlConnection.Open();

            var transaction = sqlConnection.BeginTransaction();
            try
            {
                action(new TransContext(sqlConnection, transaction, new PostgreSqlProvider()));
                transaction.Commit();
            }
            catch (Exception ex)
            {
                if (exAction != null)
                    exAction(ex);
                else
                {
                    transaction.Rollback();
                }
                CloseConnection(sqlConnection);
            }
        }
    }
}
