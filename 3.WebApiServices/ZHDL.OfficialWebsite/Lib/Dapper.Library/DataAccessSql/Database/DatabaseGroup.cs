﻿using System.Xml.Serialization;

namespace Dapper.Library.DataAccessSql
{
	/// <summary>
	///  数据库分组配置对象
	/// </summary>
	public class DatabaseGroup
	{
		/// <summary>
		/// 数据库实例数组
		/// </summary>
		[XmlElement("database")]
		public DatabaseInstance[] InstanceList { get; set; }

		/// <summary>
		/// 分组名称
		/// </summary>
		[XmlAttribute("group")]
		public string Group { get; set; }
	}
}
