﻿using System.Xml.Serialization;

namespace Dapper.Library.DataAccessSql
{
	/// <summary>
	/// SQL命令对象
	/// </summary>
	public class DataCommandInstance
	{  
		/// <summary>
	    /// 命令名称
	    /// </summary>
		[XmlAttribute("name")]
		public string Name { get; set; }

		/// <summary>
		/// SQL命令内容
		/// </summary>
		[XmlElement("commandText")]
		public string CommandText { get; set; }

		/// <summary>
		/// 数据库名称
		/// </summary>                        
		[XmlAttribute("database")]
		public string Database { get; set; }

		/// <summary>
		/// 用例：这个表示你的业务类型
		/// </summary>
		[XmlAttribute("useCase")]
		public string UseCase { get; set; }
	}
}
