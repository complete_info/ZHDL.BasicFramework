﻿using System.ComponentModel;

namespace Dapper.Library.DataAccessSql
{
    /// <summary>
    /// 数据库实例名
    /// </summary>
    public enum DatabaseName
    {
        /// <summary>
        ///基础数据库-MsSql
        /// </summary>
        [Description("MsSql_DB")]
        MsSql_DB = 1,

        /// <summary>
        /// 空间数据库链接关键字-MsSql
        /// </summary>
        [Description("MsSql_SDE")]
        MsSql_SDE = 11 ,

        /// <summary>
        ///基础数据库-MySql
        /// </summary>
        [Description("MySql_DB")]
        MySql_DB = 2,

        /// <summary>
        /// 空间数据库链接关键字-MySql
        /// </summary>
        [Description("MySql_SDE")]
        MySql_SDE = 21,

        /// <summary>
        ///基础数据库-Oracle
        /// </summary>
        [Description("Oracle_DB")]
        Oracle_DB = 3,

        /// <summary>
        /// 空间数据库链接关键字-Oracle
        /// </summary>
        [Description("Oracle_SDE")]
        Oracle_SDE = 31,

        /// <summary>
        ///基础数据库-PostgreSql
        /// </summary>
        [Description("PostgreSql_DB")]
        PostgreSql_DB = 4,

        /// <summary>
        /// 授权数据库链接关键字-PostgreSql
        /// </summary>
        [Description("PostgreSql_SDE")]
        PostgreSql_SDE = 41,
    }
}
