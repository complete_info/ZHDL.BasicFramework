﻿using System;
using System.Linq.Expressions;

namespace Dapper.Library
{
    public interface IQuerySql<T>
    {

        /// <summary>
        /// 查询扩展
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        QuerySql<T> Where(Expression<Func<T, bool>> predicate);

        /// <summary>
        /// 查询扩展
        /// </summary>
        /// <typeparam name="TWhere"></typeparam>
        /// <param name="predicate"></param>
        /// <returns></returns>
        QuerySql<T> Where<TWhere>(Expression<Func<TWhere, bool>> predicate);


        /// <summary>
        /// 使用SQL执行查询语句
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="parameter"></param>
        /// <returns></returns>
        QuerySql<T> BySql(string predicate, object parameter);




    }
}
