﻿
namespace Dapper.Library
{
    public class DataBaseContext<T> : AbstractDataBaseContext
    {
        public QuerySet<T> QuerySet => (QuerySet<T>)Set;

        public CommandSet<T> CommandSet => (CommandSet<T>)Set;
    }

    public abstract class AbstractDataBaseContext
    {
        public AbstractSet Set { get; set; }

        public EOperateType OperateType { get; set; }
    }
}
