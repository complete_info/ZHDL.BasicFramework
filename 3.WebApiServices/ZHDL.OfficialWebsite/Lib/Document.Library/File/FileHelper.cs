﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
namespace Document.Library
{

	/// <summary>
	/// 文件帮助类
	/// </summary>
	public class FileHelper
	{
		/// <summary>
		/// 根据文件路径读取文件内容
		/// </summary>
		/// <param name="path">文件路径</param>
		/// <returns></returns>
		public static string ReadFileByPath(string path)
		{
			//string path = $"{AppDomain.CurrentDomain.BaseDirectory}Test.json";
			using (StreamReader sr = new StreamReader(path))
			{
				string result = sr.ReadToEnd();
				sr.Close();
				sr.Dispose();
				return result;
			}
		}

		/// <summary>
		/// 检测目录是否存在
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static bool DirectoryExists(string fullpath)
		{
			bool result = false;
			try
			{
				result = Directory.Exists(RepairPath(fullpath));
			}
			catch (Exception e)
			{
				throw e;
			}
			return result;
		}


		/// <summary>
		/// 检测文件是否存在
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static bool FileExists(string fullpath)
		{
			bool result = false;
			try
			{
				result = System.IO.File.Exists(RepairFileName(fullpath));
			}
			catch (Exception e)
			{
				throw e;
			}
			return result;
		}


		/// <summary>
		/// 创建目录
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static bool CreateDirectory(string fullpath)
		{
			bool result = false;
			if (string.IsNullOrEmpty(fullpath))
			{
				return result;
			}

			fullpath = fullpath.Trim();
			if (string.IsNullOrEmpty(fullpath))
			{
				return result;
			}
			if (!DirectoryExists(fullpath))
			{
				string sParent = ExtractPathParent(fullpath);
				if (CreateDirectory(sParent))
				{// 创建目录成功
					try
					{
						Directory.CreateDirectory(fullpath);
						result = true;
					}
					catch (Exception e)
					{
						throw e;
					}
				}
			}
			else
			{
				result = true;
			}
			return result;
		}


		/// <summary>
		/// 删除目录,但不允许删除要盘符
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static bool EraseDirectory(string fullpath)
		{
			bool result = false;
			bool bHasError = false;
			try
			{
				if (fullpath != null)
				{
					fullpath = RepairPath(fullpath);
					if (DirectoryExists(fullpath))
					{
						if (String.Compare(fullpath.Substring(fullpath.Length - 2, 2), ":\\", true) != 0)
						{// 不是根目录

							// 删除子目录
							List<string> directories = GetDirectories(fullpath);
							if (!bHasError)
							{
								foreach (string dirName in directories)
								{// 删除子目录
									if (!EraseDirectory(dirName))
									{
										bHasError = true;
										break;
									}
								}
							}

							// 删除当前目录中的文件
							List<string> files = GetFiles(fullpath);
							foreach (string filename in files)
							{
								try
								{
									System.IO.File.Delete(filename);
								}
								catch
								{
									bHasError = true;
								}
							}

							// 删除指定的目录
							if (!bHasError)
							{
								try
								{
									Directory.Delete(fullpath);
								}
								catch
								{
									bHasError = true;
								}
							}

							result = !bHasError;
						}
						else
						{
							result = false;
						}
					}
					else
					{
						result = true;
					}
				}
				else
				{
					result = true;
				}
			}
			catch (Exception e)
			{
				throw e;
			}
			return result;
		}


		/// <summary>
		/// 获取子目录
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static List<string> GetDirectories(string fullpath)
		{
			List<string> result = new List<string>();
			try
			{
				if (DirectoryExists(RepairPath(fullpath)))
				{
					string[] list = Directory.GetDirectories(fullpath);
					foreach (string name in list)
					{
						result.Add(name);
					}
				}
			}
			catch (Exception e)
			{
				throw e;
			}
			return result;
		}


		/// <summary>
		/// 查询文件列表
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static List<string> GetFiles(string fullpath)
		{
			List<string> result = new List<string>();
			try
			{
				if (DirectoryExists(RepairPath(fullpath)))
				{
					string[] list = Directory.GetFiles(fullpath);
					foreach (string name in list)
					{
						result.Add(name);
					}
				}
			}
			catch (Exception e)
			{
				throw e;
			}
			return result;
		}


		/// <summary>
		/// 删除文件
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static bool DeleteFile(string filename)
		{
			bool result = false;
			try
			{
				filename = RepairFileName(filename);
				if (FileExists(filename))
				{
					System.IO.File.SetAttributes(filename, FileAttributes.Normal);
					System.IO.File.Delete(filename);
				}
				result = true;
			}
			catch (Exception e)
			{
				throw e;
			}
			return result;
		}


		/// <summary>
		/// 分隔出上级目录
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static string ExtractPathParent(string fullpath)
		{
			if (string.IsNullOrEmpty(fullpath))
			{
				return "";
			}

			fullpath = fullpath.Trim();
			if (string.IsNullOrEmpty(fullpath))
			{
				return "";
			}

			if (fullpath[fullpath.Length - 1] == '\\')
			{
				fullpath = fullpath.Substring(0, fullpath.Length - 1);
			}
			return ExtractFilePath(fullpath);
		}

		/// <summary>
		/// 从完整路径中,分解出目录
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static string ExtractFilePath(string fullpath)
		{
			if (string.IsNullOrEmpty(fullpath))
			{
				return "";
			}

			fullpath = fullpath.Trim();
			if (string.IsNullOrEmpty(fullpath))
			{
				return "";
			}

			char[] arrF = fullpath.Trim().ToCharArray();
			int ipos = -1;
			for (int i = arrF.Length - 1; i >= 0; i--)
			{
				if (arrF[i] == '\\')
				{
					break;
				}
				else
				{
					ipos = i;
				}
			}

			if (ipos == -1)
			{
				return "";
			}

			StringBuilder strB = new StringBuilder();
			for (int i = 0; i < ipos; i++)
			{
				strB.Append(arrF[i]);
			}
			return strB.ToString();
		}

		/// <summary>
		/// 处理目录中的反斜线
		/// </summary>
		/// <param name="fullpath"></param>
		/// <returns></returns>
		public static string RepairPath(string fullpath)
		{
			if (string.IsNullOrEmpty(fullpath))
			{
				return "";
			}

			fullpath = fullpath.Trim();
			if (string.IsNullOrEmpty(fullpath))
			{
				return "";
			}

			StringBuilder result = new StringBuilder();
			string[] arrF = fullpath.Split('\\');
			for (int i = 0; i < arrF.Length; i++)
			{
				string sline = arrF[i];
				if (string.IsNullOrEmpty(sline))
				{
					continue;
				}
				else if (string.IsNullOrEmpty(sline.Trim()))
				{
					continue;
				}
				else
				{
					result.Append(sline.Trim());
					result.Append('\\');
				}
			}
			return result.ToString();
		}


		/// <summary>
		/// 处理文件名中的反斜线
		/// </summary>
		/// <param name="filename"></param>
		/// <returns></returns>
		public static string RepairFileName(string filename)
		{
			if (string.IsNullOrEmpty(filename))
			{
				return "";
			}

			filename = filename.Trim();
			if (string.IsNullOrEmpty(filename))
			{
				return "";
			}

			StringBuilder result = new StringBuilder();
			string[] arrF = filename.Split('\\');
			for (int i = 0; i < arrF.Length; i++)
			{
				string sline = arrF[i];
				if (string.IsNullOrEmpty(sline))
				{
					continue;
				}
				else if (string.IsNullOrEmpty(sline.Trim()))
				{
					continue;
				}
				else
				{
					if (result.Length > 0)
					{
						result.Append('\\');
					}
					result.Append(sline.Trim());
				}
			}
			return result.ToString();
		}



	}
}
