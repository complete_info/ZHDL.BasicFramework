﻿using Microsoft.International.Converters.PinYinConverter;
using System;

namespace Common.Library
{
    /// <summary>
    /// 字符串帮助类
    /// </summary>
    public static class StringHelper
    {
        #region 指定的字符串列表CnStr中检索符合拼音索引字符串


        /// <summary> 
        /// 汉字转化为拼音首字母
        /// </summary> 
        /// <param name="str">汉字</param> 
        /// <returns>首字母</returns> 
        public static string GetFirstPinyin(string str)
        {
            string r = string.Empty;
            foreach (char obj in str)
            {
                try
                {
                    ChineseChar chineseChar = new ChineseChar(obj);
                    string t = chineseChar.Pinyins[0].ToString();
                    r += t.Substring(0, 1);
                }
                catch
                {
                    r += obj.ToString();
                }
            }
            return r;
        }

        /// <summary> 
        /// 汉字转化为拼音
        /// </summary> 
        /// <param name="str">汉字</param> 
        /// <returns>全拼</returns> 
        public static string GetPinyin(string str)
        {
            string r = string.Empty;
            foreach (char obj in str)
            {
                try
                {
                    ChineseChar chineseChar = new ChineseChar(obj);
                    string t = chineseChar.Pinyins[0].ToString();
                    r += t.Substring(0, t.Length - 1);
                }
                catch
                {
                    r += obj.ToString();
                }
            }
            return r;
        }


        #endregion

        #region 截取字符长度
        /// <summary>
        /// 截取字符长度
        /// </summary>
        /// <param name="str">被截取的字符串</param>
        /// <param name="len">所截取的长度</param>
        /// <returns>子字符串</returns>
        public static string CutString(string str, int len)
        {
            if (str == null || str.Length == 0 || len <= 0)
            {
                return string.Empty;
            }
            int l = str.Length;
            #region 计算长度
            int clen = 0;
            while (clen < len && clen < l)
            {
                //每遇到一个中文，则将目标长度减一。
                if ((int)str[clen] > 128) { len--; }
                clen++;
            }
            #endregion
            if (clen < l)
            {
                return str.Substring(0, clen) + "...";
            }
            else
            {
                return str;
            }
        }

        /// <summary>
        /// //截取字符串中文 字母
        /// </summary>
        /// <param name="content">源字符串</param>
        /// <param name="length">截取长度！</param>
        /// <returns></returns>
        public static string SubTrueString(object content, int length)
        {
            string strContent = content.ToString();
            bool isConvert = false;
            int splitLength = 0;
            int currLength = 0;
            int code = 0;
            int chfrom = Convert.ToInt32("4e00", 16);    //范围（0x4e00～0x9fff）转换成int（chfrom～chend）
            int chend = Convert.ToInt32("9fff", 16);
            for (int i = 0; i < strContent.Length; i++)
            {
                code = Char.ConvertToUtf32(strContent, i);
                if (code >= chfrom && code <= chend)
                {
                    currLength += 2; //中文
                }
                else
                {
                    currLength += 1;//非中文
                }
                splitLength = i + 1;
                if (currLength >= length)
                {
                    isConvert = true;
                    break;
                }
            }
            if (isConvert)
            {
                return strContent.Substring(0, splitLength);
            }
            else
            {
                return strContent;
            }
        }

        /// <summary>
        /// 获取object转字符串长度
        /// </summary>
        /// <param name="content"></param>
        /// <returns></returns>
        public static int GetStringLenth(object content)
        {
            string strContent = content.ToString();
            int currLength = 0;
            int code = 0;
            int chfrom = Convert.ToInt32("4e00", 16);//范围（0x4e00～0x9fff）转换成int（chfrom～chend）
            int chend = Convert.ToInt32("9fff", 16);
            for (int i = 0; i < strContent.Length; i++)
            {
                code = Char.ConvertToUtf32(strContent, i);
                if (code >= chfrom && code <= chend)
                {
                    currLength += 2; //中文
                }
                else
                {
                    currLength += 1;//非中文
                }

            }
            return currLength;
        }
        #endregion
    }
}
