﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Common.Library
{
    /// <summary>
    /// 时间操作帮助类
    /// </summary>
    public static class TimeHelper
    {
        /// <summary>
        /// 获取两个时间段之间的所有天时间
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static List<string> GetDayTimeList(string startTime, string endTime)
        {
            List<string> timeList = new List<string>();
            //首先保证 rq1<=rq2
            DateTime time1 = Convert.ToDateTime(startTime);
            DateTime time2 = Convert.ToDateTime(endTime);
            while (time1 <= time2)
            {
                timeList.Add(time1.ToString("yyyy-MM-dd"));
                time1 = time1.AddDays(1);
            }
            return timeList;
        }

        /// <summary>
        /// 获取两个时间段之间的所有月份时间
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static List<string> GetMonthTimeList(string startTime, string endTime)
        {
            List<string> timeList = new List<string>();
            //首先保证 rq1<=rq2
            DateTime time1 = Convert.ToDateTime(startTime);
            DateTime time2 = Convert.ToDateTime(endTime);
            while (time1 <= time2)
            {
                timeList.Add(time1.ToString("yyyy-MM"));
                time1 = time1.AddMonths(1);
            }
            return timeList;
        }


        /// <summary>
        /// 获取两个时间段之间的所有月份时间
        /// </summary>
        /// <param name="startTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        public static List<string> GetMonthTimeList(DateTime startTime, DateTime endTime)
        {
            List<string> timeList = new List<string>();
            while (startTime <= endTime)
            {
                timeList.Add(startTime.ToString("yyyy-MM"));
                startTime = startTime.AddMonths(1);
            }
            return timeList;
        }
    }
}
