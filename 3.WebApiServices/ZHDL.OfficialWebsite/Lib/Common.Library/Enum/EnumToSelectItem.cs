﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Library
{
    /// <summary>
    /// 枚举项
    /// </summary>
    public class EnumToSelectItem
    {
        public EnumToSelectItem(string text, string value)
        {
            this.Text = text;
            this.Value = value;
        }

        public override string ToString()
        {
            return "<option value='" + this.Value + "'>" + this.Text + "</option>";
        }

        public EnumToSelectItem() { }

        public string Value { get; set; }

        public string Text { get; set; }

        /// <summary>
        /// 将枚举Value转换成list
        /// </summary>
        /// <param name="type"></param>
        /// <param name="isAddAll"></param>
        /// <returns></returns>
        public static List<EnumToSelectItem> GetEnumValueList(Type type, bool isAddAll)
        {
            List<EnumToSelectItem> list = new List<EnumToSelectItem>();
            if (isAddAll)
            {
                list.Add(new EnumToSelectItem("所有", ""));
            }
            String[] EnumFuncTitle = System.Enum.GetNames(type);
            Int32 intCnt = 0;
            foreach (Int32 intValue in System.Enum.GetValues(type))
            {
                list.Add(new EnumToSelectItem(EnumFuncTitle[intCnt],Convert.ToString(intValue)));
                intCnt += 1;
            }
            return list;
        }

        /// <summary>
        /// 将枚举转Title换成list
        /// </summary>
        /// <param name="type"></param>
        /// <param name="isAddAll"></param>
        /// <returns></returns>
        public static List<EnumToSelectItem> GetEnumTitleList(Type type, bool isAddAll)
        {
            List<EnumToSelectItem> list = new List<EnumToSelectItem>();
            if (isAddAll)
            {
                list.Add(new EnumToSelectItem("所有", ""));
            }
            String[] EnumFuncTitle = System.Enum.GetNames(type);
            Int32 intCnt = 0;
            foreach (Int32 intValue in System.Enum.GetValues(type))
            {
                list.Add(new EnumToSelectItem(EnumFuncTitle[intCnt], EnumFuncTitle[intCnt]));
                intCnt += 1;
            }
            return list;
        }

        /// <summary>
        /// 将枚举转Description换成list
        /// </summary>
        /// <param name="type"></param>
        /// <param name="isAddAll"></param>
        /// <returns></returns>
        public static List<EnumToSelectItem> GetEnumDescriptionList(Type type, bool isAddAll)
        {
            List<EnumToSelectItem> list = new List<EnumToSelectItem>();
            if (isAddAll)
            {
                list.Add(new EnumToSelectItem("所有", ""));
            }
            String[] EnumFuncTitle = System.Enum.GetNames(type);
            string text = string.Empty;
            Int32 intCnt = 0;
            foreach (Int32 intValue in Enum.GetValues(type))
            {
                // 获取枚举公共成员
                var enumMembersInfo = type.GetMember(EnumFuncTitle[intCnt]);
                if (enumMembersInfo != null && enumMembersInfo.Length > 0)
                {
                    // 获取自定义特性
                    object[] attrs = enumMembersInfo[0].GetCustomAttributes(typeof(DescriptionAttribute), false);
                    if (attrs != null && attrs.Length > 0) {
                        text=((DescriptionAttribute)attrs[0]).Description;
                    }
                }
                else
                {
                    text = intValue.ToString();
                }
                list.Add(new EnumToSelectItem(text, Convert.ToString(intValue)));
                intCnt += 1;
            }
            return list;
        }
    }
}
