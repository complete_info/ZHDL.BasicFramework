﻿using System.Collections.Generic;
using System.Dynamic;

namespace Serialize.Library
{
    /// <summary>
    /// 该类主要用来包装动态类型的配置信息
    /// </summary>
    public class DynamicHelper : DynamicObject
    {
        /// <summary>
        /// 配置的成员
        /// </summary>
        Dictionary<string, object> configMembers = new Dictionary<string, object>();

        /// <summary>
        /// 获取动态成员
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="result"></param>
        /// <returns></returns>
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            return configMembers.TryGetValue(binder.Name, out result);
        }

        /// <summary>
        /// 设置动态成员
        /// </summary>
        /// <param name="binder"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            configMembers[binder.Name] = value;
            return true;
        }
    }
}
