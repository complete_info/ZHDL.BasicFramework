﻿using Common.Library;
using Common.Model;
using System.Collections.Generic;

/*
* 命名空间: Authority.Logic
*
* 功 能： 部门管理逻辑接口
*
* 类 名： ISysDepartmentService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Authority.Logic
{
    /// <summary>
    /// 部门管理逻辑接口
    /// </summary>
    public interface ISysDepartmentService
    {

        #region 部门基础信息管理模块

        #region 信息查询
        /// <summary>
        /// 根据关键字【部门名称，领导人名称，联系电话】获取所有的未删除菜单信息
        /// </summary>
        /// <param name="parameters">关键字【部门名称，领导人名称，联系电话】</param>
        /// <returns></returns>
        ResultJsonInfo<List<DepartmentResponse>> LoadPageList(ParametersInfo<string> parameters);
        /// <summary>
        /// 根据部门id获取用户情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<DepartmentUserResponse>> LoadListInfoByDeparId(DepartmentUserQueryRequest queryInfo);
        #endregion

        #region 更新操作


        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <param name="addInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Addnode(DepartmentChildAddRequest addInfo);

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(DepartmentModifyRequest modifyInfo);

        /// <summary>
        /// 禁用/启用部门
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ForbidOrEnable(string id);
        /// <summary>
        /// 修改对应部门下对应的用户
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyDeparUserInfo(DepartmentUserModifyRequest department);
        #endregion

        #region 移动顺序操作
        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Move(DepartmentMoveRequest inputInfo);
        #endregion

        #region 删除信息

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="removeInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> removeInfo);

        #endregion

        #endregion


        #region 权限相关操作

        /// <summary>
        /// 获取所有启用组织机构树状信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<TreeInfo>> LoadAllTreeList();

        /// <summary>
        /// 获取所有部门Select树状数据
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<SelectListInfo>> LoadSysPostTreeList();

        #endregion
    }
}
