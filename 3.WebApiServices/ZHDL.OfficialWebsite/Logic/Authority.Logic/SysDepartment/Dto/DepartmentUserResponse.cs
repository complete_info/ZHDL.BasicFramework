﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    public class DepartmentUserResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户名【登录名】
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 用户系统显示名
        /// </summary>
        public string displayname
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        public string password
        {
            get; set;
        }

        /// <summary>
        /// 用户描述
        /// </summary>
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// QQ号
        /// </summary>
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }
        /// <summary>
        /// 是否已经选中
        /// </summary>
        public bool lay_is_checked
        {
            get; set;
        }
    }
}
