﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 修改对应部门的用户请求实体
    /// </summary>
    public class DepartmentUserModifyRequest
    {
        /// <summary>
        ///部门唯一标识
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string department_id
        {
            get; set;
        }

        /// <summary>
        ///用户唯一标识符列表
        /// </summary>
        public List<string> menuIdList
        {
            get; set;
        }
    }
}
