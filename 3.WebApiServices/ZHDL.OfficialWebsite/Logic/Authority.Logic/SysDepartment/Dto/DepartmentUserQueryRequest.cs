﻿using Validate.Library;

namespace Authority.Logic
{
  
    public class DepartmentUserQueryRequest
    {

        /// <summary>
        ///部门唯一标识符【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string departmentid
        {
            get; set;
        }

    }
}
