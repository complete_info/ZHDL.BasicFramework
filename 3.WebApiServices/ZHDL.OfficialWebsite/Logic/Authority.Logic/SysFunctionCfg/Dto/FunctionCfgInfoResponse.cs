﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 功能请求查询条件传输实体
    /// </summary>
    public class FunctionCfgQueryRequest
    {

        /// <summary>
        /// 菜单名称【可空】
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 系统类型 空字符串  所有；运维系统 100；桌面应用系统 200；Web客户端：300
        /// </summary>
        public string system_type
        {
            get; set;
        }
    }
}
