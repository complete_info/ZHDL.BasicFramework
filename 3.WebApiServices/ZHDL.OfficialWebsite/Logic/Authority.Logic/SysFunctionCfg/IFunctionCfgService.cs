﻿using System.Collections.Generic;
using Authority.Model;
using Common.Library;
using Common.Model;

namespace Authority.Logic
{
    /// <summary>
    /// 菜单功能管理逻辑接口
    /// </summary>
    public interface IFunctionCfgService
    {

        #region 主界面菜单逻辑方法

        /// <summary>
        /// 获取用户对应系统的可操作的菜单信息
        /// </summary>
        /// <param name="systemType"></param>
        /// <returns></returns>
        ResultJsonInfo<List<MenuInfo>> LoadMenuOfUserList(string systemType);


        /// <summary>
        /// 获取用户对应系统的可操作的一级菜单信息
        /// </summary>
        /// <param name="systemType"></param>
        /// <returns></returns>
        ResultJsonInfo<List<SysFunctionCfgEntity>> LoadTopMenuOfUserList(string systemType, int level);


        /// <summary>
        /// 获取左侧二级或三级菜单信息
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="systemType">系统类型  运维系统 100 桌面应用系统 200 PC客户端应用 300</param>
        /// <returns></returns>
        ResultJsonInfo<List<MenuInfo>> LoadSubMenus(string systemTyp, string parentId);

        #endregion

        #region 菜单基础信息管理

        #region 查询
        /// <summary>
        /// 根据菜单名称或系统类型获取菜单列表信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="system_type"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FunctionCfgInfoResponse>> LoadListInfo(ParametersInfo<FunctionCfgQueryRequest> queryInfo);

        /// <summary>
        /// 根据菜单名称或系统类型获取菜单树状列表信息
        /// </summary>
        /// <param name="name"></param>
        /// <param name="system_type"></param>
        /// <returns></returns>
        ResultJsonInfo<List<MenuInfo>> LoadTreeListInfo(FunctionCfgQueryRequest queryInfo);
        #endregion

        #region 更新
        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <param name="addInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> AddInfo(FunctionCfgAddRequest addInfo);

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(FunctionCfgModifyRequest modifyInfo);

        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="moveInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> MoveSort(FunctionCfgMoveRequest moveInfo);

        /// <summary>
        /// 菜单启用与停用操作
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ForbiddenInfo(string id);

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(string[] ids);

        #endregion

        #endregion

        #region 权限配置相关

        /// <summary>
        /// 根据角色ID，获取所有可操作菜单已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<FunctionCfgRoleMenuResponse>> LoadListInfoByRoleId(FunctionCfgRoleQueryRequest queryInfo);

        /// <summary>
        /// 修改对应角色赋值的菜单操作权限
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyRoleMenuInfo(FunctionCfgRoleModifyRequest modifyInfo);

        #endregion

        #region 公用功能

        /// <summary>
        /// 获取系统类型Select列表信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadSystemTypeList();

        /// <summary>
        /// 获取功能类型Select列表信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<EnumToSelectItem>> LoadFunctionTypeList();

        #endregion
    }
}
