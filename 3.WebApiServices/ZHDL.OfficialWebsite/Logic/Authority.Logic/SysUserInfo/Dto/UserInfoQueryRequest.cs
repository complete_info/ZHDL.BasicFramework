﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 用户查询条件类
    /// </summary>
    public class UserInfoQueryRequest
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string sKeyWords{ get; set; }

        /// <summary>
        /// 岗位ID
        /// </summary>
        public string postIds { get; set; }

        /// <summary>
        /// 部门ID
        /// </summary>
        public string departmentId { get; set; }

    }
}
