﻿using Common.Model;
using System.Collections.Generic;

namespace Authority.Logic
{
    /// <summary>
    /// 管理员管理接口
    /// </summary>
    public interface ISysUserService
    {

        #region 用户登录操作和主界面用户操作

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <param name="uniqueIdentifier"></param>
        /// <returns></returns>
        ResultFileInfo LoadValidateCode(string uniqueIdentifier);

        /// <summary>
        /// 用户登录
        /// </summary>
        ResultJsonInfo<UserLoginInfo> UserLogin(UserInfoLoginRequest inputInfo);

        /// <summary>
        /// 通过RefreshToken获取用户信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<UserLoginInfo> LoadUserInfoByRefreshToken(UserInfoRefreshTokenRequest inputInfo);

        /// <summary>
        /// 查询单个用户的数据
        /// 【先查询Redis,如果Redis没有，再查询数据库，最后将信息放入到Redis】
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultJsonInfo<UserLoginInfo> LoadSingleById(string userId);

        ///// <summary>
        ///// 根据登录账号获取信息
        ///// </summary>
        ///// <param name="loginName"></param>
        ///// <returns></returns>
        //ResultJsonInfo<SysUserEntity> LoadUserByLoginName(string loginName);

        ///// <summary>
        ///// 上传图片（后期把图片上传统一处理）
        ///// </summary>
        ///// <param name="parametersInfo"></param>
        ///// <returns></returns>
        //ResultJsonInfo<string> UploadImage(string filePath, Dictionary<string, object> inputInfo);

        #endregion

        #region 用户基础信息管理操作
        #region 添加
        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        ResultJsonInfo<int> AddUser(UserInfoAddRequest userAdd);

        /// <summary>
        /// 用户配置部门
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        ResultJsonInfo<int> UserDepartment(UserInfoDepartmentRequest department);

        /// <summary>
        /// 用户配置岗位
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        ResultJsonInfo<int> UserPost(UserInfoPositionRequset position);

        #endregion

        #region 查询
        /// <summary>
        /// 根据条件分页查询用户数据
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> LoadList(ParametersInfo<UserInfoQueryRequest> parameters);
        /// <summary>
        /// 查询单个用户的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultJsonInfo<UserInfoResponse> LoadSingle(string userId);

        /// <summary>
        /// 查询自己的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultJsonInfo<UserInfoResponse> LoadUserSingle();


        #endregion


        #region 修改
        /// <summary>
        /// 修改用户数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(UserInfoModifyRequest request);

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> userIds);


        /// <summary>
        /// 禁用/启用用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ForbidOrEnable(string userId);

        /// <summary>
        /// 修改用户自身的数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyOneself(UserInfoModifyOneselfRequest request);

        /// <summary>
        /// 修改自己的密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ChangePassword(UserChangePasswordRequest request);

        #endregion


        #endregion

        #region 信息发送相关功能

        /// <summary>
        /// 根据条件分页查询除自己以外的用户数据
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> LoadListExceptMyself(ParametersInfo<UserInfoQueryRequest> inputInfo);

        /// <summary>
        /// 根据条件查询除自己以外的用户ID
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<string>> LoadIdListExceptMyself(UserInfoQueryRequest inputInfo);
        #endregion

        #region 批量导入导出
        /// <summary>
        /// 验证数据
        /// </summary>
        /// <param name="selects"></param>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoFileRequest>> VerifyThatTheFile(List<UserInfoFileRequest> selects);
        /// <summary>
        /// 事务批量导入用户信息
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<int> TranBulkImportRole(List<UserInfoFileRequest> selects);

        /// <summary>
        /// 批量导出所有
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<UserInfoResponse>> ListAll();

        #endregion
    }
}
