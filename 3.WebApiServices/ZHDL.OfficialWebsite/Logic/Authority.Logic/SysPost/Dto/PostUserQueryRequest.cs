﻿using Validate.Library;

namespace Authority.Logic
{
  
    public class PostUserQueryRequest
    {

        /// <summary>
        ///岗位唯一标识符【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string postid
        {
            get; set;
        }

    }
}
