﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 图层功能管理逻辑接口
    /// </summary>
    public interface ILayertreeCfgService
    {
        #region 图层基础管理操作
        #region 查询
        /// <summary>
        /// 查询所有的图层组
        /// </summary>
        /// <returns></returns>
        ResultJsonInfo<List<LayertreeResponse>> LoadList(ParametersInfo<string> parameters);

        /// <summary>
        /// 查询单个图层的信息
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        ResultJsonInfo<LayertreeResponse> LoadSinger(string groupId);
        #endregion

        #region 新增
        /// <summary>
        /// 新增子节点
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Addnode(LayertreeCfgChildAddRequest request);


        #endregion

        #region 修改
        /// <summary>
        /// 修改图层
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Modify(LayertreeCfgModifyAddRequest request);
        /// <summary>
        /// 删除图层
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(List<string> groupId);
        /// <summary>
        /// 启用/禁用图层
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ForbidOrEnable(string groupId);
        /// <summary>
        /// 移动图层
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Move(LayertreeCfgMoveRequest inputInfo);
        #endregion

        #endregion


        #region 与权限相关功能

        /// <summary>
        /// 根据角色ID，获取所有可操作图层已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<LayertreeCfgRoleLayerResponse>> LoadListInfoByRoleId(LayertreeCfgRoleQueryRequest queryInfo);

        /// <summary>
        /// 修改对应角色赋值的图层操作权限
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<int> ModifyRoleLayerInfo(LayertreeCfgRoleModifyRequest modifyInfo);

        #endregion
    }
}
