﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    ///  添加图层根节点参数类
    /// </summary>
    public class LayertreeCfgRootAddRequest
    {


        /// <summary>
        /// 图层名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 图层备注
        /// </summary>
        public string memo
        {
            get; set;
        }
        /// <summary>
        /// 图层表名备注
        /// </summary>
        public string tablename
        {
            get; set;
        }


        /// <summary>
        /// 图层名对应的要素类名称
        /// </summary>
        public string featureclass_name
        {
            get; set;
        }
    }
}
