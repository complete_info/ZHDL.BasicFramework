﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 图层查询返回
    /// </summary>
   public  class LayertreeResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 图层名称
        /// </summary>
        public string name
        {
            get; set;
        }
        /// <summary>
        /// 图层表名
        /// </summary>
        public string tablename {
            get; set;
        }
        /// <summary>
        /// 图层备注
        /// </summary>
        public string memo
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 图层名对应的要素类名称
        /// </summary>
        public string featureclass_name
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        public bool is_deleted
        {
            get; set;
        }
        /// <summary>
        /// 是否有兄长【同级别，排前面的】
        /// </summary>
        public bool have_elder
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄弟【同级别，排后面的】
        /// </summary>
        public bool have_younger
        {
            get; set;
        }
    }
}
