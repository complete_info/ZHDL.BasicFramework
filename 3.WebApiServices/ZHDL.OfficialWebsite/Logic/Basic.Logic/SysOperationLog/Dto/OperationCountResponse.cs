﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: Basic.Logic
*
* 功 能： 操作日志统计数量信息实体
*
* 类 名： OperationCountResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/9 11:14:59 	罗维      创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{

    /// <summary>
    /// 操作日志统计数量信息实体
    /// </summary>
    public class OperationCountResponse
    {
        /// <summary>
        /// 登录次数
        /// </summary>
        public int login_times
        {
            get; set;
        }

        /// <summary>
        /// 操作次数
        /// </summary>
        public int operation_times
        {
            get; set;
        }
    }
}
