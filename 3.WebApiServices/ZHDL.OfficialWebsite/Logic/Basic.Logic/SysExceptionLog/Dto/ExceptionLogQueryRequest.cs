﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Basic.Logic
{
    /// <summary>
    /// 日志查询条件类
    /// </summary>
    public class ExceptionLogQueryRequest
    {
        /// <summary>
        /// 关键字
        /// </summary>
        public string sKeyWords
        {
            get; set;
        }
        /// <summary>
        ///异常类型
        /// </summary>
        public int exception_type
        {
            get; set;
        }
        /// <summary>
        /// 系统类型
        /// </summary>
        public int system_type
        {
            get; set;
        }
    }

}
