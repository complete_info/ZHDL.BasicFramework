﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: Basic.Logic
*
* 功 能： 异常个数统计信息
*
* 类 名： ExceptionCountResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/9 10:58:03 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{
    /// <summary>
    /// 异常个数统计信息
    /// </summary>
    public class ExceptionCountResponse
    {
        /// <summary>
        /// 未解决数量
        /// </summary>
        public int unresolved_count
        {
            get; set;
        }

        /// <summary>
        /// 总的消息数量
        /// </summary>
        public int total
        {
            get; set;
        }

    }
}
