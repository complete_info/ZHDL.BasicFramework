﻿using Common.Model;
using Document.Library;
/*
* 命名空间: Basic.Logic
*
* 功 能： CommonServiceImpl
*
* 类 名： SysPostServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.2    2020/04/02 17:12:00 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{
    /// <summary>
    /// 公用逻辑操作类
    /// </summary>
    public class CommonServiceImpl:ICommonService
    {
        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<string> UploadImage(ImageUploadRequest inputInfo)
        {
            var result = new ResultJsonInfo<string>();

            if (!string.IsNullOrEmpty(inputInfo.originalPic))
            {
                ImageHelper.RemoveImage(inputInfo.originalPic, inputInfo.uploadFolder.ToString());
            }
            string  filePath = ImageHelper.SaveBase64Image(inputInfo.imageBase64, inputInfo.uploadFolder.ToString());

            result.Data = filePath;
            result.Code = ActionCodes.Success;

            return result;
        }
    }
}
