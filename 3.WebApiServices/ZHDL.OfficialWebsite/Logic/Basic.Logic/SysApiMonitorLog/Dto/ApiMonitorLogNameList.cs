﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: Basic.Logic
*
* 功 能： 接口安全中的名单信息【警告名单和黑名单】
*
* 类 名： ApiMonitorLogNameList
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/14 10:55:41 	罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{
    /// <summary>
    /// 接口安全中的名单信息【警告名单和黑名单】
    /// </summary>
    public class ApiMonitorLogNameList
    {
        /// <summary>
        /// 请求的IP地址
        /// </summary>
        public string request_ip
        {
            get; set;
        }
        /// <summary>
        ///名单类型
        /// </summary>
        public  int name_type
        {
            get; set;
        }
    }
}
