﻿using System;
/*
* 命名空间: Basic.Logic
*
* 功 能： 消息修改传输实体
*
* 类 名： MessageModifyRequest
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/8 11:43:42 	罗维      创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{
    /// <summary>
    /// 消息修改传输实体
    /// </summary>
    public class MessageModifyRequest
    {

        /// <summary>
        /// 唯一编码
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 信息标题
        /// </summary>
        public string title
        {
            get; set;
        }

        /// <summary>
        /// 信息类容
        /// </summary>
        public string content
        {
            get; set;
        }
    }
}
