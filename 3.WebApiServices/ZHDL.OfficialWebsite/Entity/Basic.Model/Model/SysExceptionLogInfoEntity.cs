/*
* 命名空间: Basic.Model
*
* 功 能： SysExceptionLogInfo实体类
*
* 类 名： SysExceptionLogInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Basic.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 异常日志记录表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_exception_log_info")]
    public class SysExceptionLogInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysExceptionLogInfoEntity()
        {

            //唯一标识符
            this.id = GuidHelper.GetGuid();

            //异常类型 100：警告  101：严重警告
            this.exception_type = 0;

            //系统类型
            this.system_type = 0;

            //异常标题
            this.exception_title = string.Empty;

            //操作明细
            this.detail = string.Empty;

            //创建日期
            this.create_date = DateTime.Now;

            //状态 100-未解决，101-已解决
            this.status = 100;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36, DataLength = -1, DecimalDigits = 0, ColumnName = "id", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 异常类型 100：警告  101：严重警告
        /// </summary>
        [DBFieldInfo(ByteLength = -1, DataLength = 4, DecimalDigits = 0, ColumnName = "exception_type", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int exception_type
        {
            get; set;
        }

        /// <summary>
        /// 系统类型
        /// </summary>
        [DBFieldInfo(ByteLength = -1, DataLength = 4, DecimalDigits = 0, ColumnName = "system_type", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int system_type
        {
            get; set;
        }

        /// <summary>
        /// 异常标题
        /// </summary>
        [DBFieldInfo(ByteLength = 104, DataLength = -1, DecimalDigits = 0, ColumnName = "exception_title", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string exception_title
        {
            get; set;
        }

        /// <summary>
        /// 操作明细
        /// </summary>
        [DBFieldInfo(ByteLength = 504, DataLength = -1, DecimalDigits = 0, ColumnName = "detail", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string detail
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1, DataLength = 8, DecimalDigits = 0, ColumnName = "create_date", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 状态 100-未解决，101-已解决
        /// </summary>
        [DBFieldInfo(ByteLength = -1, DataLength = 4, DecimalDigits = 0, ColumnName = "status", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int status
        {
            get; set;
        }

    }
}
