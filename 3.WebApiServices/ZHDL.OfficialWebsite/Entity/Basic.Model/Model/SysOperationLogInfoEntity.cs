/*
* 命名空间: Basic.Model
*
* 功 能： SysOperationLogInfo实体类
*
* 类 名： SysOperationLogInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Basic.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 系统操作记录表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_operation_log_info")]
    public class SysOperationLogInfoEntity
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        public SysOperationLogInfoEntity()
        {

            //功能类型 100：插入操作  101：更新操作 102：删除操作
            this.func_type = 0;

            //操作用户CODE
            this.user_code = string.Empty;

            //操作用户名称
            this.user_name = string.Empty;

            //业务标题【用户管理】
            this.business_title = string.Empty;

            //IP地址
            this.ip_address = string.Empty;

            //创建日期
            this.create_date = DateTime.Now;

            //操作明细
            this.detail = string.Empty;

            //唯一标识符
            this.id = GuidHelper.GetGuid();
        }

        /// <summary>
        /// 功能类型 100：插入操作  101：更新操作 102：删除操作
        /// </summary>
        [DBFieldInfo(ByteLength = -1, DataLength = 4, DecimalDigits = 0, ColumnName = "func_type", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public int func_type
        {
            get; set;
        }

        /// <summary>
        /// 操作用户CODE
        /// </summary>
        [DBFieldInfo(ByteLength = 36, DataLength = -1, DecimalDigits = 0, ColumnName = "user_code", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string user_code
        {
            get; set;
        }

        /// <summary>
        /// 操作用户名称
        /// </summary>
        [DBFieldInfo(ByteLength = 54, DataLength = -1, DecimalDigits = 0, ColumnName = "user_name", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string user_name
        {
            get; set;
        }

        /// <summary>
        /// 业务标题【用户管理】
        /// </summary>
        [DBFieldInfo(ByteLength = 104, DataLength = -1, DecimalDigits = 0, ColumnName = "business_title", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string business_title
        {
            get; set;
        }

        /// <summary>
        /// IP地址
        /// </summary>
        [DBFieldInfo(ByteLength = 54, DataLength = -1, DecimalDigits = 0, ColumnName = "ip_address", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string ip_address
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1, DataLength = 8, DecimalDigits = 0, ColumnName = "create_date", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 操作明细
        /// </summary>
        [DBFieldInfo(ByteLength = 1004, DataLength = -1, DecimalDigits = 0, ColumnName = "detail", Required = true, IsPrimarykey = false, IsIncrease = false)]
        public string detail
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36, DataLength = -1, DecimalDigits = 0, ColumnName = "id", Required = true, IsPrimarykey = true, IsIncrease = false)]
        public string id
        {
            get; set;
        }
    }
}
