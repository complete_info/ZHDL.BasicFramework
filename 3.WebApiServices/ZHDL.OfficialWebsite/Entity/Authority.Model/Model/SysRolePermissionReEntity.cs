/*
* 命名空间: Authority.Model
*
* 功 能： SysRolePermissionRe实体类
*
* 类 名： SysRolePermissionReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 角色-权限关系表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_role_permission_re")]
    public class SysRolePermissionReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysRolePermissionReEntity()
     {

           //角色编号
           this.role_id = string.Empty;

           //图层树或功能编号
           this.permission_id = string.Empty;

           //权限类别 100图层 200功能
           this.permission_type = 0;

           //唯一标识符
           this.id = GuidHelper.GetGuid();
      }

        /// <summary>
        /// 角色编号
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "role_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string role_id
        {
            get; set;
        }

        /// <summary>
        /// 图层树或功能编号
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "permission_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string permission_id
        {
            get; set;
        }

        /// <summary>
        /// 权限类别 100图层 200功能
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 4,DecimalDigits = 0,ColumnName = "permission_type",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public int permission_type
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
