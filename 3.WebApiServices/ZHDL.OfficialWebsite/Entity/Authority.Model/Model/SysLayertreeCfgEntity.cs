/*
* 命名空间: Authority.Model
*
* 功 能： SysLayertreeCfg实体类
*
* 类 名： SysLayertreeCfgEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 系统图层树配置表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_layertree_cfg")]
    public class SysLayertreeCfgEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysLayertreeCfgEntity()
     {

           //图层表名
           this.tablename = string.Empty;

           //修改者
           this.modifier_id = string.Empty;

           //唯一标识符
           this.id = GuidHelper.GetGuid();

           //父节点唯一标识符
           this.parent_id = string.Empty;

           //图层名称
           this.name = string.Empty;

           //图层备注
           this.memo = string.Empty;

           //排序编号
           this.sort = 0;

           //图层名对应的要素类名称
           this.featureclass_name = string.Empty;

           //创建者
           this.creator_id = string.Empty;

           //创建者姓名
           this.creator_name = string.Empty;

           //创建日期
           this.create_date = DateTime.Now;

           //修改者姓名
           this.modifier_name = string.Empty;

           //修改者日期
           this.modifier_date = DateTime.Now;

           //是否有效
           this.is_valid = true;

           //是否被逻辑删除
           this.is_deleted = false;
      }

        /// <summary>
        /// 图层表名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "tablename",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string tablename
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "modifier_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "parent_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 图层名称
        /// </summary>
        [DBFieldInfo(ByteLength = 104,DataLength = -1,DecimalDigits = 0,ColumnName = "name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 图层备注
        /// </summary>
        [DBFieldInfo(ByteLength = 304,DataLength = -1,DecimalDigits = 0,ColumnName = "memo",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string memo
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 4,DecimalDigits = 0,ColumnName = "sort",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 图层名对应的要素类名称
        /// </summary>
        [DBFieldInfo(ByteLength = 104,DataLength = -1,DecimalDigits = 0,ColumnName = "featureclass_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string featureclass_name
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "creator_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "creator_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "create_date",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "modifier_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "modifier_date",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_valid",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_deleted",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_deleted
        {
            get; set;
        }
    }
}
