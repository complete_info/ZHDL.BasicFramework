/*
* 命名空间: Authority.Model
*
* 功 能： SysUserInfo实体类
*
* 类 名： SysUserInfoEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 用户信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_user_info")]
    public class SysUserInfoEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysUserInfoEntity()
     {

           //用户描述
           this.user_describe = string.Empty;

           //唯一标识符
           this.id = GuidHelper.GetGuid();

           //用户名【登录名】
           this.name = string.Empty;

           //用户系统显示名
           this.displayname = string.Empty;

           //密码
           this.password = string.Empty;

           //性别
           this.gender = string.Empty;

           //用户头像地址
           this.picture_url = string.Empty;

           //修改者
           this.modifier_id = string.Empty;

           //用户家庭电话
           this.telphone = string.Empty;

           //用户手机号码
           this.mobile_phone = string.Empty;

           //排序编号
           this.sort = 0;

           //QQ号
           this.qq = string.Empty;

           //用户昵称
           this.nickname = string.Empty;

           //用户真实名
           this.realname = string.Empty;

           //用户邮箱
           this.email = string.Empty;

           //办公电话
           this.office_phone = string.Empty;

           //用户身份号码
           this.idnumber = string.Empty;

           //系统隐藏备注
           this.remarks = string.Empty;

           //创建者
           this.creator_id = string.Empty;

           //创建者姓名
           this.creator_name = string.Empty;

           //创建日期
           this.create_date = DateTime.Now;

           //修改者姓名
           this.modifier_name = string.Empty;

           //修改者日期
           this.modifier_date = DateTime.Now;

           //是否有效
           this.is_valid = true;

           //是否登录
           this.is_login = false;

           //是否被逻辑删除
           this.is_deleted = false;

           //是否是超级管理员
           this.is_super_user = false;
      }

        /// <summary>
        /// 用户描述
        /// </summary>
        [DBFieldInfo(ByteLength = 504,DataLength = -1,DecimalDigits = 0,ColumnName = "user_describe",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户名【登录名】
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 用户系统显示名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "displayname",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string displayname
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "password",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string password
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "gender",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        [DBFieldInfo(ByteLength = 259,DataLength = -1,DecimalDigits = 0,ColumnName = "picture_url",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "modifier_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "telphone",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "mobile_phone",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 4,DecimalDigits = 0,ColumnName = "sort",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// QQ号
        /// </summary>
        [DBFieldInfo(ByteLength = 24,DataLength = -1,DecimalDigits = 0,ColumnName = "qq",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "nickname",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "realname",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "email",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "office_phone",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "idnumber",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 系统隐藏备注
        /// </summary>
        [DBFieldInfo(ByteLength = 204,DataLength = -1,DecimalDigits = 0,ColumnName = "remarks",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "creator_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "creator_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "create_date",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "modifier_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "modifier_date",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_valid",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否登录
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_login",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_login
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_deleted",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_deleted
        {
            get; set;
        }

        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_super_user",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_super_user
        {
            get; set;
        }
    }
}
