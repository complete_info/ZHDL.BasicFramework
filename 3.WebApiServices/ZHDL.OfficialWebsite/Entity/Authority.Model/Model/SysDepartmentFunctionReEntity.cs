/*
* 命名空间: Authority.Model
*
* 功 能： SysDepartmentFunctionRe实体类
*
* 类 名： SysDepartmentFunctionReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 部门与功能关联表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_department_function_re")]
    public class SysDepartmentFunctionReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysDepartmentFunctionReEntity()
     {

           //用户组唯一标识符
           this.group_id = string.Empty;

           //功能唯一标识符
           this.function_id = string.Empty;

           //唯一标识符
           this.id = GuidHelper.GetGuid();
      }

        /// <summary>
        /// 用户组唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "group_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string group_id
        {
            get; set;
        }

        /// <summary>
        /// 功能唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "function_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string function_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
