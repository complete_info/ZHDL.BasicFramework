/*
* 命名空间: Authority.Model
*
* 功 能： SysUserPostRe实体类
*
* 类 名： SysUserPostReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 用户-岗位系表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_user_post_re")]
    public class SysUserPostReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysUserPostReEntity()
     {

           //用户唯一标识符号
           this.user_id = string.Empty;

           //职位唯一标识符
           this.post_id = string.Empty;

           //唯一标识符
           this.id = GuidHelper.GetGuid();
      }

        /// <summary>
        /// 用户唯一标识符号
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "user_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string user_id
        {
            get; set;
        }

        /// <summary>
        /// 职位唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "post_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string post_id
        {
            get; set;
        }

        /// <summary>
       ///岗位唯一标识符        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
