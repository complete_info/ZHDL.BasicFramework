/*
* 命名空间: Authority.Model
*
* 功 能： SysFunctionCfg实体类
*
* 类 名： SysFunctionCfgEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 系统菜单功能配置表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_function_cfg")]
    public class SysFunctionCfgEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysFunctionCfgEntity()
     {

           //修改者
           this.modifier_id = string.Empty;

           //父节点唯一标识符
           this.parent_id = string.Empty;

           //菜单名称
           this.name = string.Empty;

           //系统图标
           this.icon_font = string.Empty;

           //级别 10：一级 20：二级 30：三级
           this.level = 1;

           //链接路由地址【用于Web端】
           this.link_url = string.Empty;

           //系统类型【有很多系统，通过这个区分】
           this.system_type = string.Empty;

           //功能类型
           this.function_type = string.Empty;

           //排序编号
           this.sort = 0;

           //描述
           this.describe = string.Empty;

           //创建者
           this.creator_id = string.Empty;

           //创建者姓名
           this.creator_name = string.Empty;

           //创建日期
           this.create_date = DateTime.Now;

           //修改者姓名
           this.modifier_name = string.Empty;

           //修改者日期
           this.modifier_date = DateTime.Now;

           //是否有效
           this.is_valid = true;

           //是否被逻辑删除
           this.is_deleted = false;

           //功能快捷键
           this.shortcut = string.Empty;

           //唯一标识符
           this.id = GuidHelper.GetGuid();

           //是否展开
           this.is_spread = false;

           //展开方式
           this.target = string.Empty;

           //系统类型名称
           this.system_type_name = string.Empty;
      }

        /// <summary>
        /// 修改者
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "modifier_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "parent_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 菜单名称
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 系统图标
        /// </summary>
        [DBFieldInfo(ByteLength = 104,DataLength = -1,DecimalDigits = 0,ColumnName = "icon_font",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string icon_font
        {
            get; set;
        }

        /// <summary>
        /// 级别 10：一级 20：二级 30：三级
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 4,DecimalDigits = 0,ColumnName = "level",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public int level
        {
            get; set;
        }

        /// <summary>
        /// 链接路由地址【用于Web端】
        /// </summary>
        [DBFieldInfo(ByteLength = 259,DataLength = -1,DecimalDigits = 0,ColumnName = "link_url",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string link_url
        {
            get; set;
        }

        /// <summary>
        /// 系统类型【有很多系统，通过这个区分】
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "system_type",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string system_type
        {
            get; set;
        }

        /// <summary>
        /// 功能类型
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "function_type",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string function_type
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 4,DecimalDigits = 0,ColumnName = "sort",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        [DBFieldInfo(ByteLength = 104,DataLength = -1,DecimalDigits = 0,ColumnName = "describe",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string describe
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "creator_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "creator_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "create_date",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "modifier_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "modifier_date",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_valid",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否被逻辑删除
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_deleted",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_deleted
        {
            get; set;
        }

        /// <summary>
        /// 功能快捷键
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "shortcut",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public string shortcut
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 是否展开
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 1,DecimalDigits = 0,ColumnName = "is_spread",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public bool is_spread
        {
            get; set;
        }

        /// <summary>
        /// 展开方式
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "target",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public string target
        {
            get; set;
        }

        /// <summary>
        /// 系统类型名称
        /// </summary>
        [DBFieldInfo(ByteLength = 259,DataLength = -1,DecimalDigits = 0,ColumnName = "system_type_name",Required = false,IsPrimarykey = false,IsIncrease =false)]
        public string system_type_name
        {
            get; set;
        }
    }
}
