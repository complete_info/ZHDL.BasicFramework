﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: Common.Model
*
* 功 能： 基础管理逻辑模块对应Redis设置
*
* 类 名： BasicRedisInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/10 10:44:12 	罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 基础管理逻辑模块对应Redis设置
    /// </summary>
    public class BasicRedisInfo
    {

        /// <summary>
        ///日志相关存放数据库节点
        /// </summary>
        public const int RedisDbLog = 2;

        /// <summary>
        ///Hangfire存放数据库节点
        /// </summary>
        public const int RedisHangfireDbLog = 3;

        //////////////////////////////////////安全相关/////////////////////////////////////
        /// <summary>
        ///  单个IP访问同一个接口访问警告频率上线【可以手动进黑名单】
        /// </summary>
        public const int WarningOnLine = 20;

        /// <summary>
        /// 单个IP访问同一个接口访问禁止频率上线【直接进黑名单】
        /// </summary>
        public const int NoAccessOnLine =50;


        #region 异常日志

        /// <summary>
        ///异常日志HASH对应的ID
        /// </summary>
        public const string RedisExceptionHashId = "ZHDL.OfficialWebsite:ExceptionLog";

        #endregion

        #region 接口访问记录

        /// <summary>
        ///接口访问记录HASH对应的ID
        /// </summary>
        public const string RedisApiMonitorHashId = "ZHDL.OfficialWebsite:ApiMonitor";

        /// <summary>
        /// 接口访问记录失效时间
        /// </summary>
        public const int RedisApiMonitorTimeOut = 10 * 60;

        /// <summary>
        ///接口访问记录统计信息HASH对应的ID
        /// </summary>
        public const string RedisApiMonitorStatisticsHashId = "ZHDL.OfficialWebsite:ApiMonitorStatistics";

        /// <summary>
        ///接口访问记录Path统计信息HASH对应的ID
        /// </summary>
        public const string RedisApiMonitorPathStatisticsHashId = "ZHDL.OfficialWebsite:ApiMonitorPathStatistics";

        /// <summary>
        ///接口访问记录PathIp统计信息HASH对应的ID
        /// </summary>
        public const string RedisApiMonitorPathIpStatisticsHashId = "ZHDL.OfficialWebsite:ApiMonitorPathIpStatistics";

        /// <summary>
        ///接口访问黑名单和警告名单
        /// </summary>
        public const string RedisApiMonitorNameListHashId = "ZHDL.OfficialWebsite:ApiMonitorNameList";

        #endregion

    }
}
