﻿/*
* 命名空间: Common.Model
*
* 功 能： 数据库数据表信息
*
* 类 名： DBTableInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/13 15:57:22 罗维     创建
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/

using System;

namespace Common.Model
{
    /// <summary>
    /// 数据表信息
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class DBTableInfo : Attribute
    {
        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }
    }
}
