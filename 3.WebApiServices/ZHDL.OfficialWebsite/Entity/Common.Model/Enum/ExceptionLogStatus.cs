﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;


/*
* 命名空间: Common.Model
*
* 功 能： 异常日志状态
*
* 类 名： ExceptionLogStatus
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/9 11:04:27 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// 异常日志状态
    /// </summary>
    public enum ExceptionLogStatus
    {
        /// <summary>
        /// 未解决
        /// </summary>
        [Description("未解决")]
        unresolved =100,

        /// <summary>
        /// 已解决
        /// </summary>
        [Description("已解决")]
        resolved = 101,
    }
}
