﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 操作日志中的业务标题
    /// </summary>
    public enum BusinessTitleType
    {
        /// <summary>
        /// 功能信息管理
        /// </summary>
        [Description("功能信息管理")]
        FunctionManage = 110,

        /// <summary>
        /// 图层信息管理
        /// </summary>
        [Description("图层信息管理")]
        LayerManage = 120,

        /// <summary>
        /// 角色信息管理
        /// </summary>
        [Description("角色信息管理")]
        RoleManage = 130,

        /// <summary>
        /// 组织信息管理
        /// </summary>
        [Description("组织信息管理")]
        DepartmentManage =140,

        /// <summary>
        /// 岗位信息管理
        /// </summary>
        [Description("岗位信息管理")]
        PostManage = 150,

        /// <summary>
        /// 用户信息管理
        /// </summary>
        [Description("用户信息管理")]
        UserManage = 160,

        /// <summary>
        /// 日志信息管理
        /// </summary>
        [Description("日志信息管理")]
        LogManage = 170,

        /// <summary>
        /// 公用逻辑操作
        /// </summary>
        [Description("公用逻辑操作")]
        CommonLogicManage = 180,

        /// <summary>
        /// 消息逻辑操作
        /// </summary>
        [Description("消息逻辑操作")]
        MessageManage = 190,

        /// <summary>
        /// 消息逻辑操作
        /// </summary>
        [Description("接口访问记录逻辑操作")]
        ApiLogManage =200,

    }
}
