﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 操作日志类型
    /// </summary>
    public enum OperationLogType
    {
        /// <summary>
        /// 100：插入操作
        /// </summary>
        [Description("插入操作")]
        AddOperation = 100,

        /// <summary>
        /// 101：更新操作
        /// </summary>
        [Description("更新操作")]
        ModifyOperation = 101,

        /// <summary>
        /// 102：删除操作
        /// </summary>
        [Description("删除操作")]
        RemoveOperation = 102,

        /// <summary>
        /// 103：登录操作
        /// </summary>
        [Description("登录操作")]
        LoginOperation = 103,
    }
}
