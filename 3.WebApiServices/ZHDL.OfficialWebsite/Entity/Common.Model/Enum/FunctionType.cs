﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 功能类型
    /// </summary>
    public enum FunctionType
    {
        /// <summary>
        ///目录
        /// </summary>
        [Description("目录")]
        Catalog = 100,

        /// <summary>
        /// 菜单
        /// </summary>
        [Description("菜单")]
        Menu = 200,

        /// <summary>
        /// 二级菜单
        /// </summary>
        [Description("按钮")]
        Button = 300,
    }
}
