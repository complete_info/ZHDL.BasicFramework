﻿using Basic.Logic;
using Container.Library;
using Hangfire;
using System;
using static Scheduler.Library.Scheduler;
using static Scheduler.Library.Trigger;

/*
* 命名空间: Api.Manage.App_Start
*
* 功 能： 接口服务端定时任务管理
*
* 类 名： SchedulerManage
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/13 10:10:04 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Api.Manage.App_Start
{
    /// <summary>
    /// 接口服务端定时任务管理
    /// </summary>
    public class SchedulerManage
    {
        /// <summary>
        /// 接口访问监控日志Redis操作类实例
        /// </summary>
        private  IApiMonitorLogService apiMonitorLogService = null;

        /// <summary>
        /// 开启Trigger定时任务
        /// </summary>
        public void TriggerStart()
        {
            apiMonitorLogService = UnityCIContainer.Instance.GetService<IApiMonitorLogService>();

            //删除过期接口访问记录信息
            RemoveExpiredApiLog();

            //删除过期接口访问记录统计信息
            RemoveExpiredGroupInfo();

            //统计接口请求信息
            StatisticsInterfaceRequest();
        }

        #region Trigger

        /// <summary>
        /// 删除过期接口访问记录信息
        /// </summary>
        private void RemoveExpiredApiLog()
        {
            ScheduleTask("RemoveExpiredApiMonitorLog", NewTrigger.Every(new TimeSpan(0, 0, 60)).StartAt(DateTime.Now), () =>
            {
                ApiMonitorLogServiceRedis.RemoveExpiredApiMonitorLog();
            });
        }


        /// <summary>
        /// 删除过期接口访问记录统计信息
        /// </summary>
        private void RemoveExpiredGroupInfo()
        {

            ScheduleTask("RemoveExpiredGroupInfo", NewTrigger.Every(new TimeSpan(0, 0, 60)).StartAt(DateTime.Now), () =>
            {
                ApiMonitorLogServiceRedis.RemoveExpiredGroupInfo();
            });
        }


        /// <summary>
        /// 统计接口请求信息
        /// </summary>
        private void StatisticsInterfaceRequest() {

            //间隔统计时间
            int seconds = 5;

            ScheduleTask("StatisticsInterfaceRequest", NewTrigger.Every(new TimeSpan(0, 0, seconds)).StartAt(DateTime.Now), () =>
            {
                apiMonitorLogService.StatisticsInterfaceRequest(seconds);
            });
        }

        #endregion
    }
}
