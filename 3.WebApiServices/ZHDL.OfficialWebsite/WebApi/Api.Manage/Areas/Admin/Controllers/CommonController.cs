﻿using Api.Manage.App_Start;
using Authority.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

namespace Api.Manage.Areas.Admin.Controllers
{

    /// <summary>
    /// 公用操作接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Common")]
    [Route("api/[controller]")]
    [ApiController]
    public class CommonController : ControllerBase
    {
        
        private readonly ICommonService commonService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public CommonController()
        {
            commonService = UnityCIContainer.Instance.GetService<ICommonService>();
        }


        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="inputInfo">图片上传请求实体</param>
        /// <returns></returns>
        [HttpPost("UploadImage")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<string> UploadImage([FromBody]ImageUploadRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<string>();

            Try.CatchLog(() =>
            {
                inputInfo.Validate();

                resultInfo = commonService.UploadImage(inputInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "上传图片失败");

            }, $"系统错误，上传图片失败");
            return resultInfo;
        }
    }
}