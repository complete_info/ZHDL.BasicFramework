﻿using System;
using System.Collections.Generic;
using Api.Manage.App_Start;
using Basic.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

namespace Api.Manage.Areas.Basic.Controllers
{
    /// <summary>
    /// 消息管理接口控制器 
    /// </summary>
    [ApiExplorerSettings(GroupName = "Basic")]
    [Route("api/Basic/[controller]")]
    [ApiController]
    public class MessageController : ControllerBase
    {
        private readonly IMessageService messageService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public MessageController()
        {
            messageService = UnityCIContainer.Instance.GetService<IMessageService>();
        }

        #region 消息管理相关逻辑

        #region 查询操作
        /// <summary>
        /// 获取消息分页列表
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<MessageInfoResponse>> LoadList([FromBody]ParametersInfo<MessageQueryRequest> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<MessageInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = messageService.LoadPageList(inputInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取消息分页列表失败");
            }, $"系统错误，获取消息分页列表失败");
            return resultInfo;
        }
        #endregion

        #region 更新操作

        /// <summary>
        /// 发送信息
        /// </summary>
        /// <param name="sendInfo"></param>
        /// <returns></returns>
        [HttpPost("SendMessage")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> SendMessage([FromBody]MessageSendRequest sendInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                sendInfo.Validate();
                resultInfo = messageService.SendMessage(sendInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "发送信息失败");

            }, $"系统错误，发送信息失败");
            return resultInfo;
        }


        /// <summary>
        /// 读取信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("ReadMessage")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<MessageInfoResponse> ReadMessage(string id)
        {
            var resultInfo = new ResultJsonInfo<MessageInfoResponse>();

            Try.CatchLog(() =>
            {
                resultInfo = messageService.ReadMessage(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "读取信息失败");

            }, $"系统错误，读取信息失败");
            return resultInfo;
        }



        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        [HttpPost("Modify")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Modify([FromBody]MessageModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = messageService.Modify(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "修改信息失败");

            }, $"系统错误，修改信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 物理删除消息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpGet("Remove")]
        [HttpPost("Remove")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {

                resultInfo = messageService.Remove(ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "物理删除消息失败");

            }, $"系统错误，物理删除消息失败");
            return resultInfo;
        }

        #endregion

        #endregion

        #region 主页面消息相关逻辑

        /// <summary>
        /// 获取对应时间段类，自己的总的消息，未读消息数量
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("LoadMyselfMessageCount")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<MessageMyselfCountResponse> LoadMyselfMessageCount(DateTime beginTime, DateTime endTime) {

            var resultInfo = new ResultJsonInfo<MessageMyselfCountResponse>();

            Try.CatchLog(() =>
            {
                resultInfo = messageService.LoadMyselfMessageCount(beginTime, endTime);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取对应时间段类，自己的总的消息，未读消息数量失败");
            }, $"系统错误，获取对应时间段类，自己的总的消息，未读消息数量失败");
            return resultInfo;
        }


        /// <summary>
        /// 获取对应时间段内，自己未读消息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("LoadUnreadByTime")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<MessageInfoResponse>> LoadUnreadByTime(DateTime beginTime, DateTime endTime)
        {

            var resultInfo = new ResultJsonInfo<List<MessageInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = messageService.LoadUnreadByTime(beginTime, endTime);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取对应时间段内未读消息失败");
            }, $"系统错误，获取对应时间段内未读消息失败");
            return resultInfo;
        }
        #endregion
    }
}