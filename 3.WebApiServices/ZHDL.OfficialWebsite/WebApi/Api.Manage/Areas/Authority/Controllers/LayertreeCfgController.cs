﻿using System.Collections.Generic;
using Api.Manage.App_Start;
using Authority.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

/*
* 命名空间: Api.Manage.Areas.Authority.Controllers
*
* 功 能： 图层管理控制器
*
* 类 名： LayertreeCfgController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/01 14:34:43 李聪     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Api.Manage.Areas.Authority.Controllers
{
    /// <summary>
    /// 图层管理
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class LayertreeCfgController : ControllerBase
    {
        private readonly ILayertreeCfgService cfgService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public LayertreeCfgController()
        {
            cfgService = UnityCIContainer.Instance.GetService<ILayertreeCfgService>();
        }

        #region 图层基础管理操作

        #region 查询
        /// <summary>
        /// 查询所有的图层组
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<List<LayertreeResponse>> LoadList(ParametersInfo<string> parameters)
        {
            var resultInfo = new ResultJsonInfo<List<LayertreeResponse>>();

            Try.CatchLog(() =>
            {
                // userAdd.Validate();
                resultInfo = cfgService.LoadList(parameters);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "图层管理-查询所有的图层组失败");

            }, $"系统错误，图层管理-查询所有的图层组失败");
            return resultInfo;
        }

        /// <summary>
        /// 查询单个图层的信息
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpPost("LoadSinger")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<LayertreeResponse> LoadSingle(string groupId)
        {
            var resultInfo = new ResultJsonInfo<LayertreeResponse>();

            Try.CatchLog(() =>
            {
                // userAdd.Validate();
                resultInfo = cfgService.LoadSinger(groupId);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "图层管理-查询单个图层失败");

            }, $"系统错误，图层管理-查询单个图层失败");
            return resultInfo;
        }
        #endregion

        #region 新增
        /// <summary>
        /// 新增节点
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("AddNode")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<int> AddNode([FromBody]LayertreeCfgChildAddRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                request.Validate();
                resultInfo = cfgService.Addnode(request);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "图层管理-新增节点失败");

            }, $"系统错误，图层管理-新增节点失败");
            return resultInfo;
        }

       
        #endregion

        #region 修改
        /// <summary>
        /// 修改图层
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("Modify")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<int> Modify([FromBody]LayertreeCfgModifyAddRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                request.Validate();
                resultInfo = cfgService.Modify(request);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "图层管理-修改图层失败");

            }, $"系统错误，图层管理-修改图层失败");
            return resultInfo;
        }

        /// <summary>
        /// 删除图层
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<int> Remove(List<string> groupId)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = cfgService.Remove(groupId);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "图层管理-删除图层失败");

            }, $"系统错误，图层管理-删除图层失败");
            return resultInfo;
        }

        /// <summary>
        /// 启用/禁用图层
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = cfgService.ForbidOrEnable(id);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "图层管理-启用/禁用图层失败");

            }, $"系统错误，图层管理-启用/禁用图层失败");
            return resultInfo;
        }
        /// <summary>
        /// 移动顺序操作（1：上移/2：下移）
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// </remarks>
        /// <returns></returns>
        [HttpPost("Move")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Move([FromBody]LayertreeCfgMoveRequest moveInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                moveInfo.Validate();
                resultInfo = cfgService.Move(moveInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "图层管理-移动顺序操作失败");

            }, $"系统错误，图层管理-移动顺序操作失败");

            return resultInfo;
        }

        #endregion

        #endregion


        #region 与权限相关功能
        /// <summary>
        /// 根据角色ID，获取所有可操作图层已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadListInfoByRoleId")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<List<LayertreeCfgRoleLayerResponse>> LoadListInfoByRoleId([FromBody]LayertreeCfgRoleQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<LayertreeCfgRoleLayerResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = cfgService.LoadListInfoByRoleId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "图层管理-根据角色ID，获取所有可操作图层已赋权限情况信息失败");

            }, $"系统错误，图层管理-根据角色ID，获取所有可操作图层已赋权限情况信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 修改对应角色赋值的图层操作权限
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        [HttpPost("ModifyRoleLayerInfo")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<int> ModifyRoleLayerInfo([FromBody]LayertreeCfgRoleModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = cfgService.ModifyRoleLayerInfo(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "图层管理-修改对应角色赋值的图层操作权限信息失败");

            }, $"系统错误，图层管理-修改对应角色赋值的图层操作权限信息失败");

            return resultInfo;
        }
        #endregion
    }
}