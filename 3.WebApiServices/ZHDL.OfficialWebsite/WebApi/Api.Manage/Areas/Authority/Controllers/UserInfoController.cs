﻿using System.Collections.Generic;
using Api.Manage.App_Start;
using Authority.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;

/*
* 命名空间: Api.Manage.Areas.Authority.Controllers
*
* 功 能： 用户相关接口
*
* 类 名： UserInfoController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/07 14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Api.Manage.Areas.Authority.Controllers
{
    /// <summary>
    /// 用户相关接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class UserInfoController : ControllerBase
    {

        private readonly ISysUserService userService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public UserInfoController()
        {
            userService = UnityCIContainer.Instance.GetService<ISysUserService>();
        }

        #region 用户基础信息管理操作

        #region 查询

        /// <summary>
        /// 根据条件分页查询用户数据
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<UserInfoResponse>> LoadList([FromBody]ParametersInfo<UserInfoQueryRequest> parameters)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = userService.LoadList(parameters);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-获取用户分页列表失败");

            }, $"系统错误，用户管理-获取用户分页列表失败");
            return resultInfo;
        }

        /// <summary>
        /// 查询单个用户的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        [HttpGet("LoadSingle")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<UserInfoResponse> LoadSingle(string userId)
        {
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            Try.CatchLog(() =>
            {

                resultInfo = userService.LoadSingle(userId);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-获取单个用户失败");

            }, $"系统错误，用户管理-获取单个用户失败");
            return resultInfo;
        }

        /// <summary>
        /// 查询自己的数据
        /// </summary>
        /// <returns></returns>
        [HttpGet("LoadUserSingle")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<UserInfoResponse> LoadUserSingle()
        {
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            Try.CatchLog(() =>
            {

                resultInfo = userService.LoadUserSingle();

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-获取单个用户失败");

            }, $"系统错误，用户管理-获取单个用户失败");
            return resultInfo;
        }

        #endregion

        #region 添加
        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        [HttpPost("AddUser")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> AddUser([FromBody]UserInfoAddRequest userAdd)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                userAdd.Validate();
                resultInfo = userService.AddUser(userAdd);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-新增用户失败");

            }, $"系统错误，用户管理-新增用户失败");
            return resultInfo;
        }

        /// <summary>
        /// 用户配置部门
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        [HttpPost("UserDepartment")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> UserDepartment([FromBody]UserInfoDepartmentRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {

                resultInfo = userService.UserDepartment(department);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-用户配置部门失败");

            }, $"系统错误，用户管理-用户配置部门失败");
            return resultInfo;
        }

        /// <summary>
        /// 用户配置岗位
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        [HttpPost("UserPost")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> UserPost([FromBody]UserInfoPositionRequset position)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {

                resultInfo = userService.UserPost(position);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-用户配置岗位失败");

            }, $"系统错误，用户管理-用户配置岗位失败");
            return resultInfo;
        }

        #endregion

        #region 修改
        /// <summary>
        /// 修改密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("ChangePassword")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> ChangePassword([FromBody]UserChangePasswordRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                request.Validate();
                resultInfo = userService.ChangePassword(request);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-修改用户数据失败");

            }, $"系统错误，用户管理-修改用户数据失败");
            return resultInfo;
        }

        /// <summary>
        /// 修改用户数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("Modify")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Modify([FromBody]UserInfoModifyRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                request.Validate();
                resultInfo = userService.Modify(request);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-修改用户数据失败");

            }, $"系统错误，用户管理-修改用户数据失败");
            return resultInfo;
        }


        /// <summary>
        /// 修改用户自身的数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("ModifyOneself")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> ModifyOneself([FromBody]UserInfoModifyOneselfRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                request.Validate();
                resultInfo = userService.ModifyOneself(request); 

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-修改用户自身的数据失败");

            }, $"系统错误，用户管理-修改用户自身的数据失败");
            return resultInfo;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Remove(List<string> ids)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {

                resultInfo = userService.Remove(ids);

            }, ex =>
            {

                resultInfo.SystemExc(resultInfo, ex, "用户管理-删除用户失败");

            }, $"系统错误，用户管理-删除用户失败");
            return resultInfo;
        }


        /// <summary>
        /// 禁用/启用用户
        /// </summary>
        /// <param name="id">用户ID</param>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = userService.ForbidOrEnable(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-禁用/启用用户失败");

            }, $"系统错误，用户管理-禁用/启用用户失败");
            return resultInfo;
        }
        #endregion

        #endregion

        #region 信息发送相关功能

        /// <summary>
        /// 根据条件分页查询除自己以外的用户数据
        /// </summary>
        /// <param name="parameters"></param>
        /// <returns></returns>
        [HttpPost("LoadListExceptMyself")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<UserInfoResponse>> LoadListExceptMyself([FromBody]ParametersInfo<UserInfoQueryRequest> parameters)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = userService.LoadListExceptMyself(parameters);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-根据条件分页查询除自己以外的用户数据失败");

            }, $"系统错误，用户管理-根据条件分页查询除自己以外的用户数据失败");
            return resultInfo;
        }


        /// <summary>
        /// 根据条件查询除自己以外的用户id数据
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadIdListExceptMyself")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<string>> LoadIdListExceptMyself([FromBody]UserInfoQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<string>>();

            Try.CatchLog(() =>
            {
                resultInfo = userService.LoadIdListExceptMyself(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-根据条件查询除自己以外的用户id数据失败");

            }, $"系统错误，用户管理-根据条件查询除自己以外的用户id数据失败");
            return resultInfo;
        }

        #endregion

        #region 批量导入导出
        /// <summary>
        ///验证导入信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("VerifyThatTheFile")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<UserInfoFileRequest>> VerifyThatTheFile([FromBody]List<UserInfoFileRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoFileRequest>>();
            Try.CatchLog(() =>
            {
                resultInfo = userService.VerifyThatTheFile(selects);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-验证信息失败");

            }, $"系统错误，用户管理-验证信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 事务批量导入角色信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("TranBulkImportRole")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> TranBulkImportRole([FromBody]List<UserInfoFileRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<int>();
            Try.CatchLog(() =>
            {
                resultInfo = userService.TranBulkImportRole(selects);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-事务批量导入用户信息失败");

            }, $"系统错误，用户管理-事务批量导入用户信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 批量导出所有
        /// </summary>
        /// <returns></returns>
        [HttpGet("ListAll")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<UserInfoResponse>> ListAll()
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();
            Try.CatchLog(() =>
            {
                resultInfo = userService.ListAll();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "用户管理-批量导出用户信息失败");

            }, $"系统错误，用户管理-批量导出用户信息失败");
            return resultInfo;
        }
        #endregion


    }
}