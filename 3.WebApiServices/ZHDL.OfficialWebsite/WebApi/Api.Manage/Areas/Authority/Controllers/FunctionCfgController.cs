﻿using System.Collections.Generic;
using Api.Manage.App_Start;
using Authority.Logic;
using Authority.Model;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Validate.Library;
/*
* 命名空间: Api.Manage.Areas.Authority.Controllers
*
* 功 能： 菜单功能逻辑接口
*
* 类 名： FunctionCfgController
*
* Version   变更日期          负责人       变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/25     14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Api.Manage.Areas.Authority.Controllers
{
    /// <summary>
    /// 菜单功能逻辑接口
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class FunctionCfgController : ControllerBase
    {

        IFunctionCfgService functionCfgService = null;
        /// <summary>
        /// 菜单功能逻辑接口构造函数
        /// </summary>
        public FunctionCfgController()
        {
            //菜单功能逻辑注入
            functionCfgService = UnityCIContainer.Instance.GetService<IFunctionCfgService>();
        }

        #region 菜单基础功能管理

        #region 信息查询

        /// <summary>
        /// 根据菜单名称或系统类型获取菜单列表信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadListInfo")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<List<FunctionCfgInfoResponse>> LoadListInfo([FromBody]ParametersInfo<FunctionCfgQueryRequest> queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FunctionCfgInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadListInfo(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单管理-根据关键字获取所有的未删除菜单信息失败");

            }, $"系统错误，菜单管理-根据关键字获取所有的未删除菜单信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 根据菜单名称或系统类型获取菜单树状列表信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadTreeListInfo")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<List<MenuInfo>> LoadTreeListInfo([FromBody]FunctionCfgQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadTreeListInfo(queryInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单管理-根据菜单名称或系统类型获取菜单树状列表信息失败");

            }, $"系统错误，菜单管理-根据菜单名称或系统类型获取菜单树状列表信息失败");

            return resultInfo;
        }


        #endregion

        #region 更新操作

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、操作失败，返回 1205，无效操作
        /// </remarks>
        /// <param name="addInfo">根节点信息</param>
        /// <returns></returns>
        [HttpPost("AddInfo")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> AddInfo([FromBody]FunctionCfgAddRequest addInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            Try.CatchLog(() =>
            {
                addInfo.Validate();
                resultInfo = functionCfgService.AddInfo(addInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单功能管理-新增节点信息失败");

            }, $"系统错误，菜单功能管理-新增节点信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在、操作失败，返回 1205
        /// </remarks>
        /// <returns></returns>
        [HttpPost("ModifyInfo")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> ModifyInfo([FromBody]FunctionCfgModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            Try.CatchLog(() =>
            {
                modifyInfo.Validate();
                resultInfo = functionCfgService.Modify(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单功能管理-修改节点信息信息失败");

            }, $"系统错误，菜单功能管理-修改节点信息信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="moveInfo"></param>
        /// <returns></returns>
        [HttpPost("MoveSort")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> MoveSort([FromBody]FunctionCfgMoveRequest moveInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            Try.CatchLog(() =>
            {
                moveInfo.Validate();
                resultInfo = functionCfgService.MoveSort(moveInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单功能管理-移动顺序操作失败");

            }, $"系统错误，菜单功能管理-移动顺序操作失败");

            return resultInfo;
        }

        /// <summary>
        /// 菜单启用与停用操作
        /// </summary>
        /// <param name="id">菜单唯一标识符</param>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();
            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.ForbiddenInfo(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单功能管理-启用与停用操作失败");

            }, $"系统错误，菜单功能管理-启用与停用操作失败");

            return resultInfo;
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <returns></returns>
        [HttpGet("Remove")]
        [HttpPost("Remove")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Remove(string[] ids)
        {
            var resultInfo = new ResultJsonInfo<int>();
            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.Remove(ids);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单功能管理-删除节点失败");

            }, $"系统错误，菜单功能管理-删除节点操作失败");

            return resultInfo;
        }

        #endregion

        #endregion

        #region 与权限相关功能

        /// <summary>
        /// 根据角色ID，获取所有可操作菜单已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadListInfoByRoleId")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<List<FunctionCfgRoleMenuResponse>> LoadListInfoByRoleId([FromBody]FunctionCfgRoleQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FunctionCfgRoleMenuResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadListInfoByRoleId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单管理-根据角色ID，获取所有可操作菜单已赋权限情况信息失败");

            }, $"系统错误，菜单管理-根据角色ID，获取所有可操作菜单已赋权限情况信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 修改对应角色赋值的菜单操作权限
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        [HttpPost("ModifyRoleMenuInfo")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<int> ModifyRoleMenuInfo([FromBody]FunctionCfgRoleModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.ModifyRoleMenuInfo(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "菜单管理-修改对应角色赋值的菜单操作权限信息失败");

            }, $"系统错误，菜单管理-修改对应角色赋值的菜单操作权限信息失败");

            return resultInfo;
        }
        

        #endregion

        #region 公用功能

        /// <summary>
        /// 获取系统类型Select列表信息
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [UserAuthorization(true)]
        [HttpGet("LoadSystemTypeList")]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadSystemTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadSystemTypeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门管理-获取系统类型Select列表信息失败");

            }, $"系统错误，菜单基础功能管理-获取系统类型Select列表信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 获取功能类型Select列表信息
        /// </summary>
        /// <returns></returns>
        [Authorize]
        [UserAuthorization(true)]
        [HttpGet("LoadFunctionTypeList")]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadFunctionTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadFunctionTypeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门管理-获取功能类型Select列表信息失败");

            }, $"系统错误，菜单基础功能管理-获取功能类型Select列表信息失败");


            return resultInfo;
        }

        #endregion
    }
}