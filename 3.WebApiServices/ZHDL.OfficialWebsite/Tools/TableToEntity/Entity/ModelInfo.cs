﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TableToEntity
{
    /// <summary>
    /// 实体相关信息
    /// </summary>
    public class ModelInfo
    {
        /// <summary>
        /// 实体命名空间
        /// </summary>
        public string ModelNameSpace { get; set; }

        /// <summary>
        /// 实体创建人
        /// </summary>
        public string ModelAuthorName { get; set; }

        /// <summary>
        /// 试图命名空间
        /// </summary>
        public string ViewNameSpace { get; set; }


        /// <summary>
        /// 实体创建人
        /// </summary>
        public string ViewAuthorName { get; set; }
    }
}
