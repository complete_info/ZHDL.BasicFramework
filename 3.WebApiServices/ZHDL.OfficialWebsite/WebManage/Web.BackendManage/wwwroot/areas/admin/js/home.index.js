var $, tab, dataStr, layer;
layui.config({
    base: "/areas/admin/js/"
}).extend({
    bodyTab: "home.body.tab"
});
layui.use(['form', 'element', 'layer', 'laytpl', 'jquery', 'bodyTab'], function () {
    var form = layui.form,
        element = layui.element,
        $ = layui.$,
        laytpl = layui.laytpl,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        tab = layui.bodyTab({
            openTabNum: "50",  //最大可打开窗口数量
            url: "/api/Home/LoadSubMenus" //获取菜单json地址
        });

    var adminHomeIndexLogic = {
        //初始化
        init: function () {
            self = this;
            ///全局参数
            this.Parameters = {
                ac: new AsyncCaller(),    //异步方法管理
                loadTopMenuListUrl: "/api/Home/LoadTopMenuList",
                loadSubMenusUrl: "/api/Home/LoadSubMenus",

                changePwdUrl: "/Admin/Home/ChangePwd",//修改密码页面

                //请求时间标识
                requestTicket: 0
            };

            this.Parameters.ac
                .pushQueue(function () {

                    self.logicFunc.loadTopMenu(function () {
                        //页面初始化
                        self.initPage();
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                })
                .pushQueue(this.bindEvent)
                .exec();

        },
        //初始化列表
        initPage: function () {

            self.logicFunc.loadUserInfo();
        },
        //绑定事件
        bindEvent: function () {

            //隐藏左侧导航
            $(".hideMenu").click(function () {
                if ($(".topLevelMenus li.layui-this a").data("url")) {
                    layer.msg("此栏目状态下左侧菜单不可展开");  //主要为了避免左侧显示的内容与顶部菜单不匹配
                    return false;
                }
                $(".layui-layout-admin").toggleClass("showMenu");
                //渲染顶部窗口
                tab.tabMove();
            });

            //手机设备的简单适配
            $('.site-tree-mobile').on('click', function () {
                $('body').addClass('site-mobile');
            });
            $('.site-mobile-shade').on('click', function () {
                $('body').removeClass('site-mobile');
            });

            //页面加载时判断左侧菜单是否显示
            //通过顶部菜单获取左侧菜单
            $(".topLevelMenus li,.mobileTopLevelMenus dd").unbind("click").click(function () {
                if ($(this).parents(".mobileTopLevelMenus").length != "0") {
                    $(".topLevelMenus li").eq($(this).index()).addClass("layui-this").siblings().removeClass("layui-this");
                } else {
                    $(".mobileTopLevelMenus dd").eq($(this).index()).addClass("layui-this").siblings().removeClass("layui-this");
                }
                $(".layui-layout-admin").removeClass("showMenu");
                $("body").addClass("site-mobile");
                self.logicFunc.getData($(this).data("menu"));
                //渲染顶部窗口
                tab.tabMove();
            });

            //默认打开第一个菜单
            $(".topLevelMenus>li.layui-nav-item:first").trigger("click").addClass("layui-this");

            //添加新窗口
            $("body").on("click", ".layui-nav .layui-nav-item a:not('.mobileTopLevelMenus .layui-nav-item a')", function () {
                //如果不存在子级
                if ($(this).siblings().length == 0) {
                    self.logicFunc.addTab($(this));
                    $('body').removeClass('site-mobile');  //移动端点击菜单关闭菜单层
                }
                $(this).parent("li").siblings().removeClass("layui-nav-itemed");
            });

            //修改密码
            $(".changePwd").on("click", function () {

                self.logicFunc.changePwd();

            });
        },
        //事件
        logicFunc: {

            //修改密码
            changePwd: function () {
                var index = layui.layer.open({
                    title: "修改密码",
                    type: 2,
                    resize: false,
                    area: ['400px', '300px'],
                    content: self.Parameters.changePwdUrl,
                    end: function () {
                        $(window).unbind("resize");
                    }
                });
            },

            ///显示用户信息
            loadUserInfo: function () {
                var userInfo = GrdAdmin.getUserInfo();
                $("#user_img").attr("src", GrdAdmin.webURI +userInfo.picture_url);
                $("#adminName").text(userInfo.displayname);
            },

            //获取顶端菜单
            loadTopMenu: function (func) {

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.loadTopMenuListUrl,
                    data: {
                        systemType: 100
                    },
                    type: GrdAdmin.ajaxGet,
                    modal: false,
                    success: function (result) {
                        if (result.Code == 1000) {
                            var mobileGetTpl = mobileTopMenus.innerHTML;
                            laytpl(mobileGetTpl).render(result.Data, function (html) {
                                $(".mobileTopMenus").html(html);
                            });

                            var getTpl = topMenus.innerHTML;
                            laytpl(getTpl).render(result.Data, function (html) {
                                $(".topLevelMenus").html(html);
                            });
                            //初始化动态元素，一些动态生成的元素如果不设置初始化，将不会有默认的动态效果
                            //element.render();
                            element.init();

                            if (func) {
                                func();
                            }
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                            return;
                        }
                    }
                });
            },

            //通过顶部菜单获取左侧二三级菜单
            getData: function (parentId) {

                var layerLoadIndex = layer.load();
                var time = new Date().getTime();

                self.Parameters.requestTicket = time;
                layer.close(layerLoadIndex);

                var json = {
                    parentId: parentId,
                    systemType:100
                };
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.loadSubMenusUrl,
                    data: json,
                    type: GrdAdmin.ajaxGet,
                    modal: false,
                    success: function (result) {
                        /* 非本次请求忽略 */
                        if (self.Parameters.requestTicket != time) {
                            return;
                        }
                        //菜单信息
                        dataStr = result.Data;
                        //重新渲染左侧菜单
                        tab.render();
                    }
                });
            },

            //打开新窗口
            addTab: function (_this) {
                tab.tabAdd(_this);
            }
        },
        //工具
        tools: {

        }
    };
    adminHomeIndexLogic.init();
})


