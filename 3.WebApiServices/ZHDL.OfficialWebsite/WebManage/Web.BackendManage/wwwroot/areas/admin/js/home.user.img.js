﻿layui.use(['layer', 'form', 'jquery'], function () {
    var form = layui.form, layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery, laytpl = layui.laytpl, player = parent.layer || layui.layer;

    var uncropper = GrdAssist.queryString("isCropper") == null ? false : GrdAssist.queryString("isCropper");

    var adminHomeUserImgLogic = {
        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                uploadImageUrl:"/api/Common/UploadImage"
            };
            this.initPage();

            this.bindEvent();
        },
        //初始化列表
        initPage: function () {

            //弹出框水平垂直居中
            (window.onresize = function () {
                var win_height = $(window).height();
                var win_width = $(window).width();
                if (win_width <= 768) {
                    $(".tailoring-content").css({
                        "top": (win_height - $(".tailoring-content").outerHeight()) / 2,
                        "left": 0
                    });
                } else {
                    $(".tailoring-content").css({
                        "top": (win_height - $(".tailoring-content").outerHeight()) / 2,
                        "left": (win_width - $(".tailoring-content").outerWidth()) / 2
                    });
                }
            })();
            
            //cropper图片裁剪
            $('#tailoringImg').cropper({
                //aspectRatio: 750 / 415,//默认比例
                aspectRatio: parseInt(GrdAssist.queryString("width")) / parseInt(GrdAssist.queryString("height")),//默认比例
                preview: '.previewImg',//预览视图
                guides: true,  //裁剪框的虚线(九宫格)
                autoCropArea: 0.8,  //0-1之间的数值，定义自动剪裁区域的大小，默认0.8
                movable: true, //是否允许移动图片
                dragCrop: true,  //是否允许移除当前的剪裁框，并通过拖动来新建一个剪裁框区域
                movable: true,  //是否允许移动剪裁框
                resizable: true,  //是否允许改变裁剪框的大小
                zoomable: true,  //是否允许缩放图片大小
                mouseWheelZoom: true,  //是否允许通过鼠标滚轮来缩放图片
                touchDragZoom: true,  //是否允许通过触摸移动来缩放图片
                rotatable: true,  //是否允许旋转图片
                crop: function (e) {
                    // 输出结果数据裁剪图像。
                }
            });

        },
        //绑定事件
        bindEvent: function () {

            //选择图片
            $("#chooseImg").on("input", function (e) {
                self.logicFunc.selectImg(this);
            });

            //关闭
            $("#closeTailor").on("click", function () {
                self.logicFunc.closeTailor(this);
            });

            //弹出图片裁剪框
            $("#thumbnail").on("click", function () {
                $(".tailoring-container").toggle();
            });

            //裁剪后的处理
            $("#sureCut").on("click", function () {
                if ($("#tailoringImg").attr("src") == null) {
                    return false;
                }
                else {
                    var cas = $('#tailoringImg').cropper('getCroppedCanvas');//获取被裁剪后的canvas
                    var base64url = cas.toDataURL('image/jpeg'); //转换为base64地址形式
                    uncropper && (base64url = $("#tailoringImg").attr("src"));//取消切图获取原图base64
                    $("#hideimge").val(base64url);
                    var layerLoadIndex = 0;

                    var json = {
                        imageBase64: base64url,
                        uploadFolder: GrdAssist.queryString("uploadFolder"),
                        originalPic: GrdAssist.queryString("originalPic")
                    };
                    GrdAdmin.ajaxRequest({
                        url: self.Parameters.uploadImageUrl,
                        data: json,
                        success: function (result) {
                            if (result.Code == 1000) {
                                GrdAdmin.msg('上传成功', "成功", function () {
                                    //回调
                                    self.logicFunc.callback(GrdAssist.queryString("uploadFolder"), result.Data);
                                    //关闭裁剪框
                                    self.logicFunc.closeTailor();

                                    layer.close(layerLoadIndex);

                                    var index = parent.layer.getFrameIndex(window.name);
                                    parent.layer.close(index);
                                });
                            } else {
                                GrdAdmin.msg(result.Msg, "警告");
                            }
                        }
                    });
                    $("#img").prop("src", base64url);//显示为图片的形式
                }
            });

            //旋转
            $(".cropper-rotate-btn").on("click", function () {
                $('#tailoringImg').cropper("rotate", 45);
            });

            //复位
            $(".cropper-reset-btn").on("click", function () {
                $('#tailoringImg').cropper("reset");
            });

            //换向
            var flagX = true;
            $(".cropper-scaleX-btn").on("click", function () {
                if (flagX) {
                    $('#tailoringImg').cropper("scaleX", -1);
                    flagX = false;
                } else {
                    $('#tailoringImg').cropper("scaleX", 1);
                    flagX = true;
                }
                flagX != flagX;
            });
        },
        //逻辑事件
        logicFunc: {
            //图像上传
            selectImg: function (file) {
                if (!file.files || !file.files[0]) {
                    return;
                }
                var reader = new FileReader();
                reader.onload = function (evt) {
                    var replaceSrc = evt.target.result;
                    //更换cropper的图片
                    $('#tailoringImg').cropper('replace', replaceSrc, false);//默认false，适应高度，不失真
                    setTimeout(function () {
                        uncropper && ($(".preview-box-parcel").hide(), $(".cropper-container").hide(), $("#tailoringImg").removeClass("cropper-hidden").css({ "max-width": "100%", "max-height": "100%" }));//取消切图展示原图
                    }, 5)
                }
                reader.readAsDataURL(file.files[0]);
            },

            //关闭裁剪框
            closeTailor: function () {
                $(".tailoring-container").toggle();
            },
            //回调
            callback: function (name, fullUrl) {
                var callback = parent.window[GrdAssist.queryString("callback")];
                if (callback != null && $.isFunction(callback)) {
                    callback(name, fullUrl);
                }
            }
        },
        //工具
        tools: {

        }
    };
    adminHomeUserImgLogic.init();
});
