﻿layui.use(['form', 'layer', 'jquery'], function () {
    var $ = layui.jquery,
        form = layui.form,
        player = parent.layer || layui.layer;

    var interfaceInfoPageLogic = {

        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                swaggerUrl: "/index.html",
            };

            this.initPage();

            this.bindEvent();
        },
        //初始化列表
        initPage: function () {

            $('#interface_info_page').attr('src', GrdAdmin.webURI + self.Parameters.swaggerUrl);
            var userInfo = GrdAdmin.getUserInfo();
            var token= JSON.parse(userInfo.token).auth_token
            $(".apiKey").val("Bearer  " + token);

        },
        //绑定事件
        bindEvent: function () {

        },
        //逻辑方法
        logicFunc: {

        },
        //工具
        tools: {

        }
    };
    interfaceInfoPageLogic.init();
});
