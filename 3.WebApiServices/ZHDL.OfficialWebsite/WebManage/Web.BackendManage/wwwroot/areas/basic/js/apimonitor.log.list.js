﻿layui.use(['form', 'layer', 'table','laypage', 'laypage', 'jquery'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table,
        laydate = layui.laydate,
        player = parent.layer || layui.layer;

    var authorityApiMonitorLogListLogic = {

        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),    //异步方法管理
                field: 'sort',
                order: 'asc',
                listsUrl: "/api/Basic/ApiMonitorLog/LoadList",

                removeUrl: "/api/Basic/ApiMonitorLog/Remove",

            };

            this.initPage();
            this.bindEvent();
        },
        //初始化列表
        initPage: function () {
            var startTime = laydate.render({
                elem: '#startTime',
                done: function (value, date, endDate) {
                    
                }
            });
            var start1Time = laydate.render({
                elem: '#start1Time',
                done: function (value, date, endDate) {
                    endTime.config.min = {
                        year: date.year,
                        month: date.month - 1,
                        date: date.date
                    };
                }
            });

            var endTime = laydate.render({
                elem: '#endTime',
                done: function (value, date, endDate) {
                    start1Time.config.max = {
                        year: date.year != undefined ? date.year : 2099,
                        month: (date.month != undefined) ? (date.month - 1) : 12,
                        date: (date.date != undefined) ? date.date : 25

                    };
                }
            });

            var title =
                [{ type: 'checkbox', fixed: 'left' },
                { field: 'id', title: 'id', hide: true },
                    { field: 'controller_name', title: '控制器名称', width: 180, align: "left" },
                    { field: 'action_name', title: '方法名称', width: 200, align: "left" },
                    { field: 'request_path', title: '请求路由', width: 340, align: "left" },
                    { field: 'request_ip', title: 'IP地址', width: 150, align: "center" },
                    { field: 'http_method', title: '请求方式', width: 70 },
                    { field: 'start_time', title: '请求开始时间', width: 180 }
                ];

            //table加载
            GrdAdmin.tableInit({
                dom: '#apiMonitorLogList',
                url: self.Parameters.listsUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: {
                    sKeyWords: $("#txtKeyWord").val(),
                    exception_time: $("#startTime").val(),
                },
                title: title,
                onRadio: function (obj) {
                    self.Parameters.rowData = obj;
                }
            });

            //排序监听
            GrdAdmin.tableSort({
                domId: "apiMonitorLogList",
                backAction: function (obj) {
                    self.Parameters.field = obj.field;
                    self.Parameters.order = obj.type;
                    self.logicFunc.loadListInfo();
                }
            });
        },
        //绑定事件
        bindEvent: function () {

            //查询
            $("#funBtnSearch").on("click", function () {
                self.logicFunc.loadListInfo();
            });

           

            //删除
            $("#funBtnRemove").on("click", function () {
                self.logicFunc.removeInfo();
            });

          
        },
        //逻辑方法
        logicFunc: {

            ///获取数据
            loadListInfo: function () {
                GrdAdmin.tableReload({
                    domId: "apiMonitorLogList",
                    field: self.Parameters.field,
                    order: self.Parameters.order,
                    parameters: {
                        sKeyWords: $("#txtKeyWord").val(),
                        exception_time: $("#startTime").val(),
                    }

                });
            },


            //删除信息
            removeInfo: function (data) {

                var sTime = $("#start1Time").val();
                var eTime = $("#endTime").val();

                if (sTime == "") { GrdAdmin.msg("请选择开始时间！", "警告"); return; }
                if (eTime == "") { GrdAdmin.msg("请选择截止时间！", "警告"); return; }

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.removeUrl,
                    data: { startTime: sTime, endTime: eTime },
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            }
        },
        //工具
        tools: {

        }
    };
    authorityApiMonitorLogListLogic.init();
});
