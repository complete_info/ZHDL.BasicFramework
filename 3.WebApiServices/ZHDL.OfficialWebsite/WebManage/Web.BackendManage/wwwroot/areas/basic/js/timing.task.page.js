﻿layui.use(['form', 'layer', 'jquery'], function () {
    var $ = layui.jquery,
        form = layui.form,
        player = parent.layer || layui.layer;

    var timingTaskPageLogic = {

        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                hangfireUrl: "/hangfire/",
            };

            this.initPage();

            this.bindEvent();

        },
        //初始化列表
        initPage: function () {

            $('#timming_task_page').attr('src', GrdAdmin.webURI + self.Parameters.hangfireUrl);

        },
        //绑定事件
        bindEvent: function () {

        },
        //逻辑方法
        logicFunc: {

        },
        //工具
        tools: {

        }
    };
    timingTaskPageLogic.init();
});
