﻿layui.use(['form', 'layedit', 'table', 'layer', 'laydate', 'jquery'], function () {

    var form = layui.form,
        table = layui.table,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer;

    var basicMessageModifyLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                modifyInfoUrl: "/api/Basic/Message/Modify"
            };

            this.initPage();

            this.bindEvent();
        },
        //初始化列表
        initPage: function () {

            //加载信息
            self.logicFunc.loadListInfo();

        },
        //绑定事件
        bindEvent: function () {

            //监听提交
            form.on('submit(modifyButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);

        },
        //逻辑方法
        logicFunc: {

            //获取数据
            loadListInfo: function () {
                //获取信息
                var data = GrdAdmin.getUrlParameters();
                GrdAdmin.assignmentData("modify-form", data);
            },
            ///保存数据
            saveInfo: function () {
                var json = GrdAdmin.initParamsData($("#modify-form"));
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.modifyInfoUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {

                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    basicMessageModifyLogic.init();
});
