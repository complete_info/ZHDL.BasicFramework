﻿layui.use(['form', 'layer', 'jquery'], function () {
    var $ = layui.jquery,
        form = layui.form,
        player = parent.layer || layui.layer;

    var saftPageLogic = {

        //初始化
        init: function () {
            self = this;
            this.Parameters = {
                loadRequestInfoUrl: "/api/Basic/ApiMonitorLog/loadRequestInfo",
                loadRequestInfoByTimeUrl: "/api/Basic/ApiMonitorLog/loadRequestInfoByTime",
            };

            this.initPage();

            this.bindEvent();

        },
        //初始化列表
        initPage: function () {

            //接口请求实时信息
            self.logicFunc.loadInterfaceRequestInfo();

        },
        //绑定事件
        bindEvent: function () {

        },
        //逻辑方法
        logicFunc: {

            //接口请求实时信息
            loadInterfaceRequestInfo: function () {
                // 基于准备好的dom，初始化echarts实例
                var myChart = echarts.init(document.getElementById('main'), 'light');
                // 指定图表的配置项和数据
                var option = {
                    title: {
                        text: '走势图'
                    },
                    tooltip: {},
                    xAxis: {
                        type: 'category',
                        boundaryGap: false,
                        data: []
                    },
                    yAxis: {
                        type: 'value'
                    },
                    series: [{
                        type: 'line',
                        label: {
                            show: true
                        },
                        data: [],
                        lineStyle: {
                            color: {
                                type: 'linear',
                                x: 0,
                                y: 0,
                                x2: 0,
                                y2: 1,
                                colorStops: [{
                                    offset: 0, color: 'red' // 0% 处的颜色
                                }, {
                                    offset: 1, color: 'blue' // 100% 处的颜色
                                }],
                                global: false // 缺省为 false
                            }
                        }
                    }]
                };
                var newDate = GrdAssist.currentDate();
                var json = {
                    beginTime: GrdAssist.formatDate(new Date(newDate).setSeconds(-60), "yyyy-MM-dd HH:mm:ss"),
                    endTime: GrdAssist.formatDate(newDate, "yyyy-MM-dd HH:mm:ss")
                };
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.loadRequestInfoByTimeUrl,
                    type: GrdAdmin.ajaxGet,
                    data: json ,
                    success: function (result) {
                        if (result.Code == 1000) {

                            option.xAxis.data = result.Data.xAxis;
                            option.series[0].data = result.Data.yAxis;

                            //使用刚指定的配置项和数据显示图表。
                            myChart.setOption(option);
                        }
                    }
                });

                GrdAdmin.ajaxLongPolling({
                    url: self.Parameters.loadRequestInfoUrl,
                    success: function (result) {
                        if (result.Code == 1000) {
                            option.xAxis.data = result.Data.xAxis;
                            option.series[0].data = result.Data.yAxis;

                            //使用刚指定的配置项和数据显示图表。
                            myChart.setOption(option);

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            }
        },
        //工具
        tools: {

        }
    };
    saftPageLogic.init();
});
