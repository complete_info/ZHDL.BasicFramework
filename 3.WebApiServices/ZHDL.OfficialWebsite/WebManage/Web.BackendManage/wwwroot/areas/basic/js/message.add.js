﻿layui.use(['form', 'layedit', 'table', 'layer', 'laydate', 'jquery'], function () {

    var form = layui.form,
        table = layui.table,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer;

    var basicMessageSendLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                ids: [],
                listUserUrl: "/api/Authority/UserInfo/LoadListExceptMyself",//根据条件分页查询除自己以外的用户数据
                loadIdListUrl: "/api/Authority/UserInfo/LoadIdListExceptMyself",//根据条件查询除自己以外的用户id数据
                sendInfoUrl: "/api/Basic/Message/SendMessage"
            };

            this.initPage();

            this.bindEvent();
        },
        //初始化列表
        initPage: function () {

            //限制左侧树高度
            self.logicFunc.sendHeight();

            //获取用户信息
            var title =
                [{ type: 'checkbox' },
                { field: 'id', title: 'id', hide: true },
                { field: 'name', title: '登录名', width: 120, align: "center", sort: true },
                { field: 'displayname', title: '系统显示名', width: 120, align: "center" },
                { field: 'mobile_phone', title: '手机号码', align: "center" }
                ];

            //table加载
            GrdAdmin.tableInit({
                dom: '#userList',
                url: self.Parameters.listUserUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: {
                    sKeyWords: $("#txtKeyWord").val(),
                    postIds: "",
                    departmentId: ""
                },
                title: title
            });

            //获取接收者用户ID
            self.logicFunc.loadIdList();

            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //监听提交
            form.on('submit(sendButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);

            //查询
            $("#funBtnSearch").on("click", function () {
                self.logicFunc.loadListInfo();
                self.logicFunc.loadIdList();
            });

            ///根据网页变化高度或宽度，调整树高度
            window.onresize = function () {
                self.logicFunc.sendHeight();
            }
        },
        //逻辑方法
        logicFunc: {

            //发送内容高度
            sendHeight: function () {
                var clientHeight = document.documentElement.clientHeight * 0.645;
                $(".content").css({ "height": clientHeight });
            },

            //获取数据
            loadListInfo: function () {
                GrdAdmin.tableReload({
                    domId: "userList",
                    field: self.Parameters.field,
                    order: self.Parameters.order,
                    parameters: {
                        sKeyWords: $("#txtKeyWord").val(),
                        postIds: "",
                        departmentId: ""
                    }
                });
            },
            //获取id信息
            loadIdList: function () {
                var json = {
                    sKeyWords: $("#txtKeyWord").val(),
                    postIds: "",
                    departmentId: ""
                };
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.loadIdListUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            self.Parameters.ids = result.Data;

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },

            ///保存数据
            saveInfo: function () {
                var json = GrdAdmin.initParamsData($("#send-form"));
                switch (parseInt(json.receiver_type)) {
                    case 100://100：所有用户  101：列表用户 102：选中用户
                        self.Parameters.ids = [];
                        break;
                    case 102:
                        self.Parameters.ids = [];
                        var checkData = GrdAdmin.getCheckData({ domId: "userList" });
                        if (checkData!=null&&checkData.length > 0) {
                            
                            $.each(checkData, function (n, value) {
                                self.Parameters.ids.push(value.id);
                            });
                        }
                        break;
                    default:
                }
                if (json.receiver_type == 102 && self.Parameters.ids.length == 0) {
                    GrdAdmin.msg("请选择消息接收者！", "警告");
                    return;
                }
                json.user_ids = self.Parameters.ids;

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.sendInfoUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {

                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    basicMessageSendLogic.init();
});
