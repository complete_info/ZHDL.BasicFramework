﻿layui.config({
    base: '/lib/layui/extend/'
}).extend({
    excel: 'excel'
}).use(['form', 'layer', 'table', 'laypage', 'jquery', 'laytpl'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table,
        player = parent.layer || layui.layer,
        layer = layui.layer,
        laytpl = layui.laytpl;

    var basicMessageListLogic = {
        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),    //异步方法管理
                field: 'create_date',
                order: 'desc',

                listsUrl: "/api/Basic/Message/LoadList",
                removeUrl: "/api/Basic/Message/Remove",
                readMessageUrl: "/api/Basic/Message/ReadMessage",


                sendMsgUrl: "/Basic/Message/SendMessage",
                modifyUrl: "/Basic/Message/Modify",

            };

            this.initPage();

            this.bindEvent();

        },
        //初始化列表
        initPage: function () {

            var title =
                [{ type: 'checkbox' },
                { field: 'id', title: 'id', hide: true },
                {
                    field: 'Rnumber', title: '编号', width: 60, align: "center",
                    templet: function (d) {
                        return d.LAY_INDEX;
                    }
                },
                { field: 'status', title: '状态', width: 110, templet: '#switchStatusTpl', align: "center" },
                { field: 'sender_name', title: '发送者', width: 120, align: "center" },
                { field: 'receiver_name', title: '接收者', width: 120, align: "center" },
                { field: 'msg_type', title: '类型', width: 120, templet: '#switchTypeTpl', align: "center" },
                { field: 'title', title: '标题', width: 130, align: "center" },
                { field: 'content', title: '内容', width: 280, align: "center" },
                { field: 'opt', title: '操作', toolbar: '#gridTool', width: '220', align: 'left' }
                ];

            //table加载
            GrdAdmin.tableInit({
                dom: '#messageList',
                url: self.Parameters.listsUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: {
                    keyWord: $("#txtKeyWord").val(),
                    msg_type: $("#msg_type").val()
                },
                title: title
            });

            //排序监听
            GrdAdmin.tableSort({
                domId: "messageList",
                backAction: function (obj) {
                    self.Parameters.field = obj.field;
                    self.Parameters.order = obj.type;
                    self.logicFunc.loadListInfo();
                }
            });

            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //查询
            $("#funBtnSearch").on("click", function () {
                self.logicFunc.loadListInfo();
            });

            //发送
            $("#funBtnSend").on("click", function () {
                self.logicFunc.sendInfo();
            });

            //删除
            $("#funBtnRemove").on("click", function () {
                if (!$(this).hasClass("layui-btn-disabled")) {
                    self.logicFunc.removeInfo();
                }
            });

            //监听(列表操作)
            table.on('tool(messageList)', function (obj) {
                var data = obj.data;
                switch (obj.event) {
                    case "funBtnEdit"://修改
                        self.logicFunc.modifyInfo(data);
                        break;
                    case "funBtnRead"://查看
                        self.logicFunc.readInfo(data);
                        break;
                    case "funBtnRemove"://删除
                        self.logicFunc.removeInfo(data);
                        break;
                }
            });

            //复选框点击事件
            table.on('checkbox(messageList)', function (obj) {
                self.logicFunc.showHideBtn();
            });
        },
        //逻辑方法
        logicFunc: {

            ///获取数据
            loadListInfo: function () {
                GrdAdmin.tableReload({
                    domId: "messageList",
                    field: self.Parameters.field,
                    order: self.Parameters.order,
                    parameters: {
                        keyWord: $("#txtKeyWord").val(),
                        msg_type: $("#msg_type").val()
                    }
                });
            },

            //发送信息
            sendInfo: function () {
                var index = layui.layer.open({
                    title: "发送信息",
                    type: 2,
                    resize: false,
                    area: ['800px', '560px'],
                    content: self.Parameters.sendMsgUrl,
                    end: function () {
                        $(window).unbind("resize");
                    }
                });
            },

            //修改信息
            modifyInfo: function (data) {

                GrdAdmin.setUrlParameters(data);

                var index = layui.layer.open({
                    title: "修改信息",
                    type: 2,
                    resize: false,
                    area: ['500px', '420px'],
                    content: self.Parameters.modifyUrl,
                    end: function () {
                        $(window).unbind("resize");
                    }
                });
            },

            //查看信息
            readInfo: function (data) {

                if (data != undefined && data != null) {
                    if (data.is_receiver) {
                        GrdAdmin.ajaxRequest({
                            url: self.Parameters.readMessageUrl,
                            type: GrdAdmin.ajaxGet,
                            data: { id: data.id },
                            success: function (result) {
                                if (result.Code == 1000) {
                                    var index = layer.open({
                                        title: "消息详情",
                                        resize: true,
                                        area: ['500px', '380px'],
                                        content: laytpl($('#massage_detail').html()).render({ data:result.Data }),
                                        btn: ['关闭'],
                                        yes: function (index, layero) {
                                            layer.close(index); //如果设定了yes回调，需进行手工关闭
                                        },
                                        end: function () {
                                            $(window).unbind("resize");
                                        }
                                    });

                                } else {
                                    GrdAdmin.msg(result.Msg, "警告");
                                }
                            }
                        });
                    } else {

                        var index = layui.layer.open({
                            title: "消息详情",
                            resize: false,
                            tipsMore: true,
                            area: ['500px', '380px'],
                            content: laytpl($('#massage_detail').html()).render({ data: data }),
                            btn: ['关闭'],
                            yes: function (index, layero) {
                                layer.close(index); //如果设定了yes回调，需进行手工关闭
                            },
                            end: function () {
                                $(window).unbind("resize");
                            }
                        });

                    }
                }
            },

            //删除信息
            removeInfo: function (data) {

                //单选触发
                if (data != undefined && data != null) {

                    GrdAdmin.removeDataByInfo({
                        data: data,
                        removeUrl: self.Parameters.removeUrl,
                        backAction: function (data) {
                            self.logicFunc.loadListInfo();
                        }
                    });

                } else {
                    //多选触发
                    GrdAdmin.removeDataByDomId({
                        domId: "messageList",
                        removeUrl: self.Parameters.removeUrl,
                        backAction: function (data) {
                            self.logicFunc.loadListInfo();
                        }
                    });
                }
            },

            //显示或隐藏删除按钮
            showHideBtn: function () {

                var checkStatus = GrdAdmin.laytable.checkStatus("messageList");
                var data = checkStatus.data;
                if (data.length == 0) {
                    $("#funBtnRemove").addClass("layui-btn-danger");
                    $("#funBtnRemove").removeClass("layui-btn-disabled");
                } else {
                    var hasSendUser = false;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].is_sender == true) {
                            hasSendUser = true;
                            break;
                        }
                    }
                    if (hasSendUser) {
                        $("#funBtnRemove").removeClass("layui-btn-danger");
                        $("#funBtnRemove").addClass("layui-btn-disabled");
                    } else {
                        $("#funBtnRemove").addClass("layui-btn-danger");
                        $("#funBtnRemove").removeClass("layui-btn-disabled");
                    }
                }
            },
        },
        //工具
        tools: {

        }
    };
    basicMessageListLogic.init();
});
