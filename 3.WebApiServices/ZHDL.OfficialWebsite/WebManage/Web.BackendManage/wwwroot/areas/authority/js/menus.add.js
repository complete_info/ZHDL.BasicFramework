﻿layui.use(['form', 'layedit', 'layer', 'laydate', 'jquery'], function () {

    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer;

    var authorityMenusAddLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),   //异步方法管理
                queryData: undefined,    //页面传递参数
                systemTypeListUrl: "/api/Authority/FunctionCfg/LoadSystemTypeList",
                functionTypeListUrl: "/api/Authority/FunctionCfg/LoadFunctionTypeList",

                addInfoUrl: "/api/Authority/FunctionCfg/AddInfo"
            };

            this.Parameters.ac
                .pushQueue(function () {
                    self.logicFunc.loaSystemTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                })
                .pushQueue(function () {
                    self.logicFunc.loadFunctionTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                })
                .pushQueue(function () {
                    self.initPage();
                    self.Parameters.ac.notifyQueueAsyncFuncComplate();
                })
                .pushQueue(this.bindEvent)
                .exec();
        },
        //初始化列表
        initPage: function () {

            //获取信息
            self.Parameters.queryData = GrdAdmin.getUrlParameters();
            
            if (self.Parameters.queryData) {
                self.logicFunc.loadInfo();
            }

            //更新全部
            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //监听提交
            form.on('submit(addButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);
        },
        //逻辑方法
        logicFunc: {

            //获取系统类型列表信息
            loaSystemTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.systemTypeListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            $("#system_type").empty();
                            for (var i = 0; i < result.Data.length; i++) {
                                if (result.Data[i].Value != "") {
                                    $("#system_type").append("<input type='checkbox'  name='" + result.Data[i].Value + "' title=" + result.Data[i].Text + ">");
                                }
                            }
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },

            //获取功能类型列表信息
            loadFunctionTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.functionTypeListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            $("#function_type").empty();
                            for (var i = 0; i < result.Data.length; i++) {
                                if (result.Data[i].Value != "") {
                                    $("#function_type").append("<input type='radio' name='function_type' value='" + result.Data[i].Value + "' title=" + result.Data[i].Text + ">");
                                }
                            }
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },

            //获取修改的信息
            loadInfo: function () {
                $("#parent_id").val(self.Parameters.queryData.parent_id);
            },

            ///保存数据
            saveInfo: function () {

                var json = GrdAdmin.initParamsData($("#add-form"));
                var systemTypes = [];
                var systemTypeNames = [];
                $("#system_type").find("input").each(function () {
                    if (this.checked) {
                        systemTypes.push(this.name);
                        systemTypeNames.push(this.title);
                    }
                });
                if (systemTypes.length == 0) {
                    GrdAdmin.msg("请选择菜单所属系统！", "警告");
                    return;
                } else {
                    json.system_type = systemTypes.join(',');
                    json.system_type_name = systemTypeNames.join(',');

                    GrdAdmin.ajaxRequest({
                        url: self.Parameters.addInfoUrl,
                        data: json,
                        success: function (result) {
                            if (result.Code == 1000) {
                                GrdAdmin.msg(result.Msg, "成功", function () {
                                    //获取窗口索引
                                    var index = player.getFrameIndex(window.name);
                                    //关闭子页面
                                    player.close(index);
                                    //刷新父页面
                                    window.parent.location.reload();
                                });

                            } else {
                                GrdAdmin.msg(result.Msg, "警告");
                            }
                        }
                    });
                }
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    authorityMenusAddLogic.init();
});
