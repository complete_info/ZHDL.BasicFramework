﻿layui.config({
    base: '/lib/layui/extend/'
}).extend({
    formSelects: 'formSelects-v4'
}).use(['form', 'layedit', 'layer', 'table', 'tree', 'jquery', 'formSelects'], function () {
    var form = layui.form, layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer
        tree = layui.tree,
        formSelects = layui.formSelects; //很重要;

    var authorityUserModifyLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                departmentAllListUrl: "/api/Authority/Department/LoadAllSelectList",
                sysRoleAllListUrl: "/api/Authority/RoleInfo/LoadAllSelectList",
                sysPostAllListUrl: "/api/Authority/Post/LoadAllSelectList",
                saveUrl: "/api/Authority/UserInfo/Modify",

                uploadImgUrl: "/Admin/Home/UserImg",//用户上传头像界面
                isdefaultImg:true
               
            };

            this.initPage();

            this.bindEvent();

        },
        //初始化列表
        initPage: function () {

            //获取信息
            var data = GrdAdmin.getUrlParameters();
            self.logicFunc.loadInfo(data);

            form.render();
        },
        //绑定事件
        bindEvent: function () {
            //上传图片
            $(".userFaceBtn").on("click", this.logicFunc.uploadImg);

            //监听立即修改提交
            form.on('submit(modifyButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);
        },
        //逻辑方法
        logicFunc: {

            //获取修改的信息
            loadInfo: function (data) {

                $("#userFace").attr("src", GrdAdmin.webURI+data.picture_url); //头像
                $("input[name=id]").val(data.id); //id
                $("input[name=name]").val(data.name); //账号
                $("input[name=realname]").val(data.realname); //名称
                $("input[name=mobile_phone]").val(data.mobile_phone); //手机号码
                $("input[name=idnumber]").val(data.idnumber); //身份证号
                $("input[name=gender][value=" + data.gender + "]").attr("checked", true);//性别
                $("input[name=telphone]").val(data.telphone); //家庭电话
                $("input[name=office_phone]").val(data.office_phone); //办公电话
                $("input[name=email]").val(data.email); //邮箱地址
                $("input[name=displayname]").val(data.displayname); //显示名称
                $("input[name=qq]").val(data.qq); //qq
                $("input[name=nickname]").val(data.nickname); //用户昵称
                $("textarea[name=user_describe]").val(data.user_describe); //描述

                //所属部门
                var sDepartmentList = new Array();
                if (data.department_ids != null && data.department_ids != "") {
                    sDepartmentList = data.department_ids.split(",");
                }
                self.logicFunc.loadDepartmentAllList(sDepartmentList);


                //所属角色
                var sRoleList = new Array();
                if (data.role_ids != null && data.role_ids != "") {
                    sRoleList = data.role_ids.split(",");
                }
                self.logicFunc.loadSysRoleAllList(sRoleList);

                //所属岗位
                var sPostList = new Array();
                if (data.post_ids != null && data.post_ids != "") {
                    sPostList = data.post_ids.split(",");
                }
                self.logicFunc.loadSysPostAllList(sPostList);
            },

            //获取部门列表信息
            loadDepartmentAllList: function (dataInfo) {
                
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.departmentAllListUrl,
                    data: "",
                    success: function (result) {
                        if (result.Code == 1000) {
                            formSelects.data('departmentList', 'local', {
                                arr: result.Data
                            });
                            if (dataInfo.length>0) {
                                formSelects.value('departmentList', dataInfo);
                            }
                        }
                    }
                });
            },

            //获取所有角色
            loadSysRoleAllList: function (dataInfo) {
                
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.sysRoleAllListUrl,
                    data: "",
                    success: function (result) {
                        if (result.Code == 1000) {
                            //local模式
                            formSelects.data('roleList', 'local', {
                                arr: result.Data
                            });
                            if (dataInfo.length > 0) {
                                formSelects.value('roleList', dataInfo);
                            }
                        }
                    }
                });
            },

            //获取所有的岗位
            loadSysPostAllList: function (dataInfo) {
                
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.sysPostAllListUrl,
                    data: "",
                    success: function (result) {
                        if (result.Code == 1000) {
                            //local模式
                            formSelects.data('postList', 'local', {
                                arr: result.Data
                            });
                            if (dataInfo.length > 0) {
                                formSelects.value('postList', dataInfo);
                            }
                        }
                    }
                });
            },

            //上传图片
            uploadImg: function (obj) {

                var callback_name = "loadUploadImgPage_callback_" + new Date().getTime();
                window[callback_name] = function (name, fullUrl) {
                    $(".userFaceBtn").attr({ src: GrdAdmin.webURI + fullUrl + "?v=" + Math.random(), "data-savesrc": GrdAdmin.webURI + fullUrl });
                    //$(".userFaceBtn").attr({ src: fullUrl + "?v=" + Math.random(), "data-savesrc": fullUrl });

                    self.Parameters.isdefaultImg = false;
                };

                var headerPic = "";
                if ($(".userFaceBtn").prop("src").indexOf("?") > -1) {
                    headerPic = $(".userFaceBtn").prop("src").substring($(".userFaceBtn").prop("src").lastIndexOf("/") + 1, $(".userFaceBtn").prop("src").indexOf("?"));
                } else {
                    headerPic = $(".userFaceBtn").prop("src").substring($(".userFaceBtn").prop("src").lastIndexOf("/") + 1);
                }

                var index = layui.layer.open({
                    title: "图片上传",
                    type: 2,
                    resize: false,
                    area: ['820px', '650px'],
                    content: self.Parameters.uploadImgUrl + '?' + $.param({
                        originalPic: headerPic,
                        uploadFolder: "UserImage",
                        callback: callback_name,
                        width: 350,
                        height: 350
                    }),
                    end: function () {
                        delete window[callback_name];
                    }
                });
            },

            //保存数据
            saveInfo: function () {
                var json = GrdAdmin.initParamsData($("#modify-form"));

                if (json.picture_url.indexOf(GrdAdmin.webURI) > -1) {
                    json.picture_url = json.picture_url.replace(GrdAdmin.webURI, "");
                }
                if (self.Parameters.isdefaultImg) {
                    json.picture_url = json.picture_url.replace("/images/", "/src/");
                }
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.saveUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {

                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {

        }
    };
    authorityUserModifyLogic.init();
});
