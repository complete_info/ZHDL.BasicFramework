﻿layui.use(['form', 'layedit', 'layer'], function () {

    var form = layui.form, layer = parent.layer === undefined ? layui.layer : top.layer, $ = layui.jquery, player = parent.layer || layui.layer;

    var authorityMenusModifyLogic = {
        //初始化
        init: function () {
            self = this;
            this.Parameters = {
                ac: new AsyncCaller(),   //异步方法管理

                systemTypeListUrl: "/api/Authority/FunctionCfg/LoadSystemTypeList",
                functionTypeListUrl: "/api/Authority/FunctionCfg/LoadFunctionTypeList",
                saveUrl: "/api/Authority/FunctionCfg/ModifyInfo"
            };
            this.Parameters.ac
                .pushQueue(function () {
                    self.logicFunc.loaSystemTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                })
                .pushQueue(function () {
                    self.logicFunc.loadFunctionTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                })
                .pushQueue(function () {
                    self.initPage();
                    self.Parameters.ac.notifyQueueAsyncFuncComplate();
                })
                .pushQueue(this.bindEvent)
                .exec();

        },
        //初始化列表
        initPage: function () {

            //获取信息
            var data = GrdAdmin.getUrlParameters();
            self.logicFunc.loadInfo(data);

            //更新全部
            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //监听立即修改提交
            form.on('submit(modifyButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);

        },
        //逻辑方法
        logicFunc: {

            //获取系统类型列表信息
            loaSystemTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.systemTypeListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            $("#system_type").empty();
                            for (var i = 0; i < result.Data.length; i++) {
                                if (result.Data[i].Value != "") {
                                    $("#system_type").append("<input type='checkbox' name='system_type" + i +"'  opt-name='" + result.Data[i].Value + "' title=" + result.Data[i].Text + ">");
                                }
                            }
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },
            //获取功能类型列表信息
            loadFunctionTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.functionTypeListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            $("#function_type").empty();
                            for (var i = 0; i < result.Data.length; i++) {
                                if (result.Data[i].Value != "") {
                                    $("#function_type").append("<input type='radio' name='function_type'  value='" + result.Data[i].Value + "' title=" + result.Data[i].Text + ">");
                                }
                            }
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },

            //获取修改按钮的信息
            loadInfo: function (data) {

                GrdAdmin.assignmentData("modify-form", data);

                var systemTypes = data.system_type.split(",");
                $("#system_type").find("input").each(function () {
                    for (var i = 0; i < systemTypes.length; i++) {
                        if (systemTypes[i] == $(this).attr("opt-name")) {
                            this.checked = true;
                        }
                    }
                });

            },
            ///保存数据
            saveInfo: function () {

                var json = GrdAdmin.initParamsData($("#modify-form"));
                var systemTypes = [];
                var systemTypeNames = [];
                $("#system_type").find("input").each(function () {
                    if (this.checked) {
                        systemTypes.push($(this).attr("opt-name"));
                        systemTypeNames.push(this.title);
                    }
                });
                if (systemTypes.length == 0) {
                    GrdLayUI.msg("请选择菜单所属系统！", "警告");
                    return;
                } else {

                    json.system_type = systemTypes.join(',');
                    json.system_type_name = systemTypeNames.join(',');

                    GrdAdmin.ajaxRequest({
                        url: self.Parameters.saveUrl,
                        data: json,
                        success: function (result) {
                            if (result.Code == 1000) {
                                GrdAdmin.msg(result.Msg, "成功", function () {
                                    //获取窗口索引
                                    var index = player.getFrameIndex(window.name);
                                    //关闭子页面
                                    player.close(index);
                                    //刷新父页面
                                    window.parent.location.reload();
                                });

                            } else {
                                GrdAdmin.msg(result.Msg, "警告");
                            }
                        }
                    });
                }
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    authorityMenusModifyLogic.init();
});
