layui.use(['form', 'jquery'], function () {
    var form = layui.form, $ = layui.jquery, player = parent.layer || layui.layer;

    var authorityUserPasswordLogic = {
        //初始化
        init: function () {
            self = this;

            ///全局参数
            this.Parameters = {
                saveChangePwdUrl: "/Authority/User/ModifyPswInfo"
            };

            this.initPage();

            this.bindEvent();
        },
        //初始化列表
        initPage: function () {

            //获取信息
            var data = GrdAssist.queryString("data");
            self.logicFunc.loadInfo(data);

            //添加验证规则
            form.verify({
                newPwd: function (value, item) {
                    if (value.length < 6) {
                        return "密码长度不能小于6位";
                    }
                },
                confirmPwd: function (value, item) {
                    if (!new RegExp($("#oldPwd").val()).test(value)) {
                        return "两次输入密码不一致，请重新输入！";
                    }
                }
            })
        },
        //绑定事件
        bindEvent: function () {

            //提交修改信息
            form.on('submit(changePwd)', function (data) {
                self.logicFunc.SaveChangePwd();
            });

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);
        },
        //事件
        logicFunc: {

            //获取修改的信息
            loadInfo: function (data) {
                $("input[name='Code']").val(data);
            },

            //保存信息
            SaveChangePwd: function () {
                var json = {
                    parameters: GrdLayUI.initParamsData($("#edit-form"))
                };
                GrdLayUI.ajaxPost({
                    url: self.Parameters.saveChangePwdUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            //GrdLayUI.msg('修改成功', "成功");
                            GrdLayUI.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });
                        } else {
                            GrdLayUI.msg(result.Msg, "警告");
                        }
                    }
                });
            },

            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {

        }
    };
    authorityUserPasswordLogic.init();
});
