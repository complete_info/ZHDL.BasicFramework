﻿layui.config({
    base: '/lib/layui/extend/'
}).extend({
    formSelects: 'formSelects-v4'
}).use(['form', 'layedit', 'layer', 'table', 'tree', 'jquery', 'formSelects'], function () {
    var form = layui.form, layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer
    tree = layui.tree,
        formSelects = layui.formSelects; //很重要;
    var authorityUserAddLogic = {
        //初始化
        init: function () {
            self = this;

            this.Parameters = {

                departmentAllListUrl: "/api/Authority/Department/LoadAllSelectList",
                sysRoleAllListUrl: "/api/Authority/RoleInfo/LoadAllSelectList",
                sysPostAllListUrl: "/api/Authority/Post/LoadAllSelectList",
                addUrl: "/api/Authority/UserInfo/AddUser",

                uploadImgUrl: "/Admin/Home/UserImg",//用户上传头像界面
                isdefaultImg: true
            };

            this.initPage();
            this.bindEvent();

        },
        //初始化列表
        initPage: function () {

            //所属部门
            this.logicFunc.loadDepartmentAllList();

            //获取所有的角色
            this.logicFunc.loadSysRoleAllList();

            //获取所有的岗位
            this.logicFunc.loadSysPostAllList();

            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //上传图片
            $(".userFaceBtn").on("click", this.logicFunc.uploadImg);

            //监听提交
            form.on('submit(addButton)', this.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", this.logicFunc.close);
        },
        //逻辑方法
        logicFunc: {

            //获取部门列表信息
            loadDepartmentAllList: function () {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.departmentAllListUrl,
                    data: "",
                    success: function (result) {
                        if (result.Code == 1000) {
                            formSelects.data('departmentList', 'local', {
                                arr: result.Data
                            });
                        }
                    }
                });
            },

            //获取所有角色
            loadSysRoleAllList: function () {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.sysRoleAllListUrl,
                    data: "",
                    success: function (result) {
                        if (result.Code == 1000) {
                            //local模式
                            formSelects.data('roleList', 'local', {
                                arr: result.Data
                            });
                        } 
                    }
                });
            },

            //获取所有的岗位
            loadSysPostAllList: function () {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.sysPostAllListUrl,
                    data: "",
                    success: function (result) {
                        if (result.Code == 1000) {
                            //local模式
                            formSelects.data('postList', 'local', {
                                arr: result.Data
                            });
                        }
                    }
                });
            },

            //上传图片
            uploadImg: function (obj) {

                var callback_name = "loadUploadImgPage_callback_" + new Date().getTime();

                window[callback_name] = function (name, fullUrl) {

                    $(".userFaceBtn").attr({ src: GrdAdmin.webURI + fullUrl + "?v=" + Math.random(), "data-savesrc": GrdAdmin.webURI + fullUrl });
                    self.Parameters.isdefaultImg = false;
                };

                var headerPic = "";
                if ($(".userFaceBtn").prop("src").indexOf("?") > -1) {
                    headerPic = $(".userFaceBtn").prop("src").substring($(".userFaceBtn").prop("src").lastIndexOf("/") + 1, $(".userFaceBtn").prop("src").indexOf("?"));
                } else {
                    headerPic = $(".userFaceBtn").prop("src").substring($(".userFaceBtn").prop("src").lastIndexOf("/") + 1);
                }

                var index = layui.layer.open({
                    title: "图片上传",
                    type: 2,
                    resize: false,
                    area: ['820px', '650px'],
                    content: self.Parameters.uploadImgUrl + '?' + $.param({
                        originalPic: headerPic,
                        uploadFolder: "UserImage",
                        callback: callback_name,
                        width: 350,
                        height: 350
                    }),
                    end: function () {
                        delete window[callback_name];
                    }
                });
            },

            //保存数据
            saveInfo: function () {

                var json =  GrdAdmin.initParamsData($("#add-form"));
                if (json.password != json.repassword) {

                    GrdAdmin.msg("两次密码输入不一致！", "警告");
                    return;
                }
                if (json.picture_url.indexOf(GrdAdmin.webURI) > -1) {
                    json.picture_url = json.picture_url.replace(GrdAdmin.webURI, "");
                }
                if (self.Parameters.isdefaultImg) {
                    json.picture_url = json.picture_url.replace("/images/", "/src/");
                }
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.addUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {

                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    authorityUserAddLogic.init();
});
