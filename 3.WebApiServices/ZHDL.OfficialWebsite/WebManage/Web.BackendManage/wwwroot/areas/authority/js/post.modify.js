﻿layui.use(['form', 'layedit', 'layer'], function () {

    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer;

    var authorityPostModifyLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                saveUrl: "/api/Authority/Post/Modify"
            };

            this.initPage();

            this.bindEvent();

        },
        //初始化列表
        initPage: function () {
            //获取信息
            var data = GrdAdmin.getUrlParameters();
            self.logicFunc.loadInfo(data);

            //更新全部
            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //监听立即修改提交
            form.on('submit(modifyButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);

        },
        //逻辑方法
        logicFunc: {
            //获取修改按钮的信息
            loadInfo: function (data) {
                GrdAdmin.assignmentData("modify-form", data);
            },
            //保存数据
            saveInfo: function () {
                var json = GrdAdmin.initParamsData($("#modify-form"));
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.saveUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    authorityPostModifyLogic.init();
});
