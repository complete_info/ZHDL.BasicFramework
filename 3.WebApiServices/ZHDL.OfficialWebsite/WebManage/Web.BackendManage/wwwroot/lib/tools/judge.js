﻿/**
 * 用于判断操作
 */
var GrdJudge = {
    /*********************************JudgeFunc*******************************************************
    /**
     * 判断是否不为Defined，切不为Null
     */
    isNotEmpty: function (value) {
        return typeof value !== 'undefined' && value !== null && value !== "";
    },

    /**
     * 判断是否是数字类型
     */
    isNumber: function (value) {
        return typeof value === 'number';
    },

    /**
     * 判断是否是日期
     */
    isDate: function (value) {
        return toString.call(value) === '[object Date]';
    },

    /**
     * 判断是否是数组
     */
    isArray: function (value) {
        try {
            return Array.isArray(value);
        } catch (e) {
            return false;
        }
    },

    /**
     *判断是否是非空数组 
     */
    isHasValuesArray: function (value) {
        try {
            if (Array.isArray(value)) {
                return value.length > 0;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    },
    /**
     * 插入pushState
     */
    pushStateToHistroy: function (state) {
        try {
            if (history.pushState && 'pushState' in history) {
                document.title = state.title;
                window.history.pushState(state, state.title, state.url);
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    },

    /**
     * 判断是否是Decimal
     */
    isDecimal: function (num) {
        try {
            if (typeof num === 'number') {
                var numString = num.toString();
                if (numString.indexOf('.') > 0) {
                    return parseInt(numString.substring(numString.indexOf(".") + 1)) > 0;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    },

    /**
     * 判断是否是字符串
     */
    isString: function (val) {
        return typeof val === 'string';
    },

    /**
     * 判断是否是Function
     */
    isFunction: function (val) {
        return typeof val === 'function';
    },

    /**
     * 判断是否是文件
     */
    isFile: function (obj) {
        return toString.call(obj) === '[object File]';
    },

    /**
     * 判断是否是MobilePhone
     */
    isMobilePhone: function (val) {
        var patrn = /^(1\d{10})*$/;
        if (patrn.exec(val) && val != "")
            return true;
        return false;
    },
    /**
     * 判断是否是Phone
     */
    isPhone: function (val) {
        var patrn = /^((13[0-9]|14[0-9]|15[0-9]|17[0-9]|18[0-9])\d{8})*$/;
        if (patrn.exec(val) && val != "")
            return true;
        var patrn = /^(\d{3}-\d{8}|\d{3}-\d{7}|\d{4}-\d{7})*$/;
        if (patrn.exec(val) && val != "")
            return true;
        return false;
    },
    /**
     * 判断是否是有效PassWord
     */
    isPassWord: function (val) {
        var patrn = /[0-9 | A-Z | a-z]{6,18}/;
        if (patrn.exec(val) && val != "")
            return true;
        return false;
    },


    /**
     * 判断是否是网址
     */
    isWebAddress: function (val) {
        var strRegex = /(http(s)?:\/\/|^$)([\w-]+\.)+[\w-]+(\/[\w- .\/?%&=]*)?/;
        var patrn = new RegExp(strRegex);
        if (patrn.exec(val))
            return true;
        return false;
    }
};