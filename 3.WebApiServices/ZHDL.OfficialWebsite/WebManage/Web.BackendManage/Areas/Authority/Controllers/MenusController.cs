﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Authority.Controllers
{
    [Area("Authority")]
    public class MenusController : Controller
    { 
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }


        /// <summary>
        /// 增加界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 修改界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Modify()
        {
            return View();
        }

    }
}