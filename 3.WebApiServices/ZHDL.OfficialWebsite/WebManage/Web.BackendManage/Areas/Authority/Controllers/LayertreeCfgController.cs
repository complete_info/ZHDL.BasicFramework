﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Authority.Controllers
{
    [Area("Authority")]
    public class LayertreeCfgController : Controller
    {
        /// <summary>
        /// 图层列表
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 新增图层
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }
        /// <summary>
        /// 修改图层
        /// </summary>
        /// <returns></returns>
        public IActionResult Modify()
        {
            return View();
        }
        
    }
}