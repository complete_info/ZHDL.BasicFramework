﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Authority.Controllers
{
    [Area("Authority")]
    public class RoleController : Controller
    {
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 增加界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 修改界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Modify()
        {
            return View();
        }


        /// <summary>
        /// 授权菜单界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Menu()
        {
            return View();
        }

        /// <summary>
        /// 授权图层界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Layer()
        {
            return View();
        }
    }
}