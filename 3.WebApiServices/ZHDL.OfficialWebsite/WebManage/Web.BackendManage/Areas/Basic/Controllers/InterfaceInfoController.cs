﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Basic.Controllers
{
    /// <summary>
    /// 接口说明页面
    /// </summary>

    [Area("Basic")]
    public class InterfaceInfoController : Controller
    {
        /// <summary>
        /// 页面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}