﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Basic.Controllers
{
    /// <summary>
    /// 消息管理
    /// </summary>
    [Area("Basic")]
    public class MessageController : Controller
    {
        /// <summary>
        /// 列表页面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 发送信息
        /// </summary>
        /// <returns></returns>
        public IActionResult SendMessage()
        {
            return View();
        }


        /// <summary>
        /// 修改信息
        /// </summary>
        /// <returns></returns>
        public IActionResult Modify()
        {
            return View();
        }
    }
}