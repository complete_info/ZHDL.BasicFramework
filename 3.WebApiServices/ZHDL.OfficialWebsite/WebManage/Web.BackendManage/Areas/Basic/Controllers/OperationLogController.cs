﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Basic.Controllers
{
    /// <summary>
    /// 日志管理
    /// </summary>
    [Area("Basic")]
    public class OperationLogController : Controller
    {
        /// <summary>
        /// 日志列表
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}