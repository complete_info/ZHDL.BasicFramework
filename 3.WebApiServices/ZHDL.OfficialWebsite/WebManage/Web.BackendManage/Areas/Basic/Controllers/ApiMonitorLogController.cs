﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Basic.Controllers
{
    [Area("Basic")]
    public class ApiMonitorLogController : Controller
    {
        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}