﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class ErrorController : Controller
    {
        /// <summary>
        /// {0}中是错误码
        /// </summary>
        /// <returns></returns>
        [Route("/Admin/Error/Page")]
        public ActionResult Page()
        {
            //跳转到404错误页
            if (Response.StatusCode == 404)
            {
                return View("block404");
            }
            if (Response.StatusCode == 500)
            {
                return View("block500");
            }
            ViewBag.StatusCode = Response.StatusCode;
            return View();
        }

        /// <summary>
        /// 404界面
        /// </summary>
        /// <returns></returns>
        public ActionResult block404()
        {
            return View();
        }

        /// <summary>
        /// 500界面
        /// </summary>
        /// <returns></returns>
        public ActionResult block500()
        {
            return View();
        }
    }
}