﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Client.Controllers
{
    /// <summary>
    /// 主页控制器
    /// </summary>
    [Area("Client")]
    public class HomeController : Controller
    {
        /// <summary>
        /// 客户端主页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}