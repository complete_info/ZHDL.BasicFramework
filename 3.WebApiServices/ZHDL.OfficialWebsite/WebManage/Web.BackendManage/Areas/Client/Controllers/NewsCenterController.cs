﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Client.Controllers
{
    /// <summary>
    /// 新闻中心控制器
    /// </summary>
    [Area("Client")]
    public class NewsCenterController : Controller
    {
        /// <summary>
        /// 新闻中心界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}