﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Client.Controllers
{
    /// <summary>
    /// 关于我们控制器
    /// </summary>
    [Area("Client")]
    public class AboutUsController : Controller
    {
        /// <summary>
        /// 关于我们界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}