﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Client.Controllers
{
    /// <summary>
    /// 地图大数据界面控制器
    /// </summary>
    [Area("Client")]
    public class HugeDataController : Controller
    {
        /// <summary>
        /// 地图大数据界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}