﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Client.Controllers
{
    /// <summary>
    /// 项目案例控制器
    /// </summary>
    [Area("Client")]
    public class ProjectCaseController : Controller
    {
        /// <summary>
        /// 项目案例界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}