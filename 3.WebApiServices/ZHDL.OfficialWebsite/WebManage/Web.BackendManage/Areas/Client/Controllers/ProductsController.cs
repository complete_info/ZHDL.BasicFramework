﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Client.Controllers
{
    /// <summary>
    /// 产品与服务控制器
    /// </summary>
    [Area("Client")]
    public class ProductsController : Controller
    {
        /// <summary>
        /// 产品与服务界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

    }
}