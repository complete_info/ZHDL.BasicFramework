﻿using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Client.Controllers
{
    /// <summary>
    /// 联系我们控制器
    /// </summary>
    [Area("Client")]
    public class ContactUsController : Controller
    {
        /// <summary>
        /// 联系我们
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}