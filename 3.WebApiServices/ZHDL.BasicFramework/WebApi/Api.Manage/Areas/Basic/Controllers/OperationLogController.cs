﻿using Api.RiversGovernance.App_Start;
using Basic.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
/*
* 命名空间: Api.RiversGovernance.Areas.Basic.Controllers
*
* 功 能： 操作日志控制器
*
* 类 名： OperationLogController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 李聪     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Api.RiversGovernance.Areas.Basic.Controllers
{
    /// <summary>
    ///操作日志控制器 
    /// </summary>
    [ApiExplorerSettings(GroupName = "Basic")]
    [Route("api/Basic/[controller]")]
    [ApiController]
    public class OperationLogController : ControllerBase
    {
        private readonly IOperationLogService operationLogService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public OperationLogController() {

            operationLogService= UnityCIContainer.Instance.GetService<IOperationLogService>(); 
        }

        #region 操作日志管理相关逻辑
        /// <summary>
        /// 根据日志条件分页获取列表 关键字【业务标题】【操作用户名称】【操作明细】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<OperationLogInfoResponse>> LoadList([FromBody]ParametersInfo<OperationLogQueryRequest> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<OperationLogInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = operationLogService.LoadOperationList(inputInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "日志管理-获取日志分页列表失败");
            }, $"系统错误，日志管理-获取日志分页列表失败");
            return resultInfo;
        }
        /// <summary>
        /// 获取日志类型Select列表信息
        /// </summary>
        /// <returns></returns>

        [HttpGet("LoadLogTypeList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadLogTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            Try.CatchLog(() =>
            {
                resultInfo = operationLogService.LoadLogTypeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "日志管理-获取日志类型Select列表信息失败");

            }, $"系统错误，日志管理-获取日志类型Select列表信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 批量删除日志
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Remove([FromBody]PoerationLogRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = operationLogService.Remove(request);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "日志管理-删除日志失败");

            }, $"系统错误，日志管理-删除日志失败");

            return resultInfo;
        }
        #endregion

        #region 主页操作日志相关逻辑
        /// <summary>
        /// 获取对应时间段内，操作日志数量
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("LoadOperationCount")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<OperationCountResponse> LoadOperationCount(DateTime beginTime, DateTime endTime)
        {

            var resultInfo = new ResultJsonInfo<OperationCountResponse>();

            Try.CatchLog(() =>
            {
                resultInfo = operationLogService.LoadOperationCount(beginTime, endTime);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "日志管理-获取对应时间段内，操作日志数量失败");

            }, $"系统错误，日志管理-获取对应时间段内，操作日志数量失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取最新操作信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadLatestList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<OperationLogInfoResponse>> LoadLatestList()
        {
            var resultInfo = new ResultJsonInfo<List<OperationLogInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = operationLogService.LoadLatestList();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "日志管理-获取最新操作信息失败");
            }, $"系统错误，日志管理-获取最新操作信息失败");
            return resultInfo;
        }
        #endregion

    }
}