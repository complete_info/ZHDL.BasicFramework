﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Api.RiversGovernance.App_Start;
using Basic.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Api.Manage.Areas.Basic.Controllers
{
    /// <summary>
    /// 接口访问监控日志
    /// </summary>
    [ApiExplorerSettings(GroupName = "Basic")]
    [Route("api/Basic/[controller]")]
    [ApiController]
    public class ApiMonitorLogController : ControllerBase
    {

        private readonly IApiMonitorLogService apiMonitorLogService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ApiMonitorLogController()
        {
            apiMonitorLogService = UnityCIContainer.Instance.GetService<IApiMonitorLogService>();
        }

        #region 接口访问监控日志明细操作
        /// <summary>
        /// 根据访问监控日志条件分页获取列表 关键字【控制器名称】【方法名】  开始时间
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<ApiMonitorLogResponse>> LoadExceptionList([FromBody]ParametersInfo<ApiMonitorLogQueryRequest> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ApiMonitorLogResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = apiMonitorLogService.LoadApiMonitorLogList(inputInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取监控日志条件分页列表失败");
            }, $"系统错误，获取监控日志条件分页列表失败");
            return resultInfo;
        }

        /// <summary>
        /// 批量删除监控日志
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Remove([FromBody]ApiMonitorLogRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = apiMonitorLogService.Remove(request);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除监控日志失败");

            }, $"系统错误，日志管理-删除监控日志失败");

            return resultInfo;
        }
        #endregion

        #region 接口访问记录统计信息



        /// <summary>
        /// 获取接口实时访问统计信息
        /// </summary>
        /// <param name="timed"></param>
        /// <returns></returns>
        [HttpGet("loadRequestInfo")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<Echarts2DCoordinatesInfo> loadRequestInfo(DateTime timed)
        {
            var resultInfo = new ResultJsonInfo<Echarts2DCoordinatesInfo>();

            Try.CatchLog(() =>
            {
                resultInfo = apiMonitorLogService.loadRequestInfo(timed);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取接口实时访问统计信息失败");
            }, $"系统错误，获取接口实时访问统计信息失败");
            return resultInfo;
        }


        /// <summary>
        /// 获取某个时间段内的接口实时访问统计信息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("loadRequestInfoByTime")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<Echarts2DCoordinatesInfo> loadRequestInfoByTime(DateTime beginTime, DateTime endTime)
        {
            var resultInfo = new ResultJsonInfo<Echarts2DCoordinatesInfo>();

            Try.CatchLog(() =>
            {
                resultInfo = apiMonitorLogService.loadRequestInfoByTime(beginTime, endTime);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取某个时间段内的接口实时访问统计信息失败");

            }, $"系统错误，获取某个时间段内的接口实时访问统计信息失败");
            return resultInfo;
        }

        #endregion
    }
}