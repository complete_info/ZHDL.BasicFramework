﻿using System;
using System.Collections.Generic;
using Api.RiversGovernance.App_Start;
using Basic.Logic;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.RiversGovernance.Areas.Basic.Controllers
{

    /// <summary>
    ///异常日志控制器 
    /// </summary>
    [ApiExplorerSettings(GroupName = "Basic")]
    [Route("api/Basic/[controller]")]
    [ApiController]
    public class ExceptionLogController : ControllerBase
    {
        private readonly IExceptionLogService exceptionLogService = null;
        /// <summary>
        /// 构造函数
        /// </summary>
        public ExceptionLogController()
        {
            exceptionLogService = UnityCIContainer.Instance.GetService<IExceptionLogService>();
        }

        #region 异常日志管理相关逻辑

        /// <summary>
        /// 根据异常日志条件分页获取列表 关键字【业务标题】【操作明细】
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<ExceptionLogInfoResponse>> LoadList([FromBody]ParametersInfo<ExceptionLogQueryRequest> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<ExceptionLogInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = exceptionLogService.LoadExceptionList(inputInfo);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取异常日志分页列表失败");
            }, $"系统错误，获取异常日志分页列表失败");
            return resultInfo;
        }
        /// <summary>
        /// 获取异常日志类型Select列表信息
        /// </summary>
        /// <returns></returns>

        [HttpGet("LoadLogTypeList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<EnumToSelectItem>> LoadLogTypeList()
        {
            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();

            Try.CatchLog(() =>
            {
                resultInfo = exceptionLogService.LoadLogTypeList();

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取异常日志类型Select列表信息失败");

            }, $"系统错误，日志管理-获取异常日志类型Select列表信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 批量删除日志
        /// </summary>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Remove([FromBody]ExceptionLogRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = exceptionLogService.Remove(request);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "删除异常日志失败");

            }, $"系统错误，日志管理-删除异常日志失败");

            return resultInfo;
        }


        /// <summary>
        /// 解决异常问题
        /// </summary>
        /// <returns></returns>
        [HttpGet("SolveException")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> SolveException(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = exceptionLogService.SolveException(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "解决异常问题失败");

            }, $"系统错误，日志管理-解决异常问题失败");

            return resultInfo;
        }
        
        #endregion

        #region 异常日志统计相关逻辑
        /// <summary>
        /// 获取对应时间段内，异常信息数量
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        [HttpGet("LoadExceptionCount")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<ExceptionCountResponse> LoadExceptionCount(DateTime beginTime, DateTime endTime)
        {

            var resultInfo = new ResultJsonInfo<ExceptionCountResponse>();

            Try.CatchLog(() =>
            {
                resultInfo = exceptionLogService.LoadExceptionCount(beginTime, endTime);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取对应时间段内，异常信息数量失败");

            }, $"系统错误，获取对应时间段内，异常信息数量失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取最新错误信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadLatestList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<ExceptionLogInfoResponse>> LoadLatestList()
        {
            var resultInfo = new ResultJsonInfo<List<ExceptionLogInfoResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = exceptionLogService.LoadLatestList();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取最新错误信息失败");
            }, $"系统错误，获取最新错误信息失败");
            return resultInfo;
        }
        #endregion
    }
}