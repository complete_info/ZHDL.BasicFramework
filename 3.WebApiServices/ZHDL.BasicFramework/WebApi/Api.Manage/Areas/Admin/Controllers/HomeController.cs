﻿using System.Collections.Generic;
using Api.RiversGovernance.App_Start;
using Authority.Logic;
using Authority.Model;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
/*
* 命名空间: Api.RiversGovernance.Areas.Authority.Controllers
*
* 功 能： 运维系统主页面接口控制器
*
* 类 名： HomeController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Api.RiversGovernance.Areas.Admin.Controllers
{
    /// <summary>
    /// 系统主页面接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Home")]
    [Route("api/[controller]")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly ISysUserService userService = null;
        private readonly IFunctionCfgService functionCfgService = null;

        /// <summary>
        /// 构造函数
        /// </summary>
        public HomeController()
        {
            //用户管理逻辑注入
            userService = UnityCIContainer.Instance.GetService<ISysUserService>();
            //功能配置管理逻辑注入
            functionCfgService = UnityCIContainer.Instance.GetService<IFunctionCfgService>();
        }

        #region 主界面菜单逻辑方法
        /// <summary>
        /// 获取用户对应系统的可操作的所有菜单信息
        /// </summary>
        /// <param name="systemType">系统类型:运维系统:100 桌面应用系统:200 PC客户端应用:300</param>
        /// <returns></returns>
        [HttpGet("LoadMenuList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<MenuInfo>> LoadMenuList(string systemType)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();
            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadMenuOfUserList(systemType);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取用户对应系统的可操作的所有菜单信息失败");

            }, $"系统错误，系统主页面管理-获取用户对应系统的可操作的所有菜单信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取用户对应系统的可操作的一级菜单信息
        /// </summary>
        /// <param name="systemType">系统类型:运维系统:100 桌面应用系统:200 PC客户端应用:300 </param>
        /// <returns></returns>
        [HttpGet("LoadTopMenuList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<SysFunctionCfgEntity>> LoadTopMenuList(string systemType)
        {
            var resultInfo = new ResultJsonInfo<List<SysFunctionCfgEntity>>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadTopMenuOfUserList(systemType,10);
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取用户对应系统的可操作的一级菜单信息失败");

            }, $"系统错误，系统主页面管理-获取用户对应系统的可操作的一级菜单信息失败");
            return resultInfo;
        }

        /// <summary>
        /// 获取左侧菜单信息
        /// </summary>
        /// <param name="systemType">系统类型:运维系统:100 桌面应用系统:200 PC客户端应用:300</param>
        /// <param name="parentId">父节点ID【就是当前节点】</param>
        /// <returns></returns>
        [HttpGet("LoadSubMenus")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<List<MenuInfo>> LoadSubMenus(string systemType,string parentId)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();

            Try.CatchLog(() =>
            {
                resultInfo = functionCfgService.LoadSubMenus(systemType,parentId);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取左侧菜单信息失败");

            }, $"系统错误，系统主页面管理-获取左侧菜单信息失败");
            return resultInfo;
        }
        #endregion
    }
}