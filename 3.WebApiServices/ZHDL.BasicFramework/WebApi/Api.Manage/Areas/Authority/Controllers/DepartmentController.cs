﻿using Api.RiversGovernance.App_Start;
using Authority.Logic;
using Authority.Model;
using Common.Library;
using Common.Model;
using Container.Library;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Network.Library;
using System.Collections.Generic;
using Validate.Library;
/*
* 命名空间: Api.RiversGovernance.Areas.Authority.Controllers
*
* 功 能： 岗位接口控制器
*
* 类 名： PostController
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Api.RiversGovernance.Areas.Authority.Controllers
{
    /// <summary>
    /// 部门接口控制器
    /// </summary>
    [ApiExplorerSettings(GroupName = "Authority")]
    [Route("api/Authority/[controller]")]
    [ApiController]
    public class DepartmentController : ControllerBase
    {
        ISysDepartmentService departmentService = null;
        /// <summary>
        /// 部门接口控制器构造函数
        /// </summary>
        public DepartmentController()
        {
            //部门管理逻辑注入
            departmentService = UnityCIContainer.Instance.GetService<ISysDepartmentService>();
        }

        #region 部门基础信息管理模块

        #region 信息查询

        /// <summary>
        /// 根据关键字【部门名称，领导人名称，联系电话】获取所有的未删除菜单信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在，返回 1205
        /// </remarks>
        /// <param name="parameters">关键字【部门名称，领导人名称，联系电话】</param>
        /// <returns></returns>
        [HttpPost("LoadPageList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<DepartmentResponse>> LoadPageList([FromBody]ParametersInfo<string> parameters)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentResponse>>();

            Try.CatchLog(() => {

                resultInfo = departmentService.LoadPageList( parameters);

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "部门管理-根据关键字获取所有的未删除菜单信息失败");

            }, $"系统错误，部门管理-根据关键字获取所有的未删除菜单信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 根据部门ID，获取所有可操作用户情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        [HttpPost("LoadListInfoByDeparId")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<List<DepartmentUserResponse>> LoadListInfoByDeparId([FromBody]DepartmentUserQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<DepartmentUserResponse>>();

            Try.CatchLog(() =>
            {
                resultInfo = departmentService.LoadListInfoByDeparId(queryInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门管理- 根据部门ID，获取所有可操作用户情况信息失败");

            }, $"系统错误，部门管理- 根据部门ID，获取所有可操作用户情况信息失败");

            return resultInfo;
        }

        #endregion

        #region 更新操作

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、操作失败、无对应父节点信息，返回 1205
        /// </remarks>
        /// <param name="addInfo">子节点信息</param>
        /// <returns></returns>
        [HttpPost("Addnode")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Addnode([FromBody]DepartmentChildAddRequest addInfo )
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                addInfo.Validate();
                resultInfo = departmentService.Addnode(addInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门管理-新增根节点信息失败");

            }, $"系统错误，部门管理-新增根节点信息失败");

            return resultInfo;
        }

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、无对应值存在、操作失败，返回 1205
        /// </remarks>
        /// <returns></returns>
        [HttpPost("ModifyInfo")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> ModifyInfo([FromBody]DepartmentModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();
            Try.CatchLog(() =>
            {
                modifyInfo.Validate();
                resultInfo = departmentService.Modify(modifyInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门管理-修改节点信息信息失败");

            }, $"系统错误，部门管理-修改节点信息信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 禁用/启用部门
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// 2、操作失败、无对应信息，返回 1205，无效操作
        /// </remarks>
        /// <returns></returns>
        [HttpGet("ForbidOrEnable")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> ForbidOrEnable(string id)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = departmentService.ForbidOrEnable(id);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "禁用/启用部门失败");

            }, $"系统错误，禁用/启用部门失败");
            return resultInfo;
        }



        /// <summary>
        /// 修改对应部门的用户
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        [HttpPost("ModifyDeparUserInfo")]
        [UserAuthorization(true)]
        [Authorize]
        public ResultJsonInfo<int> ModifyDeparUserInfo([FromBody]DepartmentUserModifyRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = departmentService.ModifyDeparUserInfo(department);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门部门-修改对应部门的用户信息失败");

            }, $"系统错误，部门管理-修改对应部门的用户信息失败");

            return resultInfo;
        }

        #endregion

        #region 删除信息

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="removeInfo"></param>
        /// <returns></returns>
        [HttpPost("Remove")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Remove(List<string> removeInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                resultInfo = departmentService.Remove(removeInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门管理-删除信息操作失败");

            }, $"系统错误，部门管理-删除信息操作失败");

            return resultInfo;
        }

        #endregion
        #region 移动顺序操作
        /// <summary>
        /// 移动顺序操作（1：上移/2：下移）
        /// </summary>
        /// <remarks>
        ///返回Code情况：
        /// 1、操作成功 返回1000
        /// </remarks>
        /// <returns></returns>
        [HttpPost("Move")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<int> Move([FromBody]DepartmentMoveRequest moveInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            Try.CatchLog(() =>
            {
                moveInfo.Validate();
                resultInfo = departmentService.Move(moveInfo);

            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "部门管理-移动顺序操作失败");

            }, $"系统错误，部门管理-移动顺序操作失败");

            return resultInfo;
        }
        #endregion
        #endregion


        #region 权限相关模块

        /// <summary>
        /// 获取所有启用组织机构树状信息
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllTreeList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<TreeInfo>> LoadAllTreeList()
        {
            var resultInfo = new ResultJsonInfo<List<TreeInfo>>();

            Try.CatchLog(() => {

                resultInfo = departmentService.LoadAllTreeList();

            }, ex => {

                resultInfo.SystemExc(resultInfo, ex, "部门管理-获取所有启用组织机构树状信息失败");

            }, $"系统错误，部门管理-获取所有启用组织机构树状信息失败");

            return resultInfo;
        }


        /// <summary>
        /// 获取所有部门Select树状数据
        /// </summary>
        /// <returns></returns>
        [HttpPost("LoadAllSelectList")]
        [Authorize]
        [UserAuthorization(true)]
        public ResultJsonInfo<List<SelectListInfo>> LoadAllSelectList()
        {
            var resultInfo = new ResultJsonInfo<List<SelectListInfo>>();
            Try.CatchLog(() =>
            {
                resultInfo = departmentService.LoadSysPostTreeList();
            }, ex =>
            {
                resultInfo.SystemExc(resultInfo, ex, "获取所有部门Select树状数据失败！");

            }, $"系统错误，获取所有部门Select树状数据失败！");
            return resultInfo;
        }
        #endregion
    }
}