﻿using Hangfire.Annotations;
using Hangfire.Dashboard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/*
* 命名空间: Api.Manage.App_Start.Filter
*
* 功 能： 
*
* 类 名： CustomAuthorizeFilter
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/15 10:52:14 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Api.Manage.App_Start.Filter
{
    /// <summary>
    /// 专门用于hangfire
    /// </summary>
    public class CustomAuthorizeFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            //var httpcontext = context.GetHttpContext();
            //return httpcontext.User.Identity.IsAuthenticated;
            return true;
        }
    }
}
