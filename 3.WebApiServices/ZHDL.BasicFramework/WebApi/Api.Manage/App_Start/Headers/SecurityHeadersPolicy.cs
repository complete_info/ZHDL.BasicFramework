﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

/*
* 命名空间: Api.Manage.App_Start.Headers
*
* 功 能： 
*
* 类 名： SecurityHeadersPolicy
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/15 10:41:02 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Api.Manage.App_Start.Headers
{
    /// <summary>
    /// 响应头的增删集合
    /// </summary>
    public class SecurityHeadersPolicy
    {
        public IDictionary<string, string> SetHeaders { get; }
             = new Dictionary<string, string>();

        public ISet<string> RemoveHeaders { get; }
            = new HashSet<string>();
    }

}
