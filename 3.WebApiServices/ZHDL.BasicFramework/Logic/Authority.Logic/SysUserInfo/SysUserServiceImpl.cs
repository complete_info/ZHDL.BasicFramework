﻿using Common.Model;
using Document.Library;
using Encrypt.Library;
using Network.Library;
using Authority.Model;
using Common.Library;
using Container.Library;
using Dapper.Library;
using Dapper.Library.DataAccessSql;
using Dapper.PostgreSql.Library;
using System;
using System.Collections.Generic;
using Serialize.Library;
using System.Linq;
using System.Linq.Expressions;

/*
* 命名空间: Authority.Logic
*
* 功 能： 岗位功能管理逻辑
*
* 类 名： SysPostServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 李聪     创建
* V1.0.2    2020/03/18 11:12:00 罗维     增加用户登录操作和主界面用户操作
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Authority.Logic
{
    /// <summary>
    /// 管理员操作逻辑类
    /// </summary>
    public class SysUserServiceImpl : OperationLogicImpl, ISysUserService
    {
        #region 用户登录操作和主界面用户操作

        /// <summary>
        /// 获取验证码
        /// </summary>
        /// <returns></returns>
        public ResultFileInfo LoadValidateCode(string uniqueIdentifier)
        {
            var resultInfo = new ResultFileInfo();
            string code = ValidateCode.CreateValidateCode(4);
            string md5 = SecurityHelper.GetMD5Hash(uniqueIdentifier + HttpHelper.GetIP());

            SysUserServiceRedis.SaveValidateCode(md5, code, () =>
             {
                 byte[] bytes = ValidateCode.CreateValidateGraphic(code);
                 if (bytes.Length > 0)
                 {
                     resultInfo.Code = ActionCodes.Success;
                     resultInfo.FileContents = bytes;
                     resultInfo.ContentType = @"image/jpeg";
                 }
             },
            () =>
            {
                resultInfo.Code = ActionCodes.CacheFailure;
            });
            return resultInfo;
        }

        /// <summary>
        /// 用户登录
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> UserLogin(UserInfoLoginRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();
            //redis获取验证码
            string md5 = SecurityHelper.GetMD5Hash(inputInfo.uniqueIdentifier + HttpHelper.GetIP());

            SysUserServiceRedis.GetValidateCode(md5, (ValidateCode) =>
            {
                if (ValidateCode == inputInfo.vialdcode)
                {
                    using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
                    {
                        inputInfo.password = SecurityHelper.GetMD5Hash(inputInfo.password);

                        //先查询是否有该账号存在
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.name == inputInfo.name).ToList();

                        if (info != null && info.Count > 0)
                        {
                            //再核对密码是否正确
                            var result = info.Find(p => p.name == inputInfo.name && p.password == inputInfo.password);
                            if (result != null)
                            {
                                if (result.is_valid)
                                {
                                    var ticket = result.MapTo<UserLoginInfo>();

                                    //判断是否是超级管理员
                                    //ticket.is_super_admin = JudgeIsSuperAdmin(result.name);

                                    string refreshToken = Guid.NewGuid().ToString();
                                    SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                                    {
                                        ticket.token = refreshToken;
                                        resultInfo.Code = ActionCodes.Success;
                                        resultInfo.Msg = "操作成功";
                                        resultInfo.Data = ticket;

                                        AddOperationLog(OperationLogType.LoginOperation, BusinessTitleType.UserManage, $"登录成功！");
                                    },
                                    () =>
                                    {
                                        resultInfo.Msg = "缓存用户信息失败！";
                                        resultInfo.Code = ActionCodes.CacheFailure;
                                    });

                                }
                                else
                                {
                                    resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                                    resultInfo.Code = ActionCodes.InvalidOperation;
                                }
                            }
                            else
                            {
                                resultInfo.Msg = "密码错误！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "账号不存在！";
                            resultInfo.Code = ActionCodes.ObjectNotFound;
                        }
                    }
                }
                else
                {
                    resultInfo.Msg = "验证码不正确！";
                }
            }, () =>
            {
                resultInfo.Msg = "验证码失效，请重新获取！";
                resultInfo.Code = ActionCodes.InfoExpiration;
            });
            return resultInfo;
        }

        /// <summary>
        /// 通过RefreshToken获取用户信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> LoadUserInfoByRefreshToken(UserInfoRefreshTokenRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            SysUserServiceRedis.GetUserInfo(inputInfo.refreshToken, (userInfo) =>
            {
                var userLoginInfo = JsonHelper.JsonToObject<UserLoginInfo>(userInfo);

                using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
                {
                    //先查询是否有该账号存在
                    var info = con.QuerySet<View_SysUserEntity>().Where(p => p.name == inputInfo.name && p.id == userLoginInfo.id).Get();

                    if (info != null)
                    {
                        if (info.is_valid)
                        {
                            var ticket = info.MapTo<UserLoginInfo>();

                            //判断是否是超级管理员
                            //ticket.is_super_admin = JudgeIsSuperAdmin(info.name);

                            string refreshToken = Guid.NewGuid().ToString();
                            SysUserServiceRedis.SaveUserInfo(refreshToken, ticket, () =>
                            {
                                ticket.token = refreshToken;
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "操作成功";
                                resultInfo.Data = ticket;
                            },
                            () =>
                            {
                                resultInfo.Msg = "缓存用户信息失败！";
                                resultInfo.Code = ActionCodes.CacheFailure;
                            });
                        }
                        else
                        {
                            resultInfo.Msg = "用户已被冻结，请与管理员联系！";
                            resultInfo.Code = ActionCodes.InvalidOperation;
                        }
                    }
                    else
                    {
                        resultInfo.Msg = "账户不存在！";
                        resultInfo.Code = ActionCodes.ObjectNotFound;
                    }
                }
            },
            () =>
            {
                resultInfo.Msg = "RefreshToken无效，请重新登录！";
                resultInfo.Code = ActionCodes.RefreshTokeTokenInvalid;
            });
            return resultInfo;
        }

        /// <summary>
        /// 查询单个用户的数据
        /// 【先查询Redis,如果Redis没有，再查询数据库，最后将信息放入到Redis】
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<UserLoginInfo> LoadSingleById(string userId)
        {
            var resultInfo = new ResultJsonInfo<UserLoginInfo>();

            SysUserServiceRedis.GetUserInfo(userId, (userInfo) =>
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = JsonHelper.JsonToObject<UserLoginInfo>(userInfo);
            },
            () =>
            {
                using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
                {
                    var result = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == userId).Get();
                    if (result == null)
                    {
                        resultInfo.Code = ActionCodes.ObjectNotFound;
                        resultInfo.Msg = "账户不存在！";
                    }
                    else
                    {
                        var userInfo = result.MapTo<UserLoginInfo>();

                        //判断是否是超级管理员
                        //userInfo.is_super_admin = JudgeIsSuperAdmin(userInfo.name);

                        SysUserServiceRedis.SaveUserInfo(userId, userInfo);

                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Data = userInfo;
                    }
                }
            });
            return resultInfo;
        }

        /// <summary>
        /// 修改用户自身的数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyOneself(UserInfoModifyOneselfRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == user.id).Get();
                if (userinfo != null)
                {
                    userinfo.displayname = request.displayname;
                    userinfo.email = request.email;
                    userinfo.realname = request.realname;
                    userinfo.qq = request.qq;
                    userinfo.mobile_phone = request.mobile_phone;
                    userinfo.telphone = request.telphone;
                    userinfo.office_phone = request.office_phone;
                    userinfo.idnumber = request.idnumber;
                    userinfo.picture_url = request.picture_url;
                    userinfo.gender = request.gender;
                    userinfo.nickname = request.nickname;
                    userinfo.remarks = request.remarks;
                    userinfo.user_describe = request.user_describe;
                    userinfo.modifier_id = user.id;
                    userinfo.modifier_date = DateTime.Now;

                    var result = con.CommandSet<SysUserInfoEntity>().Update(userinfo);
                    if (result > 0)
                    {

                        #region 修改存在redis中的信息
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.id == userinfo.id).Get();
                        if (info != null)
                        {
                            var ticket = info.MapTo<UserLoginInfo>();
                            SysUserServiceRedis.SaveUserInfo(ticket);
                        }
                        #endregion

                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "修改成功";
                        AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改用户自身的数据成功，修改用户数据：{JsonHelper.ToJson(userinfo)}");
                    }
                    else
                    {
                        resultInfo.Msg = "修改失败！";
                    }
                }
                else
                {
                    resultInfo.Msg = "参数无效！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改自己的密码
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ChangePassword(UserChangePasswordRequest request)
        {
            var user = GetLoginUserInfo();
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var userInfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == user.id).Get();
                if (userInfo == null)
                {
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                if (userInfo.password != SecurityHelper.GetMD5Hash(request.oldpassword))
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "旧密码错误！";
                    return resultInfo;
                }
                userInfo.password = SecurityHelper.GetMD5Hash(request.password);

                var result = con.CommandSet<SysUserInfoEntity>().Update(userInfo);
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "修改成功";

                    AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改自己的密码成功！");
                }
                else
                {
                    resultInfo.Msg = "修改失败";
                }
            }
            return resultInfo;
        }
        #endregion

        #region 用户基础信息管理操作

        #region 查询

        /// <summary>
        /// 根据条件分页查询用户数据
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> LoadList(ParametersInfo<UserInfoQueryRequest> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<View_SysUserEntity>();

                if (inputInfo.parameters.sKeyWords.IsNotNullOrEmpty())
                {
                    result.Where(a => a.name.Contains(inputInfo.parameters.sKeyWords) 
                    || a.displayname.Contains(inputInfo.parameters.sKeyWords) 
                    || a.mobile_phone.Contains(inputInfo.parameters.sKeyWords)
                    || a.post_names.Contains(inputInfo.parameters.sKeyWords)
                    || a.department_names.Contains(inputInfo.parameters.sKeyWords)
                    );
                }

                #region 表达式拼装 或OrElse

                if (inputInfo.parameters.postIds.IsNotNullOrEmpty())
                {
                    var postIds = inputInfo.parameters.postIds.Split(',');

                    ParameterExpression pa = Expression.Parameter(typeof(View_SysUserEntity), "user");
                    MyExpressionVisitor visitor = new MyExpressionVisitor(pa);//统一参数类型

                    Expression<Func<View_SysUserEntity, bool>> lambdaOne = user => user.post_ids.Contains(postIds[0]);
                    Expression lambdaOneBody = visitor.Visit(lambdaOne.Body);

                    foreach (var item in postIds)
                    {
                        Expression<Func<View_SysUserEntity, bool>> lambdaItem = user => user.post_ids.Contains(item);
                        Expression lambdaBody = visitor.Visit(lambdaItem.Body);

                        lambdaOneBody = Expression.OrElse(lambdaOneBody, lambdaBody);
                    }
                    Expression<Func<View_SysUserEntity, bool>> newEx = Expression.Lambda<Func<View_SysUserEntity, bool>>(lambdaOneBody, pa);
                    result.Where(newEx);
                }

                #endregion

                if (inputInfo.parameters.departmentId.IsNotNullOrEmpty())
                {
                    result.Where(a => a.department_ids.Contains(inputInfo.parameters.departmentId));
                }

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);

                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<UserInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 查询单个用户的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<UserInfoResponse> LoadSingle(string userId)
        {
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == userId).Get();
                if (result == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = result.MapTo<UserInfoResponse>();
            }

            return resultInfo;

        }

        /// <summary>
        /// 查询自己的数据
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<UserInfoResponse> LoadUserSingle()
        {
            var user = GetLoginUserInfo();
            var resultInfo = new ResultJsonInfo<UserInfoResponse>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == user.id).Get();
                if (result == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }

                resultInfo.Code = ActionCodes.Success;
                resultInfo.Msg = "获取成功";
                resultInfo.Data = result.MapTo<UserInfoResponse>();
            }

            return resultInfo;
        }
        #endregion

        #region 添加

        /// <summary>
        /// 新增用户
        /// </summary>
        /// <param name="userAdd"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> AddUser(UserInfoAddRequest userAdd)
        {
            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();

            //判断两次密码是否输入一致
            if (userAdd.password == userAdd.repassword)
            {
                using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
                {
                    //事务功能
                    con.Transaction(tran =>
                    {
                        //判断是否存在相同登录名
                        var userInfoList = tran.QuerySet<SysUserInfoEntity>().ToList();
                        var userInfo = userInfoList.Find(p => p.name == userAdd.name);
                        if (userInfo == null)
                        {
                            //插入数据
                            var sysUser = userAdd.MapTo<SysUserInfoEntity>();
                            sysUser.password = SecurityHelper.GetMD5Hash(userAdd.password);
                            sysUser.creator_id = user.id;
                            sysUser.creator_name = user.name;
                            sysUser.modifier_date = DateTime.Now;
                            sysUser.modifier_id = user.id;
                            sysUser.modifier_name = user.name;
                            sysUser.sort = userInfoList.Max(p => p.sort) + 1;


                            var result = tran.CommandSet<SysUserInfoEntity>().Insert(sysUser);

                            if (userAdd.departmentList.IsNotNullOrEmpty())
                            {
                                SysUserDepartmentReEntity departmentReEntity = null;
                                //配置部门
                                foreach (var item in userAdd.departmentList.Split(','))
                                {
                                    departmentReEntity = new SysUserDepartmentReEntity();
                                    departmentReEntity.id = GuidHelper.GetGuid();
                                    departmentReEntity.user_id = sysUser.id;
                                    departmentReEntity.department_id = item;
                                    tran.CommandSet<SysUserDepartmentReEntity>().Insert(departmentReEntity);
                                }
                            }

                            if (userAdd.postList.IsNotNullOrEmpty())
                            {
                                SysUserPostReEntity userPostReEntity = null;
                                //配置职位
                                foreach (var item in userAdd.postList.Split(','))
                                {
                                    userPostReEntity = new SysUserPostReEntity();
                                    userPostReEntity.id = GuidHelper.GetGuid();
                                    userPostReEntity.user_id = sysUser.id;
                                    userPostReEntity.post_id = item;
                                    tran.CommandSet<SysUserPostReEntity>().Insert(userPostReEntity);
                                }
                            }


                            if (result > 0)
                            {
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Msg = "添加成功！";
                                AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, $"添加用户成功,新增用户信息：{JsonHelper.ToJson(sysUser)}");
                            }
                            else
                            {
                                resultInfo.Msg = "添加失败！";
                            }
                        }
                        else
                        {

                        }
                    }, ex =>
                    {
                        throw ex;
                    });
                }
            }
            else
            {
                resultInfo.Msg = "两次输入密码不一致！";
            }
            return resultInfo;
        }

        /// <summary>
        /// 用户配置部门
        /// </summary>
        /// <param name="department"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> UserDepartment(UserInfoDepartmentRequest department)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var userInfo = con.QuerySet<SysUserInfoEntity>().Where(p => p.id == department.userId).Get();
                if (!userInfo.is_valid)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该用户被禁用";
                    return resultInfo;
                }
                var depInfo = con.QuerySet<SysDepartmentInfoEntity>().Where(p => p.id == department.departmentId).Get();
                if (depInfo == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该部门不存在";
                    return resultInfo;
                }
                //事务功能
                con.Transaction(tran =>
                {
                    //删除原来配置的数据
                    var delete = tran.CommandSet<SysUserDepartmentReEntity>().Where(a => a.user_id == userInfo.id).Delete();

                    SysUserDepartmentReEntity reEntity = new SysUserDepartmentReEntity
                    {
                        id = GuidHelper.GetGuid(),
                        user_id = department.userId,
                        department_id = department.departmentId
                    };
                    var result = tran.CommandSet<SysUserDepartmentReEntity>()
                        .Insert(reEntity);
                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "配置成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, "配置用户部门成功");
                    }
                    else
                    {
                        resultInfo.Msg = "配置失败！";
                    }
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 用户配置岗位
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> UserPost(UserInfoPositionRequset position)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var userInfo = con.QuerySet<SysUserInfoEntity>().Where(p => p.id == position.userId).Get();
                if (!userInfo.is_valid)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该用户被禁用";
                    return resultInfo;
                }
                var depInfo = con.QuerySet<SysPostInfoEntity>().Where(p => p.id == position.postId).Get();
                if (depInfo == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "该岗位不存在";
                    return resultInfo;
                }
                //事务功能
                con.Transaction(tran =>
                {
                    //删除原来配置的数据
                    var delete = tran.CommandSet<SysUserPostReEntity>().Where(a => a.user_id == userInfo.id).Delete();

                    SysUserPostReEntity reEntity = new SysUserPostReEntity
                    {
                        id = GuidHelper.GetGuid(),
                        user_id = position.userId,
                        post_id = position.postId
                    };
                    var result = tran.CommandSet<SysUserPostReEntity>()
                        .Insert(reEntity);
                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "配置成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.UserManage, "配置用户岗位成功");
                    }
                    else
                    {
                        resultInfo.Msg = "配置失败！";
                    }
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        #endregion

        #region 修改

        /// <summary>
        /// 修改用户数据
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Modify(UserInfoModifyRequest request)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                //添加功能，直接添加
                con.Transaction(tran =>
                {
                    var user = GetLoginUserInfo();
                    request.id = request.id.IsNullOrEmpty() ? user.id : request.id;

                    var userinfo = tran.QuerySet<SysUserInfoEntity>().Where(a => a.id == request.id).Get();
                    userinfo.modifier_id = user.id;
                    userinfo.displayname = request.displayname;
                    userinfo.picture_url = request.picture_url;
                    userinfo.email = request.email;
                    userinfo.gender = request.gender;
                    userinfo.idnumber = request.idnumber;
                    userinfo.mobile_phone = request.mobile_phone;
                    userinfo.nickname = request.nickname;
                    userinfo.office_phone = request.office_phone;
                    userinfo.qq = request.qq;
                    userinfo.realname = request.realname;
                    userinfo.user_describe = request.user_describe;
                    userinfo.telphone = request.telphone;
                    userinfo.modifier_date = DateTime.Now;

                    var result = tran.CommandSet<SysUserInfoEntity>().Update(userinfo);
                    if (result > 0)
                    {
                        //先删除用户-角色关联信息
                        tran.CommandSet<SysUserRoleReEntity>().Where(p => p.user_id == userinfo.id).Delete();

                        //插入用户-角色关联信息
                        List<string> sRoleList = request.roleList.Split(',').ToList();
                        SysUserRoleReEntity sysUserRole = null;
                        foreach (var item in sRoleList)
                        {
                            sysUserRole = new SysUserRoleReEntity();
                            sysUserRole.id = GuidHelper.GetGuid();
                            sysUserRole.user_id = userinfo.id;
                            sysUserRole.role_id = item;
                            tran.CommandSet<SysUserRoleReEntity>().Insert(sysUserRole);
                        }

                        //先删除用户-部门关联信息
                        tran.CommandSet<SysUserDepartmentReEntity>().Where(p => p.user_id == userinfo.id).Delete();

                        //插入用户-部门关联信息
                        List<string> sDepartmentList = request.departmentList.Split(',').ToList();
                        SysUserDepartmentReEntity userDepartment = null;
                        foreach (var item in sDepartmentList)
                        {
                            userDepartment = new SysUserDepartmentReEntity();
                            userDepartment.id = GuidHelper.GetGuid();
                            userDepartment.user_id = userinfo.id;
                            userDepartment.department_id = item;
                            tran.CommandSet<SysUserDepartmentReEntity>().Insert(userDepartment);
                        }

                        //先删除用户-岗位关联信息
                        tran.CommandSet<SysUserPostReEntity>().Where(p => p.user_id == userinfo.id).Delete();
                        //插入用户-岗位关联信息
                        List<string> sPostList = request.postList.Split(',').ToList();
                        SysUserPostReEntity userPost = null;
                        foreach (var item in sPostList)
                        {
                            userPost = new SysUserPostReEntity();
                            userPost.id = GuidHelper.GetGuid();
                            userPost.user_id = userinfo.id;
                            userPost.post_id = item;
                            tran.CommandSet<SysUserPostReEntity>().Insert(userPost);
                        }

                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "修改成功！";
                        AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"修改用户数据成功，修改用户数据：{JsonHelper.ToJson(userinfo)}");

                        #region 修改存在redis中的信息
                        var info = con.QuerySet<View_SysUserEntity>().Where(p => p.id == userinfo.id).Get();
                        if (info != null)
                        {
                            var ticket = info.MapTo<UserLoginInfo>();
                            SysUserServiceRedis.SaveUserInfo(ticket);
                        }
                        #endregion

                    }
                    else
                    {
                        resultInfo.Msg = "修改失败！";
                    }
                });
            }

            return resultInfo;
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="userIds"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(List<string> userIds)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var ids = userIds.ToArray();
                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id.PostIn(ids)).ToList();
                if (userinfo.Count != userIds.Count)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                userinfo.ForEach(w => w.is_deleted = true);

                //事务功能
                con.Transaction(tran =>
                {
                    var result = tran.CommandSet<SysUserInfoEntity>().Update(userinfo);

                    //删除原来配置的部门数据
                    var deletebu = tran.CommandSet<SysUserDepartmentReEntity>().Where(a => a.user_id.PostIn(ids)).Delete();

                    //删除原来配置的岗位数据
                    var deletezhi = tran.CommandSet<SysUserPostReEntity>().Where(a => a.user_id.PostIn(ids)).Delete();

                    //删除原来配置的角色数据
                    var deleterole = tran.CommandSet<SysUserRoleReEntity>().Where(a => a.user_id.PostIn(ids)).Delete();

                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "删除成功！";
                        AddOperationLog(OperationLogType.RemoveOperation, BusinessTitleType.UserManage, $"删除用户成功，删除用户数据：{JsonHelper.ToJson(userIds)}");
                    }
                    else
                    {
                        resultInfo.Msg = "删除失败！";
                    }
                }, ex =>
                {
                    throw ex;
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 禁用/启用用户
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ForbidOrEnable(string userId)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {

                var userinfo = con.QuerySet<SysUserInfoEntity>().Where(a => a.id == userId).Get();
                if (userinfo == null)
                {
                    resultInfo.Code = ActionCodes.ArgumentInvalid;
                    resultInfo.Msg = "参数无效！";
                    return resultInfo;
                }
                if (userinfo.is_valid)
                {
                    userinfo.is_valid = false;
                }
                else
                {
                    userinfo.is_valid = true;
                }
                var result = con.CommandSet<SysUserInfoEntity>().Update(userinfo);
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Msg = "禁用/启用用户！";
                    AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.UserManage, $"禁用/启用用户成功，禁用/启用用户数据：{JsonHelper.ToJson(userinfo)}");
                }
                else
                {
                    resultInfo.Msg = "禁用/启用用户！";
                }
            }
            return resultInfo;
        }


        #endregion

        #endregion

        #region 信息发送相关功能

        /// <summary>
        /// 根据条件分页查询除自己以外的用户数据
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> LoadListExceptMyself(ParametersInfo<UserInfoQueryRequest> inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            var userInfo = GetLoginUserInfo();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<View_SysUserEntity>();

                if (inputInfo.parameters.sKeyWords.IsNotNullOrEmpty())
                {
                    result.Where(a => a.name.Contains(inputInfo.parameters.sKeyWords)
                    || a.displayname.Contains(inputInfo.parameters.sKeyWords)
                    || a.mobile_phone.Contains(inputInfo.parameters.sKeyWords)
                    || a.post_names.Contains(inputInfo.parameters.sKeyWords)
                    || a.department_names.Contains(inputInfo.parameters.sKeyWords)
                    );
                }

                #region 表达式拼装 或OrElse
                if (inputInfo.parameters.postIds.IsNotNullOrEmpty())
                {
                    var postIds = inputInfo.parameters.postIds.Split(',');

                    ParameterExpression pa = Expression.Parameter(typeof(View_SysUserEntity), "user");
                    MyExpressionVisitor visitor = new MyExpressionVisitor(pa);//统一参数类型

                    Expression<Func<View_SysUserEntity, bool>> lambdaOne = user => user.post_ids.Contains(postIds[0]);
                    Expression lambdaOneBody = visitor.Visit(lambdaOne.Body);

                    foreach (var item in postIds)
                    {
                        Expression<Func<View_SysUserEntity, bool>> lambdaItem = user => user.post_ids.Contains(item);
                        Expression lambdaBody = visitor.Visit(lambdaItem.Body);

                        lambdaOneBody = Expression.OrElse(lambdaOneBody, lambdaBody);
                    }
                    Expression<Func<View_SysUserEntity, bool>> newEx = Expression.Lambda<Func<View_SysUserEntity, bool>>(lambdaOneBody, pa);
                    result.Where(newEx);
                }
                #endregion

                if (inputInfo.parameters.departmentId.IsNotNullOrEmpty())
                {
                    result.Where(a => a.department_ids.Contains(inputInfo.parameters.departmentId));
                }

                //排除自己
                result.Where(p => p.id != userInfo.id);

                #region 排序
                if (inputInfo.field.IsNullOrEmpty())
                {
                    inputInfo.field = " sort";
                }
                if (inputInfo.order.IsNullOrEmpty())
                {
                    inputInfo.order = " asc";
                }
                if (inputInfo.order.ToLower() == "asc")
                {
                    result.OrderBy(inputInfo.field);
                }
                else
                {
                    result.OrderByDescing(inputInfo.field);
                }
                #endregion

                var listInfo = result.PageList(inputInfo.page, inputInfo.limit);

                if (listInfo.Items.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo.Items.MapToList<UserInfoResponse>();
                    resultInfo.Count = listInfo.Total;
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 根据条件查询除自己以外的用户ID
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<string>> LoadIdListExceptMyself(UserInfoQueryRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<List<string>>();

            var userInfo = GetLoginUserInfo();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<View_SysUserEntity>();

                if (inputInfo.sKeyWords.IsNotNullOrEmpty())
                {
                    result.Where(a => a.name.Contains(inputInfo.sKeyWords)
                    || a.displayname.Contains(inputInfo.sKeyWords)
                    || a.mobile_phone.Contains(inputInfo.sKeyWords)
                    || a.post_names.Contains(inputInfo.sKeyWords)
                    || a.department_names.Contains(inputInfo.sKeyWords)
                    );
                }

                #region 表达式拼装 或OrElse
                if (inputInfo.postIds.IsNotNullOrEmpty())
                {
                    var postIds = inputInfo.postIds.Split(',');

                    ParameterExpression pa = Expression.Parameter(typeof(View_SysUserEntity), "user");
                    MyExpressionVisitor visitor = new MyExpressionVisitor(pa);//统一参数类型

                    Expression<Func<View_SysUserEntity, bool>> lambdaOne = user => user.post_ids.Contains(postIds[0]);
                    Expression lambdaOneBody = visitor.Visit(lambdaOne.Body);

                    foreach (var item in postIds)
                    {
                        Expression<Func<View_SysUserEntity, bool>> lambdaItem = user => user.post_ids.Contains(item);
                        Expression lambdaBody = visitor.Visit(lambdaItem.Body);

                        lambdaOneBody = Expression.OrElse(lambdaOneBody, lambdaBody);
                    }
                    Expression<Func<View_SysUserEntity, bool>> newEx = Expression.Lambda<Func<View_SysUserEntity, bool>>(lambdaOneBody, pa);
                    result.Where(newEx);
                }
                #endregion

                if (inputInfo.departmentId.IsNotNullOrEmpty())
                {
                    result.Where(a => a.department_ids.Contains(inputInfo.departmentId));
                }
                //排除自己
                var listInfo = result.Where(p => p.id != userInfo.id).ToList(p => p.id);

                if (listInfo.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = listInfo;
                }
                else
                {
                    resultInfo.Msg = "无对应信息！";
                }
            }
            return resultInfo;
        }
        #endregion

        #region 批量导入导出

        public ResultJsonInfo<List<UserInfoFileRequest>> VerifyThatTheFile(List<UserInfoFileRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoFileRequest>>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                foreach (UserInfoFileRequest item in selects)
                {
                    if (item.role.IsNotNullOrEmpty())
                    {
                        var rl = item.role.Split(',').ToList();
                        var newrl = new List<string>();
                        for (int i = 0; i < rl.Count; i++)
                        {
                            var rlid = con.QuerySet<SysRoleInfoEntity>().Where(a => a.name.Equals(rl[i])).Get();
                            if (rlid != null)
                            {
                                newrl.Add(rlid.id);
                            }

                        }

                        item.roleid = string.Join(",", newrl.ToArray());
                    }
                    if (item.depart.IsNotNullOrEmpty())
                    {
                        var depar = item.depart.Split(',').ToList();
                        var newdepar = new List<string>();
                        for (int i = 0; i < depar.Count; i++)
                        {
                            var deparid = con.QuerySet<SysDepartmentInfoEntity>().Where(a => a.name.Equals(depar[i])).Get();
                            if (deparid != null)
                            {
                                newdepar.Add(deparid.id);
                            }

                        }

                        item.departid = string.Join(",", newdepar.ToArray());
                    }

                    if (item.post.IsNotNullOrEmpty())
                    {
                        var post = item.post.Split(',').ToList();
                        var newpost = new List<string>();
                        for (int i = 0; i < post.Count; i++)
                        {
                            var postid = con.QuerySet<SysPostInfoEntity>().Where(a => a.name.Equals(post[i])).Get();
                            if (postid != null)
                            {
                                newpost.Add(postid.id);
                            }

                        }

                        item.postid = string.Join(",", newpost.ToArray());
                    }
                }
            }
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Data = selects;
            return resultInfo;
        }



        /// <summary>
        /// 事务批量导入用户信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<int> TranBulkImportRole(List<UserInfoFileRequest> selects)
        {
            var resultInfo = new ResultJsonInfo<int>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {
                    var result = 0;

                    for (int i = 0; i < selects.Count; i++)
                    {
                        SysUserInfoEntity sysUser = new SysUserInfoEntity();
                        sysUser.name = selects[i].name;
                        sysUser.displayname = selects[i].displayname;
                        sysUser.mobile_phone = selects[i].mobile_phone;
                        sysUser.gender = selects[i].gender;
                        sysUser.email = selects[i].email;
                        sysUser.is_valid = selects[i].is_valid;

                        tran.CommandSet<SysUserInfoEntity>().Insert(sysUser);

                        if (selects[i].departid.IsNotNullOrEmpty())
                        {
                            var deper = selects[i].departid.Split(',').ToList();
                            for (int j = 0; j < deper.Count; j++)
                            {
                                SysUserDepartmentReEntity sysUserDepartment = new SysUserDepartmentReEntity();
                                sysUserDepartment.department_id = deper[j];
                                sysUserDepartment.user_id = sysUser.id;
                                result += tran.CommandSet<SysUserDepartmentReEntity>().Insert(sysUserDepartment);
                            }
                        }
                        if (selects[i].roleid.IsNotNullOrEmpty())
                        {
                            var roleid = selects[i].roleid.Split(',').ToList();
                            for (int j = 0; j < roleid.Count; j++)
                            {
                                SysUserRoleReEntity sysUserRoleRe = new SysUserRoleReEntity();
                                sysUserRoleRe.role_id = roleid[j];
                                sysUserRoleRe.user_id = sysUser.id;
                                result += tran.CommandSet<SysUserRoleReEntity>().Insert(sysUserRoleRe);
                            }
                        }
                        if (selects[i].postid.IsNotNullOrEmpty())
                        {
                            var postid = selects[i].postid.Split(',').ToList();
                            for (int j = 0; j < postid.Count; j++)
                            {
                                SysUserPostReEntity sysUserPostRe = new SysUserPostReEntity();
                                sysUserPostRe.post_id = postid[j];
                                sysUserPostRe.user_id = sysUser.id;
                                result += tran.CommandSet<SysUserPostReEntity>().Insert(sysUserPostRe);
                            }
                        }

                    }

                    if (result > 0)
                    {
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "导入成功！";
                        AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.RoleManage, $"事务导入角色成功，导入角色信息：{JsonHelper.ToJson(selects)}");
                    }

                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 批量导出所有
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<UserInfoResponse>> ListAll()
        {
            var resultInfo = new ResultJsonInfo<List<UserInfoResponse>>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<View_SysUserEntity>().ToList();

                if (result.Count > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result.MapToList<UserInfoResponse>();
                }
                else
                {
                    resultInfo.Data = new List<UserInfoResponse>();
                    resultInfo.Msg = "无对应信息！";
                }
            }

            return resultInfo;
        }

        #endregion

    }
}