﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 配置用户组参数类
    /// </summary>
    public class UserInfoGroupRequset
    {  /// <summary>
       /// 用户ID
       /// </summary>
        public string userId { get; set; }
        /// <summary>
        /// 用户组ID
        /// </summary>
        public string groupId { get; set; }
    }
}
