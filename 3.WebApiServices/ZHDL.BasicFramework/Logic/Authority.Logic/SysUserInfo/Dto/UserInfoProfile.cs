﻿
using Authority.Model;
using AutoMapper;
using Common.Model;

namespace Authority.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class UserInfoProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public UserInfoProfile()
        {
            CreateMap<UserInfoAddRequest,SysUserInfoEntity>();

            CreateMap<SysUserInfoEntity, UserInfoResponse>();

            CreateMap<UserInfoModifyRequest, SysUserInfoEntity>();

            CreateMap<View_SysUserEntity, UserLoginInfo>()
                  .ForMember(p => p.token, opt => opt.Ignore());

            CreateMap<View_SysUserEntity, UserInfoResponse>();

            CreateMap<UserInfoModifyOneselfRequest, SysUserInfoEntity>();
            
        }
    }
}
