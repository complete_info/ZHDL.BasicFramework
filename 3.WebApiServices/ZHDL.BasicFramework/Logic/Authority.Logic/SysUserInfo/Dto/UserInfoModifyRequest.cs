﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 修改用户参数类
    /// </summary>
    public class UserInfoModifyRequest
    {
        /// <summary>
        ///用户id【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 32, Description = "用户id")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户系统显示名【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "系统显示名称")]
        public string displayname
        {
            get; set;
        }

        /// <summary>
        /// 用户描述
        /// </summary>
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户手机号码")]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// 性别【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "性别")]
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// QQ号
        /// </summary>
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户真实名")]
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// 用户邮箱
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "用户身份号码")]
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 255, Description = "用户头像地址")]
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 部门id组[使用“,”符号，将之分开]
        /// </summary>
        public string departmentList
        {
            get; set;
        }
        /// <summary>
        /// 职位id组[使用“,” 符号，将之分开]
        /// </summary>
        public string postList
        {
            get; set;
        }
        /// <summary>
        /// 角色id组[使用“,” 符号，将之分开]
        /// </summary>
        public string roleList
        {
            get; set;
        }

    }
}
