﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 配置岗位参数
    /// </summary>
    public class UserInfoPositionRequset
    {
        /// <summary>
        /// 用户ID
        /// </summary>
        public string userId { get; set; }
        /// <summary>
        /// 岗位ID
        /// </summary>
        public string postId { get; set; }
    }
}
