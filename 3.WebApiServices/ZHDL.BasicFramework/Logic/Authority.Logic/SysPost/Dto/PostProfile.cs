﻿
using Authority.Model;
using AutoMapper;
using Common.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class PostProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public PostProfile()
        {
            CreateMap<PostAddRequest, SysPostInfoEntity>();
            CreateMap<SysPostInfoEntity, PostResponse>();
            CreateMap<SysUserInfoEntity, PostUserResponse>();
            CreateMap<SysPostInfoEntity, SelectListInfo>()
                .ForMember(p => p.value, opt => { opt.MapFrom(t => t.id); })
                .ForMember(p => p.name, opt => { opt.MapFrom(t => t.name); });

        }
    }
}
