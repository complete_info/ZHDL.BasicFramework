﻿using Authority.Model;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    public class LayertreeCfgProfile : Profile
    {
        public LayertreeCfgProfile() {
            CreateMap<LayertreeCfgChildAddRequest,SysLayertreeCfgEntity > ();
            CreateMap<LayertreeCfgRootAddRequest, SysLayertreeCfgEntity>();
            CreateMap<SysLayertreeCfgEntity,LayertreeResponse>();

            ///权限相关
            CreateMap<SysLayertreeCfgEntity, LayertreeCfgRoleLayerResponse>();
            
        }
    }
}
