﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Authority.Logic
{

    /// <summary>
    /// 修改对应角色赋值的图层操作权限请求实体
    /// </summary>
    public class LayertreeCfgRoleModifyRequest
    {
        /// <summary>
        ///角色唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string roleid
        {
            get; set;
        }

        /// <summary>
        ///菜单唯一标识符列表
        /// </summary>
        public List<string> layerIdList
        {
            get; set;
        }

    }
}
