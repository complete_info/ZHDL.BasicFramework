﻿namespace Authority.Logic
{
    /// <summary>
    /// 角色ID对应所有可操作图层已赋权限情况
    /// </summary>
    public class LayertreeCfgRoleLayerResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 图层名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 图层表名
        /// </summary>
        public string tablename
        {
            get; set;
        }

        /// <summary>
        /// 图层备注
        /// </summary>
        public string memo
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 图层名对应的要素类名称
        /// </summary>
        public string featureclass_name
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否已经赋值可操作权限
        /// </summary>
        public bool lay_is_checked
        {
            get; set;
        }
    }
}
