﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Authority.Logic
{
    public class LayertreeCfgMoveRequest
    { /// <summary>
      /// 操作类型 上/下移（1：上移/2：下移）
      /// </summary>
        [Validate(ValidateType.NotEmpty, Description = "操作类型")]
        public int Flag { get; set; }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [Validate(ValidateType.NotEmpty, Description = "唯一标识符")]
        public string id { get; set; }
    }
}
