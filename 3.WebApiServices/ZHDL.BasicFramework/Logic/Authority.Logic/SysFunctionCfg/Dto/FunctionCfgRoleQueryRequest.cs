﻿using Validate.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 根据角色ID获取所有可操作菜单 已赋权限情况，请求实体
    /// </summary>
    public class FunctionCfgRoleQueryRequest
    {

        /// <summary>
        /// 角色唯一标识符【必填】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色唯一标识符")]
        public string roleId
        {
            get; set;
        }

    }
}
