﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 菜单返回信息
    /// </summary>
    public class FunctionCfgInfoResponse
    {

        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 菜单名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 系统图标
        /// </summary>
        public string icon_font
        {
            get; set;
        }

        /// <summary>
        /// 级别 10：一级 20：二级 30：三级
        /// </summary>
        public int level
        {
            get; set;
        }

        /// <summary>
        /// 链接路由地址【用于Web端】
        /// </summary>
        public string link_url
        {
            get; set;
        }

        /// <summary>
        /// 展开方式
        /// </summary>
        public string target
        {
            get; set;
        }


        /// <summary>
        /// 系统类型【有很多系统，通过这个区分】
        /// </summary>
        public string system_type
        {
            get; set;
        }

        /// <summary>
        /// 系统类型名称
        /// </summary>
        public string system_type_name
        {
            get; set;
        }

        /// <summary>
        /// 功能快捷键
        /// </summary>
        public string shortcut
        {
            get; set;
        }

        /// <summary>
        /// 功能类型
        /// </summary>
        public string function_type
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        public string describe
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }


        /// <summary>
        /// 是否展开
        /// </summary>
        public bool is_spread
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄长【同级别，排前面的】
        /// </summary>
        public bool have_elder
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄弟【同级别，排后面的】
        /// </summary>
        public bool have_younger
        {
            get; set;
        }
    }
}
