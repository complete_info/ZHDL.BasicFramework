﻿using Validate.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 菜单操作请求类
    /// </summary>
    public class FunctionCfgAddRequest
    {
       
        /// <summary>
        /// 父节点编码【可空】--如果为空，就增加根节点
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 系统类型【不可空，最大长度50】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "系统类型")]
        public string system_type
        {
            get; set;
        }

        /// <summary>
        /// 系统类型名称【不可空，最大长度255】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 255, Description = "系统类型名称")]
        public string system_type_name
        {
            get; set;
        }

        /// <summary>
        /// 功能类型【不可空，最大长度50】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "功能类型")]
        public string function_type
        {
            get; set;
        }

        /// <summary>
        /// 菜单名称【不可空，最大长度25】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 25, Description = "菜单名称")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 菜单标签【不可空，最大长度50】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "菜单标签")]
        public string icon_font
        {
            get; set;
        }

        /// <summary>
        /// 菜单链接【可空】
        /// </summary>
        public string link_url
        {
            get; set;
        }

        /// <summary>
        /// 是否启用
        /// </summary>
        [Validate(ValidateType.NotEmpty,Description = "是否启用")]
        public bool is_valid
        {
            get; set;
        }


        /// <summary>
        /// 备注【可空】
        /// </summary>
        public string describe
        {
            get; set;
        }
    }
}
