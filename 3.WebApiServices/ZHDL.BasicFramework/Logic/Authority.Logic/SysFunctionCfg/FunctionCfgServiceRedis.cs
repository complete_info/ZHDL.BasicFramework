﻿using Authority.Model;
using Common.Library;
using Common.Model;
using Redis.Library;
using System;
using System.Collections.Generic;

namespace Authority.Logic
{
    /// <summary>
    /// 菜单功能管理Redis操作逻辑类
    /// </summary>
    internal static class FunctionCfgServiceRedis
    {

        #region 操作用户的菜单信息

        /// <summary>
        /// 将对应用户可操作性菜单保存redis
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void SaveFunctionCfgOfeUser(string key, List<SysFunctionCfgEntity> value)
        {
             RedisClient.GetRedisDb(AuthorityRedisInfo.RedisDbWebOfMenu).HashSet(AuthorityRedisInfo.RedisMenusCode,key, value);
        }

        /// <summary>
        /// 获取用户可操作性菜单信息
        /// </summary>
        /// <param name="key"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void GetFunctionCfgOfeUser(string key,Action<List<SysFunctionCfgEntity>> successAction = null, Action failAction = null)
        {
            var resultInfo = RedisClient.GetRedisDb(AuthorityRedisInfo.RedisDbWebOfMenu).HashGet<List<SysFunctionCfgEntity>>(AuthorityRedisInfo.RedisMenusCode, key);
            if (resultInfo!=null)
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }

        #endregion
    }
}
