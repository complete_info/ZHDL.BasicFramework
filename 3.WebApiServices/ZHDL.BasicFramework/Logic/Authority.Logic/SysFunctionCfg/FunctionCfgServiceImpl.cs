﻿using Authority.Model;
using Common.Library;
using Common.Model;
using Container.Library;
using Dapper.Library;
using Dapper.Library.DataAccessSql;
using Dapper.PostgreSql.Library;
using Serialize.Library;
using System;
using System.Collections.Generic;
using System.Linq;

/*
* 命名空间: Authority.Logic
*
* 功 能： 菜单功能管理
*
* 类 名： FunctionCfgServiceImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/124 14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Authority.Logic
{
    /// <summary>
    /// 菜单功能管理
    /// </summary>
    public class FunctionCfgServiceImpl : OperationLogicImpl, IFunctionCfgService
    {
        #region 主界面菜单逻辑方法

        /// <summary>
        /// 获取【用户】对应系统的可操作的菜单信息
        /// </summary>
        /// <param name="systemType"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<MenuInfo>> LoadMenuOfUserList(string systemType)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();
            List<MenuInfo> listInfo = new List<MenuInfo>();
            //获取用户登录信息
            var loginUserInfo = GetLoginUserInfo();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {
                    //如果是超级管理员，就直接获取相应系统所有可操作菜单
                    if (loginUserInfo.is_super_user)
                    {
                        var functionListInfo = tran.QuerySet<SysFunctionCfgEntity>()
                                                      .Where(p => p.is_deleted == false && p.is_valid == true)
                                                      .OrderBy(p => p.sort).ToList();

                        if (functionListInfo.Count > 0)
                        {
                            //可操作菜单，放redis中
                            FunctionCfgServiceRedis.SaveFunctionCfgOfeUser(loginUserInfo.name, functionListInfo);
                            listInfo = GetFunctionChildTreeInfo(functionListInfo.FindAll(p => p.system_type.Contains(systemType)), "");
                        }
                    }
                    else
                    {
                        //可操作菜单，redis中获取
                        FunctionCfgServiceRedis.GetFunctionCfgOfeUser(loginUserInfo.name,
                        (menusInfo) =>
                        {
                            var functionListInfo = menusInfo.FindAll(p => p.system_type.Contains(systemType));
                            listInfo = GetFunctionChildTreeInfo(functionListInfo, "");
                        },
                        //如果Redis中没有保存信息，只有从数据库中再次获取一次，并保存Redis
                        () =>
                        {
                            var roleIds = loginUserInfo.role_ids.Split(',').ToArray();
                            //权限类型
                            int permissionType = (int)PermissionType.Function;

                            var functionIds = tran.QuerySet<SysRolePermissionReEntity>()
                                .Where(p => p.role_id.PostIn(roleIds) && p.permission_type.Equals(permissionType))
                                .ToList().Select(p => p.permission_id)
                                .ToArray();

                            if (functionIds.Length > 0)
                            {
                                var functionListInfo = tran.QuerySet<SysFunctionCfgEntity>()
                                                        .Where(p => p.is_deleted == false && p.is_valid == true && p.id.PostIn(functionIds))
                                                        .OrderBy(p => p.sort).ToList();

                                if (functionListInfo.Count > 0)
                                {
                                    //可操作菜单，放redis中
                                    FunctionCfgServiceRedis.SaveFunctionCfgOfeUser(loginUserInfo.name, functionListInfo);

                                    listInfo = GetFunctionChildTreeInfo(functionListInfo.FindAll(p => p.system_type.Contains(systemType)), "");
                                }
                            }
                        });
                    }
                });
            }
            if (listInfo.Count > 0)
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = listInfo;
                resultInfo.Msg = "获取成功！";
            }
            else
            {
                resultInfo.Msg = "获取失败！";
            }
            return resultInfo;
        }

        /// <summary>
        /// 获取【用户】对应系统的可操作的菜单信息
        /// </summary>
        /// <param name="systemType"></param>
        /// <param name="level">层级级别 0：所有 10：一级 20：二级 30：三级</param>
        /// <returns></returns>
        public ResultJsonInfo<List<SysFunctionCfgEntity>> LoadTopMenuOfUserList(string systemType, int level)
        {
            var resultInfo = new ResultJsonInfo<List<SysFunctionCfgEntity>>();

            //获取用户登录信息
            var loginUserInfo = GetLoginUserInfo();

            List<SysFunctionCfgEntity> listInfo = null;

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {
                    //如果是超级管理员，就直接获取相应系统所有可操作菜单
                    if (loginUserInfo.is_super_user)
                    {
                        listInfo = tran.QuerySet<SysFunctionCfgEntity>()
                                                 .Where(p => p.is_deleted.Equals(false)
                                                         && p.is_valid.Equals(true))
                                                 .OrderBy(p => p.sort)
                                                 .ToList();
                    }
                    else
                    {
                        var roleIds = loginUserInfo.role_ids.Split(',').ToArray();
                        var functionIds = tran.QuerySet<SysRolePermissionReEntity>()
                            .Where(p => p.role_id.PostIn(roleIds))
                            .ToList().Select(p => p.permission_id)
                            .ToArray();

                        if (functionIds.Length > 0)
                        {
                            listInfo = tran.QuerySet<SysFunctionCfgEntity>()
                                                  .Where(p => p.is_deleted.Equals(false)
                                                          && p.is_valid.Equals(true)
                                                          && p.id.PostIn(functionIds))
                                                  .OrderBy(p => p.sort)
                                                  .ToList();
                        }
                    }
                });
                if (listInfo.Count > 0)
                {
                    //可操作菜单，放redis中
                    FunctionCfgServiceRedis.SaveFunctionCfgOfeUser(loginUserInfo.name, listInfo);

                    resultInfo.Code = ActionCodes.Success;
                    if (level == 0)
                    {
                        resultInfo.Data = listInfo.FindAll(p => p.system_type.Contains(systemType));
                    }
                    else
                    {
                        resultInfo.Data = listInfo.FindAll(p => p.system_type.Contains(systemType) && p.level == level);
                    }
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "获取失败！";
                }
            }

            return resultInfo;
        }

        /// <summary>
        /// 获取【用户拥有的】左侧二级或三级菜单信息
        /// </summary>
        /// <param name="parentId"></param>
        /// <param name="systemType">系统类型  运维系统 100 桌面应用系统 200 PC客户端应用 300</param>
        /// <returns></returns>
        public ResultJsonInfo<List<MenuInfo>> LoadSubMenus(string systemType, string parentId)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();
            List<MenuInfo> listInfo = new List<MenuInfo>();
            //获取用户登录信息
            var loginUserInfo = GetLoginUserInfo();


            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {
                    //如果是超级管理员，就直接获取相应系统所有可操作菜单
                    if (loginUserInfo.is_super_user)
                    {
                        var functionListInfo = tran.QuerySet<SysFunctionCfgEntity>()
                                                      .Where(p => p.is_deleted.Equals(false) && p.is_valid.Equals(true))
                                                      .OrderBy(p => p.sort).ToList();

                        if (functionListInfo.Count > 0)
                        {
                            //可操作菜单，放redis中
                            FunctionCfgServiceRedis.SaveFunctionCfgOfeUser(loginUserInfo.name, functionListInfo);

                            listInfo = GetFunctionChildTreeInfo(functionListInfo.FindAll(p => p.system_type.Contains(systemType)), parentId);
                        }
                    }
                    else
                    {
                        //可操作菜单，redis中获取
                        FunctionCfgServiceRedis.GetFunctionCfgOfeUser(loginUserInfo.name,
                       (menusInfo) =>
                       {
                           var functionListInfo = menusInfo.FindAll(p => p.system_type.Contains(systemType));
                           listInfo = GetFunctionChildTreeInfo(functionListInfo, parentId);
                       },
                       //如果Redis中没有保存信息，只有从数据库中再次获取一次，并保存Redis
                       () =>
                       {
                           var roleIds = loginUserInfo.role_ids.Split(',').ToArray();
                           var functionIds = tran.QuerySet<SysRolePermissionReEntity>()
                               .Where(p => p.role_id.PostIn(roleIds))
                               .ToList().Select(p => p.permission_id)
                               .ToArray();
                           if (functionIds.Length > 0)
                           {
                               var functionListInfo = tran.QuerySet<SysFunctionCfgEntity>()
                                                       .Where(p => p.is_deleted.Equals(false) && p.is_valid.Equals(true) && p.id.PostIn(functionIds))
                                                       .OrderBy(p => p.sort).ToList();

                               if (functionListInfo.Count > 0)
                               {
                                   //可操作菜单，放redis中
                                   FunctionCfgServiceRedis.SaveFunctionCfgOfeUser(loginUserInfo.name, functionListInfo);

                                   listInfo = GetFunctionChildTreeInfo(functionListInfo.FindAll(p => p.system_type.Contains(systemType)), parentId);
                               }
                           }
                       });
                    }
                });
            }
            if (listInfo.Count > 0)
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = listInfo;
                resultInfo.Msg = "获取成功！";
            }
            else
            {
                resultInfo.Msg = "获取失败！";
            }
            return resultInfo;
        }

        #endregion

        #region 菜单基础信息管理

        #region 查询

        /// <summary>
        /// 根据菜单名称或系统类型获取菜单列表信息
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FunctionCfgInfoResponse>> LoadListInfo(ParametersInfo<FunctionCfgQueryRequest> queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FunctionCfgInfoResponse>>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<SysFunctionCfgEntity>();

                if (queryInfo.parameters != null)
                {
                    if (queryInfo.parameters.system_type.IsNotNullOrEmpty())
                    {
                        result.Where(p => p.system_type.Contains(queryInfo.parameters.system_type));
                    }
                    if (queryInfo.parameters.name.IsNotNullOrEmpty())
                    {
                        result.Where(p => p.name.Contains(queryInfo.parameters.name));
                    }
                }

                var functionListInfo = result.Where(p => p.is_deleted == false)
                                  .OrderBy(p => p.sort).PageList(queryInfo.page, queryInfo.limit);

                if (functionListInfo.Items.Count > 0)
                {
                    var menusList = functionListInfo.Items.MapToList<FunctionCfgInfoResponse>();

                    ///这里获取所有，查询是否有兄弟节点
                    var functionListInfoAll = result.Where(p => p.is_deleted == false)
                         .OrderBy(p => p.sort).ToList();
                    foreach (var item in menusList)
                    {
                        //NET算术运算溢出问题
                        item.have_elder = functionListInfoAll.Exists(p => p.sort < item.sort && p.level == item.level && p.id != item.id && p.parent_id == item.parent_id);
                        item.have_younger = functionListInfoAll.Exists(p => p.sort > item.sort && p.level == item.level && p.id != item.id && p.parent_id == item.parent_id);
                    }
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = menusList;
                    resultInfo.Count = functionListInfo.Total;
                    resultInfo.Msg = "获取成功！";
                }
                else
                {
                    resultInfo.Msg = "无对应数据！";
                    resultInfo.Data = new List<FunctionCfgInfoResponse>();
                }
            }

            return resultInfo;
        }

        /// <summary>
        /// 根据菜单名称或系统类型获取菜单树状列表信息
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<MenuInfo>> LoadTreeListInfo(FunctionCfgQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<MenuInfo>>();
            List<MenuInfo> listInfo = new List<MenuInfo>();
            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = con.QuerySet<SysFunctionCfgEntity>();
                if (queryInfo.system_type.IsNotNullOrEmpty())
                {
                    result.Where(p => p.system_type == queryInfo.system_type);
                }
                if (queryInfo.name.IsNotNullOrEmpty())
                {
                    result.Where(p => p.name.Contains(queryInfo.name));
                }
                var functionListInfo = result.Where(p => p.is_deleted == false)
                                  .OrderBy(p => p.sort).ToList();

                listInfo = GetFunctionChildTreeInfo(functionListInfo, "");
            }
            if (listInfo.Count > 0)
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = listInfo;
                resultInfo.Msg = "获取成功！";
            }
            else
            {
                resultInfo.Msg = "获取失败！";
            }
            return resultInfo;
        }
        #endregion

        #region 更新

        /// <summary>
        /// 新增节点信息
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> AddInfo(FunctionCfgAddRequest inputInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = 0;
                var sysFunctionCfg = inputInfo.MapTo<SysFunctionCfgEntity>();
                var userLoginInfo = GetLoginUserInfo();

                if (inputInfo.parent_id.IsNotNullOrEmpty())
                {
                    con.Transaction(tran =>
                    {
                        var allFunction = tran.QuerySet<SysFunctionCfgEntity>().ToList();//.Where(p => p.is_deleted == false)

                        var sysMenuInfo = allFunction.Find(p => p.id == inputInfo.parent_id);

                        if (sysMenuInfo != null)
                        {
                            //查询出排序在该节点之后的所有节点
                            var sysDepartmentInfos = allFunction.FindAll(p => p.sort > sysMenuInfo.sort);
                            foreach (var item in sysDepartmentInfos)
                            {
                                item.sort = item.sort + 1;
                                tran.CommandSet<SysFunctionCfgEntity>().Update(item);
                            }

                            int count= tran.QuerySet<SysFunctionCfgEntity>().Where(p => p.name== sysFunctionCfg.name).Count();
                            if (count==0)
                            { 
                                //插入数据
                                sysFunctionCfg.id = GuidHelper.GetGuid();
                                sysFunctionCfg.sort = sysMenuInfo.sort + 1;
                                sysFunctionCfg.level = sysMenuInfo.level + 10;
                                sysFunctionCfg.creator_id = userLoginInfo.id;
                                sysFunctionCfg.creator_name = userLoginInfo.name;
                                sysFunctionCfg.modifier_id = userLoginInfo.id;
                                sysFunctionCfg.modifier_name = userLoginInfo.name;
                                result = tran.CommandSet<SysFunctionCfgEntity>().Insert(sysFunctionCfg);

                                if (result > 0)
                                {
                                    resultInfo.Code = ActionCodes.Success;
                                    resultInfo.Data = result;
                                    resultInfo.Msg = "新增子节点信息成功！";
                                    AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.FunctionManage, $"新增子节点信息:{JsonHelper.ToJson(sysFunctionCfg)}");
                                }
                                else
                                {
                                    resultInfo.Msg = "操作失败！";
                                }
                            }
                            else
                            {
                                resultInfo.Msg = "菜单名重复！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "无对应父节点信息！";
                        }
                    });
                }
                else
                {
                    con.Transaction(tran =>
                    {
                        int count = con.QuerySet<SysFunctionCfgEntity>().Where(p => p.name == sysFunctionCfg.name).Count();
                        if (count == 0)
                        {
                            //获取所有菜单信息
                            var maxSort = con.QuerySet<SysFunctionCfgEntity>().Max(p => p.sort);
                            sysFunctionCfg.id = GuidHelper.GetGuid();
                            sysFunctionCfg.sort = maxSort + 1;
                            sysFunctionCfg.level = (int)MenuLevel.First;
                            sysFunctionCfg.creator_id = userLoginInfo.id;
                            sysFunctionCfg.creator_name = userLoginInfo.name;
                            sysFunctionCfg.modifier_id = userLoginInfo.id;
                            sysFunctionCfg.modifier_name = userLoginInfo.name;

                            result = con.CommandSet<SysFunctionCfgEntity>().Insert(sysFunctionCfg);
                            if (result > 0)
                            {
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Data = result;
                                resultInfo.Msg = "新增节点信息成功！";
                                AddOperationLog(OperationLogType.AddOperation, BusinessTitleType.FunctionManage, $"新增根节点信息:{JsonHelper.ToJson(sysFunctionCfg)}");
                            }
                            else
                            {
                                resultInfo.Msg = "菜单名重复！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "菜单名重复！";
                        }
                    });
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改节点信息
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Modify(FunctionCfgModifyRequest modifyInfo)
        {

            var resultInfo = new ResultJsonInfo<int>();
            var sysMenu = modifyInfo.MapTo<SysFunctionCfgEntity>();
            var userLoginInfo = GetLoginUserInfo();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                var result = 0;
                con.Transaction(tran =>
                {

                    var sysFunctionCfg = tran.QuerySet<SysFunctionCfgEntity>().Where(p => p.id == sysMenu.id).Get();
                    if (sysFunctionCfg != null)
                    {
                        sysFunctionCfg.name = sysMenu.name;
                        sysFunctionCfg.system_type = modifyInfo.system_type;
                        sysFunctionCfg.system_type_name = modifyInfo.system_type_name;
                        sysFunctionCfg.function_type = modifyInfo.function_type;
                        sysFunctionCfg.describe = modifyInfo.describe;
                        sysFunctionCfg.icon_font = sysMenu.icon_font;
                        sysFunctionCfg.link_url = sysMenu.link_url;
                        sysFunctionCfg.is_valid = sysMenu.is_valid;
                        sysFunctionCfg.modifier_id = userLoginInfo.id;
                        sysFunctionCfg.modifier_name = userLoginInfo.name;


                        var exists = tran.QuerySet<SysFunctionCfgEntity>().Where(p => p.id != modifyInfo.id && p.name == modifyInfo.name).Count();
                        if (exists == 0)
                        {
                            result = tran.CommandSet<SysFunctionCfgEntity>().Update(sysFunctionCfg);
                            if (result > 0)
                            {
                                //修改时，如果是禁用菜单。。需要将它的子菜单一起禁用
                                if (sysMenu.is_valid == false)
                                {
                                    var listInfo = new List<SysFunctionCfgEntity>();
                                    var allInfo = tran.QuerySet<SysFunctionCfgEntity>().Where(p => p.is_deleted == false).ToList();
                                    var rootInfo = allInfo.FindAll(p => p.name.Contains(sysMenu.name));
                                    if (rootInfo.Count > 0)
                                    {
                                        listInfo.AddRange(rootInfo);
                                        listInfo.AddRange(allInfo.GetChildList(rootInfo));
                                    }
                                    foreach (var item in listInfo)
                                    {
                                        item.is_valid = false;
                                        tran.CommandSet<SysFunctionCfgEntity>().Update(item);
                                    }
                                }
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "菜单名称重复！";
                        }
                    }
                    else
                    {
                        resultInfo.Msg = "无对应菜单信息！";
                    }
                });
                if (result > 0)
                {
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = result;
                    resultInfo.Msg = "修改菜单信息成功！";
                    AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.FunctionManage, $"修改节点信息成功，节点信息：{JsonHelper.ToJson(modifyInfo)}");
                }
                else
                {
                    resultInfo.Msg = "操作失败,请检查菜单名称是否重复！";
                }
            }
            return resultInfo;
        }

        /// <summary>
        /// 移动顺序操作
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> MoveSort(FunctionCfgMoveRequest inputInfo)
        {

            var result = new ResultJsonInfo<int>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {
                    //（1）所有功能
                    var adminFuns = tran.QuerySet<SysFunctionCfgEntity>().OrderBy(p => p.sort).ToList();//.Where(p => p.is_deleted == false)

                    //（2）当前功能 
                    SysFunctionCfgEntity currAdminFun = adminFuns.Find(p => p.id == inputInfo.id);

                    //（3）当前功能兄弟节点
                    List<SysFunctionCfgEntity> brothersAdminFun = adminFuns.FindAll(p => p.parent_id == currAdminFun.parent_id);

                    SysFunctionCfgEntity broAdminFun = null;

                    foreach (SysFunctionCfgEntity item in brothersAdminFun)
                    {
                        if (inputInfo.flag == 1)//上移
                        {
                            if (item.sort < currAdminFun.sort) { broAdminFun = item; }
                            if (item.sort > currAdminFun.sort) { break; } //找到上一个兄弟节点
                        }
                        else //下移
                        {
                            if (item.sort > currAdminFun.sort) { broAdminFun = item; break; } //找到下一个兄弟节点
                        }
                    }

                    List<SysFunctionCfgEntity> currFuns = new List<SysFunctionCfgEntity>();
                    currFuns.Add(currAdminFun);
                    GetMoveNode(adminFuns, currAdminFun.id, currFuns);

                    List<SysFunctionCfgEntity> moveFuns = new List<SysFunctionCfgEntity>();
                    moveFuns.Add(broAdminFun);
                    GetMoveNode(adminFuns, broAdminFun.id, moveFuns);

                    //上移
                    if (inputInfo.flag == 1)
                    {
                        foreach (SysFunctionCfgEntity item in currFuns)
                        {
                            item.sort = short.Parse((item.sort - short.Parse(moveFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysFunctionCfgEntity>().Update(item);
                        }

                        foreach (SysFunctionCfgEntity item in moveFuns)
                        {
                            item.sort = short.Parse((item.sort + short.Parse(currFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysFunctionCfgEntity>().Update(item);
                        }
                    }
                    else
                    {
                        foreach (SysFunctionCfgEntity item in currFuns)
                        {
                            item.sort = short.Parse((item.sort + short.Parse(moveFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysFunctionCfgEntity>().Update(item);
                        }
                        foreach (SysFunctionCfgEntity item in moveFuns)
                        {
                            item.sort = short.Parse((item.sort - short.Parse(currFuns.Count.ToString())).ToString());
                            tran.CommandSet<SysFunctionCfgEntity>().Update(item);
                        }
                    }
                });
                result.Code = ActionCodes.Success;
                result.Data = 1;
            }
            return result;
        }

        /// <summary>
        /// 上下一定子功能（递归）
        /// </summary>
        /// <param name="adminFuns"></param>
        /// <param name="funId"></param>
        /// <param name="move"></param>
        private void GetMoveNode(List<SysFunctionCfgEntity> adminFuns, string funId, List<SysFunctionCfgEntity> move)
        {
            List<SysFunctionCfgEntity> borgs = adminFuns.FindAll(r => r.parent_id == funId);
            foreach (SysFunctionCfgEntity item in borgs)
            {
                if (item.parent_id == funId)
                {
                    move.Add(item);
                    List<SysFunctionCfgEntity> tempFuns = adminFuns.FindAll(r => r.parent_id == item.id);
                    if (tempFuns != null && tempFuns.Count > 0)
                    {
                        GetMoveNode(adminFuns, item.id, move);
                    }
                }
            }
        }

        /// <summary>
        /// 菜单启用与停用操作
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ForbiddenInfo(string id)
        {

            var resultInfo = new ResultJsonInfo<int>();

            var user = GetLoginUserInfo();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {

                    var infoList = tran.QuerySet<SysFunctionCfgEntity>().Where(p => p.id == id || p.parent_id == id).ToList();
                    if (infoList != null && infoList.Count > 0)
                    {

                        var info = infoList.Find(p => p.id == id);
                        if (info.is_valid)
                        {
                            foreach (var item in infoList)
                            {
                                if (item.is_valid)
                                {
                                    item.is_valid = false;
                                }
                                else
                                {
                                    item.is_valid = true;
                                }
                                item.modifier_date = DateTime.Now;
                                item.modifier_id = user.id;
                                item.modifier_name = user.name;
                                tran.CommandSet<SysFunctionCfgEntity>().Update(item);
                            }
                        }
                        else
                        {
                            if (info.is_valid)
                            {
                                info.is_valid = false;
                            }
                            else
                            {
                                info.is_valid = true;
                            }
                            tran.CommandSet<SysFunctionCfgEntity>().Update(info);
                        }
                        resultInfo.Code = ActionCodes.Success;
                        resultInfo.Msg = "操作成功！";
                        AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.FunctionManage, $"禁用/启用菜单成功，禁用/启用菜单信息：{JsonHelper.ToJson(id)}");

                    }
                    else
                    {
                        resultInfo.Code = ActionCodes.ArgumentInvalid;
                        resultInfo.Msg = "id无效！";
                    }
                });
            }
            return resultInfo;
        }

        /// <summary>
        /// 删除节点
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> Remove(string[] ids)
        {

            var resultInfo = new ResultJsonInfo<int>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {

                    var result = 0;
                    if (ids.Length >= 0)
                    {
                        var listInfo = new List<SysFunctionCfgEntity>();
                        var allInfo = tran.QuerySet<SysFunctionCfgEntity>().Where(p => p.is_deleted == false).ToList();
                        var rootInfo = allInfo.FindAll(p => ids.Contains(p.id));
                        if (rootInfo.Count > 0)
                        {
                            listInfo.AddRange(rootInfo);
                            listInfo.AddRange(allInfo.GetChildList(rootInfo));
                        }
                        var listCode = listInfo.Select(p => p.id).ToArray();

                        //查看是否被角色使用，如果没使用，可以删除
                        var roleMenuCount = tran.QuerySet<SysRolePermissionReEntity>().Where(p => p.id.PostIn(listCode)).Count();
                        if (roleMenuCount == 0)
                        {
                            //逻辑删除菜单
                            result = tran.CommandSet<SysFunctionCfgEntity>().Where(p => p.id.PostIn(listCode))
                                        .Update(a =>
                                            new SysFunctionCfgEntity
                                            {
                                                is_deleted = true
                                            });

                            if (result > 0)
                            {
                                resultInfo.Code = ActionCodes.Success;
                                resultInfo.Data = result;
                                resultInfo.Msg = "操作成功！";

                                AddOperationLog(OperationLogType.ModifyOperation, BusinessTitleType.FunctionManage, $"删除节点成功，删除节点ID信息：{JsonHelper.ToJson(ids)}");
                            }
                            else
                            {
                                resultInfo.Msg = "操作失败！";
                            }
                        }
                        else
                        {
                            resultInfo.Msg = "选中菜单已被角色已使用，无法删除！";
                        }
                    }
                    else
                    {
                        resultInfo.Msg = "id为空，操作失败！";
                    }
                });
            }
            return resultInfo;
        }
        #endregion

        #endregion

        #region 权限配置相关

        /// <summary>
        /// 根据角色ID，获取所有可操作菜单已赋权限情况
        /// </summary>
        /// <param name="queryInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<List<FunctionCfgRoleMenuResponse>> LoadListInfoByRoleId(FunctionCfgRoleQueryRequest queryInfo)
        {
            var resultInfo = new ResultJsonInfo<List<FunctionCfgRoleMenuResponse>>();

            var menusList = new List<FunctionCfgRoleMenuResponse>();

            List<SysFunctionCfgEntity> listInfo = null;
            List<SysRolePermissionReEntity> sysRoleMenu = null;

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {

                con.Transaction(tran =>
                {

                    listInfo = tran.QuerySet<SysFunctionCfgEntity>()
                              .Where(p => p.is_deleted == false && p.is_valid == true)
                              .OrderBy(p => p.sort)
                              .ToList();

                    int permissionType = (int)PermissionType.Function;
                    sysRoleMenu = tran.QuerySet<SysRolePermissionReEntity>()
                        .Join<SysRolePermissionReEntity, SysFunctionCfgEntity>((a, b) => a.permission_id == b.id)
                        .From<SysRolePermissionReEntity, SysFunctionCfgEntity>()
                        .OrderBy<SysFunctionCfgEntity>(p => p.sort)
                        .Where((a, b) => a.permission_type.Equals(permissionType) && b.is_valid == true && a.role_id == queryInfo.roleId)
                        .ToList((a, b) => new SysRolePermissionReEntity
                        {
                            id = a.id,
                            role_id = a.role_id,
                            permission_id = a.permission_id,
                            permission_type = a.permission_type
                        });

                    menusList = listInfo.MapToList<FunctionCfgRoleMenuResponse>();

                    foreach (var item in menusList)
                    {
                        if (sysRoleMenu.Exists(p => p.permission_id == item.id))
                        {
                            item.lay_is_checked = true;
                        }
                    }
                });
            }

            if (listInfo.Count > 0)
            {
                resultInfo.Code = ActionCodes.Success;
                resultInfo.Data = menusList;
                resultInfo.Count = menusList.Count;
                resultInfo.Msg = "获取成功！";
            }
            else
            {
                resultInfo.Msg = "无对应信息！";
            }
            return resultInfo;
        }

        /// <summary>
        /// 修改对应角色赋值的菜单操作权限
        /// </summary>
        /// <param name="modifyInfo"></param>
        /// <returns></returns>
        public ResultJsonInfo<int> ModifyRoleMenuInfo(FunctionCfgRoleModifyRequest modifyInfo)
        {
            var resultInfo = new ResultJsonInfo<int>();

            using (var con = PostgreDataBase.GetConnection(DatabaseName.PostgreSql_DB))
            {
                con.Transaction(tran =>
                {
                    var menuCodes = modifyInfo.menuIdList;

                    #region 获取当前选中节点和选中节点的子节点
                    var allInfo = tran.QuerySet<SysFunctionCfgEntity>().Where(p => p.is_deleted == false && p.is_valid == true)
                                                                .OrderBy(p=>p.sort)
                                                                .ToList();
                    var rootInfo = allInfo.FindAll(p => menuCodes.Contains(p.id));

                    var listInfo = new List<SysFunctionCfgEntity>();
                    if (rootInfo.Count > 0)
                    {
                        listInfo.AddRange(rootInfo);
                        listInfo.AddRange(allInfo.GetParentList(rootInfo));
                    }
                    menuCodes = listInfo.Select(t => t.id).Distinct().ToList();

                    #endregion

                    //获取该角色原有的菜单权限
                    int permissionType = (int)PermissionType.Function;
                    var roleMenuListInfo = tran.QuerySet<SysRolePermissionReEntity>()
                                    .Where(p => p.role_id.Equals(modifyInfo.roleid) && p.permission_type.Equals(permissionType))
                                    .ToList();

                    //1、获取需要删除的，删除掉。
                    var needRemoveRoleMenuListInfo = roleMenuListInfo.FindAll(p => !menuCodes.Contains(p.permission_id))
                                    .Select(p => p.permission_id)
                                    .ToArray();

                    //删除掉不用的菜单
                    tran.CommandSet<SysRolePermissionReEntity>()
                                    .Where(p => p.permission_id.PostIn(needRemoveRoleMenuListInfo) && p.role_id.Equals(modifyInfo.roleid))
                                    .Delete();

                    //2、获取需要增加的，增加上。
                    var needAddRoleMenuListInfo = menuCodes.ToList()
                                    .FindAll(p => !roleMenuListInfo.Select(roleMenu => roleMenu.permission_id).Contains(p));

                    SysRolePermissionReEntity roleMenuItem = null;
                    foreach (var item in needAddRoleMenuListInfo)
                    {
                        roleMenuItem = new SysRolePermissionReEntity();
                        roleMenuItem.id = GuidHelper.GetGuid();
                        roleMenuItem.role_id = modifyInfo.roleid;
                        roleMenuItem.permission_id = item;
                        roleMenuItem.permission_type = (int)PermissionType.Function;
                        tran.CommandSet<SysRolePermissionReEntity>().Insert(roleMenuItem);
                    }
                    resultInfo.Code = ActionCodes.Success;
                    resultInfo.Data = 1;
                    resultInfo.Msg = "操作成功！";
                });
            }
            return resultInfo;
        }

        #endregion

        #region 公用功能

        /// <summary>
        /// 获取系统类型Select列表信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<EnumToSelectItem>> LoadSystemTypeList()
        {

            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();
            resultInfo.Data = EnumToSelectItem.GetEnumDescriptionList(typeof(SystemType), true);
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Msg = "获取成功！";

            return resultInfo;
        }


        /// <summary>
        /// 获取功能类型Select列表信息
        /// </summary>
        /// <returns></returns>
        public ResultJsonInfo<List<EnumToSelectItem>> LoadFunctionTypeList()
        {

            var resultInfo = new ResultJsonInfo<List<EnumToSelectItem>>();
            resultInfo.Data = EnumToSelectItem.GetEnumDescriptionList(typeof(FunctionType), true);
            resultInfo.Code = ActionCodes.Success;
            resultInfo.Msg = "获取成功！";

            return resultInfo;
        }

        #endregion
    }
}
