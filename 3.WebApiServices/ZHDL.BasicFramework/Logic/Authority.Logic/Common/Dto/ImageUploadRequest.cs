﻿using Common.Model;
using Validate.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 图片上传请求实体
    /// </summary>
    public class ImageUploadRequest
    {

        /// <summary>
        /// 图片base64信息【不可空】
        /// </summary>
        [Validate(ValidateType.NotEmpty,Description = "图片base64信息")]
        public string imageBase64
        {
            get; set;
        }

        /// <summary>
        /// 原图片名称【可空】
        /// </summary>
        public string originalPic
        {
            get; set;
        }

        /// <summary>
        /// 文件夹名称【不可空】
        /// </summary>
        [Validate(ValidateType.NotEmpty, Description = "文件夹名称")]
        public UploadFolderType uploadFolder
        {
            get; set;
        }

    }
}
