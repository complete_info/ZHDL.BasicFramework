﻿
using Authority.Model;
using AutoMapper;
using Common.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class DepartmentProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public DepartmentProfile()
        {

            CreateMap<SysDepartmentInfoEntity, DepartmentResponse>();
            CreateMap<SysUserInfoEntity,DepartmentUserResponse>();
            CreateMap<DepartmentRootAddRequest, SysDepartmentInfoEntity>();

            CreateMap<DepartmentChildAddRequest, SysDepartmentInfoEntity>();

            CreateMap<DepartmentModifyRequest, SysDepartmentInfoEntity>();

        }
    }
}
