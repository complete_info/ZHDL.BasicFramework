﻿using System;

/*
* 命名空间: Authority.Model
*
* 功 能： 部门返回信息实体类
*
* 类 名： DepartmentResponse
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Authority.Logic
{
    /// <summary>
    /// 部门返回信息实体类
    /// </summary>
    public class DepartmentResponse
    {
        /// <summary>
        /// 唯一标识符
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 父节点唯一标识符
        /// </summary>
        public string parent_id
        {
            get; set;
        }

        /// <summary>
        /// 部门名称
        /// </summary>
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序
        /// </summary>
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 领导人名称
        /// </summary>
        public string leader
        {
            get; set;
        }

        /// <summary>
        /// 联系电话
        /// </summary>
        public string phone
        {
            get; set;
        }

        /// <summary>
        /// 邮件地址
        /// </summary>
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 创建者
        /// </summary>
        public string creator_id
        {
            get; set;
        }

        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string creator_name
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 修改者
        /// </summary>
        public string modifier_id
        {
            get; set;
        }

        /// <summary>
        /// 修改者姓名
        /// </summary>
        public string modifier_name
        {
            get; set;
        }

        /// <summary>
        /// 修改者日期
        /// </summary>
        public DateTime modifier_date
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄长【同级别，排前面的】
        /// </summary>
        public bool have_elder
        {
            get; set;
        }

        /// <summary>
        /// 是否有兄弟【同级别，排后面的】
        /// </summary>
        public bool have_younger
        {
            get; set;
        }
    }
}
