﻿using Validate.Library;
namespace Authority.Logic
{
    /// <summary>
    /// 部门根节点操作
    /// </summary>
    public class DepartmentRootAddRequest
    {

        /// <summary>
        /// 部门名称【要求非空，且最大长度100】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "部门名称")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 负责人【要求非空，且最大长度 50】
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 20, Description = "负责人")]
        public string leader
        {
            get; set;
        }

        /// <summary>
        /// 联系电话【要求满足，手机号码或座机号码格式】
        /// </summary>
        [Validate(ValidateType.MatchingRegex | ValidateType.MaxLength, Regex = ValidateRegex.TelphoneOrMobile, MaxLength = 100, Description = "联系电话")]
        public string phone
        {
            get; set;
        }

        /// <summary>
        /// 邮箱【要求满足，邮箱格式】
        /// </summary>
        [Validate(ValidateType.MatchingRegex | ValidateType.MaxLength, Regex = ValidateRegex.Email, MaxLength = 100, Description = "邮箱")]
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string remarks
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        public bool is_valid
        {
            get; set;
        }
    }
}
