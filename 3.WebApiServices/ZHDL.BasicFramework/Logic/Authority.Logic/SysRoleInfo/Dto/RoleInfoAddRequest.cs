﻿using System;
using System.Collections.Generic;
using System.Text;
using Validate.Library;

namespace Authority.Logic
{
    /// <summary>
    /// 添加角色数据传输
    /// </summary>
    public class RoleInfoAddRequest
    {

        /// <summary>
        /// 角色名称
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 50, Description = "角色名称")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [Validate(ValidateType.NotEmpty, Regex = ValidateRegex.Number, Description = "排序字段")]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 角色描述
        /// </summary>
        [Validate(ValidateType.NotEmpty | ValidateType.MaxLength, MaxLength = 100, Description = "角色描述")]
        public string describe
        {
            get; set;
        }

    }
}
