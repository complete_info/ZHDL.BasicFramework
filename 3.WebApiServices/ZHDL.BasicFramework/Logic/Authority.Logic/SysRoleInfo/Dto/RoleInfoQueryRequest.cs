﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authority.Logic
{
    /// <summary>
    /// 角色查询条件类
    /// </summary>
    public class RoleInfoQueryRequest
    {
        /// <summary>
        /// 关键字 角色名称 描述
        /// </summary>
        public string sKeyWords
        {
            get; set;
        }
    }
}
