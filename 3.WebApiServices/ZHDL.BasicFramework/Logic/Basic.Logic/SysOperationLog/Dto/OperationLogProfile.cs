﻿using AutoMapper;
using Basic.Model;

namespace Basic.Logic
{
    /// <summary>
    /// 权限相关映射
    /// </summary>
    public class OperationLogProfile : Profile
    {
        /// <summary>
        /// 权限数据实体与传输实体映射
        /// </summary>
        public OperationLogProfile()
        {

            CreateMap<SysOperationLogInfoEntity, OperationLogInfoResponse>(); 


        }
    }
}
