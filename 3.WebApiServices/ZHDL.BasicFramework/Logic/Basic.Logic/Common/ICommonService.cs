﻿using Common.Model;

namespace Basic.Logic
{
    /// <summary>
    /// 公用逻辑操作类接口
    /// </summary>
    public interface ICommonService
    {

        /// <summary>
        /// 上传图片
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<string> UploadImage(ImageUploadRequest inputInfo);
       
    }
}
