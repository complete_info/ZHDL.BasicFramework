﻿using System;
using System.Collections.Generic;
using System.Text;


/*
* 命名空间: Basic.Logic
*
* 功 能： 按时间分组的接口请求记录
*
* 类 名： ApiMonitorLogGroup
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/14 10:43:57 				罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{
    /// <summary>
    /// 按时间分组的接口请求记录
    /// </summary>
    public class ApiMonitorLogGroup
    {

        /// <summary>
        /// key
        /// </summary>
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime statistics_time
        {
            get; set;
        }

        /// <summary>
        /// 请求次数
        /// </summary>
        public int frequency
        {
            get; set;
        }
    }
}
