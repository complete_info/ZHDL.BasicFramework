﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Basic.Logic
{
    public class ApiMonitorLogResponse
    {
        /// <summary>
        /// 控制器名称
        /// </summary>
       
        public string controller_name
        {
            get; set;
        }

        /// <summary>
        /// Action名称
        /// </summary>
        public string action_name
        {
            get; set;
        }

        /// <summary>
        /// action参数
        /// </summary>
        
        public string action_params
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
       
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 请求开始时间
        /// </summary>
     
        public DateTime start_time
        {
            get; set;
        }

        /// <summary>
        /// 请求结束时间
        /// </summary>
      
        public DateTime end_time
        {
            get; set;
        }

        /// <summary>
        /// Http请求头
        /// </summary>
       
        public string http_header
        {
            get; set;
        }

        /// <summary>
        /// 请求的IP地址
        /// </summary>
       
        public string request_ip
        {
            get; set;
        }

        /// <summary>
        /// 请求结果返回码
        /// </summary>
       
        public string result_code
        {
            get; set;
        }

        /// <summary>
        /// 请求路径
        /// </summary>
       
        public string request_path
        {
            get; set;
        }

        /// <summary>
        /// Http请求方式
        /// </summary>
       
        public string http_method
        {
            get; set;
        }
    }
}
