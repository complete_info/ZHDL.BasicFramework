﻿using Common.Model;
using System;
using System.Collections.Generic;
/*
* 命名空间: Authority.Logic
*
* 功 能： 接口访问监控日志Redis操作类接口
*
* 类 名： IApiMonitorLogService
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/17 14:34:43 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{
    /// <summary>
    /// 接口访问监控日志Redis操作类接口
    /// </summary>
    public interface IApiMonitorLogService
    {

        #region 接口访问监控日志明细操作

        /// <summary>
        /// 根据访问监控日志条件分页获取列表 关键字【控制器名称】【方法名】  开始时间
        /// </summary>
        /// <param name="inputInfo"></param>
        /// <returns></returns>
        ResultJsonInfo<List<ApiMonitorLogResponse>> LoadApiMonitorLogList(ParametersInfo<ApiMonitorLogQueryRequest> inputInfo);

        /// <summary>
        /// 删除记录信息
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        ResultJsonInfo<int> Remove(ApiMonitorLogRequest request);

        #endregion


        #region 接口访问监控日志统计操作

        /// <summary>
        /// 统计接口请求信息
        /// </summary>
        /// <param name="seconds">向后偏移秒数</param>
        void StatisticsInterfaceRequest(int seconds);

        /// <summary>
        /// 获取接口实时访问统计信息
        /// </summary>
        /// <param name="timed"></param>
        /// <returns></returns>
        ResultJsonInfo<Echarts2DCoordinatesInfo> loadRequestInfo(DateTime timed);

        /// <summary>
        /// 获取某个时间段内的接口实时访问统计信息
        /// </summary>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <returns></returns>
        ResultJsonInfo<Echarts2DCoordinatesInfo> loadRequestInfoByTime(DateTime beginTime, DateTime endTime);
        #endregion
    }
}
