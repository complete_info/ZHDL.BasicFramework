﻿using Basic.Model;
using Common.Model;
using Redis.Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
/*
* 命名空间: Basic.Logic
*
* 功 能： 接口访问监控日志Redis操作类
*
* 类 名： ApiMonitorLogServiceRedis
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/10 11:13:35 	罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Basic.Logic
{
    /// <summary>
    /// 接口访问监控日志Redis操作类
    /// </summary>
    public class ApiMonitorLogServiceRedis
    {
        #region 明细操作

        /// <summary>
        /// 插入接口访问监控日志
        /// </summary>
        /// <param name="apiMonitorLogInfo"></param>
        /// <param name="successAction"></param>
        /// <param name="failAction"></param>
        public static void SaveApiMonitorLog(SysApiMonitorLogEntity apiMonitorLogInfo, Action successAction = null, Action failAction = null)
        {
            bool result = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSet(BasicRedisInfo.RedisApiMonitorHashId, apiMonitorLogInfo.id, apiMonitorLogInfo);
            if (result)
            {
                successAction?.Invoke();
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 获取访问监控日志信息
        /// </summary>
        /// <param name="successAction">成功执行</param>
        /// <param name="failAction">失败执行</param>
        /// <returns></returns>
        public static void GetAllApiMonitorLog(Action<List<SysApiMonitorLogEntity>> successAction = null, Action failAction = null)
        {
            var resultInfo = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashGetAll<SysApiMonitorLogEntity>(BasicRedisInfo.RedisApiMonitorHashId);
            if (resultInfo != null && resultInfo.Count > 0)
            {
                successAction?.Invoke(resultInfo);
            }
            else
            {
                failAction?.Invoke();
            }
        }

        /// <summary>
        /// 批量删除监控日志信息
        /// </summary>
        /// <param name="keys"></param>
        public static void RemoveApiMonitorLog(List<string> keys)
        {
            RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashDeletes(BasicRedisInfo.RedisApiMonitorHashId, keys);
        }

        /// <summary>
        /// 删除过期接口访问记录信息
        /// </summary>
        public static void RemoveExpiredApiMonitorLog()
        {
            var apiMonitorList = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashGetAll<SysApiMonitorLogEntity>(BasicRedisInfo.RedisApiMonitorHashId);
            if (apiMonitorList.Count > 0)
            {
                DateTime nowTime = DateTime.Now;
                List<string> keys = apiMonitorList.Where(p => p.start_time.AddSeconds(BasicRedisInfo.RedisApiMonitorTimeOut) <= nowTime).Select(p=>p.id).ToList();
                if (keys.Count > 0)
                {
                    RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashDeletes(BasicRedisInfo.RedisApiMonitorHashId, keys);

                    #region 删除任务日志记录
                    var apiMonitorLogList = RedisClient.GetRedisDb(BasicRedisInfo.RedisHangfireDbLog).QueueGetList(BasicRedisInfo.RedisHangfireSucceededListId);
                    List<string> hangfirejobList = new List<string>();
                    List<string> hangfirejobHistoryList = new List<string>();
                    List<string> hangfirejobStateList = new List<string>();
                    foreach (var item in apiMonitorLogList)
                    {
                        hangfirejobList.Add(BasicRedisInfo.RedisHangfireJobHashId + item);//hash
                        hangfirejobHistoryList.Add(BasicRedisInfo.RedisHangfireJobHashId + item + ":history");//list
                        hangfirejobStateList.Add(BasicRedisInfo.RedisHangfireJobHashId + item + ":state");//hash
                    }
                    RedisClient.GetRedisDb(BasicRedisInfo.RedisHangfireDbLog).StringKeysDelete(hangfirejobList);
                    RedisClient.GetRedisDb(BasicRedisInfo.RedisHangfireDbLog).StringKeysDelete(hangfirejobHistoryList);
                    RedisClient.GetRedisDb(BasicRedisInfo.RedisHangfireDbLog).StringKeysDelete(hangfirejobStateList);
                    RedisClient.GetRedisDb(BasicRedisInfo.RedisHangfireDbLog).StringKeyDelete(BasicRedisInfo.RedisHangfireSucceededListId);
                    #endregion
                }
            }
        }

        /// <summary>
        /// 清空Hangfire信息
        /// </summary>
        public static void EmptyHangfire() {

           RedisClient.GetRedisDb(BasicRedisInfo.RedisHangfireDbLog).Excute("flushdb",null);
        }


        #endregion

        #region 统计与安全操作

        /// <summary>
        /// 获取所有名单信息
        /// </summary>
        public static List<ApiMonitorLogNameList> GetAllNameList()
        {
            var resultInfo = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashGetAll<ApiMonitorLogNameList>(BasicRedisInfo.RedisApiMonitorNameListHashId);

            return resultInfo;

        }


        /// <summary>
        /// 插入按照请求路由统计的信息
        /// </summary>
        /// <param name="group"></param>
        public static bool SaveGroup(List<ApiMonitorLogGroup> group)
        {
            bool result = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSets(BasicRedisInfo.RedisApiMonitorStatisticsHashId, group.Select(p => p.id).ToList(), group);

            return result;
        }

        /// <summary>
        /// 获取所有接口请求统计信息
        /// </summary>
        public  static List<ApiMonitorLogGroup> GetAllApiMonitorLogGroup()
        {
            var resultInfo = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashGetAll<ApiMonitorLogGroup>(BasicRedisInfo.RedisApiMonitorStatisticsHashId);

            return resultInfo;
        }

        /// <summary>
        /// 插入按照请求路由统计的信息
        /// </summary>
        /// <param name="pathGroup"></param>
        public static bool SaveGroupPath(List<ApiMonitorLogPathGroup> pathGroup)
        {
            bool result = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSets(BasicRedisInfo.RedisApiMonitorPathStatisticsHashId, pathGroup.Select(p => p.id).ToList(), pathGroup);

            return result;
        }

        /// <summary>
        /// 插入按照请求路由和Ip统计的信息
        /// </summary>
        /// <param name="pathIpGroup"></param>
        public static bool SaveGroupPathIp(List<ApiMonitorLogPathIpGroup> pathIpGroup)
        {
            bool result = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSets(BasicRedisInfo.RedisApiMonitorPathIpStatisticsHashId, pathIpGroup.Select(p => p.id).ToList(),pathIpGroup);

            return result;
        }

        /// <summary>
        /// 删除过期接口访问记录统计信息
        /// </summary>
        public static void RemoveExpiredGroupInfo()
        {
            //按照请求路由统计的信息
            var groupPathList = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashGetAll<ApiMonitorLogPathGroup>(BasicRedisInfo.RedisApiMonitorPathStatisticsHashId);
            if (groupPathList.Count > 0)
            {
                DateTime nowTime = DateTime.Now;
                List<string> keys = groupPathList.Where(p=>p.statistics_time.AddSeconds(BasicRedisInfo.RedisApiMonitorTimeOut) <= nowTime)
                                    .Select(p=>p.id)
                                    .ToList();
                if (keys.Count > 0)
                {
                    RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashDeletes(BasicRedisInfo.RedisApiMonitorPathStatisticsHashId, keys);
                }
            }

            //按请求路由和Ip统计的信息
            var groupPathIpList = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashGetAll<ApiMonitorLogPathIpGroup>(BasicRedisInfo.RedisApiMonitorPathIpStatisticsHashId);
            if (groupPathIpList.Count > 0)
            {
                DateTime nowTime = DateTime.Now;

                List<string> keys = groupPathIpList.Where(p => p.statistics_time.AddSeconds(BasicRedisInfo.RedisApiMonitorTimeOut) <= nowTime)
                                    .Select(p => p.id)
                                    .ToList();
                if (keys.Count > 0)
                {
                    RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashDeletes(BasicRedisInfo.RedisApiMonitorPathIpStatisticsHashId, keys);
                }
            }
        }

        /// <summary>
        /// 插入名单信息
        /// </summary>
        /// <param name="nameList"></param>
        public static bool SaveNameList(ApiMonitorLogNameList nameList)
        {
            //这里是按照请求IP来做Key的，所以在记录中一个Ip对应一条数据。。。有了黑名单，就不会有警告
            bool result = RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSet(BasicRedisInfo.RedisApiMonitorNameListHashId, nameList.request_ip, nameList);

            return result;
        }

        #endregion
    }
}


