﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Basic.Controllers
{
    /// <summary>
    /// 定时任务管理
    /// </summary>
    [Area("Basic")]
    public class TimingTaskController : Controller
    {
        /// <summary>
        /// 界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
    }
}