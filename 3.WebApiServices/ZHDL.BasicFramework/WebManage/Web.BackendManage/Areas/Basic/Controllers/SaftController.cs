﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Basic.Controllers
{
    /// <summary>
    /// 接口安全管理
    /// </summary>
    [Area("Basic")]
    public class SaftController : Controller
    {
        /// <summary>
        /// 接口安全管理主页面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

    }
}