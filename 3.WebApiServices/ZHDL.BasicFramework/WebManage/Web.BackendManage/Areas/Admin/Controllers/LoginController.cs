﻿using Microsoft.AspNetCore.Mvc;


namespace Web.BackendManage.Areas.Admin.Controllers
{
    /// <summary>
    /// 登录控制器
    /// </summary>
    [Area("Admin")]
    public class LoginController : Controller
    {
        /// <summary>
        /// 登录界面
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }
    }
}