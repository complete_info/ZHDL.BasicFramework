﻿using Microsoft.AspNetCore.Mvc;


namespace Web.BackendManage.Areas.Admin.Controllers
{
    /// <summary>
    /// 主界面操作
    /// </summary>
    [Area("Admin")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public ActionResult Main()
        {
            return View();
        }

        /// <summary>
        /// 用户信息界面
        /// </summary>
        /// <returns></returns>
        public ActionResult UserInfo()
        {
            return View();
        }
        /// <summary>
        /// 修改密码界面
        /// </summary>
        /// <returns></returns>
        public ActionResult ChangePwd()
        {
            return View();

        }

        /// <summary>
        /// 用户头像操作界面
        /// </summary>
        /// <returns></returns>
        public ActionResult UserImg()
        {
            return View();

        }
        
    }
}