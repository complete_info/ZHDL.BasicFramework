﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Authority.Controllers
{
    [Area("Authority")]
    public class PostController : Controller
    {
        /// <summary>
        /// 岗位列表
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 新增岗位
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }
        /// <summary>
        /// 修改岗位
        /// </summary>
        /// <returns></returns>
        public IActionResult Modify()
        {
            return View();
        }
        /// <summary>
        /// 用户
        /// </summary>
        /// <returns></returns>
        public IActionResult User()
        {
            return View();
        }
    }
}