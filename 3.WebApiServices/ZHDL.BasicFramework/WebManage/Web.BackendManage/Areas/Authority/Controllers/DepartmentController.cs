﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Authority.Controllers
{
    [Area("Authority")]
    public class DepartmentController : Controller
    {
        /// <summary>
        /// 部门列表
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
        /// <summary>
        /// 新增部门
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }
        /// <summary>
        /// 修改部门
        /// </summary>
        /// <returns></returns>
        public IActionResult Modify()
        {
            return View();
        }

        /// <summary>
        /// 配置用户
        /// </summary>
        /// <returns></returns>
        public IActionResult User()
        {
            return View();
        }
    }
}