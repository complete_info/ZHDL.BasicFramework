﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Web.BackendManage.Areas.Authority.Controllers
{
    /// <summary>
    /// 用户管理控制器
    /// </summary>
    [Area("Authority")]
    public class UserController : Controller
    {

        /// <summary>
        /// 列表界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// 添加界面
        /// </summary>
        /// <returns></returns>
        public IActionResult Add()
        {
            return View();
        }

        /// <summary>
        /// 编辑界面
        /// </summary>
        public IActionResult Modify()
        {
            return View();
        }

        /// <summary>
        /// 修改密码
        /// </summary>
        /// <returns></returns>
        public IActionResult ChangePwd()
        {
            return View();
        }

    }
}