var GrdAdmin = {
    ///前端网页URI
    webURI: "http://localhost:8081",
    ajaxGet: "GET",
    ajaxPost: "POST",
    layer: null,//layer 弹框控件
    laydate: null,//日历控件
    laytable: null,//表格控件
    form: null,//表单
    treeGrid: null,//表单
    init: function () {
        if (window.top.layui.layer != null && window.top.layui.layer != undefined) {
            GrdAdmin.layer = window.top.layui.layer;
            GrdAdmin.laydate = layui.laydate;
            GrdAdmin.laytable = layui.table;
            GrdAdmin.form = layui.form;
            GrdAdmin.treeGrid = layui.treeGrid; //很重要
        } else {
            layui.use(['layer', 'form', 'table', 'laypage', 'laydate', 'jquery'], function () {
                GrdAdmin.layer = layui.layer;
                GrdAdmin.laydate = layui.laydate;
                GrdAdmin.laytable = layui.table;
                GrdAdmin.form = layui.form;
            });
        }
    },
    /*
    * jQuery        Ajax调用封装
    * url:			调用地址
    * data:			可选参数,表示Ajax调用参数
    * type          POST/GET方式
    * success:	    可选参数,成功回调函数,函数签名为  function(data), data参数为调用结果
    * onError:		可选参数,失败回调函数,函数签名为  function (XMLHttpRequest, textStatus, errorThrown)
    * modal:		可选参数,是否作为模态对话框显示，默认为true
    * datatype:		可选参数,Ajax返回数据类型,默认为 "json"
    * cache:		可选参数,Ajax调用是否启用缓存,默认为 false
    * async:		可选参数,是否异步调用，默认为true
    */
    ajaxRequest: function (param) {
        param.modal = (param.modal === false ? false : true);
        if (param.modal) {
            this.loading.start();
        }
        var userInfo = null;
        if (param.auth != false) {
            userInfo = GrdAdmin.getUserInfo();
        }
        var ajaxHandler =
            $.ajax({
                url: GrdAdmin.webURI + param.url,
                data: param.type != undefined && param.type != null ? param.data : JSON.stringify(param.data),
                type: param.type != undefined && param.type != null ? param.type : "POST",
                dataType: param.datatype ? param.datatype : "json",
                contentType: "application/json;charset=utf-8",
                headers: { "Authorization": userInfo != null ? "Bearer  " + JSON.parse(userInfo.token).auth_token : "" },//通过请求头来发送token，放弃了通过cookie的发送方式
                cache: param.cache ? param.cache : false,
                async: (param.async == null || param.async == undefined) ? true : param.async,
                success: function (json) {
                    if (param.modal) {
                        GrdAdmin.loading.end();
                    }
                    var result = json || null;
                    try {
                        result.Data = JSON.parse(result.Data || null);
                    } catch (e) {
                    }
                    switch (result.Code) {
                        case 4000:
                        case 4001:
                            GrdAdmin.msg(result.Data, "错误", function () {
                                top.window.location = '/Admin/Login/Index';
                            });
                            break;
                        default:
                            param.success(result);
                            if (!result.Success) {
                                //信息框
                                GrdAdmin.layer.alert(result.Msg, "警告");
                            }
                    }

                },
                //统一的错误信息处理
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    debugger
                    var error = (XMLHttpRequest && XMLHttpRequest.responseText) || '未知错误';
                    if (param.onError) {
                        param.onError(XMLHttpRequest, textStatus, errorThrown, error);
                    }
                    ///如果不是无权限访问，就走这里
                    if (errorThrown != "Unauthorized") {
                        //信息框
                        GrdAdmin.alert(error, "错误");
                    }
                    GrdAdmin.loading.end();
                },
                complete: function (XMLHttpRequest, status) {
                    //请求完成后最终执行参数
                    if (status == 'timeout') {
                        //超时,status还有success,error等值的情况
                        GrdAdmin.alert("访问超时!", "警告");
                        this.ajaxHandler.abort();
                        GrdAdmin.loading.end();
                    }
                },
                statusCode: {
                    500: function () {
                        GrdAdmin.alert("系统500错误!", "错误!");
                    },
                    401: function () {
                        GrdAdmin.msg("无操作权限！", "警告", function () {
                            window.top.location.href = "/Admin/Login/Index";
                        });
                    }
                }
            });
    },

    /**
    * ajax长连接
    * url:			调用地址
    * success:	    可选参数,成功回调函数,函数签名为  function(data), data参数为调用结果
    * cache:		可选参数,Ajax调用是否启用缓存,默认为 false
    * async:		可选参数,是否异步调用，默认为true
     * @param {any} param
     */
    ajaxLongPolling: function (param) {
        var userInfo = null;
        if (param.auth != false) {
            userInfo = GrdAdmin.getUserInfo();
        }
        var ajaxHandler =
            $.ajax({
                url: GrdAdmin.webURI + param.url,
                data: { timed: GrdAssist.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss") },
                type: "GET",
                timeout: 20000,//设置为20s后断开连接
                dataType: "json",
                contentType: "application/json;charset=utf-8",
                headers: { "Authorization": userInfo != null ? "Bearer  " + JSON.parse(userInfo.token).auth_token : "" },//通过请求头来发送token，放弃了通过cookie的发送方式
                cache: param.cache ? param.cache : false,
                async: (param.async == null || param.async == undefined) ? true : param.async,
                success: function (json) {
                    var result = json || null;
                    try {
                        result.Data = JSON.parse(result.Data || null);
                    } catch (e) {
                    }
                    switch (result.Code) {
                        case 4000:
                        case 4001:
                            GrdAdmin.msg(result.Data, "错误", function () {
                                top.window.location = '/Admin/Login/Index';
                            });
                            break;
                        default:
                            param.success(result);
                            GrdAdmin.ajaxLongPolling(param);
                    }
                },
                //统一的错误信息处理
                error: function (XMLHttpRequest, textStatus, errorThrown) {

                    var error = (XMLHttpRequest && XMLHttpRequest.responseText) || '未知错误';
                    if (param.onError) {
                        param.onError(XMLHttpRequest, textStatus, errorThrown, error);
                    }
                    //如果不是无权限访问，就走这里
                    if (errorThrown != "Unauthorized") {
                        //再次发起长连接
                        GrdAdmin.ajaxLongPolling(param);
                    }
                },
                complete: function (XMLHttpRequest, status) {
                    //请求完成后最终执行参数
                    if (status == 'timeout') {
                        //超时,status还有success,error等值的情况
                        this.ajaxHandler.abort();
                    }
                },
                statusCode: {
                    500: function () {
                        GrdAdmin.alert("系统500错误!", "错误!");
                    },
                    401: function () {
                        GrdAdmin.msg("无操作权限！", "警告", function () {
                            window.top.location.href = "/Admin/Login/Index";
                        });
                    }
                }
            });
    },

    /**
     * 保存User信息到userInfo
     * @param {any} data
     */
    setUserInfo: function (data) {
        window.sessionStorage.setItem("userInfo", JSON.stringify(data));
    },
    /**
     * 获取用户信息
     * */
    getUserInfo: function () {
        var userInfo = window.sessionStorage.getItem("userInfo");
        if (userInfo != undefined && userInfo != null && userInfo != "") {
            var dataObj = JSON.parse(userInfo);
            return dataObj;
        }
        return null;
    },

    setSession: function (key, value) {
        window.top.localStorage.setItem(key, JSON.stringify(value));
    },
    getSession: function (key) {
        return JSON.parse(window.top.localStorage.getItem(key));
    },

    /**
     * 界面参数set操作
     */
    setUrlParameters: function (value) {
        window.top.localStorage.setItem('UrlParameters', JSON.stringify(value));
    },

    /**界面参数get操作 */
    getUrlParameters: function () {
        var result = window.top.localStorage.getItem('UrlParameters')
        window.top.localStorage.removeItem('UrlParameters');
        return JSON.parse(result);
    },

    /**
     * 获取界面唯一标识符
     * */
    getUniqueIdentifier: function () {
        var data = window.top.localStorage.getItem('UniqueIdentifier');
        if (data != null && data != undefined) {
            return data;
        } else {
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 36; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[14] = "4";
            s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
            s[8] = s[13] = s[18] = s[23] = "-";
            data = s.join("");
            window.top.localStorage.setItem('UniqueIdentifier', data);
        }
        return data;

        //var data = window.top.localStorage.getItem('UniqueIdentifier');
        //if (data != null && data != undefined) {
        //    var dataObj = JSON.parse(data);
        //    if (new Date().getTime() - dataObj.time > 5 * 1000 * 60) {
        //        var s = [];
        //        var hexDigits = "0123456789abcdef";
        //        for (var i = 0; i < 36; i++) {
        //            s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        //        }
        //        s[14] = "4";
        //        s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
        //        s[8] = s[13] = s[18] = s[23] = "-";
        //        var uuid = s.join("");
        //        var curTime = new Date().getTime();
        //        window.top.localStorage.setItem('UniqueIdentifier', JSON.stringify({ data: uuid, time: curTime }));
        //    }  
        //    return dataObj;
        //} else {
        //    var s = [];
        //    var hexDigits = "0123456789abcdef";
        //    for (var i = 0; i < 36; i++) {
        //        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
        //    }
        //    s[14] = "4";
        //    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);
        //    s[8] = s[13] = s[18] = s[23] = "-";
        //    var uuid = s.join("");
        //    var curTime = new Date().getTime();
        //    var dataObj = { data: uuid, time: curTime };
        //    window.top.localStorage.setItem('UniqueIdentifier', JSON.stringify(dataObj));
        //    return dataObj;
        //}
    },
    //消息框
    alert: function (content, type, title) {
        if (type === "警告")
            return GrdAdmin.layer.alert(content, { icon: 0, offset: "auto", title: title || "警告" });
        else if (type === "成功")
            return GrdAdmin.layer.alert(content, { icon: 1, offset: "auto", title: title || "成功" });
        else if (type === "错误")
            return GrdAdmin.layer.alert(content, { icon: 2, offset: "auto", title: title || "错误" });
        else
            return GrdAdmin.layer.alert(content, { offset: "auto", title: title || "提醒" });
    },
    //提示框
    msg: function (content, type, func, time) {
        if (type === "警告")
            return GrdAdmin.layer.msg(content, { icon: 0, offset: "auto", time: time || 4 * 1000 }, func);
        else if (type === "成功")
            return GrdAdmin.layer.msg(content, { icon: 1, offset: "auto", time: time || 2 * 1000 }, func);
        else if (type === "错误")
            return GrdAdmin.layer.msg(content, { icon: 2, offset: "auto", time: time || 4 * 1000 }, func);
        else
            return GrdAdmin.layer.msg(content, { offset: "auto", time: time || 2 * 1000 }, func);
    },
    //询问框
    confirm: function (content, yes, cancel, title) {
        GrdAdmin.layer.confirm(content, { icon: 3, offset: "auto", title: title || "提醒" }, yes, cancel);
    },
    //加载
    loading: {
        index: 0,
        start: function () {
            if (GrdAdmin.layer != null) {
                this.index = GrdAdmin.layer.load(1, { shade: [0.1, "#fff"], time: 600 * 60 * 1000 });
            }
        },
        end: function () {
            var _this = this;
            if (GrdAdmin.layer != null) {
                GrdAdmin.layer.close(_this.index);
            }
        }
    },
    /**
     * 根据Iframe的name 获取Ifram对象 dx对象可不用传递
     * @param {any} name
     * @param {any} dx
     */
    getIframeObj: function (name, dx) {
        var f;
        if (dx == "" || dx == null || dx == undefined) {
            dx = "top"; f = eval(dx + ".frames");
        }
        else {
            f = eval(dx);
        }
        for (var i = 0; i < f.length; i++) {
            if (f[i].length > 0) {
                return GrdAdmin.getIframeObj(name, dx + ".frames['" + f[i].name + "']");
            }
            else {
                if (f[i].name == name) {
                    return dx + ".frames['" + name + "']";
                }
            }
        }
    },
    /**
     * 执行某个Iframe中的函数 funName：函数名（参数1，参数2，...）  iframeName：iframe名字
     * @param {any} funName
     * @param {any} iframeName
     */
    exFunction: function (funName, iframeName) {
        try {
            eval(GrdAdmin.getIframeObj(iframeName) + "." + funName + ";");
            return true;
        } catch (e) {
            throw new Error(e.message);
            return false;
        }
    },
    getParentFrameName: function () {
        return top.$("#" + GrdAdmin.getIframeName()).attr("data-parentiframename");
    },
    /**
     * 获取layeriframe窗口的index
     */
    getLayerIframeIndex: function () {
        return GrdAdmin.layer.getFrameIndex(window.name);
    },
    getIframeName: function () {
        return "layui-layer-iframe" + GrdAdmin.getLayerIframeIndex();
    },
    /*
     * 查找带回
     */
    findBack: {
        close: function (row) {
            var findback = GrdAdmin.getQueryString("findback");
            if (findback) {
                GrdAdmin.setSession('findBackJson', row);
                GrdAdmin.layer.close(GrdAdmin.getLayerIframeIndex());
            }
        },
        open: function (content, title, callBack, w, h, isFull, parentIframeName) {
            GrdAdmin.openWindow({
                content: content,
                title: title || "查找带回",
                IsFull: isFull,
                width: w ? w : "1200px",
                height: h ? h : "1200px",
                btn: false,
                success: function (layero) {
                    $(layero).find("iframe").attr("data-parentiframename", parentIframeName || GrdAdmin.getIframeName());
                },
                end: function () {
                    var findBackJson = GrdAdmin.getSession("findBackJson");
                    if (callBack && findBackJson && findBackJson.length > 0) {
                        callBack(findBackJson);
                    }
                    GrdAdmin.setSession("findBackJson", []);
                }
            });
        }
    },

    /**
     * 将图片对象转换为 base64
     * @param {any} obj
     * @param {any} callBack
     */
    readFile: function (obj, callBack) {
        var file = obj.files[0];
        var resVal;
        //判断类型是不是图片  
        if (!/image\/\w+/.test(file.type)) {
            alert("请确保文件为图像类型");
            return false;
        }
        var reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = function (e) {
            //alert(this.result); //就是base64  
            resVal = this.result;
            if (callBack) callBack(resVal);
            //return resVal;
        }

    },

    /**
     * Form清空
     */
    clearForm: function (FormId) {
        $("#" + FormId).find("input,textarea,select").each(function () {
            var type = $(this)[0].tagName.toLowerCase();
            var classText = $(this).attr("class");
            switch (type) {
                case "input":
                case "textarea":
                    $(this).val("");
                    if (classText != undefined && classText.indexOf("easyui") > -1) {
                        $(this).textbox('setValue', "");
                    }
                    break;
                case "select":
                    $(this).val("");
                    if (classText != undefined && classText.indexOf("easyui") > -1) {
                        $(this).textbox('setValue', "");
                    }
                    break;
                default:
            }
        });
    },
    /**
     * Form清空,ids中的保留
     */
    clearFormByCondition: function (FormId, ids) {
        $("#" + FormId).find("input,textarea,select").each(function () {
            var type = $(this)[0].tagName.toLowerCase();
            var classText = $(this).attr("class");
            var id = $(this).attr("id");

            if ($.inArray(id, ids) < 0) {
                switch (type) {
                    case "input":
                    case "textarea":
                        $(this).val("");
                        if (classText.indexOf("easyui") > -1) {
                            $(this).textbox('setValue', "");
                        }
                        break;
                    case "select":
                        $(this).val("");
                        if (classText.indexOf("easyui") > -1) {
                            $(this).textbox('setValue', "");
                        }
                        break;
                    default:
                }
            }
        });
    },

    /*
     * @pNode:from或table大界定范围
     * @param:pNodes dom对象
     * @summary:自动拉取pNode子集input/textarea、img值生成model
    */
    initParamsData: function (pNodes) {
        var params = {};
        for (var i = 0; i < pNodes.length; i++) {
            var saveNode = pNodes[i];
            ($("input:not(input:file,input:radio,input:checkbox),textarea", saveNode).not("[except]")).each(function () {
                params[(this.name || this.id)] = $(this).val().trim();
            });
            ($("input:radio:checked", saveNode).not("[except]")).each(function () {
                params[this.getAttribute("name")] = ($(this).val() || 0);
            });
            ($("input:checkbox", saveNode).not("[except]")).each(function () {
                params[(this.name || this.id)] = $(this).prop("checked");
            });
            ($("input:hidden[savedata]", saveNode).not("[except]")).each(function () {
                params[this.name] = $(this).val().trim();
            });
            ($("select", saveNode).not("[except]")).each(function () {
                params[(this.name || this.id)] = $(this).val();
                $(this).attr("isText") && (params[this.name] = $(this).find("option:selected").text());
            });
            ($("img", pNodes).not("[except]")).each(function () {
                params[(this.name || this.id)] = ($(this).attr('data-savesrc') || $(this).attr('src'));
            })
        }
        return params;
    },
    /**
     * 赋值
     * @param {any} pNodes
     *  @param {string} jsonStr
     */
    assignmentData: function (pNodes, jsonObj) {
        var jsonData = null;
        if (typeof jsonObj == "string") {
            jsonData = JSON.parse(decodeURIComponent(jsonObj));
        } else {
            jsonData = jsonObj;
        }
        for (var i = 0; i < pNodes.length; i++) {
            var node = pNodes[i];
            $('input:not(input:file,input:radio,input:checkbox),textarea').each(function () {

                //console.log("input:not(input:file,input:radio,input:checkbox),textarea ---" + this.name);
                //debugger

                var eValue = eval('jsonData.' + this.name);
                if (eValue) {
                    $(this).val(eValue);
                }
            });

            $("input[type='radio']").each(function () {

                //console.log("radio ---" + this.name);
                //debugger

                var eValue = eval('jsonData.' + this.getAttribute("name"));
                $("input[name='" + this.getAttribute("name") + "'][value=" + eValue + "]").attr("checked", true);
            });


            $("input:checkbox").each(function () {

                //console.log("checkbox ---" + this.name);
                //debugger

                var eValue = eval('jsonData.' + this.name);
                if (eValue) {
                    $(this).attr("checked", "checked");
                    //$(this).prop("checked") = eValue;
                }
            });

            $("input:hidden[savedata]").each(function () {

                //console.log("savedata ---" + this.name);
                //debugger

                var eValue = eval('jsonData.' + this.name);
                if (eValue) {
                    $(this).val(eValue);
                }
            });

            $("select").each(function () {

                //console.log("select ---" + this.name);
                //debugger

                var eValue = eval('jsonData.' + this.name);
                if (eValue) {
                    $(this).val(eValue);
                }
            });

            $("img").each(function () {

                //console.log("img ---" + this.name);
                //debugger

                var eValue = eval('jsonData.' + this.name);
                if (eValue) {
                    $(this).attr('src') = eValue;
                    $(this).attr('data-savesrc') = eValue;
                }
            })
        }
    },
    /**
     * 表单赋值
     * @param {any} formId
     * @param {any} jsonObj
     */
    assignmentFormData: function (formId, jsonObj) {
        var jsonData = null;
        if (typeof jsonObj == "string") {
            jsonData = JSON.parse(decodeURIComponent(jsonObj));
        } else {
            jsonData = jsonObj;
        }
        GrdAdmin.form.val(formId, jsonData);
    },

    /**
     * 初始化table
     * @param {any} param
     * param.dom  容器
     * param.url 请求地址
     * param.title cols
     */
    tableInit: function (param) {
        var userInfo = null;
        if (param.auth != false) {
            userInfo = GrdAdmin.getUserInfo();
        }
        //列表
        GrdAdmin.laytable.render({
            elem: param.dom,
            headers: { "Authorization": userInfo != null ? "Bearer  " + JSON.parse(userInfo.token).auth_token : "" },//通过请求头来发送token，放弃了通过cookie的发送方式
            cellMinWidth: 100,
            method: 'post',
            contentType: "application/json;charset=utf-8",
            url: GrdAdmin.webURI + param.url,
            where: {
                field: param.field,
                order: param.order,
                parameters: param.parameters
            },
            page: true,
            sort: true, //重点1：这里的sort表示 table表在取得接口数据后，对页面渲染后的table数据进行排序。同时，这里的true 会影响页面sort 上下小箭头的 显示效果
            height: param.height != undefined ? param.height : "full-110",
            limits: [15, 30, 45, 60],
            limit: 15,
            parseData: function (res) { //res 即为原始返回的数据
                return {
                    "code": 0, //解析接口状态
                    "msg": res.Msg, //解析提示文本
                    "count": res.Count, //解析数据长度
                    "data": res.Data //解析数据列表
                };
            },
            cols: [param.title]
        });
        param.checkbox = (param.checkbox === false ? false : true);
        if (param.checkbox) {
            //单击行勾选checkbox事件
            $(document).on("click", ".layui-table-body table.layui-table tbody tr", function () {
                var index = $(this).attr('data-index');
                var tableBox = $(this).parents('.layui-table-box');
                //存在固定列
                if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length > 0) {
                    tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
                } else {
                    tableDiv = tableBox.find(".layui-table-body.layui-table-main");
                }
                var CheckLength = tableDiv.find("tr[data-index=" + index + "]").find(
                    "td div.layui-form-checked").length;

                var checkCell = tableDiv.find("tr[data-index=" + index + "]").find(
                    "td div.laytable-cell-checkbox div.layui-form-checkbox I");
                if (checkCell.length > 0) {
                    checkCell.click();
                }
            });
            $(document).on("click", "td div.laytable-cell-checkbox div.layui-form-checkbox", function (e) {
                e.stopPropagation();
            });
        }

        //对于单选框按钮点击行的选中功能：
        param.radio = (param.radio === false ? false : true);
        if (param.radio == true) {
            $(document).on("click", ".layui-table-body table.layui-table tbody tr", function () {
                /*当单击表格行时,把单选按钮设为选中状态*/
                var tableDiv;
                var index = $(this).attr('data-index');
                var tableBox = $(this).parents('.layui-table-box');
                //存在固定列
                if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length > 0) {
                    tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
                } else {
                    tableDiv = tableBox.find(".layui-table-body.layui-table-main");
                }
                var checkCell = tableDiv.find("tr[data-index=" + index + "]").find("td div.layui-table-cell div.layui-form-radio I");
                if (checkCell.length > 0) {
                    checkCell.click();
                }
            });
            //对td的单击事件进行拦截停止，防止事件冒泡再次触发上述的单击事件  将此代码在页面初始化后执行一次即可以。
            $(document).on("click", "td div.layui-table-cell div.layui-form-radio", function (e) {
                e.stopPropagation();
            });
        }
    },
    /**
     * table重载
     * */
    tableReload: function (param) {
        GrdAdmin.laytable.reload(
            param.domId,
            {
                page: {
                    curr: 1 //重新从第 1 页开始
                },
                where: {
                    field: param.field,
                    order: param.order,
                    parameters: param.parameters
                }
            });
    },
    /**
     * table //排序监听
     */
    tableSort: function (param) {

        GrdAdmin.laytable.on('sort(' + param.domId + ')',
            function (obj) {
                param.backAction(obj);
                //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
                //obj.field 当前排序的字段名
                //obj.type 当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
                //self.Parameters.field = obj.field;
                //self.Parameters.order = obj.type;
                //self.logicFunc.loadListInfo();
            });
    },
    /**
     * 获取选中数据
     */
    getCheckData: function (param) {
        var checkStatus = GrdAdmin.laytable.checkStatus(param.domId);
        var data = checkStatus.data;
        if (data.length <= 0) {
            GrdAdmin.msg("请选择需操作行数据", "警告");
            return;
        }
        if (param.backAction) {
            param.backAction(data);
        } else {
            return data;
        }
    },

    //获取单选数据
    getSingleCheckData: function (param) {
        var checkStatus = GrdAdmin.laytable.checkStatus(param.domId);
        var data = checkStatus.data;
        if (data.length <= 0) {
            GrdAdmin.msg("请选择需操作行数据", "警告");
            return;
        }
        if (data.length > 1) {
            GrdAdmin.msg("请选择一行需操作行数据", "警告");
            return;
        }
        if (param.backAction) {
            param.backAction(data[0]);
        } else {
            return data;
        }
    },

    /**
     * 根据DomId删除选中信息
     * @param {any} param
     */
    removeDataByDomId: function (param) {
        var checkStatus = GrdAdmin.laytable.checkStatus(param.domId);
        var data = checkStatus.data;
        if (data.length <= 0) {
            GrdAdmin.msg("请选择需操作行数据", "警告");
            return;
        }
        GrdAdmin.confirm("确定删除选中信息吗？", function () {
            var arrayObj = new Array();　//创建一个数组
            $.each(data, function (n, value) {
                arrayObj.push(value.id);
            });
            GrdAdmin.ajaxRequest({
                url: param.removeUrl,
                data: arrayObj,
                success: function (result) {
                    if (result.Code == 1000) {
                        GrdAdmin.msg(result.Msg, "成功", function () {
                            if (param.backAction) {
                                param.backAction(result);
                            } else {
                                return data;
                            }
                        });
                    } else {
                        GrdAdmin.msg(result.Msg, "警告");
                    }
                }
            });
        });
    },

    /**
     * 根据传入信息删除信息
     * @param {any} param
     */
    removeDataByInfo: function (param) {
        var paramData = param.data;
        if (paramData.length <= 0) {
            GrdAdmin.msg("请选择需操作行数据", "警告");
            return;
        }
        GrdAdmin.confirm("确定删除信息吗？", function () {
            //判断是否为数组  返回Boolean
            var bool = $.isArray(paramData);
            //创建一个数组
            var arrayObj = new Array();
            if (bool) {
                $.each(paramData, function (n, value) {
                    arrayObj.push(value.id);
                });
            } else {
                arrayObj.push(paramData.id);
            }
            GrdAdmin.ajaxRequest({
                url: param.removeUrl,
                data: arrayObj,
                success: function (result) {
                    if (result.Code == 1000) {
                        GrdAdmin.msg(result.Msg, "成功", function () {
                            if (param.backAction) {
                                param.backAction(result);
                            } else {
                                return data;
                            }
                        });
                    } else {
                        GrdAdmin.msg(result.Msg, "警告");
                    }
                }
            });
        });
    }
};
//GrdAdmin 框架初始化
GrdAdmin.init();