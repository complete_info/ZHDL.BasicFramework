﻿layui.use(['layer', 'laytpl', 'jquery'], function () {

    var layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        player = parent.layer || layui.layer;

    var adminLoginMainLogic = {

        //初始化
        init: function () {
            self = this;
            ///全局参数
            this.Parameters = {
                getValidateCodeUrl: "/api/Login/LoadValidateCode",
                loginCheckUrl: "/api/Login/LoginCheck",
                homeUrl: "/Admin/Home/Index"
            };
            //页面初始化
            this.initPage();
            //时间初始化
            this.bindEvent();

        },
        //初始化列表
        initPage: function () {

             //加载验证码
            self.events.loadValidate();
        },
        //绑定事件
        bindEvent: function () {

            //获取验证码
            $("#img-vialdcode").bind("click", function () {
                self.events.loadValidate();
            });
            //登录
            $("#SubLogin").click(function () {
                self.events.login();
            });
            $(document).keydown(function (event) {
                if (event.keyCode == 13) {
                    self.events.login();
                }
            });
        },
        //事件
        events: {
            //加载验证码
            loadValidate: function () {
                GrdAdmin.ajaxRequest({
                    auth: false,
                    url: self.Parameters.getValidateCodeUrl,
                    data: {
                        uniqueIdentifier: GrdAdmin.getUniqueIdentifier()
                    },
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        $("#img-vialdcode").attr("src", "data:image/jpg;base64," + result.FileContents);
                    }
                });
            },
            //登录
            login: function () {

                var name = $("#name").val();
                var password = $("#password").val();
                var vialdcode = $("#vialdcode").val();

                if (name == "") { GrdAdmin.msg("请输入用户名！", "警告"); return; }
                if (password == "") { GrdAdmin.msg("请输入密码！", "警告"); return; }
                if (vialdcode == "") { GrdAdmin.msg("请输入验证码！", "警告"); return; }

                var json = {
                    name: name,
                    password: password,
                    vialdcode: vialdcode,
                    uniqueIdentifier: GrdAdmin.getUniqueIdentifier()
                };

                GrdAdmin.ajaxRequest({
                    auth: false,
                    url: self.Parameters.loginCheckUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            //sessionStorage用于本地存储一个会话(session)中的数据，这些数据只有在同一个会话中的页面才能访问，并且当会话结束之后数据也随之销毁。
                            //因此sessionStorage不是一种持久化的本地存储，仅仅是会话级别的存储。
                            //sessionStorage生命周期为当前窗口或标签页，一旦窗口或标签页被永久关闭了，那么所有通过sessionStorage存储的数据也就被清空了。
                            //localStorage生命周期是永久，这意味着除非用户显示在浏览器提供的UI上清除localStorage信息，否则这些信息将永远存在。
                            GrdAdmin.setUserInfo(result.Data);
                            $("#SubLogin").val('登录成功，正在跳转');
                            location.href = self.Parameters.homeUrl;
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            }
        },
        //工具
        tools: {

        }
    };
    adminLoginMainLogic.init();
});

