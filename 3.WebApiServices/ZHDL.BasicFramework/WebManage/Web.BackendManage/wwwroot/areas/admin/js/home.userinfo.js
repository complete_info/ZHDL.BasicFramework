﻿layui.use(['form', 'layedit', 'layer', 'laydate', 'jquery'], function () {

    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer;

    var adminHomeUserInfoLogic = {

        //初始化
        init: function () {
            self = this;
            ///全局参数
            this.Parameters = {
                acquireUrl: "/api/Authority/UserInfo/LoadUserSingle",
                submitUrl: "/api/Authority/UserInfo/ModifyOneself",

                uploadImgUrl: "/Admin/Home/UserImg",//用户上传头像界面
            };
            //页面初始化
            this.initPage();
            //事件初始化
            this.bindEvent();
        },
        //初始化页面数据
        initPage: function () {
            GrdAdmin.ajaxRequest({
                url: self.Parameters.acquireUrl,
                data: "",
                type: GrdAdmin.ajaxGet,
                success: function (result) {
                    if (result.Code == 1000) {

                        $("#userFace").attr("src", GrdAdmin.webURI + result.Data.picture_url); //头像
                        $(".name").val(result.Data.name);
                        $(".email").val(result.Data.email);
                        $(".displayname").val(result.Data.displayname);
                        $(".realname").val(result.Data.realname);
                        $(".remarks").val(result.Data.remarks);
                        $(".nickname").val(result.Data.nickname);
                        $(".qq").val(result.Data.qq);
                        $(".mobile_phone").val(result.Data.mobile_phone);
                        $(".telphone").val(result.Data.telphone);
                        $(".office_phone").val(result.Data.office_phone);
                        $(".idnumber").val(result.Data.idnumber);
                        $(".user_describe").val(result.Data.user_describe);
                        $("input[name=gender][value=" + result.Data.gender + "]").attr("checked", true);//性别

                    } else {
                        GrdAdmin.msg(result.Msg, "警告");
                    }
                }
            });
            //更新全部
            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //上传图片
            $(".userFaceBtn").on("click", self.logicFunc.uploadImg);

            //监听立即修改提交
            form.on('submit(changeUser)', self.logicFunc.saveInfo);

        },
        //事件
        logicFunc: {
         
            //上传图片
            uploadImg: function (obj) {

                var callback_name = "loadUploadImgPage_callback_" + new Date().getTime();
                window[callback_name] = function (name, fullUrl) {
                    $(".userFaceBtn").attr({ src: GrdAdmin.webURI + fullUrl + "?v=" + Math.random(), "data-savesrc": GrdAdmin.webURI + fullUrl });
                };

                var headerPic = "";
                if ($(".userFaceBtn").prop("src").indexOf("?")>-1) {
                    headerPic = $(".userFaceBtn").prop("src").substring($(".userFaceBtn").prop("src").lastIndexOf("/") + 1, $(".userFaceBtn").prop("src").indexOf("?"));
                } else {
                    headerPic = $(".userFaceBtn").prop("src").substring($(".userFaceBtn").prop("src").lastIndexOf("/") + 1);  
                }

                var index = layui.layer.open({
                    title: "图片上传",
                    type: 2,
                    resize: false,
                    area: ['820px', '650px'],
                    content: self.Parameters.uploadImgUrl + '?' + $.param({
                        originalPic: headerPic,
                        uploadFolder: "UserImage",
                        callback: callback_name,
                        width: 350,
                        height: 350
                    }),
                    end: function () {
                        delete window[callback_name];
                    }
                });
            },

            //确认修改
            saveInfo: function () {

                var json = GrdAdmin.initParamsData($("#modify-form"));

                if (json.picture_url.indexOf(GrdAdmin.webURI) > -1) {
                    json.picture_url = json.picture_url.replace(GrdAdmin.webURI, "");
                }
                json.picture_url = json.picture_url.replace("/images/", "/src/");

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.submitUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {

                            //修改用户本地缓存
                            var userInfo = GrdAdmin.getUserInfo();
                            userInfo.displayname = json.displayname;
                            userInfo.picture_url = json.picture_url;
                            GrdAdmin.setUserInfo(userInfo);

                            GrdAdmin.msg("修改成功", "成功");
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            }
        },
        //工具
        tools: {

        }
    };
    adminHomeUserInfoLogic.init();
});

