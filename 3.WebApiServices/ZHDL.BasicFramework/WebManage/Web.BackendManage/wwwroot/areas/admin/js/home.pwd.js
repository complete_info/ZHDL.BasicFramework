﻿layui.use(['layer', 'form', 'laytpl', 'jquery'], function () {

    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer;

    var adminHomePwdLogic = {

        //初始化
        init: function () {
            self = this;
            ///全局参数
            this.Parameters = {
                changePasswordUrl: "/api/Authority/UserInfo/ChangePassword"

            };
            //页面初始化
            this.initPage();
            //事件初始化
            this.bindEvent();

        },
        //初始化页面数据
        initPage: function () {
            //添加验证规则
            form.verify({
                newPwd: function (value, item) {
                    if (value.length < 6) {
                        return "密码长度不能小于6位";
                    }
                },
                confirmPwd: function (value, item) {
                    if (!new RegExp($(".newpassword").val()).test(value)) {
                        return "两次输入密码不一致，请重新输入！";
                    }
                }
            })
        },
        //绑定事件
        bindEvent: function () {
            //监听立即修改提交
            form.on('submit(modifyButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);
        },
        //事件
        logicFunc: {

            //确认修改
            saveInfo: function () {

                var json = GrdAdmin.initParamsData($("#modify-form"));
                if (json.password != json.newpassword) { GrdAdmin.msg("两次输入密码不一致！", "警告"); return; }
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.changePasswordUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功");

                            //获取窗口索引
                            var index = player.getFrameIndex(window.name);
                            //关闭子页面
                            player.close(index);

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {

        }
    };
    adminHomePwdLogic.init();
});

