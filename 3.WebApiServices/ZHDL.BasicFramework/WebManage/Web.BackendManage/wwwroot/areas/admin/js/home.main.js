layui.use(['layer', 'form', 'laytpl', 'jquery'], function () {

    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        laytpl = layui.laytpl,
        player = parent.layer || layui.layer;

    var adminHomeMainLogic = {

        //初始化
        init: function () {

            self = this;

            ///全局参数
            this.Parameters = {
                ac: new AsyncCaller(),    //异步方法管理
                messageCountUrl: "/api/Basic/Message/LoadMyselfMessageCount",//获取对应时间段类，自己的总的消息，未读消息数量
                currentMessageUrl: "/api/Basic/Message/LoadUnreadByTime",//获取对应时间段内，自己未读消息
                sendInfoUrl: "/api/Basic/Message/SendMessage",//发送信息
                readMessageUrl: "/api/Basic/Message/ReadMessage",//查看消息
                removeMessageUrl: "/api/Basic/Message/Remove",

                operationCountUrl: "/api/Basic/OperationLog/LoadOperationCount",//获取对应时间段内，操作日志数量
                operationLatestListUrl: "/api/Basic/OperationLog/LoadLatestList",//获取最新操作信息

                exceptionCountUrl: "/api/Basic/ExceptionLog/LoadExceptionCount",//获取对应时间段内，异常信息数量
                exceptionLatestListUrl: "/api/Basic/ExceptionLog/LoadLatestList",//获取最异常日志信息
            };

            this.Parameters.ac
                .pushQueue(function () {

                    //获取最新未读信息
                    self.logicMessageFunc.loadCurrentMessage(function () {
                        //页面初始化
                        self.initPage();
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });

                })
                .pushQueue(this.bindEvent)
                .exec();

            //this.initPage();
            //this.bindEvent();
        },
        //初始化列表
        initPage: function () {
            //获取消息数量信息
            self.logicMessageFunc.loadMessageCount();
            //获取异常日志数量信息
            self.logicLogFunc.loadExceptionCount();
            //获取最新操作日志列表
            self.logicLogFunc.loadOperationList();


            //获取对应时间段内，操作日志数量
            self.logicLogFunc.loadOperationCount();
            //获取最新异常日志数量列表
            self.logicLogFunc.loadExceptionList();
        },
        //绑定事件
        bindEvent: function () {

            //回复
            $(".replyNote").unbind("click").click(function () {
                var receiver_id = $(this).attr("data-sender_id");
                var id = $(this).attr("data-id");
                self.logicMessageFunc.replayMassage(receiver_id, id);
            });

            //删除
            $(".removeNote").unbind("click").click(function () {
                var data = {
                    id: $(this).attr("data-id")
                };
                self.logicMessageFunc.removeInfo(data);
            });
        },
        //消息事件
        logicMessageFunc: {

            //获取消息数量信息
            loadMessageCount: function () {

                var beginTime = GrdAssist.formatDate(self.tools.loadPastTime(7), "yyyy-MM-dd HH:mm:ss");
                var endTime = GrdAssist.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.messageCountUrl,
                    modal:false,
                    type: GrdAdmin.ajaxGet,
                    data: {
                        beginTime: beginTime,
                        endTime: endTime
                    },
                    success: function (result) {
                        if (result.Code == 1000) {
                            $(".latest_news").text(result.Data.total+"(条)");
                            $(".latest_news_unread").text(result.Data.unread_count + "(条)");
                        } 
                    }
                });
            },

            //获取最新未读信息
            loadCurrentMessage: function (func) {
                var beginTime = GrdAssist.formatDate(self.tools.loadPastTime(7), "yyyy-MM-dd HH:mm:ss");
                var endTime = GrdAssist.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.currentMessageUrl,
                    modal: false,
                    type: GrdAdmin.ajaxGet,
                    data: {
                        beginTime: beginTime,
                        endTime: endTime
                    },
                    success: function (result) {
                        if (result.Code == 1000) {
                            for (var i = 0; i < result.Data.length; i++) {
                                result.Data[i].create_date = GrdAssist.formatDate(result.Data[i].create_date, 'MM月dd日 HH点mm分');
                            }
                            var latestNewsTpl = latestNews.innerHTML;
                            laytpl(latestNewsTpl).render(result.Data, function (html) {
                                $(".latestNews").html(html);
                            });
                        }
                        if (func) {
                            func();
                        }
                    }
                });

            },

            //回复消息
            replayMassage: function (receiver_id, id) {
                var index = layer.open({
                    type: 1,
                    title: "回复消息",
                    resize: true,
                    area: ['500px', '410px'],
                    content: laytpl($('#massage_reply').html()).render({ data: receiver_id }),
                    btn: ['确认回复', '关闭'],
                    yes: function (index, layero) {
                        var receiver_id = layero.find('input[name="receiver_id"]')[0].value;
                        var title = layero.find('textarea[name="title"]')[0].value;
                        var content = layero.find('textarea[name="content"]')[0].value;
                        if (title != null && title != "" && content != null && content != "") {

                            GrdAdmin.ajaxRequest({
                                url: self.Parameters.sendInfoUrl,
                                modal: false,
                                data: {
                                    user_ids: [receiver_id],
                                    title: title,
                                    content: content
                                },
                                success: function (result) {
                                    if (result.Code == 1000) {
                                        GrdAdmin.msg(result.Msg, "成功", function () {

                                            layer.close(index); //如果设定了yes回调，需进行手工关闭

                                            self.logicMessageFunc.readMassage(id);

                                        });
                                    } else {
                                        GrdAdmin.msg(result.Msg, "警告");
                                    }
                                }
                            });
                        } else  {
                           GrdAdmin.msg("标题和内容不能为空！", "警告");
                        }
                    },
                    btn2: function (index) {
                       layer.close(index);
                    },
                    end: function () {
                        $(window).unbind("resize");
                    }
                });
            },

            //查看信息
            readMassage: function (id) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.readMessageUrl,
                    modal: false,
                    type: GrdAdmin.ajaxGet,
                    data: { id:id },
                    success: function (result) {
                        if (result.Code == 1000) {

                            //获取最新未读信息
                            self.logicMessageFunc.loadCurrentMessage(function () {
                                //页面初始化
                                self.initPage();
                            });
                        }
                    }
                });
            },

            //删除信息
            removeInfo: function (data) {
                GrdAdmin.removeDataByInfo({
                    data: data,
                    removeUrl: self.Parameters.removeMessageUrl,
                    backAction: function (data) {

                        //获取最新未读信息
                        self.logicMessageFunc.loadCurrentMessage(function () {
                            //页面初始化
                            self.initPage();
                        });

                    }
                });
            },
            
        },
        //日志事件
        logicLogFunc: {

            //获取对应时间段内，操作日志数量
            loadOperationCount: function () {

                var beginTime = GrdAssist.formatDate(self.tools.loadPastTime(7), "yyyy-MM-dd HH:mm:ss");
                var endTime = GrdAssist.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.operationCountUrl,
                    modal: false,
                    type: GrdAdmin.ajaxGet,
                    data: {
                        beginTime: beginTime,
                        endTime: endTime
                    },
                    success: function (result) {
                        if (result.Code == 1000) {
                            $(".login_times").text(result.Data.login_times + "(次)");
                            $(".operation_times").text(result.Data.operation_times + "(次)");
                        }
                    }
                });
            },

            //获取最新操作日志列表
            loadOperationList: function () {

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.operationLatestListUrl,
                    modal:false,
                    data: {},
                    success: function (result) {
                        if (result.Code == 1000) {

                            for (var i = 0; i < result.Data.length; i++) {
                                result.Data[i].create_date = GrdAssist.formatDate(result.Data[i].create_date, 'MM月dd日 HH点mm分');
                                result.Data[i].detail = result.Data[i].detail.replace('"','')
                            }
                            var operationListTpl = operationList.innerHTML;
                            laytpl(operationListTpl).render(result.Data, function (html) {
                                $(".operationList").html(html);
                            });
                        }
                    }
                });

            },

            //获取异常日志数量信息
            loadExceptionCount: function () {

                var beginTime = GrdAssist.formatDate(self.tools.loadPastTime(7), "yyyy-MM-dd HH:mm:ss");
                var endTime = GrdAssist.formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.exceptionCountUrl,
                    modal: false,
                    type: GrdAdmin.ajaxGet,
                    data: {
                        beginTime: beginTime,
                        endTime: endTime
                    },
                    success: function (result) {
                        if (result.Code == 1000) {

                            $(".unresolved_error").text(result.Data.unresolved_count + "(个)");
                            $(".error_total").text(result.Data.total + "(个)");

                        }
                    }
                });
            },

            //获取最新异常日志数量列表
            loadExceptionList: function () {

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.exceptionLatestListUrl,
                    modal: false,
                    data: {},
                    success: function (result) {
                        if (result.Code == 1000) {

                            for (var i = 0; i < result.Data.length; i++) {
                                result.Data[i].create_date = GrdAssist.formatDate(result.Data[i].create_date, 'MM月dd日 HH点mm分');
                                //result.Data[i].detail = result.Data[i].detail.replace('"', '')
                            }
                            var exceptionListTpl = exceptionList.innerHTML;
                            laytpl(exceptionListTpl).render(result.Data, function (html) {
                                $(".exceptionList").html(html);
                            });
                        }
                    }
                });

            },
        },

        //工具
        tools: {

            loadPastTime: function (day) {
                var pastDay = new Date();
                pastDay.setDate(pastDay.getDate() - day);
                return pastDay;
            }
        }
    };
    adminHomeMainLogic.init();
})


