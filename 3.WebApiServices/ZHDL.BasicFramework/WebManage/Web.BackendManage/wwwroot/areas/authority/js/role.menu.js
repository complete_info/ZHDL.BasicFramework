﻿layui.config({
    base: '/Lib/layui/extend/'
}).extend({
    treeGrid: 'treeGrid'
}).use(['form', 'layer', 'table', 'laypage', 'laytpl', 'laydate', 'tree', 'treeGrid', 'jquery'], function () {
    var $ = layui.jquery,
        form = layui.form,
        player = parent.layer || layui.layer,
        treeGrid = layui.treeGrid; //很重要

    var authorityRoleMenuLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),    //异步方法管理
                functionypeList: undefined,//功能类型
                queryData: undefined,
                rowData: undefined,//选中行信息

                functionTypeListUrl: "/api/Authority/FunctionCfg/LoadFunctionTypeList",
                menusListsUrl: "/api/Authority/FunctionCfg/LoadListInfoByRoleId",
                modifyRoleMenuInfoUrl: "/api/Authority/FunctionCfg/ModifyRoleMenuInfo",
            };

            this.Parameters.ac
                .pushQueue(function () {
                    self.logicFunc.loadFunctionTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                })
                .pushQueue(function () {
                    self.initPage();
                    self.Parameters.ac.notifyQueueAsyncFuncComplate();
                })
                .pushQueue(this.bindEvent)
                .exec();
        },
        //初始化列表
        initPage: function () {

            //获取信息
            self.Parameters.queryData = GrdAdmin.getUrlParameters();
            self.logicFunc.loadListInfo();
        },
        //绑定事件
        bindEvent: function () {

            //监听提交
            form.on('submit(menuButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);
        },
        //逻辑方法
        logicFunc: {

            //获取功能类型列表信息
            loadFunctionTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.functionTypeListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            self.Parameters.functionypeList = result.Data;
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },

            //获取数据
            loadListInfo: function () {

                var title =
                    [
                        { type: 'checkbox' },
                        {
                            field: 'name', title: '菜单名称', width: '180',
                            templet: function (d) {
                                return '<a><i class="iconfont ' + d.icon_font + '" data-icon="' + d.icon_font + '" ></i><cite>' + d.name + '</cite></a>';
                            }
                        },
                        { field: 'icon_font', title: '标签icon', width: '200' },
                        { field: 'link_url', title: '链接地址', width: '230' },
                        { field: 'system_type_name', title: '系统类型', width: '180' },
                        {
                            field: 'function_type', title: '功能类型', width: '100',
                            templet: function (d) {
                                for (var i = 0; i < self.Parameters.functionypeList.length; i++) {
                                    if (self.Parameters.functionypeList[i].Value == d.function_type) {
                                        return self.Parameters.functionypeList[i].Text
                                    }
                                }
                            }
                        }
                    ];
                var userInfo = GrdAdmin.getUserInfo();
                var treeTable = treeGrid.render({
                    elem: '#menusList',
                    headers: { "Authorization": userInfo != null ? "Bearer  " + JSON.parse(userInfo.token).auth_token : "" },//通过请求头来发送token，放弃了通过cookie的发送方式
                    url: GrdAdmin.webURI + self.Parameters.menusListsUrl,
                    where: {
                        roleId:self.Parameters.queryData.id
                    },
                    cellMinWidth: 100,
                    contentType: "application/json;charset=utf-8",
                    height: "full-100",
                    idField: 'id',//必須字段
                    treeId: 'id',//树形id字段名称
                    treeUpId: 'parent_id',//树形父id字段名称
                    treeShowName: 'name',//以树形式显示的字段
                    isOpenDefault: true,//节点默认是展开还是折叠【默认展开】
                    cheDisabled: true,
                    isPage: false,
                    cols: [title],
                    onRadio: function (obj) {
                        self.Parameters.rowData = obj;
                    }
                });

                //单击行勾选checkbox事件
                $(document).on("click", ".layui-table-body table.layui-table tbody tr", function () {
                    var index = $(this).attr('data-index');
                    var tableBox = $(this).parents('.layui-table-box');
                    //存在固定列
                    if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length > 0) {
                        tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
                    } else {
                        tableDiv = tableBox.find(".layui-table-body.layui-table-main");
                    }
                    var CheckLength = tableDiv.find("tr[data-index=" + index + "]").find(
                        "td div.layui-form-checked").length;

                    var checkCell = tableDiv.find("tr[data-index=" + index + "]").find(
                        "td div.laytable-cell-checkbox div.layui-form-checkbox I");
                    if (checkCell.length > 0) {
                        checkCell.click();
                    }
                });

                $(document).on("click", "td div.laytable-cell-checkbox div.layui-form-checkbox", function (e) {
                    e.stopPropagation();
                });
            },

            //保存数据
            saveInfo: function () {

                var parametersList = new Array();　//创建一个数组

                var selectMenusListInfo = treeGrid.treeFindRowCheck("menusList");

                $.each(selectMenusListInfo, function (n, value) { 
                    parametersList.push(value.id);
                });  

                var json = {
                    menuIdList: parametersList,
                    roleid: self.Parameters.queryData.id
                };
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.modifyRoleMenuInfoUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },

            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    authorityRoleMenuLogic.init();
});
