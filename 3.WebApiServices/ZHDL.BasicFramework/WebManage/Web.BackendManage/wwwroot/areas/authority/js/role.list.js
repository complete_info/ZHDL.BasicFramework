﻿layui.config({
    base: '/lib/layui/extend/'
}).extend({
    excel: 'excel'
}).use(['form', 'layer', 'table', 'laypage', 'jquery', 'upload', 'excel', 'laytpl', 'element', 'code'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table,
        player = parent.layer || layui.layer;
    var upload = layui.upload;
    var layer = layui.layer;
    var excel = layui.excel;
    var laytpl = layui.laytpl;
    var element = layui.element;

    var authorityRoleListLogic = {

        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),    //异步方法管理
                field: 'sort',
                order: 'asc',
                listsUrl: "/api/Authority/RoleInfo/LoadList",
                forbiddenUrl: "/api/Authority/RoleInfo/ForbidOrEnable",
                removeUrl: "/api/Authority/RoleInfo/Remove",
                bulkimportUrl: "/api/Authority/RoleInfo/BulkImportRole",
                tranBulkimportUrl: "/api/Authority/RoleInfo/TranBulkImportRole",
                listAll: "/api/Authority/RoleInfo/ListAll",

                addUrl: "/Authority/Role/Add",
                modifyUrl: "/Authority/Role/Modify",
                menuUrl: "/Authority/Role/Menu",
                layerUrl: "/Authority/Role/Layer",
            };

            this.initPage();
            this.bindEvent();
            //upload上传实例
            var uploadInst = upload.render({
                elem: '#funBtnUpload' //绑定元素
                , url: '/upload/' //上传接口（PS:这里不用传递整个 excel）
                , auto: false //选择文件后不自动上传
                , accept: 'file'
                , choose: function (obj) {// 选择文件回调
                    var files = obj.pushFile()
                    var fileArr = Object.values(files)// 注意这里的数据需要是数组，所以需要转换一下

                    // 用完就清理掉，避免多次选中相同文件时出现问题
                    for (var index in files) {
                        if (files.hasOwnProperty(index)) {
                            delete files[index]
                        }
                    }
                    $('#funBtnUpload').next().val('');
                    self.tools.uploadExcel(fileArr);
                    // uploadExcel(fileArr) // 如果只需要最新选择的文件，可以这样写： uploadExcel([files.pop()])
                }
            });
        },
        //初始化列表
        initPage: function () {
            var title =
                [{ type: 'checkbox', fixed: 'left' },
                { field: 'id', title: 'id', hide: true },
                {
                    field: 'Rnumber', title: '编号', width: 60, align: "center", fixed: 'left',
                    templet: function (d) {
                        //每页都从1开始 return d.LAY_INDEX; //只有第一页才从1开始，（相当于数据表的所有记录数的下标。）
                        return d.LAY_INDEX;
                    }
                },
                { field: 'name', title: '角色名称', width: 130, align: "center" },
                { field: 'sort', title: '排序', width: 80, align: "center" },
                { field: 'describe', title: '描述', width: 250 },
                { field: 'is_valid', title: '是否启用', width: 140, templet: '#switchValidTpl', align: "center" },
                { field: 'opt', title: '操作', toolbar: '#gridTool', width: '280', align: 'left' }
                ];

            //table加载
            GrdAdmin.tableInit({
                dom: '#roleList',
                url: self.Parameters.listsUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: {
                    sKeyWords: $("#txtKeyWord").val()
                },
                title: title
            });

            //排序监听
            GrdAdmin.tableSort({
                domId: "roleList",
                backAction: function (obj) {
                    self.Parameters.field = obj.field;
                    self.Parameters.order = obj.type;
                    self.logicFunc.loadListInfo();
                }
            });
        },
        //绑定事件
        bindEvent: function () {

            //查询
            $("#funBtnSearch").on("click", function () {
                self.logicFunc.loadListInfo();
            });

            //新增
            $("#funBtnAdd").on("click", function () {
                self.logicFunc.addInfo();
            });

            //修改
            $("#funBtnEdit").on("click", function () {
                self.logicFunc.modifyInfo();
            });
            //批量导出
            $("#funBtnDerive").on("click", function () {
                self.tools.exportApiDemo();
            });
            //菜单
            $("#funBtnMenu").on("click", function () {
                self.logicFunc.menuInfo();
            });

            //图层
            $("#funBtnLayer").on("click", function () {
                self.logicFunc.layerInfo();
            });

            //删除
            $("#funBtnRemove").on("click", function () {
                self.logicFunc.removeInfo();
            });

            //监听(列表操作)
            table.on('tool(roleList)', function (obj) {
                var data = obj.data;
                switch (obj.event) {
                    case "funBtnEdit"://修改
                        self.logicFunc.modifyInfo(data);
                        break;
                    case "funBtnMenu"://菜单
                        self.logicFunc.menuInfo(data);
                        break;
                    case "funBtnLayer"://图层
                        self.logicFunc.layerInfo(data);
                        break;
                    case "funBtnRemove"://删除
                        self.logicFunc.removeInfo(data);
                        break;
                }
            });

            //监听启用/停用操作
            form.on('switch(funBtnValid)', function (obj) {
                self.logicFunc.forbiddenInfo(this.value);
            });
        },
        //逻辑方法
        logicFunc: {

            ///获取数据
            loadListInfo: function () {
                GrdAdmin.tableReload({
                    domId: "roleList",
                    field: self.Parameters.field,
                    order: self.Parameters.order,
                    parameters: {
                        sKeyWords: $("#txtKeyWord").val()
                    }
                });
            },

            //增加信息
            addInfo: function () {
                var index = layui.layer.open({
                    title: "增加角色信息",
                    type: 2,
                    resize: false,
                    area: ['500px', '350px'],
                    content: self.Parameters.addUrl,
                    end: function () {
                        $(window).unbind("resize");
                    }
                });
                //layui.layer.full(index);
            },

            //非事务批量导入
            bulkimport: function (data) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.bulkimportUrl,
                    data: data,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //事务批量导入
            tranbulkimport: function (data) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.tranBulkimportUrl,
                    data: data,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //修改信息
            modifyInfo: function (data) {
                if (data != undefined && data != null) {

                    //存储界面传递参数
                    GrdAdmin.setUrlParameters(data);

                    var index = layui.layer.open({
                        title: "修改角色信息",
                        type: 2,
                        resize: false,
                        area: ['500px', '380px'],
                        content: self.Parameters.modifyUrl,
                        end: function () {
                            $(window).unbind("resize");
                        }
                    });
                } else {
                    GrdAdmin.getSingleCheckData({
                        domId: "roleList",
                        backAction: function (data) {

                            //存储界面传递参数
                            GrdAdmin.setUrlParameters(data);

                            var index = layui.layer.open({
                                title: "修改角色信息",
                                type: 2,
                                resize: false,
                                area: ['500px', '380px'],
                                content: self.Parameters.modifyUrl,
                                end: function () {
                                    $(window).unbind("resize");
                                }
                            });
                        }
                    });
                }
            },

            //启用或停用
            forbiddenInfo: function (id) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.forbiddenUrl,
                    data: {
                        id: id
                    },
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },

            //菜单
            menuInfo: function (data) {
                if (data != undefined && data != null) {

                    //存储界面传递参数
                    GrdAdmin.setUrlParameters(data);

                    var index = layui.layer.open({
                        title: "角色授权-菜单信息",
                        type: 2,
                        resize: false,
                        area: ['720px', '700px'],
                        content: self.Parameters.menuUrl,
                        end: function () {
                            $(window).unbind("resize");
                        }
                    });

                } else {

                    GrdAdmin.getSingleCheckData({
                        domId: "roleList",
                        backAction: function (data) {

                            //存储界面传递参数
                            GrdAdmin.setUrlParameters(data);

                            var index = layui.layer.open({
                                title: "角色授权-菜单信息",
                                type: 2,
                                resize: false,
                                area: ['720px', '700px'],
                                content: self.Parameters.menuUrl,
                                end: function () {
                                    $(window).unbind("resize");
                                }
                            });
                        }
                    });
                }
            },

            //图层
            layerInfo: function (data) {
                if (data != undefined && data != null) {

                    //存储界面传递参数
                    GrdAdmin.setUrlParameters(data);

                    var index = layui.layer.open({
                        title: "角色授权-图层信息",
                        type: 2,
                        resize: false,
                        area: ['720px', '700px'],
                        content: self.Parameters.layerUrl,
                        end: function () {
                            $(window).unbind("resize");
                        }
                    });

                } else {
                    GrdAdmin.getSingleCheckData({
                        domId: "roleList",
                        backAction: function (data) {

                            //存储界面传递参数
                            GrdAdmin.setUrlParameters(data);

                            var index = layui.layer.open({
                                title: "角色授权-图层信息",
                                type: 2,
                                resize: false,
                                area: ['720px', '700px'],
                                content: self.Parameters.layerUrl,
                                end: function () {
                                    $(window).unbind("resize");
                                }
                            });
                        }
                    });
                }
            },

            //删除信息
            removeInfo: function (data) {

                //单选触发
                if (data != undefined && data != null) {

                    GrdAdmin.removeDataByInfo({
                        data: data,
                        removeUrl: self.Parameters.removeUrl,
                        backAction: function (data) {
                            self.logicFunc.loadListInfo();
                        }
                    });

                } else {
                    //多选触发
                    GrdAdmin.removeDataByDomId({
                        domId: "roleList",
                        removeUrl: self.Parameters.removeUrl,
                        backAction: function (data) {
                            self.logicFunc.loadListInfo();
                        }
                    });
                }
            }
        },
        //工具
        tools: {
            uploadExcel: function (files) {
                try {
                    excel.importExcel(files, {
                        // 可以在读取数据的同时梳理数据
                        /*fields: {
                          'id': 'A'
                          , 'username': 'B'
                          , 'experience': 'C'
                          , 'sex': 'D'
                          , 'score': 'E'
                          , 'city': 'F'
                          , 'classify': 'G'
                          , 'wealth': 'H'
                          , 'sign': 'I'
                        }*/
                    }, function (data, book) {

                        // data: {1: {sheet1: [{id: 1, name: 2}, {...}]}}// 工作表的数据对象
                        // book: {1: {Sheets: {}, Props: {}, ....}} // 工作表的整个原生对象，https://github.com/SheetJS/js-xlsx#workbook-object
                        // 也可以全部读取出来再进行数据梳理
                        data = excel.filterImportData(data, {
                            'name': 'A',
                            'sort': 'B',
                            'describe': 'C'

                        })

                        // 如果不需要展示直接上传，可以再次 $.ajax() 将JSON数据通过 JSON.stringify() 处理后传递到后端即可
                        /**
                         * 2019-06-21 JeffreyWang 应群友需求，加一个单元格合并还原转换
                         * 思路：
                         * 1. 渲染时为每个cell加上唯一的ID，demo里边采用 table-export-文件索引-sheet名称-行索引-列索引
                         * 2. 根据 book[文件索引].Sheets[sheet名称]['!merge'] 参数，取左上角元素设置 colspan 以及 rowspan，并删除其他元素
                         */
                        layer.open({
                            title: '文件转换结果'
                            , area: ['799px', '399px']
                            , tipsMore: true
                            , content: laytpl($('#excelExportTpl').html()).render({ data: data, files: files })
                            , btn: ['非事务导入', '事务导入']
                            , yes: function (index, layero) {
                                var da = eval('data[0].' + book[0].SheetNames[0]);
                                da.splice(0, 1);
                                self.logicFunc.bulkimport(da);
                            }
                            , btn2: function (index, layero) {
                                var da = eval('data[0].' + book[0].SheetNames[0]);
                                da.splice(0, 1);
                                self.logicFunc.tranbulkimport(da);
                            }

                            , success: function () {
                                element.render('tab')
                                //layui.code({})
                                // 处理合并
                                for (var file_index in book) {
                                    if (!book.hasOwnProperty(file_index)) {
                                        continue
                                    }
                                    // 遍历每个Sheet
                                    for (var sheet_name in book[file_index].Sheets) {
                                        if (!book[file_index].Sheets.hasOwnProperty(sheet_name)) {
                                            continue
                                        }
                                        var sheetObj = book[file_index].Sheets[sheet_name]
                                        // 仅在有合并参数时进行操作
                                        if (!sheetObj['!merges']) {
                                            continue
                                        }
                                        // 遍历每个Sheet中每个 !merges
                                        for (var merge_index = 0; merge_index < sheetObj['!merges'].length; merge_index++) {
                                            var mergeObj = sheetObj['!merges'][merge_index]
                                            // 每个合并参数的 s.c 表示左上角单元格的列，s.r 表示左上角单元格的行，e.c 表示右下角单元格的列，e.r 表示右下角单元格的行，计算时注意 + 1
                                            $('#table-export-' + file_index + '-' + sheet_name + '-' + mergeObj.s.r + '-' + mergeObj.s.c)
                                                .prop('rowspan', mergeObj.e.r - mergeObj.s.r + 1)
                                                .prop('colspan', mergeObj.e.c - mergeObj.s.c + 1)
                                            for (var r = mergeObj.s.r; r <= mergeObj.e.r; r++) {
                                                for (var c = mergeObj.s.c; c <= mergeObj.e.c; c++) {
                                                    // 排除左上角
                                                    if (r === mergeObj.s.r && c === mergeObj.s.c) {
                                                        continue
                                                    }
                                                    $('#table-export-' + file_index + '-' + sheet_name + '-' + r + '-' + c).remove()
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        });

                    })
                } catch (e) {
                    layer.alert(e.message)
                }
            },

            /**
            * 导出接口数据的样例
            * @return {[type]} [description]
            */
            exportApiDemo: function () {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.listAll,
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        var data = result.Data;
                        // 重点！！！如果后端给的数据顺序和映射关系不对，请执行梳理函数后导出
                        data = excel.filterExportData(data, {
                            name: 'name'
                            , sort: 'sort'
                            , describe: 'describe'
                            , is_valid: 'is_valid'
                        })
                        // 重点2！！！一般都需要加一个表头，表头的键名顺序需要与最终导出的数据一致
                        data.unshift({
                            name: "角色名",
                            sort: "排序",
                            describe: '描述',
                            is_valid: '是否启用'
                        })

                        var timestart = Date.now()
                        excel.exportExcel({
                            sheet1: data
                        }, '导出接口数据.xlsx', 'xlsx')
                        //var timeend = Date.now()

                        //var spent = (timeend - timestart) / 1000
                        //layer.alert('单纯导出耗时 ' + spent + ' s')
                    }
                    , error: function () {
                        layer.alert('获取数据失败，请检查是否部署在本地服务器环境下')
                    }
                });

             
            }

        }
    };
    authorityRoleListLogic.init();
});
