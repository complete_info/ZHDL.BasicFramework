﻿layui.use(['form', 'layer', 'table', 'laypage', 'jquery'], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table,
        player = parent.layer || layui.layer;

    var authorityPostListLogic = {

        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),    //异步方法管理
                field: 'sort',
                order: 'asc',
                listsUrl: "/api/Authority/Post/LoadList",
                addUrl: "/Authority/Post/Add",
                forbiddenUrl: "/api/Authority/Post/ForbidOrEnable",
                removeUrl: "/api/Authority/Post/Remove",
                modifyUrl: "/Authority/Post/Modify",
                userUrl:"/Authority/Post/User"
            };

            this.initPage();
            this.bindEvent();
        },
        //初始化列表
        initPage: function () {
            var title =
                [{ type: 'checkbox', fixed: 'left' },
                { field: 'id', title: 'id', hide: true },
                {
                    field: 'Rnumber', title: '编号', width: 60, align: "center", fixed: 'left',
                    templet: function (d) {
                        //每页都从1开始 return d.LAY_INDEX; //只有第一页才从1开始，（相当于数据表的所有记录数的下标。）
                        return d.LAY_INDEX;
                    }
                },
                { field: 'name', title: '岗位名称', width: 130, align: "center" },
                { field: 'sort', title: '排序', width: 80, align: "center" },
                    { field: 'brevity_code', title: '简码', width: 250 },
                    { field: 'remarks', title: '备注', width: 250 },
                { field: 'is_valid', title: '是否启用', width: 140, templet: '#switchValidTpl', align: "center" },
                { field: 'opt', title: '操作', toolbar: '#gridTool', width: '240', align: 'left' }
                ];

            //table加载
            GrdAdmin.tableInit({
                dom: '#postList',
                url: self.Parameters.listsUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: $("#txtKeyWord").val(),
                title: title,
                onRadio: function (obj) {
                    self.Parameters.rowData = obj;
                }
            });

            //排序监听
            GrdAdmin.tableSort({
                domId: "postList",
                backAction: function (obj) {
                    self.Parameters.field = obj.field;
                    self.Parameters.order = obj.type;
                    self.logicFunc.loadListInfo();
                }
            });
        },
        //绑定事件
        bindEvent: function () {

            //查询
            $("#funBtnSearch").on("click", function () {
                self.logicFunc.loadListInfo();
            });

            //新增
            $("#funBtnAdd").on("click", function () {
                self.logicFunc.addInfo();
            });

            //修改
            $("#funBtnEdit").on("click", function () {
                self.logicFunc.modifyInfo();
            });
            //用户
            $("#funBtnMenu").on("click", function () {
                self.logicFunc.menuInfo();
            });
            
            //删除
            $("#funBtnRemove").on("click", function () {
                self.logicFunc.removeInfo();
            });

            //监听(列表操作)
            table.on('tool(postList)', function (obj) {
                var data = obj.data;
                switch (obj.event) {
                    case "funBtnEdit"://修改
                        self.logicFunc.modifyInfo(data);
                        break;
                    case "funBtnMenu"://用户
                        self.logicFunc.menuInfo(data);
                        break;
                    case "funBtnRemove"://删除
                        self.logicFunc.removeInfo(data);
                        break;
                }
            });
            //监听启用/停用操作
            form.on('switch(funBtnValid)', function (obj) {
                self.logicFunc.forbiddenInfo(this.value);
            });
        },
        //逻辑方法
        logicFunc: {

            ///获取数据
            loadListInfo: function () {
                GrdAdmin.tableReload({
                    domId: "postList",
                    field: self.Parameters.field,
                    order: self.Parameters.order,
                    parameters:$("#txtKeyWord").val()
                    
                });
            },

            //用户
            menuInfo: function (data) {

                if (data != undefined && data != null) {

                    //存储界面传递参数
                    GrdAdmin.setUrlParameters(data);

                    var index = layui.layer.open({
                        title: "配置用户",
                        type: 2,
                        resize: false,
                        area: ['70%', '700px'],
                        content: self.Parameters.userUrl,
                        end: function () {
                            $(window).unbind("resize");
                        }
                    });

                } else {
                    GrdAdmin.getSingleCheckData({
                        domId: "postList",
                        backAction: function (data) {

                            //存储界面传递参数
                            GrdAdmin.setUrlParameters(data);

                            var index = layui.layer.open({
                                title: "配置用户",
                                type: 2,
                                resize: false,
                                area: ['70%', '700px'],
                                content: self.Parameters.userUrl,
                                end: function () {
                                    $(window).unbind("resize");
                                }
                            });
                        }
                    });
                }
            },
            //增加信息
            addInfo: function () {
                var index = layui.layer.open({
                    title: "增加角色信息",
                    type: 2,
                    resize: false,
                    area: ['500px', '350px'],
                    content: self.Parameters.addUrl,
                    end: function () {
                        $(window).unbind("resize");
                    }
                });
                //layui.layer.full(index);
            },

            //修改信息
            modifyInfo: function (data) {
                if (data != undefined && data != null) {
                    //存储界面传递参数
                    GrdAdmin.setUrlParameters(data);

                    var index = layui.layer.open({
                        title: "修改角色信息",
                        type: 2,
                        resize: false,
                        area: ['500px', '380px'],
                        content: self.Parameters.modifyUrl,
                        end: function () {
                            $(window).unbind("resize");
                        }
                    });
                } else {
                    GrdAdmin.getSingleCheckData({
                        domId: "roleList",
                        backAction: function (data) {

                            //存储界面传递参数
                            GrdAdmin.setUrlParameters(data);

                            var index = layui.layer.open({
                                title: "修改角色信息",
                                type: 2,
                                resize: false,
                                area: ['500px', '380px'],
                                content: self.Parameters.modifyUrl,
                                end: function () {
                                    $(window).unbind("resize");
                                }
                            });
                        }
                    });
                }
            },

            //启用或停用
            forbiddenInfo: function (id) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.forbiddenUrl,
                    data: {
                        postId: id
                    },
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            
            //删除信息
            removeInfo: function (data) {

                //单选触发
                if (data != undefined && data != null) {

                    GrdAdmin.removeDataByInfo({
                        data: data,
                        removeUrl: self.Parameters.removeUrl,
                        backAction: function (data) {
                            self.logicFunc.loadListInfo();
                        }
                    });

                } else {
                    //多选触发
                    GrdAdmin.removeDataByDomId({
                        domId: "postList",
                        removeUrl: self.Parameters.removeUrl,
                        backAction: function (data) {
                            self.logicFunc.loadListInfo();
                        }
                    });
                }
            }
        },
        //工具
        tools: {

        }
    };
    authorityPostListLogic.init();
});
