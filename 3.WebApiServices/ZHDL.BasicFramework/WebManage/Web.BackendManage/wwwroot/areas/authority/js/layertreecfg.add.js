﻿layui.use(['form', 'layedit', 'layer', 'laydate', 'jquery'], function () {

    var form = layui.form,
        layer = parent.layer === undefined ? layui.layer : top.layer,
        $ = layui.jquery,
        player = parent.layer || layui.layer;

    var authorityLayertreecfgAddLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),   //异步方法管理
                queryData: undefined,    //页面传递参数

                addInfoUrl: "/api/Authority/LayertreeCfg/AddNode"
            };


            self.initPage();
            self.bindEvent();
        },
        //初始化列表
        initPage: function () {
            //获取信息
            self.Parameters.queryData = GrdAdmin.getUrlParameters();

            if (self.Parameters.queryData) {
                self.logicFunc.loadInfo();
            }
            //更新全部
            form.render();
        },
        //绑定事件
        bindEvent: function () {

            //监听提交
            form.on('submit(addButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);
        },
        //逻辑方法
        logicFunc: {
            //获取修改的信息
            loadInfo: function () {
                $("#parent_id").val(self.Parameters.queryData.parent_id);
            },
            ///保存数据
            saveInfo: function () {
                var json = GrdAdmin.initParamsData($("#add-form"));
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.addInfoUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });

                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });

            },
            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    authorityLayertreecfgAddLogic.init();
});
