﻿layui.config({
    base: '/Lib/layui/extend/'
}).extend({
    treeGrid: 'treeGrid'
}).use(['form', 'layer', 'table', 'laypage', 'laytpl', 'laydate', 'tree', 'treeGrid', 'jquery'], function () {
    var $ = layui.jquery,
        form = layui.form,
        player = parent.layer || layui.layer,
        treeGrid = layui.treeGrid; //很重要

    var authorityRoleLayerLogic = {
        //初始化
        init: function () {

            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),//异步方法管理
                queryData: undefined,
                rowData: undefined,//选中行信息

                layerListsUrl: "/api/Authority/LayertreeCfg/LoadListInfoByRoleId",
                modifyRoleLayerInfoUrl: "/api/Authority/LayertreeCfg/ModifyRoleLayerInfo",
            };

            this.initPage();
            this.bindEvent();
        },
        //初始化列表
        initPage: function () {

            //获取信息
            self.Parameters.queryData = GrdAdmin.getUrlParameters();
            self.logicFunc.loadListInfo();
        },
        //绑定事件
        bindEvent: function () {

            //监听提交
            form.on('submit(layerButton)', self.logicFunc.saveInfo);

            //关闭
            $("#btnClose").on("click", self.logicFunc.close);
        },
        //逻辑方法
        logicFunc: {


            //获取数据
            loadListInfo: function () {

                var title =
                    [
                        { type: 'checkbox' },
                        { field: 'name', title: '图层名称', width: '130' },
                        { field: 'tablename', title: '图层表名', width: '220' },
                        { field: 'featureclass_name', title: '要素类名称', width: '280' }
                    ];
                var userInfo = GrdAdmin.getUserInfo();
                var treeTable = treeGrid.render({
                    elem: '#layerList',
                    headers: { "Authorization": userInfo != null ? "Bearer  " + JSON.parse(userInfo.token).auth_token : "" },//通过请求头来发送token，放弃了通过cookie的发送方式
                    url: GrdAdmin.webURI + self.Parameters.layerListsUrl,
                    where: {
                        roleId:self.Parameters.queryData.id
                    },
                    cellMinWidth: 100,
                    contentType: "application/json;charset=utf-8",
                    height: "full-100",
                    idField: 'id',//必須字段
                    treeId: 'id',//树形id字段名称
                    treeUpId: 'parent_id',//树形父id字段名称
                    treeShowName: 'name',//以树形式显示的字段
                    isOpenDefault: true,//节点默认是展开还是折叠【默认展开】
                    cheDisabled: true,
                    isPage: false,
                    cols: [title],
                    onRadio: function (obj) {
                        self.Parameters.rowData = obj;
                    }
                });

                //单击行勾选checkbox事件
                $(document).on("click", ".layui-table-body table.layui-table tbody tr", function () {
                    var index = $(this).attr('data-index');
                    var tableBox = $(this).parents('.layui-table-box');
                    //存在固定列
                    if (tableBox.find(".layui-table-fixed.layui-table-fixed-l").length > 0) {
                        tableDiv = tableBox.find(".layui-table-fixed.layui-table-fixed-l");
                    } else {
                        tableDiv = tableBox.find(".layui-table-body.layui-table-main");
                    }
                    var CheckLength = tableDiv.find("tr[data-index=" + index + "]").find(
                        "td div.layui-form-checked").length;

                    var checkCell = tableDiv.find("tr[data-index=" + index + "]").find(
                        "td div.laytable-cell-checkbox div.layui-form-checkbox I");
                    if (checkCell.length > 0) {
                        checkCell.click();
                    }
                });

                $(document).on("click", "td div.laytable-cell-checkbox div.layui-form-checkbox", function (e) {
                    e.stopPropagation();
                });
            },

            //保存数据
            saveInfo: function () {

                var parametersList = new Array();　//创建一个数组

                var selectMenusListInfo = treeGrid.treeFindRowCheck("layerList");

                $.each(selectMenusListInfo, function (n, value) { 
                    parametersList.push(value.id);
                });  

                var json = {
                    layerIdList: parametersList,
                    roleid: self.Parameters.queryData.id
                };
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.modifyRoleLayerInfoUrl,
                    data: json,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                //获取窗口索引
                                var index = player.getFrameIndex(window.name);
                                //关闭子页面
                                player.close(index);
                                //刷新父页面
                                window.parent.location.reload();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },

            //取消
            close: function () {
                player.closeAll();
            }
        },
        //工具
        tools: {
        }
    };
    authorityRoleLayerLogic.init();
});
