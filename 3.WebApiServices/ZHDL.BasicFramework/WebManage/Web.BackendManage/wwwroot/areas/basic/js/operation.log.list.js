﻿layui.use(['form', 'layer', 'table', 'laypage', 'jquery', "laydate"], function () {
    var $ = layui.jquery,
        form = layui.form,
        table = layui.table,
        laydate = layui.laydate,
        player = parent.layer || layui.layer;

    var basicOperationLogListLogic = {

        //初始化
        init: function () {
            self = this;

            this.Parameters = {
                ac: new AsyncCaller(),//异步方法管理
                functionypeList: undefined,//功能类型
                extiontypeList: undefined,//异常类型
                systemtypeList: undefined,//功能类型
                field: 'create_date',
                order: 'desc',
                systemtionLogListUrl: "/api/Authority/FunctionCfg/LoadSystemTypeList",

                listsUrl: "/api/Basic/OperationLog/LoadList",
                operationLogListUrl: "/api/Basic/OperationLog/LoadLogTypeList",
                removeUrl: "/api/Basic/OperationLog/Remove",

                exlistsUrl: "/api/Basic/ExceptionLog/LoadList",
                exoperationLogListUrl: "/api/Basic/ExceptionLog/LoadLogTypeList",
                exremoveUrl: "/api/Basic/ExceptionLog/Remove",
                solveExceptionUrl: "/api/Basic/ExceptionLog/SolveException",
            };
            this.Parameters.ac
                .pushQueue(function () {
                    self.logicFunc.loadLogTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                }).pushQueue(function () {
                    self.logicFunc.loadExoperationTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                }).pushQueue(function () {
                    self.logicFunc.loadSystemTypeList(function () {
                        self.Parameters.ac.notifyQueueAsyncFuncComplate();
                    });
                })
                .pushQueue(function () {
                    self.initPage();
                    self.Parameters.ac.notifyQueueAsyncFuncComplate();
                })
                .pushQueue(this.bindEvent)
                .exec();
        },
        //初始化列表
        initPage: function () {
            var startTime = laydate.render({
                elem: '#startTime',
                done: function (value, date, endDate) {
                    endTime.config.min = {
                        year: date.year,
                        month: date.month - 1,
                        date: date.date
                    };
                }
            });

            var endTime = laydate.render({
                elem: '#endTime',
                done: function (value, date, endDate) {
                    startTime.config.max = {
                        year: date.year != undefined ? date.year : 2099,
                        month: (date.month != undefined) ? (date.month - 1) : 12,
                        date: (date.date != undefined) ? date.date : 25
       
                    };
                }
            });

            var exstartTime = laydate.render({
                elem: '#exstartTime',
                done: function (value, date, endDate) {
                    exendTime.config.min = {
                        year: date.year,
                        month: date.month - 1,
                        date: date.date
                    };
                }
            });

            var exendTime = laydate.render({
                elem: '#exendTime',
                done: function (value, date, endDate) {
                    exstartTime.config.max = {
                        year: date.year != undefined ? date.year : 2099,
                        month: (date.month != undefined) ? (date.month - 1) : 12,
                        date: (date.date != undefined) ? date.date : 25
                    };
                }
            });

            ////////////////////////////////////////////////////////////操作日志
            ////////////////////////////////////////////////////////////操作日志
            var title =
                [
                    { field: 'id', title: 'id', hide: true },
                    {
                        field: 'Rnumber', title: '编号', width: 60, align: "center",
                        templet: function (d) {
                            //每页都从1开始 return d.LAY_INDEX; //只有第一页才从1开始，（相当于数据表的所有记录数的下标。）
                            return d.LAY_INDEX;
                        }
                    },
                    {
                        field: 'func_type', title: '功能类型', width: 130, align: "center", sort: true,
                        templet: function (d) {
                            for (var i = 0; i < self.Parameters.functionypeList.length; i++) {
                                if (self.Parameters.functionypeList[i].Value == d.func_type) {
                                    return self.Parameters.functionypeList[i].Text;
                                }
                            }
                        }
                    },
                    { field: 'business_title', title: '业务标题', width: 120, align: "center", sort: true },
                    { field: 'user_name', title: '操作用户名', width: 110 },
                    { field: 'ip_address', title: 'IP地址', width: 130 },
                    { field: 'create_date', title: '创建时间', width: 250 },
                    { field: 'detail', title: '操作明细', width: 450 }

                ];

            //操作日志表格加载
            GrdAdmin.tableInit({
                dom: '#logList',
                height: "full-160",
                url: self.Parameters.listsUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: {
                    sKeyWords: $("#txtKeyWord").val(),
                    func_type: $("#operationLog_type").val() == "" ? 0 : $("#operationLog_type").val()
                },
                title: title
            });
            //排序监听
            GrdAdmin.tableSort({
                domId: "logList",
                backAction: function (obj) {
                    self.Parameters.field = obj.field;
                    self.Parameters.order = obj.type;
                    self.logicFunc.loadListInfo();
                }
            });

            //////////////////////////////////////////////异常日志///////////////////////////////////////////
            /////////////////////////////////////////////异常日志///////////////////////////////////////////
            var extitle =
                [
                    { field: 'id', title: 'id', hide: true },
                    {
                        field: 'Rnumber', title: '编号', width: 60, align: "center",
                        templet: function (d) {
                            //每页都从1开始 return d.LAY_INDEX; //只有第一页才从1开始，（相当于数据表的所有记录数的下标。）
                            return d.LAY_INDEX;
                        }
                    },
                    {
                        field: 'exception_type', title: '异常类型', width: 130, align: "center",
                        templet: function (d) {
                            for (var i = 0; i < self.Parameters.extiontypeList.length; i++) {
                                if (self.Parameters.extiontypeList[i].Value == d.exception_type) {
                                    return self.Parameters.extiontypeList[i].Text
                                }
                            }
                        }
                    },
                    {
                        field: 'system_type', title: '系统类型', width: 130, align: "center",
                        templet: function (d) {
                            for (var i = 0; i < self.Parameters.systemtypeList.length; i++) {
                                if (self.Parameters.systemtypeList[i].Value == d.system_type) {
                                    return self.Parameters.systemtypeList[i].Text
                                }
                            }
                        }
                    },
                    { field: 'exception_title', title: '异常标题', width: 220, align: "center" },
                    { field: 'detail', title: '操作明细', width: 320 },
                    { field: 'status', title: '是否解决', width: 140, templet: '#switchStatusTpl', align: "center" },
                    { field: 'create_date', title: '创建时间', width: 180 }
                ];

            //操作日志表格加载
            GrdAdmin.tableInit({
                dom: '#logList',
                height: "full-160",
                url: self.Parameters.listsUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: {
                    sKeyWords: $("#txtKeyWord").val(),
                    func_type: $("#operationLog_type").val() == "" ? 0 : $("#operationLog_type").val()
                },
                title: title
            });

            //异常日志表格加载
            GrdAdmin.tableInit({
                dom: '#exList',
                height: "full-160",
                url: self.Parameters.exlistsUrl,
                field: self.Parameters.field,
                order: self.Parameters.order,
                parameters: {
                    sKeyWords: $("#extxtKeyWord").val(),
                    exception_type: $("#exception_type").val() == "" ? 0 : $("#exception_type").val(),
                    system_type: $("#system_type").val() == "" ? 0 : $("#system_type").val()
                },
                title: extitle
            });

            //排序监听
            GrdAdmin.tableSort({
                domId: "logList",
                backAction: function (obj) {
                    self.Parameters.field = obj.field;
                    self.Parameters.order = obj.type;
                    self.logicFunc.loadListInfo();
                }
            });
            //排序异常日志表格监听
            GrdAdmin.tableSort({
                domId: "exList",
                backAction: function (obj) {
                    self.Parameters.field = obj.field;
                    self.Parameters.order = obj.type;
                    self.logicFunc.loadExceptionListInfo();
                }
            });

            form.render();

        },
        //绑定事件
        bindEvent: function () {

            //查询
            $("#funBtnSearch").on("click", function () {
                
                self.logicFunc.loadListInfo();
            });

            //删除
            $("#funBtnRemove").on("click", function () {
                self.logicFunc.removeInfo();
            });

            //异常查询
            $("#exBtnSearch").on("click", function () {
                self.logicFunc.loadExceptionListInfo();
            });

            //异常删除
            $("#exBtnRemove").on("click", function () {
                self.logicFunc.removeException();
            });

            //监听启用/停用操作
            form.on('switch(funBtnStatus)', function (obj) {
                self.logicFunc.solveException(this.value);
            });
        },
        //逻辑方法
        logicFunc: {
            //获取日志类型列表信息
            loadLogTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.operationLogListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            $("#operationLog_type").empty();
                            self.Parameters.functionypeList = result.Data;
                            for (var i = 0; i < result.Data.length; i++) {
                                $("#operationLog_type").append("<option value=" + result.Data[i].Value + ">" + result.Data[i].Text + "</option>");
                            }
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },
            //获取异常类型列表信息
            loadExoperationTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.exoperationLogListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            $("#exceptionLog_type").empty();
                            self.Parameters.extiontypeList = result.Data;
                            for (var i = 0; i < result.Data.length; i++) {
                                $("#exceptionLog_type").append("<option value=" + result.Data[i].Value + ">" + result.Data[i].Text + "</option>");
                            }
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },
            //获取系统类型列表信息
            loadSystemTypeList: function (func) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.systemtionLogListUrl,
                    data: {},
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            $("#system_type").empty();
                            self.Parameters.systemtypeList = result.Data;
                            for (var i = 0; i < result.Data.length; i++) {
                                $("#system_type").append("<option value=" + result.Data[i].Value + ">" + result.Data[i].Text + "</option>");
                            }
                        }
                        if (func) {
                            func();
                        }
                    }
                });
            },
            ///获取数据
            loadListInfo: function () {
                GrdAdmin.tableReload({
                    domId: "logList",
                    field: self.Parameters.field,
                    order: self.Parameters.order,
                    parameters: {
                        sKeyWords: $("#txtKeyWord").val(),
                        func_type: $("#operationLog_type").val() == "" ? 0 : $("#operationLog_type").val()
                    }
                });
            },
            //异常日志获取
            loadExceptionListInfo: function () {
                GrdAdmin.tableReload({
                    domId: "exList",
                    field: self.Parameters.field,
                    order: self.Parameters.order,
                    parameters: {
                        sKeyWords: $("#extxtKeyWord").val(),
                        exception_type: $("#exceptionLog_type").val() == "" ? 0 : $("#exceptionLog_type").val(),
                        system_type: $("#system_type").val() == "" ? 0 : $("#system_type").val()
                    }
                });
            },

            //删除信息
            removeInfo: function () {

                var sTime = $("#startTime").val();
                var eTime = $("#endTime").val();

                if (sTime == "") { GrdAdmin.msg("请选择开始时间！", "警告"); return; }
                if (eTime == "") { GrdAdmin.msg("请选择截止时间！", "警告"); return; }

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.removeUrl,
                    data: { startTime: sTime, endTime: eTime },
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },
            //删除异常信息
            removeException: function () {

                var sTime = $("#exstartTime").val();
                var eTime = $("#exendTime").val();

                if (sTime == "") { GrdAdmin.msg("请选择开始时间！", "警告"); return; }
                if (eTime == "") { GrdAdmin.msg("请选择截止时间！", "警告"); return; }

                GrdAdmin.ajaxRequest({
                    url: self.Parameters.exremoveUrl,
                    data: { startTime: sTime, endTime: eTime },
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadExceptionListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            },

            //解决异常日志
            solveException: function (id) {
                GrdAdmin.ajaxRequest({
                    url: self.Parameters.solveExceptionUrl,
                    data: {
                        id: id
                    },
                    type: GrdAdmin.ajaxGet,
                    success: function (result) {
                        if (result.Code == 1000) {
                            GrdAdmin.msg(result.Msg, "成功", function () {
                                self.logicFunc.loadExceptionListInfo();
                            });
                        } else {
                            GrdAdmin.msg(result.Msg, "警告");
                        }
                    }
                });
            }
        },
        //工具
        tools: {

        }
    };
    basicOperationLogListLogic.init();
});
