﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System;
using System.IO;

namespace Web.BackendManage
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        /// <summary>
        /// 此方法由运行时调用。使用此方法将服务添加到容器
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                //此lambda确定给定请求是否需要用户对非必需cookie的同意。
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            #region 跨域配置
            //配置跨域处理
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin", builder =>
                {
                    builder
                    //.WithOrigins("http://localhost:64251")
                    .AllowAnyOrigin() //允许任何来源的主机访问
                    .AllowAnyMethod()
                    .AllowAnyHeader()
                    .AllowCredentials();//指定处理cookie
                });
            });
            #endregion

            #region Session
            //启用内存缓存(该步骤需在AddSession()调用前使用)
            //启用session之前必须先添加内存
            services.AddDistributedMemoryCache();

            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromMinutes(60 * 2);//设置session的过期时间
                options.Cookie.HttpOnly = true;//设置在浏览器不能通过js获得该cookie的值
                options.Cookie.IsEssential = true;
                //防止edge浏览器访问session丢失问题
                options.Cookie.SameSite = SameSiteMode.None;
            });

            #endregion
        }

        /// <summary>
        ///  此方法由运行时调用。使用此方法配置http请求管道
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {

            #region Session
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();//UseSession配置在UseMvc之前    开启session
            #endregion

            #region 错误处理
            app.UseStatusCodePagesWithReExecute("/Admin/Error/Page");
            #endregion

            #region 使用跨域
            app.UseCors("AllowSpecificOrigin");
            #endregion

            #region 服务器资源路径置换
            //服务器资源路径置换，这样可以防止客户端猜测服务端文件路径，制造一个虚拟的隐射进行访问，提高了安全性。
            //app.UseStaticFiles(new StaticFileOptions()
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), @"wwwroot/files")),
            //    RequestPath = new PathString("/src")
            //});
            #endregion

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                     name: "Areas",
                     template: "{area=Admin}/{controller=Login}/{action=Index}/{id?}"
                     );

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
