﻿namespace Scheduler.Library
{
    using System;

    /// <summary>
    /// 触发器
    /// </summary>
    public class Trigger : ICloneable
    {
        #region 属性

        /// <summary>
        /// 标识对应的任务是否重复运行：默认为true
        /// </summary>
        public bool Repeat { get; set; } = true;

        /// <summary>
        /// 任务的开始时间：默认为DateTime.MinValue，即即刻执行
        /// </summary>
        public DateTime StartTime { get; set; } = DateTime.MinValue;

        /// <summary>
        /// 任务的结束时间：默认为DateTime.MinValue，即不限时间，可无限循环执行
        /// </summary>
        public DateTime EndTime { get; set; } = DateTime.MinValue;

        /// <summary>
        /// 触发的间隔时间(毫秒)：默认为0，即即刻触发
        /// </summary>
        public int Interval { get; set; } = 0;

        /// <summary>
        /// 是否有最大迭代数：默认没有
        /// </summary>
        public bool HasMaxIterations { get; set; } = false;

        /// <summary>
        /// 最大迭代数：默认是int的最大值，判断根据HasMaxIterations为主
        /// <para>
        /// 注：如果HasMaxIterations设置为true，该值才有参考价值
        /// </para>
        /// </summary>
        public int MaxIterations { get; set; } = int.MaxValue;

        /// <summary>
        /// 是否需要手动开启任务：默认是不开启
        /// </summary>
        public bool IsOnDemand { get; set; } = false;

        #endregion

        #region 方法

        /// <summary>
        /// 设置手动触发任务
        /// <para>
        /// 当设置了手动触发任务，则不再重复执行，并且最大迭代数为0，且是否有最大迭代数设置为false
        /// </para>
        /// </summary>
        /// <returns>当前触发器对象</returns>
        public Trigger OnDemand()
        {
            IsOnDemand = true;
            Repeat = false;
            MaxIterations = 0;
            HasMaxIterations = false;
            return this;
        }

        /// <summary>
        /// 设置最大运行数
        /// <para>
        /// 当设置了最大运行次数，则开启重复执行，并且手动触发标志设置为false，是否有最大迭代数设置为true
        /// </para>
        /// </summary>
        /// <param name="maxRuns">最大运行次数</param>
        /// <returns>当前触发器对象</returns>
        public Trigger MaxRuns(int maxRuns)
        {
            Repeat = true;
            IsOnDemand = false;
            MaxIterations = maxRuns;
            HasMaxIterations = true;
            return this;
        }

        private Trigger() { }

        /// <summary>
        /// 创建一个新触发器
        /// </summary>
        public static Trigger NewTrigger
        {
            get
            {
                return new Trigger();
            }
        }

        /// <summary>
        /// 设置停止时间
        /// </summary>
        /// <param name="timeToStop">停止的时间</param>
        /// <returns>当前触发器对象</returns>
        public Trigger StopAt(DateTime timeToStop)
        {
            EndTime = timeToStop;
            return this;
        }

        /// <summary>
        /// 创建一个重复触发任务的间隔时间
        /// </summary>
        /// <param name="timespan">时间簇</param>
        /// <returns>当前触发器对象</returns>
        public Trigger Every(TimeSpan timespan)
        {
            IsOnDemand = false;
            Interval = (int)timespan.TotalMilliseconds;
            return this;
        }

        /// <summary>
        /// 设置开始时间
        /// </summary>
        /// <param name="timeToSart">开始时间</param>
        /// <returns>当前触发器对象</returns>
        public Trigger StartAt(DateTime timeToSart)
        {
            StartTime = timeToSart;
            return this;
        }
        #endregion

        /// <summary>
        /// 克隆当前触发器 
        /// </summary>
        /// <returns>当前对象的浅表副本</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }
    }
}
