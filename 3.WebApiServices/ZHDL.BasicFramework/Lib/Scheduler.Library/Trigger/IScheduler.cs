﻿namespace Scheduler.Library
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 任务调度器接口
    /// </summary>
    public interface IScheduler
    {
        /// <summary>
        /// 调度器是否运行
        /// </summary>
        bool IsStarted { get; }

        /// <summary>
        /// 调度器是否关闭
        /// </summary>
        bool IsShutDown { get; }

        /// <summary>
        /// 调度任务
        /// </summary>
        /// <param name="name">任务的名称</param>
        /// <param name="trigger">任务的触发器</param>
        /// <param name="start">是否立即开始任务</param>
        /// <param name="executor">执行的回调</param>
        /// <param name="onCompletedAction">完成任务的回调</param>
        void Schedule(string name, Trigger trigger, Action executor, Action<SchedulerTask> onCompletedAction, bool start = true);

        /// <summary>
        /// 返回调度任务名称数组
        /// </summary>
        /// <returns>调度任务名称数组</returns>
        string[] GetTaskNames();

        /// <summary>
        /// 暂停
        /// </summary>
        /// <param name="name">任务名称</param>
        void Pause(string name);

        /// <summary>
        /// 运行一个指定的任务
        /// </summary>
        /// <param name="name">任务名称</param>
        void Run(string name);

        /// <summary>
        /// 恢复一个指定的任务
        /// </summary>
        /// <param name="name">任务名称</param>
        void Resume(string name);

        /// <summary>
        /// 删除一个指定的任务
        /// </summary>
        /// <param name="name">任务名称</param>
        void Delete(string name);

        /// <summary>
        /// 暂停全部任务
        /// </summary>
        void PauseAll();

        /// <summary>
        /// 恢复全部任务
        /// </summary>
        void ResumeAll();

        /// <summary>
        /// 获取所有任务的状态
        /// </summary>
        /// <returns>所有任务的状态</returns>
        IList<TaskSummary> GetStatus();

        /// <summary>
        /// 获取指定任务的摘要信息
        /// </summary>
        /// <param name="name">任务名称</param>
        /// <returns>任务状态</returns>
        TaskSummary GetStatus(string name);

        /// <summary>
        /// 启动调度器
        /// </summary>
        void StartUp();

        /// <summary>
        /// 关闭调度器
        /// </summary>
        void ShutDown();
    }
}
