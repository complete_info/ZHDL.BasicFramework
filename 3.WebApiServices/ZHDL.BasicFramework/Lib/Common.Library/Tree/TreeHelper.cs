﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;
using System.Linq;

namespace Common.Library
{
    /// <summary>
    /// 树操作帮助类
    /// </summary>
    public static class TreeHelper
    {
        /// <summary>
        /// 指定ID ，向下查询所有的子节点------递归获取
        /// </summary>
        /// <param name="id"></param>
        public static List<TreeInfo> GetChildTreeInfo<T>(this List<T> orgList, string id)
        {
            List<TreeInfo> reultList = new List<TreeInfo>();
            //原始数据
            string strJson = JsonConvert.SerializeObject(orgList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
            List<Dictionary<string, object>> dicList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(strJson);

            //根据NodeID，获取当前子节点列表
            TreeInfo treeInfo = null;
            List<Dictionary<string, object>> chidList = dicList.FindAll(q => q["parent_id"] != null && q["parent_id"].ToString() == id).ToList();
            if (chidList.Count > 0)
            {
                foreach (var item in chidList)
                {
                    treeInfo = new TreeInfo()
                    {
                        id = item["id"].ToString(),
                        title = item["name"].ToString(),
                        field = item["name"].ToString(),
                        children = new List<TreeInfo>()
                    };
                    strJson = JsonConvert.SerializeObject(orgList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
                    var dicListInfo = JsonConvert.DeserializeObject<List<T>>(strJson);
                    //递归获取下一级
                    treeInfo.children = dicListInfo.GetChildTreeInfo<T>(item["id"].ToString());
                    reultList.Add(treeInfo);
                }
            }
            return reultList;
        }


        /// <summary>
        /// 指定ID ，向下查询所有的子节点------递归获取
        /// </summary>
        /// <param name="id"></param>
        public static List<SelectListInfo> GetSelectChildInfo<T>(this List<T> orgList, string id)
        {
            List<SelectListInfo> reultList = new List<SelectListInfo>();
            //原始数据
            string strJson = JsonConvert.SerializeObject(orgList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
            List<Dictionary<string, object>> dicList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(strJson);

            //根据NodeID，获取当前子节点列表
            SelectListInfo treeInfo = null;
            List<Dictionary<string, object>> chidList = dicList.FindAll(q => q["parent_id"] != null && q["parent_id"].ToString() == id).ToList();
            if (chidList.Count > 0)
            {
                foreach (var item in chidList)
                {
                    treeInfo = new SelectListInfo()
                    {
                        value = item["id"].ToString(),
                        name = item["name"].ToString(),
                        children = new List<SelectListInfo>()
                    };
                    strJson = JsonConvert.SerializeObject(orgList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
                    var dicListInfo = JsonConvert.DeserializeObject<List<T>>(strJson);
                    //递归获取下一级
                    treeInfo.children = dicListInfo.GetSelectChildInfo<T>(item["id"].ToString());
                    reultList.Add(treeInfo);
                }
            }
            return reultList;
        }

        /// <summary>
        /// 获取传入根节点对应的包括传入根节点的List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetChildList<T>(this List<T> allList, List<T> rootList) where T : new()
        {
            List<T> reultList = new List<T>();

            //原始数据
            string strJson = JsonConvert.SerializeObject(allList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
            List<Dictionary<string, object>> allDicList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(strJson);

            //根节点数据
            string strRootJson = JsonConvert.SerializeObject(rootList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
            List<Dictionary<string, object>> rootDicList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(strRootJson);

            foreach (var rootItem in rootDicList)
            {
                //根据NodeID，获取当前子节点列表
                List<Dictionary<string, object>> chidList = allDicList.FindAll(q => q["parent_id"] != null && q["parent_id"].ToString() == rootItem["id"].ToString()).ToList();

                if (chidList.Count > 0)
                {
                    var newChidList = JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(chidList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" }));

                    reultList.AddRange(newChidList);

                    reultList.AddRange(allList.GetChildList<T>(newChidList));
                }
            }
            return reultList;
        }


        /// <summary>
        /// 获取传入根节点对应的包括传入根节点的List
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public static List<T> GetParentList<T>(this List<T> allList, List<T> rootList) where T : new()
        {
            List<T> reultList = new List<T>();

            //原始数据
            string strJson = JsonConvert.SerializeObject(allList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
            List<Dictionary<string, object>> allDicList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(strJson);

            //根节点数据
            string strRootJson = JsonConvert.SerializeObject(rootList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" });
            List<Dictionary<string, object>> rootDicList = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(strRootJson);

            foreach (var rootItem in rootDicList)
            {
                if (rootItem["parent_id"] !=null)
                {
                    //根据NodeID，获取当前子节点列表
                    List<Dictionary<string, object>> parentList = allDicList.FindAll(q => q["id"].ToString() == rootItem["parent_id"].ToString()).ToList();

                    if (parentList.Count > 0)
                    {
                        var newParentList = JsonConvert.DeserializeObject<List<T>>(JsonConvert.SerializeObject(parentList, new IsoDateTimeConverter() { DateTimeFormat = "yyyy-MM-dd HH:mm:ss" }));

                        reultList.AddRange(newParentList);

                        reultList.AddRange(allList.GetParentList<T>(newParentList));
                    }
                }
            }
            return reultList;
        }
    }
}
