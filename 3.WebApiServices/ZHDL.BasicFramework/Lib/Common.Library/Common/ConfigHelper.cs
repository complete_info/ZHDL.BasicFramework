﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;

namespace Common.Library
{
    /// <summary>
    /// 专门获取appsettings.json 配置信息帮助类
    /// </summary>
    public class ConfigHelper
    {
        public static IConfiguration Configuration { get; set; }

        static ConfigHelper()
        {
            //ReloadOnChange = true 当appsettings.json被修改时重新加载            
            #region 方式1（ok）

            Configuration = new ConfigurationBuilder()
                .Add(new JsonConfigurationSource
                {
                    Path = "appsettings.json",
                    ReloadOnChange = true

                }).Build();
            #endregion

            ////读取一级配置节点配置
            //var name = ConfigManager.Configuration["TestName"];
            ////读取二级子节点配置
            //var a = ConfigManager.Configuration["Appsettings:SystemName"];

            #region 方式2（ok）
            //Configuration = new ConfigurationBuilder()
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .AddJsonFile("appsettings.json").Build();
            #endregion
        }
    }
}
