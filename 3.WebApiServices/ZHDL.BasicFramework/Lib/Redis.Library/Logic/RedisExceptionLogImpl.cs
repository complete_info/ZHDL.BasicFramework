﻿using Common.Model;
using System;
using System.Collections.Generic;
using System.Text;
/*
* 命名空间: Redis.Library
*
* 功 能： 异常日志处理类
*
* 类 名： ExceptionLogImpl
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/10 9:55:22 	罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Redis.Library
{
    /// <summary>
    /// 异常日志处理类
    /// </summary>
    public static class RedisExceptionLogImpl
    {

        /// <summary>
        /// 插入异常警告日志到Redis
        /// </summary>
        /// <param name="messageTitle"></param>
        /// <param name="exp"></param>
        public static void SaveWarnLog(string messageTitle, Exception exp)
        {
            string key = Guid.NewGuid().ToString().Replace("-", "").ToUpper();
            RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSet(BasicRedisInfo.RedisExceptionHashId, key,  new RedisExceptionLogInfo()
            {
                id = key,
                exception_type = 100,
                system_type = 100,
                exception_title = messageTitle,
                detail = exp.StackTrace,
                create_date = DateTime.Now,
                status = 100
            });
        }


        /// <summary>
        /// 插入异常错误日志到Redis
        /// </summary>
        /// <param name="messageTitle"></param>
        /// <param name="exp"></param>
        public static void SaveErrorLog(string messageTitle, Exception exp)
        {
            string key = Guid.NewGuid().ToString().Replace("-", "").ToUpper();

            RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSet(BasicRedisInfo.RedisExceptionHashId, key, new RedisExceptionLogInfo()
            {
                id = key,
                exception_type = 102,
                system_type = 100,
                exception_title = messageTitle,
                detail = exp.StackTrace,
                create_date = DateTime.Now,
                status = 100
            });
        }

        /// <summary>
        /// 批量插入异常错误日志
        /// </summary>
        /// <param name="messageTitles"></param>
        /// <param name="exps"></param>
        public static void SaveErrorLogs(List<string> messageTitles,List<Exception>  exps)
        {
            int index = 0;
            List<string> logInfoKeys  = new List<string>();
            List<RedisExceptionLogInfo> logInfos = new List<RedisExceptionLogInfo>();

            foreach (var item in messageTitles)
            {
                string key = Guid.NewGuid().ToString().Replace("-", "").ToUpper();
                logInfoKeys.Add(key);
                logInfos.Add(new RedisExceptionLogInfo()
                {
                    id = key,
                    exception_type = 102,
                    system_type = 100,
                    exception_title = item,
                    detail = exps[index].StackTrace,
                    create_date = DateTime.Now,
                    status = 100
                });
                index++;
            }
            RedisClient.GetRedisDb(BasicRedisInfo.RedisDbLog).HashSets(BasicRedisInfo.RedisExceptionHashId, logInfoKeys, logInfos);
        }
    }
}
