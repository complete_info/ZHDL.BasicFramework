﻿using System.Collections.Generic;

namespace Redis.Library
{
    /// <summary>
    /// 分页信息类
    /// </summary>
    public class PagedList<T>
    {

        public int PageIndex
        {
            get; set;
        }

        public int PageSize
        {
            get; set;
        }

        public long Count
        {
            get; set;
        }

        public List<T> List
        {
            get; set;
        }

        public PagedList()
        {
            this.PageIndex = 1;
            this.PageSize = 20;
            this.Count = 0;
            this.List = new List<T>();
        }
    }
}
