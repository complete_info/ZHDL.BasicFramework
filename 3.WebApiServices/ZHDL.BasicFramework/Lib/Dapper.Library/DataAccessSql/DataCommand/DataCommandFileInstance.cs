﻿using System.Xml.Serialization;


namespace Dapper.Library.DataAccessSql
{

	/// <summary>
	/// SQL命令文件对象
	/// </summary>
	public class DataCommandFileInstance
	{
		/// <summary>
		/// 文件的路径
		/// </summary>
		[XmlAttribute("filePath")]
		public string FilePath { get; set; }
	}
}
