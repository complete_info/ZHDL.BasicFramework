﻿using System.Xml.Serialization;

namespace Dapper.Library.DataAccessSql
{
	/// <summary>
	/// SQL命令集合
	/// </summary>
	[XmlRoot("dataCommands", Namespace = "http://www.Framework.com/Framework/DataAccess")]
	public class DataCommands
	{
		/// <summary>
		/// SQL命令的集合
		/// </summary>
		[XmlElement("dataCommand")]
		public DataCommandInstance[] InstanceList { get; set; }
	}
}
