﻿using System.Xml.Serialization;

namespace Dapper.Library.DataAccessSql
{
    /// <summary>
    /// 数据库实例配置对象
    /// </summary>
    public class DatabaseInstance
    {
        /// <summary>
        /// 数据库实例名称
        /// </summary>
        [XmlAttribute("name")]
        public string Name { get; set; }

        /// <summary>
        /// 数据库类型
        /// </summary>
        [XmlAttribute("type")]
        public DatabaseType Type { get; set; }

        /// <summary>
        /// 是否加密
        /// </summary>
        [XmlAttribute("isEncryption")]
        public string IsEncryption { get; set; }

        /// <summary>
        /// 连接字符串
        /// </summary>
        [XmlElement("connectionString")]
        public string ConnectionString { get; set; }
    }
}
