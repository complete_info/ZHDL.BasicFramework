﻿namespace Dapper.Library
{
    public enum EOrderBy
    {
        Asc = 1,
        Desc = -1
    }
}
