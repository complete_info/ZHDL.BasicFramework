﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using System.Linq;
using System.Reflection;
using Common.Model;

namespace Dapper.Library
{
	public abstract class SqlProvider
	{

		public AbstractDataBaseContext Context { get; set; }

		protected SqlProvider()
		{
			Params = new DynamicParameters();
			JoinList = new List<JoinAssTable>();
			AsTableNameDic = new Dictionary<Type, string>();
		}

		public abstract IProviderOption ProviderOption { get; set; }

		public string SqlString { get; set; }
		/// <summary>
		/// 连接对象集合
		/// </summary>
		public List<JoinAssTable> JoinList { get; set; }
		/// <summary>
		/// 参数对象
		/// </summary>
		public DynamicParameters Params { get; set; }
		/// <summary>
		/// 重命名目录
		/// </summary>
		public Dictionary<Type, string> AsTableNameDic { get; set; }

		/// <summary>
		/// 拼装单条查询SQL
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public abstract SqlProvider FormatToSingle<T>();

		/// <summary>
		/// 拼装多条查询SQL
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		public abstract SqlProvider FormatToList<T>();

		/// <summary>
		/// 拼装分页查询SQL
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="pageIndex"></param>
		/// <param name="pageSize"></param>
		/// <returns></returns>
		public abstract SqlProvider FormatToPageList<T>(int pageIndex, int pageSize);

		/// <summary>
		/// 获取COUNT()函数SQL
		/// </summary>
		/// <returns></returns>
		public abstract SqlProvider FormatCount();

		/// <summary>
		/// 获取DeleteSQL
		/// </summary>
		/// <returns></returns>
		public abstract SqlProvider FormatDelete();

		/// <summary>
		/// 获取ExecuteNoQuerySQ
		/// </summary>
		/// <returns></returns>
		public abstract SqlProvider FormatExecuteNoQuery();

		/// <summary>
		/// 获取Insert-SQL
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity"></param>
		/// <param name="excludeFields"></param>
		/// <returns></returns>
		public abstract SqlProvider FormatInsert<T>(T entity, string[] excludeFields);

		public abstract SqlProvider FormatInsertIdentity<T>(T entity, string[] excludeFields);

		/// <summary>
		///  获取Update-SQL
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="updateExpression"></param>
		/// <returns></returns>
		public abstract SqlProvider FormatUpdate<T>(Expression<Func<T, T>> updateExpression);

		public abstract SqlProvider FormatUpdate<T>(T entity, string[] excludeFields, bool isBatch = false);

		/// <summary>
		///  获取Sum-SQL
		/// </summary>
		/// <param name="sumExpression"></param>
		/// <returns></returns>
		public abstract SqlProvider FormatSum(LambdaExpression sumExpression);

		public abstract SqlProvider FormatMin(LambdaExpression MinExpression);

		public abstract SqlProvider FormatMax(LambdaExpression MaxExpression);

		/// <summary>
		///  获取先Update-再Select-SQL
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="updator"></param>
		/// <returns></returns>
		public abstract SqlProvider FormatUpdateSelect<T>(Expression<Func<T, T>> updator);

		/// <summary>
		/// 获取批量插入-SQL
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="conn"></param>
		/// <param name="list"></param>
		/// <returns></returns>
		//public abstract SqlProvider ExcuteBulkCopy<T>(IDbConnection conn, IEnumerable<T> list);


		public abstract SqlProvider CreateNew();

		/// <summary>
		/// 获取表名称
		/// </summary>
		/// <param name="isNeedFrom"></param>
		/// <param name="isAsName"></param>
		/// <param name="tableType">连接查询时会用到</param>
		/// <returns></returns>

		public string FormatTableName(bool isNeedFrom = true, bool isAsName = true, Type tableType = null)
		{
			var entity = EntityCache.QueryEntity(tableType == null ? Context.Set.TableType : tableType);

			string schema = string.IsNullOrEmpty(entity.Schema) ? "" : ProviderOption.CombineFieldName(entity.Schema) + ".";

			string fromName = tableType == null ? Context.Set.TableType.GetTableAttributeName():tableType.GetTableAttributeName();

			//函数AsTableName优先级大于一切
			string asTableName;
			if (AsTableNameDic.TryGetValue(entity.Type, out asTableName))
			{
				fromName = asTableName;
			}
			//是否存在实体特性中的AsName标记
			if (isAsName) {
				fromName = entity.AsName.Equals(fromName) ? ProviderOption.CombineFieldName(fromName) : $"{ProviderOption.CombineFieldName(fromName)} {entity.AsName}";
			}
			else
			{
				fromName = ProviderOption.CombineFieldName(fromName);
			}
				
			SqlString = $" {schema}{fromName} ";
			if (isNeedFrom)
				SqlString = " FROM " + SqlString;

			return SqlString;
		}

		protected string[] FormatInsertParamsAndValues<T>(T t, string[] excludeFields = null)
		{
			var paramSqlBuilder = new StringBuilder(64);
			var valueSqlBuilder = new StringBuilder(64);

			var entity = EntityCache.QueryEntity(t.GetType());
			var properties = entity.Properties;

			var isAppend = false;
			foreach (var propertiy in properties)
			{
				//是否是排除字段
				if (excludeFields != null && excludeFields.Contains(propertiy.Name))
				{
					continue;
				}
				var customAttributes = propertiy.GetCustomAttributess(true);
				//导航属性排除
				if (customAttributes.Any(x => x.GetType().Equals(typeof(ForeignKey))))
				{
					continue;
				}

                //自增组件主键标识
                var dbFieldInfo = propertiy.GetCustomAttribute<DBFieldInfo>();
                bool isIncrease = dbFieldInfo.IsIncrease;
                if (isIncrease)
                {
                    continue;
                }

                //排除掉时间格式为最小值的字段
                if (propertiy.PropertyType == typeof(DateTime))
				{
					if (Convert.ToDateTime(propertiy.GetValue(t)) == DateTime.MinValue)
					{
						continue;
					}
				}
				if (isAppend)
				{
					paramSqlBuilder.Append(",");
					valueSqlBuilder.Append(",");
				}
				var name = propertiy.GetColumnAttributeName();
				paramSqlBuilder.AppendFormat("{0}{1}{2}", ProviderOption.OpenQuote, entity.FieldPairs[name], ProviderOption.CloseQuote);
				valueSqlBuilder.Append(ProviderOption.ParameterPrefix + name);
				Params.Add(ProviderOption.ParameterPrefix + name, propertiy.GetValue(t));
				isAppend = true;
			}
			return new[] { paramSqlBuilder.ToString(), valueSqlBuilder.ToString() };
		}

		protected DataBaseContext<T> DataBaseContext<T>()
		{
			return (DataBaseContext<T>)Context;
		}
	
		/// <summary>
		/// 根据主键获取条件
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="entity"></param>
		/// <returns></returns>
		protected string GetIdentityWhere<T>(T entity, DynamicParameters param)
		{
			var entityObject = EntityCache.QueryEntity(typeof(T));
            object id = null;
            //判断是否存在自增主键
            if (!string.IsNullOrEmpty(entityObject.Primarykey))
            {
                //获取主键数据
                id = entityObject.Properties
                    .FirstOrDefault(x => x.Name == entityObject.Primarykey)
                    .GetValue(entity);
                //设置参数
                param.Add(entityObject.Primarykey, id);
                return $" AND { ProviderOption.CombineFieldName(entityObject.Primarykey)}={ProviderOption.ParameterPrefix}{entityObject.Primarykey} ";
            }
            else
            {
                if (!string.IsNullOrEmpty(entityObject.Primarykey))
                {
                    //获取主键数据
                    id = entityObject.Properties
                        .FirstOrDefault(x => x.Name == entityObject.Primarykey)
                        .GetValue(entity);
                    //设置参数
                    param.Add(entityObject.Primarykey, id);
                }
                else
                {
                    throw new DapperExtensionException("主键不存在!请前往实体类使用[Identity]特性设置主键。");
                }
                return $" AND { ProviderOption.CombineFieldName(entityObject.Primarykey)}={ProviderOption.ParameterPrefix}{entityObject.Primarykey} ";
            }
		}
	}
}
