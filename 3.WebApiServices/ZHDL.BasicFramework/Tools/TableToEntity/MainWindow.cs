﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace TableToEntity
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 生成表数据实体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateModel_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtModel.Text))
            {
                MessageBox.Show("请输入数据实体命名空间");
                return;
            }
            if (string.IsNullOrEmpty(txtModelAuthorName.Text))
            {
                MessageBox.Show("请输入注释作者名");
                return;
            }
            string[] tbnames = null;
            var ret = new Dictionary<string, List<FiledInfo>>();

            ///获取数据库信息
            DBInfo dbInfo = GetDbInfo();
            if (dbInfo != null)
            {
                ///获取实体信息
                ModelInfo modelInfo = GetModelInfo();

                switch (dbInfo.DBType.ToLower())
                {
                    case "mssql":
                        tbnames = CreateMsSqlModel.GetTableNames(0, dbInfo, modelInfo);
                        ret = CreateMsSqlModel.GetTableInfo(tbnames, dbInfo);
                        if (ret != null)
                            CreateDTO(ret);
                        break;
                    case "oracle":
                        break;
                    case "postgresql":
                        tbnames = CreatePostgreSqlModel.GetTableNames(0, dbInfo, modelInfo);
                        ret = CreatePostgreSqlModel.GetTableInfo(tbnames, dbInfo);
                        if (ret != null)
                            CreateDTO(ret);
                        break;
                    case "mysql":
                        break;
                    default:
                        break;
                }
            }

        }

        /// <summary>
        /// 生成视图实体
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCreateView_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtView.Text))
            {
                MessageBox.Show("请输入视图实体命名空间");
                return;
            }

            if (string.IsNullOrEmpty(txtViewAuthorName.Text))
            {
                MessageBox.Show("请输入注释作者名");
                return;
            }

            string[] tbnames = null;
            var ret = new Dictionary<string, List<FiledInfo>>();

            ///获取数据库信息
            DBInfo dbInfo = GetDbInfo();
            if (dbInfo != null)
            {
                ///获取实体信息
                ModelInfo modelInfo = GetModelInfo();

                switch (dbInfo.DBType.ToLower())
                {
                    case "mssql":
                        tbnames = CreateMsSqlModel.GetTableNames(1, dbInfo, modelInfo);
                        ret = CreateMsSqlModel.GetViewInfo(tbnames, dbInfo);
                        if (ret != null)
                            CreateView(ret);
                        break;
                    case "oracle":
                        break;
                    case "postgresql":
                        tbnames = CreatePostgreSqlModel.GetTableNames(1, dbInfo, modelInfo);
                        ret = CreatePostgreSqlModel.GetViewInfo(tbnames, dbInfo);
                        if (ret != null)
                            CreateView(ret);
                        break;
                    case "mysql":
                        break;
                    default:
                        break;
                }
            }
        }

        /// <summary>
        /// 获取数据库信息
        /// </summary>
        /// <returns></returns>
        private DBInfo GetDbInfo()
        {
            DBInfo result = null;

            if (string.IsNullOrEmpty(comDBType.Text))
            {
                MessageBox.Show("请选择数据库类型");
                return result;
            }
            if (string.IsNullOrEmpty(txtDBAddress.Text))
            {
                MessageBox.Show("请输入数据库服务地址");
                return result;
            }
            if (string.IsNullOrEmpty(txtPort.Text))
            {
                MessageBox.Show("请输入数据库服务端口");
                return result;
            }
            if (string.IsNullOrEmpty(txtDBName.Text))
            {
                MessageBox.Show("请输入数据库名称");
                return result;
            }
            if (string.IsNullOrEmpty(txtAccount.Text))
            {
                MessageBox.Show("请输入数据库账号");
                return result;
            }
            if (string.IsNullOrEmpty(txtPassword.Text))
            {
                MessageBox.Show("请输入数据库密码");
                return result;
            }
            result = new DBInfo()
            {
                DBType = comDBType.Text,
                Service = txtDBAddress.Text,
                Port = txtPort.Text,
                DbName = txtDBName.Text,
                Account = txtAccount.Text,
                Password = txtPassword.Text
            };
            return result;
        }

        /// <summary>
        /// 获取实体信息
        /// </summary>
        /// <returns></returns>
        private ModelInfo GetModelInfo()
        {
            ModelInfo result = null;

            if (string.IsNullOrEmpty(txtModel.Text))
            {
                MessageBox.Show("请输入实体命名空间");
                return result;
            }
            if (string.IsNullOrEmpty(txtModelAuthorName.Text))
            {
                MessageBox.Show("请输入实体作者名字");
                return result;
            }
            if (string.IsNullOrEmpty(txtView.Text))
            {
                MessageBox.Show("请输入视图命名空间");
                return result;
            }
            if (string.IsNullOrEmpty(txtViewAuthorName.Text))
            {
                MessageBox.Show("请输入视图作者名字");
                return result;
            }
            result = new ModelInfo()
            {
                ModelNameSpace = txtModel.Text,
                ModelAuthorName = txtModelAuthorName.Text,
                ViewNameSpace = txtView.Text,
                ViewAuthorName = txtViewAuthorName.Text
            };
            return result;
        }

        /// <summary>
        /// 生成实体文件
        /// </summary>
        /// <param name="tinfo"></param>
        private void CreateDTO(Dictionary<string, List<FiledInfo>> tinfo)
        {
            var nameSpace = txtModel.Text;
            var authorName = txtModelAuthorName.Text;
            string dir = System.IO.Path.Combine(System.Environment.CurrentDirectory, "DTO");
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            StringBuilder sb = new StringBuilder();
            foreach (var tab in tinfo)
            {
                string className = string.Empty;
                var classNameInfos = tab.Key.Split('_');
                foreach (var item in classNameInfos)
                {
                    className += FirstLetterToUpper(item);
                }

                string fileName = Path.Combine(dir, className + "Entity.cs");
                using (StreamWriter sw = File.CreateText(fileName))
                {
                    sb.AppendLine("/*");
                    sb.AppendLine(string.Format("* 命名空间: {0}", nameSpace));
                    sb.AppendLine("*");
                    sb.AppendLine(string.Format(string.Format("* 功 能： {0}实体类", className)));
                    sb.AppendLine("*");
                    sb.AppendLine(string.Format("* 类 名： {0}Entity", className));
                    sb.AppendLine("*");
                    sb.AppendLine("* Version   变更日期            负责人     变更内容");
                    sb.AppendLine("* ─────────────────────────────────────────────────");
                    sb.AppendLine(string.Format("* V1.0.1    {0} {1}     创建", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), authorName));
                    sb.AppendLine("*");
                    sb.AppendLine("* Copyright (c) 2020 Lir Corporation. All rights reserved.");
                    sb.AppendLine("*/");

                    sb.AppendLine("");
                    sb.AppendLine(string.Format("namespace {0}", nameSpace));
                    sb.AppendLine("{");

                    sb.AppendLine("    using System;");
                    sb.AppendLine("    using Common.Library;");
                    sb.AppendLine("    using Common.Model;");


                    sb.AppendLine("");
                    sb.AppendLine("    /// <summary>");
                    if (tab.Value.Count > 0)
                    {
                        sb.AppendLine(string.Format("    /// {0}", tab.Value[0].tabDescribe));
                    }
                    sb.AppendLine("    /// </summary>");
                    sb.AppendLine("    [Serializable]");
                    sb.AppendLine(string.Format("    [DBTableInfo(TableName = \"{0}\")]", tab.Key));
                    sb.AppendLine(string.Format("    public class {0}Entity", className));
                    sb.AppendLine("    {");

                    sb.AppendLine("    /// <summary>");
                    sb.AppendLine("     /// 构造函数");
                    sb.AppendLine("    /// </summary>");
                    sb.AppendLine(string.Format("    public {0}Entity()", className));
                    sb.AppendLine("     {");

                    foreach (var field in tab.Value)
                    {
                        sb.AppendLine("");
                        sb.AppendLine(string.Format("           //{0}", field.colDescribe));

                        switch (field.dbType)
                        {
                            case "uniqueidentifier":
                            case "nvarchar":
                            case "varchar":
                            case "hierarchyid":
                                if (field.isPrimarykey.ToLower()=="true")
                                {
                                    sb.AppendLine(string.Format("           this.{0} = GuidHelper.GetGuid();", field.colName));
                                }
                                else
                                {
                                    if (IsNumber(field.devalue))
                                    {
                                        sb.AppendLine(string.Format("           this.{0} = {1};", field.colName, field.devalue));
                                    }
                                    else
                                    {
                                        sb.AppendLine(string.Format("           this.{0} = string.Empty;", field.colName));
                                    }
                                }
                                break;
                            case "int":
                            case "int2":
                            case "int4":
                            case "bigint":
                            case "int8":
                            case "float":
                            case "float4":
                            case "float8":
                            case "tinyint":
                            case "smallint":
                            case "real":
                            case "money":
                            case "decimal":
                            case "numeric":
                                if (IsNumber(field.devalue))
                                {
                                    sb.AppendLine(string.Format("           this.{0} = {1};", field.colName, field.devalue));
                                }
                                else
                                {
                                    sb.AppendLine(string.Format("           this.{0} = 0;", field.colName.ToString()));
                                }
                                break;
                            case "datetime":
                            case "date":
                            case "timestamp":
                                sb.AppendLine(string.Format("           this.{0} = DateTime.Now;", field.colName.ToString()));
                                break;
                            case "bit":
                            case "bool":
                                if (field.devalue.ToLower()=="false")
                                {
                                    sb.AppendLine(string.Format("           this.{0} = false;", field.colName.ToString()));
                                }
                                else
                                {
                                    sb.AppendLine(string.Format("           this.{0} = true;", field.colName.ToString()));
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    sb.AppendLine("      }");


                    foreach (var field in tab.Value)
                    {
                        sb.AppendLine("");
                        sb.AppendLine("        /// <summary>");
                        sb.AppendLine(string.Format("        /// {0}", field.colDescribe));
                        sb.AppendLine("        /// </summary>");

                        switch (field.dbType)
                        {
                            case "uniqueidentifier":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "Guid", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "nvarchar":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(),field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0} {1}", "string", field.colName.ToString()));
                                break;
                            case "varchar":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0} {1}", "string", field.colName.ToString()));
                                break;
                            case "int":
                            case "int2":
                            case "int4":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "int", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "bigint":
                            case "int8":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "long", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "datetime":
                            case "date":
                            case "timestamp":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "DateTime", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "bit":
                            case "bool":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(),field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "bool", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "float":
                            case "float4":
                            case "float8":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "double", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "tinyint":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(),  field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "byte", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "smallint":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(),  field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "short", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "real":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "float", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "money":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "decimal", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "decimal":
                            case "numeric":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(), field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0}{1} {2}", "decimal", field.required == "false" ? "?" : "", field.colName.ToString()));
                                break;
                            case "hierarchyid":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ByteLength = {0},DataLength = {1},DecimalDigits = {2},ColumnName = \"{3}\",Required = {4},IsPrimarykey = {5},IsIncrease =false)]", field.byteLength, field.dbLength, field.decimalDigits, field.colName, field.required.ToLower(),field.isPrimarykey));
                                sb.AppendLine(string.Format("        public {0} {1}", "string", field.colName.ToString()));
                                break;
                            default:
                                break;
                        }
                        sb.AppendLine("        {");
                        sb.AppendLine("            get; set;");
                        sb.AppendLine("        }");
                    }

                    sb.AppendLine("    }");
                    sb.AppendLine("}");
                    sw.Write(sb.ToString());
                    sb.Clear();
                    txtResult.AppendText("[" + tab.Key + "]已生成完毕........." + Environment.NewLine);
                }
            }
        }

        /// <summary>
        /// 创建视图实体
        /// </summary>
        /// <param name="vinfo"></param>
        private void CreateView(Dictionary<string, List<FiledInfo>> vinfo)
        {
            var nameSpace = txtView.Text;
            var authorName = txtViewAuthorName.Text;

            string dir = Path.Combine(System.Environment.CurrentDirectory, "View");
            if (!Directory.Exists(dir)) Directory.CreateDirectory(dir);
            StringBuilder sb = new StringBuilder();
            foreach (var tab in vinfo)
            {
                string className = string.Empty;

                var classNameInfos = tab.Key.Split('_');
                foreach (var item in classNameInfos)
                {
                    if (item.ToLower() == "view")
                    {
                        className += "View_";
                    }
                    else
                    {
                        className += FirstLetterToUpper(item);
                    }
                }

                string fileName = Path.Combine(dir, className + "Entity.cs");
                using (StreamWriter sw = File.CreateText(fileName))
                {
                    sb.AppendLine("/*");
                    sb.AppendLine(string.Format("* 命名空间: {0}", nameSpace));
                    sb.AppendLine("*");
                    sb.AppendLine(string.Format(string.Format("* 功 能： {0}视图实体类", className)));
                    sb.AppendLine("*");
                    sb.AppendLine(string.Format("* 类 名： {0}Entity", className));
                    sb.AppendLine("*");
                    sb.AppendLine("* Version   变更日期            负责人     变更内容");
                    sb.AppendLine("* ─────────────────────────────────────────────────");
                    sb.AppendLine(string.Format("* V1.0.1    {0} {1}     创建", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"), authorName));
                    sb.AppendLine("*");
                    sb.AppendLine("* Copyright (c) 2020 Lir Corporation. All rights reserved.");
                    sb.AppendLine("*/");

                    sb.AppendLine("");
                    sb.AppendLine(string.Format("namespace {0}", nameSpace));
                    sb.AppendLine("{");
                    sb.AppendLine("    using System;");
                    sb.AppendLine("    using Common.Library;");
                    sb.AppendLine("    using Common.Model;");
                    sb.AppendLine("");
                    sb.AppendLine("    /// <summary>");
                    sb.AppendLine(string.Format("    /// {0}", tab.Value[0].tabDescribe));
                    sb.AppendLine("    /// </summary>");
                    sb.AppendLine("    [Serializable]");
                    sb.AppendLine(string.Format("    [DBTableInfo(TableName = \"{0}\")]", tab.Key));
                    sb.AppendLine(string.Format("    public class {0}Entity", className));
                    sb.AppendLine("    {");

                    foreach (var field in tab.Value)
                    {
                        sb.AppendLine("");
                        sb.AppendLine("        /// <summary>");
                        sb.AppendLine(string.Format("        /// {0}", field.colDescribe));
                        sb.AppendLine("        /// </summary>");

                        switch (field.dbType)
                        {
                            case "uniqueidentifier":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "Guid", field.colName.ToString()));
                                break;
                            case "nvarchar":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "string", field.colName.ToString()));
                                break;
                            case "varchar":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "string", field.colName.ToString()));
                                break;
                            case "int":
                            case "int2":
                            case "int4":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "int", field.colName.ToString()));
                                break;
                            case "bigint":
                            case "int8":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "long", field.colName.ToString()));
                                break;
                            case "datetime":
                            case "date":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "DateTime", field.colName.ToString()));
                                break;
                            case "bit":
                            case "bool":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "bool", field.colName.ToString()));
                                break;
                            case "float":
                            case "float4":
                            case "float8":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "double", field.colName.ToString()));
                                break;
                            case "tinyint":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "byte", field.colName.ToString()));
                                break;
                            case "smallint":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "short", field.colName.ToString()));
                                break;
                            case "real":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "float", field.colName.ToString()));
                                break;
                            case "money":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "decimal", field.colName.ToString()));
                                break;
                            case "decimal":
                            case "numeric":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "decimal", field.colName.ToString()));
                                break;
                            case "hierarchyid":
                            case "timestamp":
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "string", field.colName.ToString()));
                                break;
                            default:
                                sb.AppendLine(string.Format("        [DBFieldInfo(ColumnName = \"{0}\")]", field.colName));
                                sb.AppendLine(string.Format("        public {0} {1}", "string", field.colName.ToString()));
                                break;
                        }
                        sb.AppendLine("        {");
                        sb.AppendLine("            get; set;");
                        sb.AppendLine("        }");
                    }

                    sb.AppendLine("    }");
                    sb.AppendLine("}");
                    sw.Write(sb.ToString());
                    sb.Clear();
                    txtResult.AppendText("[" + tab.Key + "]已生成完毕........." + Environment.NewLine);
                }
            }
        }


        public string FirstLetterToUpper(string str)
        {
            if (str == null)
                return null;
            if (str.Length > 1)
                return char.ToUpper(str[0]) + str.Substring(1);
            return str.ToUpper();
        }

        /// <summary>
        /// 判断字符串是否是数字
        /// </summary>
        public static bool IsNumber(string s)
        {
            if (string.IsNullOrWhiteSpace(s)) return false;
            const string pattern = @"^\d+$";
            Regex rx = new Regex(pattern);
            return rx.IsMatch(s);
        }
    }
}