/*
* 命名空间: Authority.Model
*
* 功 能： SysDepartmentLayertreeRe实体类
*
* 类 名： SysDepartmentLayertreeReEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/03 10:49:03 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 部门与树图层关联表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_department_layertree_re")]
    public class SysDepartmentLayertreeReEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysDepartmentLayertreeReEntity()
     {

           //部门编号
           this.department_id = string.Empty;

           //图层编号
           this.treelayer_id = string.Empty;

           //唯一标识符
           this.id = GuidHelper.GetGuid();
      }

        /// <summary>
        /// 部门编号
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "department_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string department_id
        {
            get; set;
        }

        /// <summary>
        /// 图层编号
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "treelayer_id",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string treelayer_id
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }
    }
}
