/*
* 命名空间: Authority.Model
*
* 功 能： View_SysUser视图实体类
*
* 类 名： View_SysUserEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/19 11:05:58 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Authority.Model
{
    using System;
    using Common.Model;

    /// <summary>
    /// 后端管理员视图
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "view_sys_user")]
    public class View_SysUserEntity
    {

        /// <summary>
        /// 用户ID
        /// </summary>
        [DBFieldInfo(ColumnName = "id")]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 用户名称
        /// </summary>
        [DBFieldInfo(ColumnName = "name")]
        public string name
        {
            get; set;
        }

        /// <summary>
        /// 密码
        /// </summary>
        [DBFieldInfo(ColumnName = "password")]
        public string password
        {
            get; set;
        }

        /// <summary>
        /// 用户系统显示名
        /// </summary>
        [DBFieldInfo(ColumnName = "displayname")]
        public string displayname
        {
            get; set;
        }

        /// <summary>
        /// 性别
        /// </summary>
        [DBFieldInfo(ColumnName = "gender")]
        public string gender
        {
            get; set;
        }

        /// <summary>
        /// 用户家庭电话
        /// </summary>
        [DBFieldInfo(ColumnName = "telphone")]
        public string telphone
        {
            get; set;
        }

        /// <summary>
        /// 用户手机号码
        /// </summary>
        [DBFieldInfo(ColumnName = "mobile_phone")]
        public string mobile_phone
        {
            get; set;
        }

        /// <summary>
        /// qq
        /// </summary>
        [DBFieldInfo(ColumnName = "qq")]
        public string qq
        {
            get; set;
        }

        /// <summary>
        /// 用户昵称
        /// </summary>
        [DBFieldInfo(ColumnName = "nickname")]
        public string nickname
        {
            get; set;
        }

        /// <summary>
        /// 用户真实名
        /// </summary>
        [DBFieldInfo(ColumnName = "realname")]
        public string realname
        {
            get; set;
        }

        /// <summary>
        /// email
        /// </summary>
        [DBFieldInfo(ColumnName = "email")]
        public string email
        {
            get; set;
        }

        /// <summary>
        /// 办公电话
        /// </summary>
        [DBFieldInfo(ColumnName = "office_phone")]
        public string office_phone
        {
            get; set;
        }

        /// <summary>
        /// 用户身份号码
        /// </summary>
        [DBFieldInfo(ColumnName = "idnumber")]
        public string idnumber
        {
            get; set;
        }

        /// <summary>
        /// 用户头像地址
        /// </summary>
        [DBFieldInfo(ColumnName = "picture_url")]
        public string picture_url
        {
            get; set;
        }

        /// <summary>
        /// 创建日期
        /// </summary>
        [DBFieldInfo(ColumnName = "create_date")]
        public DateTime create_date
        {
            get; set;
        }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DBFieldInfo(ColumnName = "sort")]
        public double sort
        {
            get; set;
        }

        /// <summary>
        /// 描述
        /// </summary>
        [DBFieldInfo(ColumnName = "user_describe")]
        public string user_describe
        {
            get; set;
        }

        /// <summary>
        /// 所属角色
        /// </summary>
        [DBFieldInfo(ColumnName = "role_ids")]
        public string role_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属角色名称
        /// </summary>
        [DBFieldInfo(ColumnName = "role_names")]
        public string role_names
        {
            get; set;
        }

        /// <summary>
        /// 所属岗位
        /// </summary>
        [DBFieldInfo(ColumnName = "post_ids")]
        public string post_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属岗位名称
        /// </summary>
        [DBFieldInfo(ColumnName = "post_names")]
        public string post_names
        {
            get; set;
        }

        /// <summary>
        /// 所属部门
        /// </summary>
        [DBFieldInfo(ColumnName = "department_ids")]
        public string department_ids
        {
            get; set;
        }

        /// <summary>
        /// 所属部门名称
        /// </summary>
        [DBFieldInfo(ColumnName = "department_names")]
        public string department_names
        {
            get; set;
        }

        /// <summary>
        /// 是否有效
        /// </summary>
        [DBFieldInfo(ColumnName = "is_valid")]
        public bool is_valid
        {
            get; set;
        }

        /// <summary>
        /// 是否是超级管理员
        /// </summary>
        [DBFieldInfo(ColumnName = "is_super_user")]
        public bool is_super_user
        {
            get; set;
        }
    }
}
