/*
* 命名空间: Basic.Model
*
* 功 能： SysApiMonitorLog实体类
*
* 类 名： SysApiMonitorLogEntity
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/04/10 15:55:39 罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/

namespace Basic.Model
{
    using System;
    using Common.Library;
    using Common.Model;

    /// <summary>
    /// 接口服务  接口请求记录信息表
    /// </summary>
    [Serializable]
    [DBTableInfo(TableName = "sys_api_monitor_log")]
    public class SysApiMonitorLogEntity
    {
    /// <summary>
     /// 构造函数
    /// </summary>
    public SysApiMonitorLogEntity()
     {

           //控制器名称
           this.controller_name = string.Empty;

           //Action名称
           this.action_name = string.Empty;

           //action参数
           this.action_params = string.Empty;

           //唯一标识符
           this.id = GuidHelper.GetGuid();

           //请求开始时间
           this.start_time = DateTime.Now;

           //请求结束时间
           this.end_time = DateTime.Now;

           //Http请求头
           this.http_header = string.Empty;

           //请求的IP地址
           this.request_ip = string.Empty;

           //请求结果返回码
           this.result_code = string.Empty;

           //请求路径
           this.request_path = string.Empty;

           //Http请求方式
           this.http_method = string.Empty;
      }

        /// <summary>
        /// 控制器名称
        /// </summary>
        [DBFieldInfo(ByteLength = 104,DataLength = -1,DecimalDigits = 0,ColumnName = "controller_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string controller_name
        {
            get; set;
        }

        /// <summary>
        /// Action名称
        /// </summary>
        [DBFieldInfo(ByteLength = 104,DataLength = -1,DecimalDigits = 0,ColumnName = "action_name",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string action_name
        {
            get; set;
        }

        /// <summary>
        /// action参数
        /// </summary>
        [DBFieldInfo(ByteLength = 504,DataLength = -1,DecimalDigits = 0,ColumnName = "action_params",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string action_params
        {
            get; set;
        }

        /// <summary>
        /// 唯一标识符
        /// </summary>
        [DBFieldInfo(ByteLength = 36,DataLength = -1,DecimalDigits = 0,ColumnName = "id",Required = true,IsPrimarykey = true,IsIncrease =false)]
        public string id
        {
            get; set;
        }

        /// <summary>
        /// 请求开始时间
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "start_time",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime start_time
        {
            get; set;
        }

        /// <summary>
        /// 请求结束时间
        /// </summary>
        [DBFieldInfo(ByteLength = -1,DataLength = 8,DecimalDigits = 0,ColumnName = "end_time",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public DateTime end_time
        {
            get; set;
        }

        /// <summary>
        /// Http请求头
        /// </summary>
        [DBFieldInfo(ByteLength = 504,DataLength = -1,DecimalDigits = 0,ColumnName = "http_header",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string http_header
        {
            get; set;
        }

        /// <summary>
        /// 请求的IP地址
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "request_ip",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string request_ip
        {
            get; set;
        }

        /// <summary>
        /// 请求结果返回码
        /// </summary>
        [DBFieldInfo(ByteLength = 54,DataLength = -1,DecimalDigits = 0,ColumnName = "result_code",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string result_code
        {
            get; set;
        }

        /// <summary>
        /// 请求路径
        /// </summary>
        [DBFieldInfo(ByteLength = 504,DataLength = -1,DecimalDigits = 0,ColumnName = "request_path",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string request_path
        {
            get; set;
        }

        /// <summary>
        /// Http请求方式
        /// </summary>
        [DBFieldInfo(ByteLength = 24,DataLength = -1,DecimalDigits = 0,ColumnName = "http_method",Required = true,IsPrimarykey = false,IsIncrease =false)]
        public string http_method
        {
            get; set;
        }
    }
}
