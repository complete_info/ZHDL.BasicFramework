﻿using System;
using System.Collections.Generic;
using System.Text;

/*
* 命名空间: Common.Model
*
* 功 能： Echarts中2D坐标系返回信息
*
* 类 名： Echarts2DCoordinatesInfo
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/4/14 15:50:09 	罗维     创建
*
* Copyright (c) 2020 Lir Corporation. All rights reserved.
*/
namespace Common.Model
{
    /// <summary>
    /// Echarts中2D坐标系返回信息
    /// </summary>
    public class Echarts2DCoordinatesInfo
    {
        /// <summary>
        /// X轴信息
        /// </summary>
        public List<string> xAxis { get; set; }

        /// <summary>
        /// Y轴信息
        /// </summary>
        public List<string> yAxis { get; set; }

    }
}
