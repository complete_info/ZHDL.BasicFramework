﻿/*
* 命名空间: Common.Model
*
* 功 能： 前端请求实体信息
*
* 类 名： ParametersInfo<T>
*
* Version   变更日期            负责人     变更内容
* ─────────────────────────────────────────────────
* V1.0.1    2020/03/13 15:57:22 罗维     创建
*
* Copyright (c) 2020 SCZH Corporation. All rights reserved.
*/

namespace Common.Model
{
    /// <summary>
    /// 前端请求实体信息
    /// </summary>
    public class ParametersInfo<T>
    {
        /// <summary>
        /// 分页索引，以1开始
        /// </summary>
        public int page { get; set; }

        /// <summary>
        /// 分页大小
        /// </summary>
        public int limit { get; set; }

        /// <summary>
        /// 排序字段
        /// </summary>
        public string field { get; set; }

        /// <summary>
        /// desc  asc
        /// </summary>
        public string order { get; set; }

        /// <summary>
        /// 参数信息
        /// </summary>
        public T parameters { get; set; }
    }
}

