﻿using System.ComponentModel;

namespace Common.Model
{
    /// <summary>
    /// 系统类型
    /// </summary>
    public enum SystemType
    {

        /// <summary>
        /// 运维系统
        /// </summary>
        [Description("运维系统")]
        MaintainSystem = 100,

        /// <summary>
        /// 桌面应用系统
        /// </summary>
        [Description("桌面应用系统")]
        DesktopSystem = 200,

        /// <summary>
        /// Web客户端
        /// </summary>
        [Description("Web客户端")]
        ClientSystem = 300,

    }
}
