﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Common.Model
{
    /// <summary>
    /// 上传文件，文件夹类型
    /// </summary>
    public enum UploadFolderType
    {
        /// <summary>
        /// 用户头像信息
        /// </summary>
        [Description("用户头像信息")]
        UserImage = 100,


        /// <summary>
        /// 用户档案信息
        /// </summary>
        [Description("用户头像信息")]
        UserArchives = 200,
    }
}
